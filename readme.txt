N E M O - A G E v0.32.0 [10 Mar 2023]

Most recent version available at: https://bitbucket.org/ecoevo/nemo-age-release/

Nemo-age is free software distributed under the GNU Public License version 3
or later.

What is Nemo-age?
-----------------

Nemo-age is a forward-in-time, individual-based, genetically and spatially 
explicit, stochastic population simulation program.

In Nemo-age, it is possible to model genetic and phenotypic evolution in 
populations with, for instance, overlapping generations, a seed bank, and 
multiple age classes with stage-specific transition rates, fecundities, 
selection pressures, and dispersal rates, among other things.

Nemo-age is a feature-rich and flexible program with many components that can be 
freely assembled to set up complex demo-genetic simulations in a spatially 
explicit context, efficiently. Eco-evolutionary dynamics of populations set in 
large geographical areas can be modeled using input parameter values from extant 
species. Such data encompass species' life history parameters (fecundities, 
survival and transition rates), dispersal kernels, geographical distribution and 
environmental data describing the ecological niche across space. It is then 
possible to simulate local adaptation of a species to spatially and temporally 
varying environments via selection on multivariate polygenic traits.

Nemo-age is also an advanced forward-in-time population genetic simulator able 
to simulate genetic diversity data a multiple loci placed on a genetic map. Loci 
may carry continuous values contributing to polygenic trait variation, 
deleterious mutations contributing directly to fitness or no phenotypic value 
and be neutral. All types of loci can be linked on the same genetic map.


How to use Nemo-age?
----------------

Nemo-age is command-line tool with simple parameter-file interface.

Please refer to the user manual to have info on how to use Nemo-age as a
command-line tool.


Requirements
------------

The following operating system software is required to compile, install
and run the software:

	- Any version of Linux, MacOSX, or Windows with a GNU-compatible C++ 
compiler
	- CygWin is recommended on Windows, at least to compile Nemo-age 
	  (http://www.cygwin.com). Nemo-age can be run without CygWin using 
	  the DOS command prompt.
	
Nemo-age uses the following third-party software pieces that should be
present on your system as well (at least for building Nemo-age from source):

	- The GSL: GNU Scientific Library 
	  
	  The GSL is available at: http://www.gnu.org/software/gsl/
	  
	- The SPRNG: Scalable Parallel Random Number Generators Library
version 2.0, used for the MPI version only

	  SPRNG is available at: http://sprng.cs.fsu.edu/


Compilation and Installation
----------------------------

Please read the INSTALL file for compilation and installation instructions.


Getting Support and Additional Resources
-----------------------------------------

If the Nemo-Age documentation does not answer your question, you can post it on
the mailing list available at:
	https://groups.google.com/g/nemo-age
	
To report bug, please head to the issue tracker on bitbucket.org:
    https://bitbucket.org/ecoevo/nemo-age-release/issues
	
Subscribe to the nemo-announce mailing list to receive notifications about new
releases of the Nemo products:
    https://groups.google.com/g/nemo-announce
	
The Authors
-----------

Frederic Guillaume
Olivier Cotto
Max Schmid



Legal
-----

Nemo-age is Copyright 2020-2023 by The Authors.

Nemo-age is free software, you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3 or any later version.

The GNU General Public License does not permit this software to be
redistributed in proprietary programs.

Nemo-age is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the COPYING
file for more information.

The GSL code is provided under the GNU General Public License as well.

SPRNG code is provided "as is" by the NCSA.

