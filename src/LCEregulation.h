/** $Id: $
 *
 *  @file LCEregulation.h
 *  Nemo-age
 *
 *   Copyright (C) 2020
 *   guillaume.fred@gmail.com
 *
 *   This file is part of Nemo
 *
 *   Nemo is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Nemo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  created on @date 16.06.2005
 *
 *  @author fred
 */

#ifndef LCEREGULATION_H_
#define LCEREGULATION_H_

#include "lifecycleevent.h"

// LCE_Regulation
//
/** Regulates the patches to their carrying capacity, acts on each age class separately.
 *  Randomly removes individuals in each patch until each age class has reached the
 *  (sex-specific) carrying capacities of the patch.
 **/
class LCE_Regulation: public LifeCycleEvent
{
private:

  TMatrix*       _competition_regulation;
  unsigned int   _K;
  unsigned int  _num_stage;
  double*       _stage_size_proportions; //for ceiling regulation
  unsigned int* _stage_size_rmultinom;   //for ceiling regulation
  age_idx       _age_target;
  age_t         _age_flag;
  TMatrix*      _affected_age; // holds the matrix of age classes affected by density dependent regulation

  // pointer to function used for regulation
  vector<void (LCE_Regulation::*) (Patch*, sex_t, unsigned int)> _regulate_ptrs;

public:

  LCE_Regulation( ) : LifeCycleEvent("regulation",""), _competition_regulation(0), _K(0), _affected_age(0),
  _num_stage(0), _stage_size_proportions(0), _stage_size_rmultinom(0), _age_target(OFFSx), _age_flag(ADULTS)
{
    add_parameter("regulation_by_competition",DBL,false,false,0,0);
    add_parameter("regulation_by_competition_affected_age",INT,false,false,0,0);
    add_parameter("regulation_by_competition_count_age_flag",INT,false,true,1,4294967295);
    add_parameter("regulation_carrying_capacity",BOOL,false,false,0,0);
    add_parameter("regulation_by_competition_model",STR,false,false,0,0);

}

  virtual ~LCE_Regulation( )
  {
    if(_affected_age) delete _affected_age;
    if(_stage_size_rmultinom) delete _stage_size_rmultinom;
    if(_stage_size_proportions) delete _stage_size_proportions;
  }

  // regulate by competition:
  void regulatePatch (Patch* patch, sex_t sex, unsigned int pop_size);
  void regulatePatch_Ricker (Patch* patch, sex_t sex, unsigned int pop_size);
  void regulatePatch_multiAge (Patch* patch, sex_t sex, unsigned int pop_size); // regulation on multiple age classes
  void regulatePatch_multiAge_Ricker (Patch* patch, sex_t sex, unsigned int pop_size); // regulation on multiple age classes

  // ceiling regulation to carrying capacity:
  void regulatePatchCeiling (Patch* patch, sex_t sex, unsigned int pop_size);

  //implementations:
  virtual bool setParameters ();// {return true;}
  virtual void execute ();
  virtual LCE_Regulation* clone ( ) {return new LCE_Regulation();}
  virtual void loadFileServices ( FileServices* loader ) {}
  virtual void loadStatServices ( StatServices* loader ) {}
  virtual age_t removeAgeClass ( ) {return NONE;}
  virtual age_t addAgeClass ( ) {return ALL;}
  virtual age_t requiredAgeClass () {return ALL;}
};


#endif /* LCEREGULATION_H_ */


