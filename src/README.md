Welcome to the git repository of **Nemo-age** !


**Nemo-age** is a forward-in-time, individual-based, genetically and spatially 
explicit, stochastic population simulation program.

**Nemo-age** is the age/stage-structured version of [Nemo](http://nemo2.sourceforge.net).

### What's in Nemo-age?

In **Nemo-age**, it is possible to model genetic and phenotypic evolution in 
populations with, for instance, overlapping generations, a seed bank, and 
multiple age classes with stage-specific transition rates, fecundities, 
selection pressures, and dispersal rates, among other things.

**Nemo-age** is a feature-rich and flexible program with many components that can be 
freely assembled to set up complex demo-genetic simulations in a spatially 
explicit context, efficiently. Eco-evolutionary dynamics of populations set in 
large geographical areas can be modeled using input parameter values from extant 
species. Such data encompass species' life history parameters (fecundities, 
survival and transition rates), dispersal kernels, geographical distribution and 
environmental data describing the ecological niche across space. It is then 
possible to simulate local adaptation of a species to spatially and temporally 
varying environments via selection on multivariate polygenic traits.

**Nemo-age** is also an advanced forward-in-time population genetic simulator able 
to simulate genetic diversity data a multiple loci placed on a genetic map. Loci 
may carry continuous values contributing to polygenic trait variation, 
deleterious mutations contributing directly to fitness or no phenotypic value 
and be neutral. All types of loci can be linked on the same genetic map.

### What can I find here?

You will find here instructions on how to get Nemo-age's code and how to install it on your computer.

Information on how to setup a simulation and run it on your computer, or your cluster, is available in the user's manual.

-----------
### Resources

**Reference**: Cotto Olivier, Schmid Max, and Guillaume Frederic. 2020. Methods in Ecology and Evolution

**Manual**: you can download the manual from the Downloads section [here](https://bitbucket.org/ecoevo/nemo-age-release/downloads/NEMOAGE-usermanual.pdf)

**Mailing lists**: 

**nemosub**:
A utility to launch multiple simulations on a cluster from a single parameter file. `nemosub` is available [here](https://bitbucket.org/ecoevo/nemosub). It works for all main types of cluster scheduler (Slurm, OAR, Moab/Torq etc.)

# How to use Nemo-age?
----------------

Nemo-age is command-line tool with a simple parameter-file interface. It can be run like this:

    $ nemoage path/to/myparameterfile.ini

The parameter file collects the parameters necessary to run the desired simulation. A very basic simulation has parameters for the simulation and population structure and the events executed during the life cycle.

    # example of a parameter file
    
    ## SIMULATION ##
    run_mode overwrite
    random_seed 2
    root_dir examples
    
    filename example1
    
    replicates      10
    generations     10000
    
    ## POPULATION ##
    patch_number    5
    patch_capacity  1000
    
    ### AGE STRUCTURE and TRANSITION MATRIX ###
    pop_age_structure     {{0, 1, 2, 3}}
    pop_transition_matrix {{0, 0, 12, 40}
                           {0.83, 0, 0, 0}
                           {0, 0.92, 0, 0}
                           {0, 0, 0.99, 0}}
    
    ## LIFE CYCLE EVENTS ##
    breed       1
    disperse    2
    save_stats  3
    regulation  4
    aging_multi 5
    save_files  6
    
    ## BREED parameters
    mating_system 6
    
    ## DISPERSE parameters 
    dispersal_rate   0.01
    dispersal_model  1
    
    ## REGULATION parameters
    regulation_carrying_capacity
    
    ## OUTPUT stats
    stat          demography migrants fstat
    stat_log_time 10
    stat_dir      data
    
    ## GENETICS:
    ## NEUTRAL markers (SNP)
    ntrl_loci                   1000
    ntrl_all                    2
    ntrl_mutation_rate          1e-5
    ntrl_mutation_model         2
    ntrl_random_genetic_map     {{10,10,10,10,10,10,10,10,10,10}}
    ntrl_genetic_map_resolution 0.01
    ntrl_save_freq
    ntrl_output_dir             ntrl
    ntrl_output_logtime         1000
    
Please refer to the user's manual for info about the parameters. Only neutral markers not under selection are simulated, with 100 di-allelic loci per chromosome randomly spread on 10 chromosomes of 10cM each.


## Install binaries
---

Executable files compiled for Mac OSX and Windows (CygWin) are available in the respective release packages. Click on `Downloads` on the left to access them. Refer to the INSTALL file for installation instructions.

## Build from source
---

To compile **Nemo-age**, follow the instructions in the INSTALL file.

The following should work on a *nix system (Mac OSX, Linux): 

Download and uncompress the release package `nemo-agex.y.z`:

    $ tar xzf nemo-agex.y.z

Change directory to the package directory just created:

    $ cd nemo-agex.y.z

Compile and install:

    $ make
    $ sudo make install
    
Try Nemo-age:

    $ nemoage

\FG

