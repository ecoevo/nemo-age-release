/** $Id: ttquanti.h,v 1.11.2.6 2017-03-16 09:39:14 fred Exp $
*
*  @file ttquanti.h
*  Nemo2
*
*   Copyright (C) 2006-2020 Frederic Guillaume
*   frederic.guillaume@ieu.uzh.ch
*
*   This file is part of Nemo
*
*   Nemo is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   Nemo is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*  created on @date 14.11.2005
* 
*  @author fred
*/

#ifndef TTQUANTI_H
#define TTQUANTI_H

#include <cmath>
#include <vector>
#include "ttrait_with_map.h"
#include "filehandler.h"
#include "stathandler.h"
#include "metapop.h"
#include "datatable.h"
#ifdef HAS_GSL
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_eigen.h>
#endif

class TTQuantiSH;
class TTQuantiFH;
class TTQFreqExtractor;
class TProtoQuanti;

// ------------------------------------------------------------------------------
/**
*  TTQuanti
 */
// ------------------------------------------------------------------------------
class TTQuanti : public TTrait  {
  
public:
  
  TTQuanti () : _sequence(0),_phenotypes(0),_myProto(0)
  { }
  
  TTQuanti (const TTQuanti& T) : _sequence(0), _phenotypes(0), _myProto(T._myProto)
  { }

  virtual ~TTQuanti () {reset();}
  
  //implements TTrait:
  virtual   void            init ();
  virtual   void            init_sequence ();
  virtual   void            reset ();
  virtual   void            inherit (TTrait* mother, TTrait* father);
  virtual   void            mutate ();
  virtual   void*           set_trait (void* value)                 {return value;}
  virtual   void            set_sequence (void** seq);
  virtual   void            set_value ();
  virtual   void*           getValue () const                       {return _phenotypes;}
  virtual   trait_t         get_type () const                       {return QUANT;}
  virtual   void**          get_sequence () const                   {return (void**)_sequence;}
  virtual   double          get_allele_value (int loc, int all);
  virtual   void            set_allele_value (unsigned int locus, unsigned int allele, double value);
  virtual   void            show_up  ();
  virtual   TTQuanti*       clone ()                                {return new TTQuanti(*this);}
  virtual   TTQuanti&       operator= (const TTrait& T);
  virtual   bool            operator== (const TTrait& T);
  virtual   bool            operator!= (const TTrait& T);
  
  
  //implements StorableComponent:
  virtual   void            store_data    ( BinaryStorageBuffer* saver  );
  virtual   bool            retrieve_data ( BinaryStorageBuffer* reader );
  
  //accessors:
  void                      set_proto    (TProtoQuanti* proto)      {_myProto = proto;}
  double                    get_genotype (unsigned int trait);
  void                      set_allele   (int locus, int allele, double value) {_sequence[allele][locus] = value;}
  void                      set_phenotype (unsigned int trait, double value);
  void                      set_value_VE ();
  void                      set_value_noVE ();
  
private:
    
  double **_sequence;
  double *_phenotypes;

  TProtoQuanti* _myProto;

};

// ------------------------------------------------------------------------------
/**
 *  TProtoQuanti
 */
// ------------------------------------------------------------------------------
class TProtoQuanti : public TTProtoWithMap {

public:
  
  TProtoQuanti ();
  TProtoQuanti (const TProtoQuanti& T);
  virtual ~TProtoQuanti ();
  
  unsigned int              get_nb_traits()                     {return _nb_traits;}
  unsigned int              get_nb_locus()                      {return _nb_locus;}
  unsigned int              get_seq_length ()                   {return _seq_length;}
  vector<double>            get_env_var ()                      {return _eVariance;}
  double                    get_trait_var (unsigned int trait)  {return _mutation_matrix->get(trait, trait);}
  double                    get_mutation_correlation()          {return _mutation_correlation;}
  unsigned int              get_allele_model ()                 {return _allele_model;}
  double **                 get_allele_values () const          {return _allele_value;}
  double                    get_init_value (unsigned int i)     {return _init_value[i]; }
  void                      set_init_values (const double *values, unsigned int nval);

  void                      reset_mutation_pointers();
  bool                      setMutationParameters ();
  bool                      setDiallelicMutationModel();
  bool                      setContinuousMutationModel();
  void                      set_mutation_matrix_decomposition ();
  
   
  double*                   getMutationEffectMultivariateGaussian (unsigned int loc);
  double*                   getMutationEffectBivariateGaussian    (unsigned int loc);
  double*                   getMutationEffectUnivariateGaussian   (unsigned int loc);
  double*                   getMutationEffectUnivariateDiallelic  (unsigned int loc);
  double*                   getMutationEffectBivariateDiallelic   (unsigned int loc);
  void                      inherit      (sex_t SEX, double* seq, double** parent);
  void                      inherit_free (sex_t SEX, double* seq, double** parent);
  void                      inherit_low  (sex_t SEX, double* seq, double** parent);
  void                      mutate       (TTQuanti* ind);
  void                      mutate_nill  (TTQuanti* ind);
  void                      mutate_noHC  (TTQuanti* ind);
  void                      mutate_HC    (TTQuanti* ind);
  void                      mutate_diallelic_HC (TTQuanti* ind);
    
  //implements TraitPrototype:
  virtual   void            reset ()                            {TTProtoWithMap::reset();}
  virtual   TTQuanti*       hatch ();
  virtual   TProtoQuanti*   clone ()                            {return new TProtoQuanti(*this);}
  virtual   trait_t         get_type () const                   {return QUANT;}
  
  //implementation of SimComponent:
  virtual   bool            setParameters();
  virtual   void            loadFileServices ( FileServices* loader );
  virtual   void            loadStatServices ( StatServices* loader );
  
  //implementation of StorableComponent:
  virtual   void            store_data    ( BinaryStorageBuffer* saver  )
    {saver->store(&_seq_length,sizeof(int));}
  
  virtual   bool            retrieve_data ( BinaryStorageBuffer* reader )
    {reader->read(&_seq_length,sizeof(int));return true;}
  
private:    
  
  /**Total number of loci, for all traits*/
  unsigned int    _nb_locus;
  /**Number of traits*/
  unsigned int    _nb_traits;
  /**Total number of loci, same as _nb_locus*/
  unsigned int    _seq_length;
  unsigned int    _allele_model;
  double          **_allele_value;
  
  //mutations:
  TMatrix         *_mutation_matrix;
  gsl_matrix      *_gsl_mutation_matrix;
  gsl_matrix      *_evect;
  gsl_vector      *_eval;
  gsl_vector      *_effects_multivar;
  gsl_vector      *_ws;
  double          _genomic_mutation_rate;
  double          _mutation_correlation;
  double          *_mutation_sigma;
  double          *_init_value;
  double          _effects_bivar[2];
  unsigned int    _doInitMutation;
  
  //recombination:
  bool*           _all_chooser;
  size_t          _locusByteSize;
  size_t          _sizeofLocusType;
  
  vector<double>  _eVariance;

  void    (TProtoQuanti::* _inherit_fun_ptr)   (sex_t, double*, double**);
  void    (TProtoQuanti::* _mutation_func_ptr) (TTQuanti*);
  double* (TProtoQuanti::* _getMutationValues) (unsigned int);
  void    (TTQuanti::* _set_value_func_ptr)    ();
  
  friend class TTQuanti;
  
  TTQuantiSH* _stats;
  TTQuantiFH* _writer;
  TTQuantiFH* _reader;
  TTQFreqExtractor* _freqExtractor;
};
// ------------------------------------------------------------------------------
/**
 *  TTQuantiSH
 */
// ------------------------------------------------------------------------------
class TTQuantiSH : public TraitStatHandler<TProtoQuanti, TTQuantiSH> {

  double *_meanP, *_meanG, *_Va, *_Vb, *_Vp, *_covar,*_eigval,**_eigvect;
  double **_pmeanP, **_pmeanG, **_pVa, **_pVp, **_pcovar, **_peigval, **_peigvect;

  unsigned int _nb_trait, _patchNbr;
  bool _eVar;

  gsl_matrix *_G, *_evec;
  gsl_vector *_eval;
  gsl_eigen_symmv_workspace *_ws;
  
  DataTable< double > _phenoTable, _genoTable;
  unsigned int _table_set_gen, _table_set_age, _table_set_repl;

public:

    TTQuantiSH(TProtoQuanti* TP) 
    : TraitStatHandler<TProtoQuanti, TTQuantiSH> (TP), 
    _meanP(0), _meanG(0), _Va(0), _Vb(0), _Vp(0), _covar(0), _eigval(0), _eigvect(0),
    _pmeanP(0), _pmeanG(0), _pVa(0), _pVp(0), _pcovar(0), _peigval(0), _peigvect(0),
    _nb_trait(0),_patchNbr(0), _eVar(0), _G(0),_evec(0),_eval(0),_ws(0),
    _table_set_gen(999999), _table_set_age(999999), _table_set_repl(999999)
    {}
  
  virtual ~TTQuantiSH() {resetPtrs();}
  
  void  resetPtrs();
  
  virtual void init ( );
  
  virtual bool      setStatRecorders (std::string& token);
  void addQuanti (age_t AGE);
  void addEigen (age_t AGE);
  void addEigenValues (age_t AGE);
  void addEigenVect1 (age_t AGE);
  void addQuantiPerPatch (age_t AGE);
  void addAvgPerPatch (age_t AGE);
  void addVarPerPatch (age_t AGE);
  void addCovarPerPatch (age_t AGE);
  void addEigenPerPatch (age_t AGE);
  void addEigenValuesPerPatch (age_t AGE);
  void addEigenVect1PerPatch (age_t AGE);
  void addEigenStatsPerPatcg (age_t AGE);
  void addSkewPerPatch(age_t AGE);

  void   setDataTables             (age_t AGE);
  void   setAdultStats             ( ) {setStats(ADULTS);}
  void   setOffsprgStats           ( ) {setStats(OFFSPRG);}
  void   setStats                  (age_t AGE);
  double getMeanPhenot             (unsigned int i) {return _meanP[i];}
  double getVa                     (unsigned int i) {return _Va[i];}
  double getVb                     (unsigned int i) {return _Vb[i];}
  double getVp                     (unsigned int i) {return _Vp[i];}
  double getQst                    (unsigned int i) {return _Vb[i]/(_Vb[i]+2*_Va[i]);}
  double getCovar                  (unsigned int i) {return _covar[i];}
  double getEigenValue             (unsigned int i) {return _eigval[i];}
  double getEigenVectorElt         (unsigned int t1, unsigned int t2) {return _eigvect[t2][t1];}//eigenvectors arranged column-wise
  
  double getMeanPhenotPerPatch     (unsigned int i, unsigned int p) {return _pmeanP[i][p];}
  double getVaPerPatch             (unsigned int i, unsigned int p) {return _pVa[i][p];}
  double getVpPerPatch             (unsigned int i, unsigned int p) {return _pVp[i][p];}
  double getEigenValuePerPatch     (unsigned int i, unsigned int p) {return _peigval[i][p];}
  double getCovarPerPatch          (unsigned int p, unsigned int i) {return _pcovar[p][i];}
  double getEigenVectorEltPerPatch (unsigned int p, unsigned int v) {return _peigvect[p][v];}
  double getSkewPerPatch           (unsigned int i, unsigned int p);
};
// ------------------------------------------------------------------------------
/**
 *  TTQuantiFH
 */
// ------------------------------------------------------------------------------
class TTQuantiFH : public TraitFileHandler<TProtoQuanti> {

  string _output_option;
  
public:
  TTQuantiFH(TProtoQuanti* T) : TraitFileHandler<TProtoQuanti>(T,".quanti") {}
  virtual ~TTQuantiFH(){}
  
  void setOutputOption (string opt) {_output_option = opt;}
  
  virtual void  FHwrite ();
  void write_TABLE ();
  void print(ofstream& FH, age_idx Ax, bool print_gene, bool print_genotype);
  void write_PLINK ();
  void print_PLINK_PED(ofstream& FH, age_idx Ax, Patch *patch);
  void print_PLINK_FAM(ofstream& FH, age_idx Ax, Patch *patch);
  virtual void FHread (string& filename);
};

// ------------------------------------------------------------------------------
/**
 *  TTQFreqExtractor
 */
// ------------------------------------------------------------------------------
class TTQFreqExtractor : public TraitFileHandler<TProtoQuanti> {

  vector< string > _records;

public:

  TTQFreqExtractor(TProtoQuanti* T) : TraitFileHandler<TProtoQuanti>(T,".qfreq") {}

  void resetTable();

  virtual ~TTQFreqExtractor () {}
  virtual void FHwrite ();
  virtual void FHread (string& filename) {}
};

#endif
