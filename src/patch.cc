/**  $Id: patch.cc,v 1.5.2.3 2017-06-09 11:07:51 fred Exp $
*
*  @file patch.cc
*  Nemo2
*
*   Copyright (C) 2006-2011 Frederic Guillaume
*   frederic.guillaume@env.ethz.ch
*
*   This file is part of Nemo
*
*   Nemo is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   Nemo is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*  @author fred
*/

#include <iostream>
#include "metapop.h"
#include "Uniform.h"
#include "output.h"
#include "simenv.h"

using namespace std;

// ----------------------------------------------------------------------------------------
// init
// ----------------------------------------------------------------------------------------
Patch* Patch::init(unsigned int nb_age_classes, unsigned int nbfem, unsigned int nbmal, unsigned int id)
{
  _ID = id;
  _KFem = nbfem;
  _KMal = nbmal;
  _K = _KFem + _KMal;
  _isExtinct = false;
  _age = 0;

  if(_nb_age_class != nb_age_classes) reset_containers(nb_age_classes);
  
//  _nb_age_class = nb_age_classes; this is done within reset_containers();
  
  //in case age structure didn't change and patch is not empty:
  if(size(ALL) != 0) flush(ALL, SIMenv::MainSim->get_pop()); 
  
  reset_counters();
  
  return this;
}
// ----------------------------------------------------------------------------------------
// reset_counters
// ----------------------------------------------------------------------------------------
void Patch::reset_counters()
{
  nbEmigrant = 0;
  nbImigrant = 0;
  nbPhilopat = 0;
  nbKolonisers = 0;
}
// ----------------------------------------------------------------------------------------
// reset_containers
// ----------------------------------------------------------------------------------------
void Patch::reset_containers(unsigned int new_nb_classes)
{  
  
  for(unsigned int i = 0; i < 2; i++) {
    
    if(_containers[i]) {
      
      flush(ALL, SIMenv::MainSim->get_pop());
      
      for(unsigned int j = 0; j < _nb_age_class; j++) {
        
        delete [] _containers[i][j];
      }
      
      delete [] _containers[i];
    }
    
    _containers[i] = new Individual** [new_nb_classes];
    
    if (_sizes[i]) {
      delete [] _sizes[i];
    }
     
    _sizes[i] = new unsigned int  [new_nb_classes];
    
    if (_capacities[i]) {
      delete [] _capacities[i];
    }
    
    _capacities[i] = new unsigned int  [new_nb_classes];
  }
    
  for(unsigned int i = 0; i < new_nb_classes; i++) {
    
    _containers[MAL][i] = new Individual* [ _KMal ];
    
    for(unsigned int j = 0; j < _KMal; ++j)
      _containers[MAL][i][j] = 0;
    
    _containers[FEM][i] = new Individual* [ _KFem ];
    
    for(unsigned int j = 0; j < _KFem; ++j)
      _containers[FEM][i][j] = 0;
    
    _sizes[MAL][i] = 0;
    _sizes[FEM][i] = 0;
    _capacities[MAL][i] = _KMal;
    _capacities[FEM][i] = _KFem;
  }

  _nb_age_class = new_nb_classes;
}
// ----------------------------------------------------------------------------------------
// setNewGeneration
// ----------------------------------------------------------------------------------------
void Patch::setNewGeneration(age_t AGE, Metapop* pop)
{
  unsigned int mask = 1;
  unsigned int age;
  bool has_init_size = pop->hasInitPatchSize();
  TMatrix sizes(pop->getInitPatchSize());

  for(unsigned int i = 0; i < _nb_age_class; i++) {

    age = pop->getAgeStructure()->get(0,i);

    if(has_init_size) { //we override the AGE tag specified to honor user's choice of init sizes

      setNewGeneration(FEM, age_idx(i), sizes.get(getID(), i), age, pop);
      setNewGeneration(MAL, age_idx(i), sizes.get(getID(), i), age, pop);

    } else if( mask & AGE ) {

      setNewGeneration(FEM, age_idx(i), _KFem, age, pop);
      setNewGeneration(MAL, age_idx(i), _KMal, age, pop);

    }
    mask<<=1;
  }
}
// ----------------------------------------------------------------------------------------
// setNewGeneration
// ----------------------------------------------------------------------------------------
void Patch::setNewGeneration(sex_t SEX, age_idx IDX, unsigned int size, unsigned int age, Metapop* pop)
{  
  Individual *new_ind;
  //--------------------------------------------------------------------
  //if too many inds in the Patch, flush them into the RecyclingPOOL
  if(this->size(SEX, IDX) > 0) flush(SEX, IDX, pop);
  
  for(unsigned int i = 0; i < size; i++) {
    new_ind = pop->makeNewIndividual(0,0,SEX,_ID);
    new_ind->create_first_gen();
    new_ind->setAge(age);
    add(SEX, IDX, new_ind);
  }
  
}
// ----------------------------------------------------------------------------------------
// ~Patch
// ----------------------------------------------------------------------------------------
Patch::~Patch()
{
#ifdef _DEBUG_
//  message("Patch::~Patch\n");
#endif
  
  for (unsigned int i = 0; i < 2; ++i) {
    for(unsigned int j = 0; j < _nb_age_class; ++j) {
      for(unsigned int k = 0; k < _sizes[i][j] ; ++k)
        if(_containers[i][j][k] != 0) delete _containers[i][j][k];
      delete [] _containers[i][j];
    }
    delete [] _containers[i];
    delete [] _sizes[i];
    delete [] _capacities[i];
  }
  

}
// ----------------------------------------------------------------------------------------
// show_up
// ----------------------------------------------------------------------------------------
void Patch::show_up()
{
  message("Patch %i:\n  age: %i; K: %i, K_fem: %i; K_mal: %i\n",_ID, _age, _K, _KFem, _KMal);
  for(unsigned int j = 0; j < _nb_age_class; ++j)
    message("   age class %i: females: %i; males: %i\n", j, _sizes[FEM][j], _sizes[MAL][j]);
}
