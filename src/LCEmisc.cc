/** $Id: LCEmisc.cc,v 1.15.2.14 2017-06-09 11:07:51 fred Exp $
 *
 *  @file LCEmisc.cc
 *  Nemo2
 *
 *   Copyright (C) 2006-2011 Frederic Guillaume
 *   frederic.guillaume@env.ethz.ch
 *
 *   This file is part of Nemo
 *
 *   Nemo is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Nemo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  created on @date 16.06.2005
 * 
 *  @author fred
 */

#include <cmath>
#include "LCEmisc.h"
#include "metapop.h"
#include "Uniform.h"
#include "output.h"
#include "tstring.h"

/*_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/*/

//                           ******** LCE_Aging ********

// ----------------------------------------------------------------------------------------
// LCE_Aging::execute
// ----------------------------------------------------------------------------------------
void LCE_Aging::execute ()
{  
#ifdef _DEBUG_
  message("LCE_Aging::execute (Patch nb: %i offsprg nb: %d adlt nb: %d; ",_popPtr->getPatchNbr()
          ,_popPtr->size( OFFSPRG ), _popPtr->size( ADULTS ));
#endif

  if(_popPtr->getAgeStructure()->getNbCols() > 2) {
    error("the \"aging\" LCE can only work with 2-stage populations and non-overlapping generations.\n");
    fatal("please use the \"aging_multi\" and \"regulation\" LCEs for other cases.\n");
  }

  unsigned int nbInd = 0;
  Patch *patch;
  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); i++) {

    patch = _popPtr->getPatch(i);

    // remove all adults
    patch->flush(ADLTx, _popPtr);

    nbInd = 0;

    while(nbInd++ < patch->get_KFem() && patch->size(FEM, OFFSx) != 0) 
      patch->move(FEM, OFFSx, ADLTx, RAND::Uniform( patch->size(FEM, OFFSx) ) );

    nbInd = 0;

    while(nbInd++ < patch->get_KMal() && patch->size(MAL, OFFSx) != 0)
      patch->move(MAL, OFFSx, ADLTx, RAND::Uniform( patch->size(MAL, OFFSx) ) );

    // remove remaining offspring
    patch->flush(OFFSx, _popPtr);

    //update age tags:
    for(unsigned int i = 0; i < patch->size(FEM, ADLTx); ++i)
      patch->get(FEM, ADLTx, i)->Aging();

    for(unsigned int i = 0; i < patch->size(MAL, ADLTx); ++i)
      patch->get(MAL, ADLTx, i)->Aging();

    //set the Patch extinction and age tags:

    patch->set_isExtinct( (patch->size(ADULTS) == 0) );

//    if(patch->size(ADULTS) == 0) {
//      patch->set_isExtinct(true);
//      patch->set_age(0);
//    } else {
//      patch->set_isExtinct(false);
//      patch->set_age( patch->get_age() + 1 );
//    }

  }
#ifdef _DEBUG_
  message("after: %i)\n",_popPtr->size( ));
#endif
}
/*_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/*/

//                                   LCE_Aging_Multi

// ----------------------------------------------------------------------------------------
bool LCE_Aging_Multi::setParameters ( )
{
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Aging_Multi::AgeOrDie
// ----------------------------------------------------------------------------------------
void LCE_Aging_Multi::AgeOrDie (Patch* patch, sex_t SEX, age_idx age, double survival_rate)
{
  Individual* current_ind;

#ifdef _DEBUG_
  message(" %s transitions at stage %i ",(SEX == FEM ? "female" : "male"), age);
  unsigned int stayed = 0, died = 0, mean_age = 0, ind_cntr = patch->size(SEX, age);
#endif

  for(int k = patch->size(SEX, age) -1; k >= 0; --k)
  {
    current_ind = patch->get(SEX, age, k);

#ifdef _DEBUG_
    mean_age += current_ind->getAge();
#endif

    //check for survival
    if(RAND::Uniform() > survival_rate)
    {

      _popPtr->recycle( patch->remove(SEX, age, k) );

#ifdef _DEBUG_
      died++;
#endif

    } else {
      current_ind->Aging(); //this one survived, increase its age

#ifdef _DEBUG_
      stayed++;
#endif
    }
  }
#ifdef _DEBUG_
  message("(average age was %.2g): %i stayed (%.2g); %i died (%.2g)\n",
          (double)mean_age/ind_cntr, stayed, (double)stayed/ind_cntr, died, (double)died/ind_cntr);
#endif
}
// ----------------------------------------------------------------------------------------
// LCE_Aging_Multi::stageTransition
// ----------------------------------------------------------------------------------------
void LCE_Aging_Multi::stageTransition (Patch* patch, sex_t SEX, unsigned int stage, age_idx age,
      double survival_rate, double transition_rate, double sigma, double gamma)
{

#ifdef _DEBUG_
  message(" %s transitions at stage %i ",(SEX == FEM ? "female" : "male"), stage);
  unsigned int transited = 0, stayed = 0, died = 0, mean_age = 0, ind_cntr = patch->size(SEX, age);
#endif

  unsigned ind_age;
  Individual* ind;

  unsigned next_age = (unsigned int)_popPtr->getAgeStructure()->get(0, stage+1);

  for(int k = patch->size(SEX, age) -1; k >= 0; --k) {

    ind = patch->get(SEX, age, k);

    ind_age = (unsigned int)ind->getAge(); //this is age in generation/cycle number

#ifdef _DEBUG_
    mean_age += ind_age;
#endif
    //first check if individual must move to upper age class
    if( ind_age + 1 >=  next_age)
    {
      //check for survival

      if(RAND::Uniform() > transition_rate) {// this is transition rate

        _popPtr->recycle(ind);
        patch->remove(SEX, age, k);

#ifdef _DEBUG_
        died++;
#endif

      } else {
        ind->Aging();
        patch->move(SEX, age, age_idx (age + 1), k);

#ifdef _DEBUG_
        transited++;
#endif

      }
    }

    else   //individuals may stay in the same stage class if they survive

    {

      if(survival_rate == 0){

        //that means the individual hasn't reached the max age, but cannot survive in the same stage
        //we move it to the next stage if it survives (instead of killing it instantly):

        if(RAND::Uniform() > transition_rate) {// this is transition rate

          _popPtr->recycle(ind);
          patch->remove(SEX, age, k);

#ifdef _DEBUG_
          died++;
#endif

        } else {
          ind->Aging();
          patch->move(SEX, age, age_idx (age + 1), k);

#ifdef _DEBUG_
          transited++;
#endif

        }

        //otherwise, the individual can stay in the same stage and survive there or move to the next stage
      } else {

        if(RAND::Uniform() > sigma) {                  // ind dies

          _popPtr->recycle(ind);
          patch->remove(SEX, age, k);

#ifdef _DEBUG_
          died++;
#endif

        } else {                                                   // ind survives

          if(RAND::Uniform() > gamma){                       // ind stays in the same stage

            ind->Aging();

#ifdef _DEBUG_
            stayed++;
#endif

          } else {                                           // ind moves to the next stage

            ind->Aging();
            patch->move(SEX, age, age_idx (age + 1), k);

#ifdef _DEBUG_
            transited++;
#endif

          }
        } //end else individual survives and may stay
      } //end else individual cannot remain in same stage
    } //end else individual not too old to stay
  } //end for individuals in patch

#ifdef _DEBUG_
  message("(average age was %.2g): %i transited (%.2g); %i stayed (%.2g); %i died (%.2g)\n",
          (double)mean_age/ind_cntr, transited, (double)transited/ind_cntr, stayed,
          (double)stayed/ind_cntr, died, (double)died/ind_cntr);
#endif
}
// ----------------------------------------------------------------------------------------
// LCE_Aging_Multi::offsprgTransition
// ----------------------------------------------------------------------------------------
void LCE_Aging_Multi::offsprgTransition (Patch* patch, sex_t SEX, double survival_rate, double transition_rate,
                                         double sigma, double gamma)
{
  unsigned ind_age;
  Individual* ind;

#ifdef _DEBUG_
  message(" %s transitions at stage 0 ",(SEX == FEM ? "female" : "male"));
  unsigned int transited = 0, stayed = 0, died = 0, mean_age = 0, ind_cntr = patch->size(SEX, OFFSx);
#endif

  unsigned int cntrFEM = 0, agedFEM = 0, surv_in_bankFEM = 0;// for check

  // go faster if all offspring transit to next stage (no seed bank)
  if(transition_rate == 1 && survival_rate == 0) {


#ifdef _DEBUG_
          transited += patch->size(SEX, OFFSx);
#endif

    //update age tag:
    for(unsigned int i = 0; i < patch->size(SEX, OFFSx); ++i)
      patch->get(SEX, OFFSx, i)->Aging();

    //copy all offspring to first adult stage, they are added to the other container
    patch->slice(SEX, OFFSx, ADLTx, (*patch), 0, patch->size(SEX, OFFSx) );

    //clear up the offspring container
    patch->clear(SEX, OFFSx);

  } else {

    // transition rate != 1

    ///// NO SEED BANK /////

    if(survival_rate == 0){

      //determine how many survive:
      unsigned int num_survive = RAND::Binomial2(transition_rate, (int)patch->size(SEX, OFFSx));
      unsigned int at;

      //move them to the first adult stage
      for (unsigned int i = 0; i < num_survive; ++i) {
        // choose randomly
        at = RAND::Uniform(patch->size(SEX, OFFSx));
        // increase age
        patch->get(SEX, OFFSx, at)->Aging();
        // move to next stage
        patch->move(SEX, OFFSx, ADLTx, at);

      }

#ifdef _DEBUG_
       transited += num_survive;
       died += patch->size(SEX, OFFSx);
#endif

      //flush what remains in the container
      patch->flush(SEX, OFFSx, _popPtr);

    } else {

      ///// SEED BANK /////

      //process each individual one by one because we need to check its age each time

      unsigned next_age = _popPtr->getAgeStructure()->get(0, 1);

      for(int k = (int)patch->size(SEX, OFFSx) - 1; k >= 0; --k) {

        ind = patch->get(SEX, OFFSx, k);

        ind_age = (unsigned int)ind->getAge(); //this is age in number of cycles (years)

#ifdef _DEBUG_
        mean_age += ind_age;
#endif

        //first check if individual must move to the next stage because of the age limit in the off stage
        //note that in this case Survival off-adlt is the maturation rate//

        if( ind_age + 1 == next_age){

          //check for transition to next age class

          if(RAND::Uniform() > transition_rate) {//mortality is (1 - t_(j+1,j) )

            _popPtr->recycle(ind);
            patch->remove(SEX, OFFSx, k);

#ifdef _DEBUG_
            died++;
#endif

          } else {

            ind->Aging();
            patch->move(SEX, OFFSx, ADLTx, k);

#ifdef _DEBUG_
            transited++;
#endif

          }

          // else, ind didn't reach limit age
        } else {

          if(RAND::Uniform() > sigma){                   // ind dies, mortality = 1 - sigma

            _popPtr->recycle(ind);
            patch->remove(SEX,OFFSx, k);

#ifdef _DEBUG_
            died++;
#endif

          } else {                                      // ind survives, proba = sigma

            if(RAND::Uniform() > gamma){                // ind stays in stage class, but ages (1 - gamma)

              ind->Aging();

#ifdef _DEBUG_
              stayed++;
#endif

            } else {                                    // ind moves to next stage and ages (gamma)

              ind->Aging();
              patch->move(SEX, OFFSx, ADLTx, k);

#ifdef _DEBUG_
              transited++;
#endif

            }
          } // end stays or leaves seed bank
        } // end reached age limit
      } // end for each ind
    } // end if seed bank
  } // end if check survival rates

#ifdef _DEBUG_
  message("(average age was %.2g): %i transited (%.2g); %i stayed (seed bank)(%.2g); %i died (%.2g)\n",
          (double)mean_age/ind_cntr, transited, (double)transited/ind_cntr, stayed,
          (double)stayed/ind_cntr, died, (double)died/ind_cntr);
#endif


}
// ----------------------------------------------------------------------------------------
// LCE_Aging_Multi::execute
// ----------------------------------------------------------------------------------------
void LCE_Aging_Multi::execute ()
{
  Individual* current_ind;
  Patch* current_patch;

  unsigned int nb_class;
  double surv_rate;

  //this corrects the problem mentioned in the param setter above:
  _survival = _popPtr->getLeslieMatrix();

  nb_class = _survival->getNbCols();

  unsigned int last = nb_class - 1;

  age_idx last_idx = age_idx (nb_class -1);

  age_idx current_age;

#ifdef _DEBUG_
  message("LCE_Aging_Multi::execute (Patch nb: %i offsprg nb: %d adlt nb: %d)\n",_popPtr->getPatchNbr()
          ,_popPtr->size( OFFSPRG ), _popPtr->size( ADULTS ));
#endif


  //for each patch
  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); i++)
  {
    current_patch = _popPtr->getPatch(i);

#ifdef _DEBUG_
    message("LCE_Aging_Multi:: in patch %i (%i offsp, %i adlts)\n",
            current_patch->getID()+1,
            current_patch->size(OFFSPRG),
            current_patch->size(ADULTS));
#endif

    ///start with the last age class, check if aggregated class:
    if( (surv_rate = _survival->get(last, last)) != 0) //get the last row and column of the leslie matrix
    {

      //individuals female in that class may stay if they survive
      AgeOrDie(current_patch, FEM, last_idx, surv_rate);

      //individual male in that class may stay if they survive
      AgeOrDie(current_patch, MAL, last_idx, surv_rate);


    } else { //all individuals in the last age class die


#ifdef _DEBUG_
      message("LCE_Aging_Multi::flushing last adult age class (%i)\n",last_idx);
#endif

      current_patch->flush(last_idx, _popPtr);
    }



    ///Regulation in other age-classes - EXCEPT OFFSPRING
    double s, t, sigma, gamma;

    for( int j = (int)nb_class - 2; j > 0; j--) {

      current_age = age_idx (j); //this is the index in the containers

      s = _survival->get(j, j);
      t = _survival->get(j+1, j);
      sigma = s + t;                // total survival probability
      gamma = t / sigma;            // transition probability of those surviving

      if(_popPtr->getTransitionByAge()) {
        gamma = 0; //transition is forbidden before age limit is reached
        s = (s == 0? sigma : s); //we make sure s_j,j !=0, otherwise the algorithm will force individuals to transit
      }


#ifdef _DEBUG_
  message("LCE_Aging_Multi::stage %i: transit age: %i; transit by age %s; s=%.2g, t=%.2g, sigma=%.2g, gamma=%.2g\n", j,
      (int)_popPtr->getAgeStructure()->get(0, j+1),
      (_popPtr->getTransitionByAge()? "YES" : "NO"),
      s, t, sigma, gamma);
#endif
      stageTransition(current_patch, FEM, j, current_age, s, t, sigma, gamma); //passing t_ij, t_ij+s_jj, t_ij/(t_ij+s_jj)

      stageTransition(current_patch, MAL, j, current_age, s, t, sigma, gamma);

      
    } //end_for j stages

    /// STAGE 0
    /// OFFSPRING aging
    /// it includes survival in a seed bank

    s = _survival->get(0, 0);
    t = _survival->get(1, 0);
    sigma = s + t;                // total survival probability
    gamma = t / sigma;            // transition probability of those surviving

    if(_popPtr->getTransitionByAge()) {
      gamma = 0; //transition is forbidden before age limit is reached
      s = (s == 0? sigma : s); //we make sure s_j,j !=0, otherwise the algorithm will force individuals to transit
    }

#ifdef _DEBUG_
  message("LCE_Aging_Multi::stage 0: transit age: %i; transit by age %s; s=%.2g, t=%.2g, sigma=%.2g, gamma=%.2g\n",
      (int)_popPtr->getAgeStructure()->get(0, 1),
      (_popPtr->getTransitionByAge()? "YES" : "NO"),
      s, t, sigma, gamma);
#endif

    offsprgTransition(current_patch, FEM, s, t, sigma, gamma);

    offsprgTransition(current_patch, MAL, s, t, sigma, gamma);

#ifdef _DEBUG_
    message("LCE_Aging_Multi::finished aging in patch %i (%i offsp, %i adlts)\n",
            current_patch->getID(),
            current_patch->size(OFFSPRG),
            current_patch->size(ADULTS));
#endif

    current_patch->set_isExtinct( (current_patch->size(ALL) == 0) );

//    //set the Patch extinction and age tags:
//    if(current_patch->size(ALL) == 0) {
//      current_patch->set_isExtinct(true);
//      current_patch->set_age(0);
//
//    } else {
//      current_patch->set_isExtinct(false);
//      current_patch->set_age( current_patch->get_age() + 1 );
//    }

  } //end_for i patchNbr

#ifdef _DEBUG_
  cout<<"   stages after transitions: ";
  for(unsigned int i =0; i < nb_class; i++)
    cout<<i<<":"<<_popPtr->size(age_idx (i))<<" | ";
  cout<<endl;
  _popPtr->show_up();
#endif

}



