/**  $Id: LCEcomposite.cc,v 1.10.2.5 2017-03-15 16:37:22 fred Exp $
 *
 *  @file LCEcomposite.cc
 *  Nemo2
 *
 *   Copyright (C) 2006-2011 Frederic Guillaume
 *   frederic.guillaume@env.ethz.ch
 *
 *   This file is part of Nemo
 *
 *   Nemo is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Nemo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  created on @date 09.10.2007
 *
 *  @author fred
 */

#include "LCEcomposite.h"
#include <vector>
#include <numeric>
#include <algorithm>
#include <functional>
#include <valarray>

/*_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/*/

//                                LCE_Breed_Disperse

// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::LCE_Breed_Disperse
// ----------------------------------------------------------------------------------------
LCE_Breed_Disperse::LCE_Breed_Disperse() : LifeCycleEvent("breed_disperse",""),_breed_disperse(0),
_num_colonizers(0),_dispersing_sex(MAL),_growthRates(0),_make_offspring(0),_get_numFemOffspring(0),
_get_numMalOffspring(0),_get_patchFecundity(0),_repr_prop(0)
{
  ParamUpdater<LCE_Breed_Disperse>* updater = new ParamUpdater<LCE_Breed_Disperse>(&LCE_Breed_Disperse::setParameters);

  LCE_Disperse_base::addParameters("breed_disperse", updater);

  add_parameter("breed_disperse_dispersing_sex", STR, false, false, 0, 0, updater);
  add_parameter("breed_disperse_colonizers", INT,false,false,0,0, updater);
  add_parameter("breed_disperse_growth_model", INT, false, true, 1, 8, updater);
  add_parameter("breed_disperse_growth_rate", DBL, false, false, 0, 0, updater);
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::setParameters
// ----------------------------------------------------------------------------------------
bool LCE_Breed_Disperse::setParameters ()
{
  if(!LCE_Breed_base::setParameters()) return false;

  LCE_Disperse_base::set_isForward(false);

  if(!LCE_Disperse_base::setBaseParameters("breed_disperse")) return false;

  if(!LCE_Breed_Disperse::setReproProp()) return false;

  _num_colonizers = (int)get_parameter_value("breed_disperse_colonizers");

  _get_numFemOffspring = (_num_colonizers == -1 ?
      &LCE_Breed_Disperse::numFemOffspring :
      &LCE_Breed_Disperse::numFemOffspring_colonizers);
  _get_numMalOffspring = &LCE_Breed_Disperse::numMalOffspring_notrandom;

  if(get_parameter("breed_disperse_dispersing_sex")->isSet()) {

    if (get_parameter("breed_disperse_dispersing_sex")->getArg() == "female") {
      _dispersing_sex = FEM;
    } else if (get_parameter("breed_disperse_dispersing_sex")->getArg() == "male") {
      _dispersing_sex = MAL;
    } else {
      error("the \"breed_disperse_dispersing_sex\" parameter only takes \"female\" or \"male\" as argument\n");
    }

  } else {
    _dispersing_sex = FEM;
  }

  //check mating system:  
  unsigned int model = this->getMatingSystem();

  if(model == 2 || model == 3) {

    return error("Polygyny and Monogamy are not implemented within the breed_disperse LCE.\n");

  } else if(model == 1) { //promiscuity/random mating

    _make_offspring = &LCE_Breed_Disperse::mate_random;

    _get_numMalOffspring = (_num_colonizers == -1 ?
        &LCE_Breed_Disperse::numMalOffspring_random :
        &LCE_Breed_Disperse::numMalOffspring_random_colonizers);

  } else if(model == 4) { //selfing

    if(this->getMatingProportion() != 1)
      _make_offspring = &LCE_Breed_Disperse::mate_selfing;
    else
      _make_offspring = &LCE_Breed_Disperse::mate_full_selfing;

  } else if(model == 5) { //cloning

    _make_offspring = &LCE_Breed_Disperse::mate_cloning;

  } else if(model == 6) { //random mating among hermaphrodites (selfing = 1/N)

    _make_offspring = &LCE_Breed_Disperse::mate_random_hermaphrodite;

  }

  //check dispersal model:
  model = this->getDispersalModel();

  if(model == 2) //propagule-pool island model
    _breed_disperse = &LCE_Breed_Disperse::do_breed_disperse_propagule;
  else
    _breed_disperse = &LCE_Breed_Disperse::do_breed_disperse;

  //growth model
  if(_paramSet->isSet("breed_disperse_growth_model"))
    model = get_parameter_value("breed_disperse_growth_model");
  else 
    model = 1;

  switch (model)
  {
  case 1:
    _get_patchFecundity = &LCE_Breed_Disperse::instantGrowth;
    break;
  case 2:
    _get_patchFecundity = &LCE_Breed_Disperse::logisticGrowth;
    break;
  case 3:
    _get_patchFecundity = &LCE_Breed_Disperse::stochasticLogisticGrowth;
    break;
  case 4:
    _get_patchFecundity = &LCE_Breed_Disperse::conditionalLogisticGrowth;
    break;
  case 5:
    _get_patchFecundity = &LCE_Breed_Disperse::conditionalStochasticLogisticGrowth;
    break;
  case 6:
    _get_patchFecundity = &LCE_Breed_Disperse::fixedFecundityGrowth;
    break;
  case 7:
    _get_patchFecundity = &LCE_Breed_Disperse::stochasticFecundityGrowth;
    break;
    case 8:
      _get_patchFecundity = &LCE_Breed_Disperse::exponentialGrowth;
      break;
  default:
    _get_patchFecundity = &LCE_Breed_Disperse::instantGrowth;
    break;
  }

  if(_mean_fecundity.get(0,0) == -1) //wasn't set
    if(model > 3 && model < 8) //4 - 7: conditional logistic growth, fecundity growth
      return error("parameter \"mean_fecundity\" is needed with growth models 4 to 7 in LCE breed_disperse\n");
  //growth rate
  if ((model > 1 && model < 6) || model == 8) {

    if(!_paramSet->isSet("breed_disperse_growth_rate")) {

      return error("parameter \"breed_disperse_growth_rate\" needs to be set\n");
    }


    if(_growthRates) delete [] _growthRates;

    _growthRates = new double [ _popPtr->getPatchNbr() ];

    if(_paramSet->isMatrix("breed_disperse_growth_rate")) {

      TMatrix tmp;

      _paramSet->getMatrix("breed_disperse_growth_rate", &tmp);

      if(tmp.getNbCols() != _popPtr->getPatchNbr()){

        return error("matrix argument to \"breed_disperse_growth_rate\" has wrong number of elements,\
              must equal the number of patches.\n");
      }

      for (unsigned int i = 0; i < _popPtr->getPatchNbr(); i++) {
        _growthRates[i] = tmp.get(0, i);
      }


    } else { //not a matrix

      _growthRates[0] = get_parameter_value("breed_disperse_growth_rate");

      for (unsigned int i = 1; i < _popPtr->getPatchNbr(); i++) {
        _growthRates[i] = _growthRates[0];
      }
    }


    if(model == 8) { //exponential growth
      _NatT0.reset(1, _popPtr->getPatchNbr());
    }
  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::setReproProp
// ----------------------------------------------------------------------------------------
bool LCE_Breed_Disperse::setReproProp()
{
  // set up tables to store observed proportions of reproductive adults
  _repro_ind[0].assign(_popPtr->getPatchNbr(), vector<double>()); // males
  _repro_ind[1].assign(_popPtr->getPatchNbr(), vector<double>()); // females

  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); ++i) {
    _repro_ind[0][i].assign(_repro_class.size(), 0);
    _repro_ind[1][i].assign(_repro_class.size(), 0);
  }

  // get sum of fecundities
  double sum = 0;

  for (unsigned int i = 0; i < _repro_class.size(); i++){

    sum += _LeslieMatrix->get(0, _repro_class[i]);

  }

  //store the sum of fecundity across all repro classes as the mean total fecundity
  this->setMeanFecundity(sum);

  //calculate proportional fecundity per repro class
  _repr_prop.assign(_repro_class.size(), 0);

  // get proportional fecundities
  for (unsigned int i = 0; i < _repro_class.size(); i++) {

    _repr_prop[i] = _LeslieMatrix->get(0, _repro_class[i])/sum;

  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::execute
// ----------------------------------------------------------------------------------------
void LCE_Breed_Disperse::execute()
{
#ifdef _DEBUG_
  message("LCE_Breed_Disperse::execute (Patch nb: %i offsprg nb: %i adlt nb: %i "
      ,_popPtr->getPatchNbr(), _popPtr->size( OFFSPRG ), _popPtr->size( ADULTS ));
#endif

  if(_npatch != _popPtr->getPatchNbr()) {
    _npatch = _popPtr->getPatchNbr();
    if(!updateDispMatrix()) fatal("bailing out\n");
  }

  LCE_Disperse_base::reset_counters();

  (this->*_breed_disperse)();

#ifdef _DEBUG_
  unsigned int a = 0, b = 0, c = 0;
  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); i++){
    a += _popPtr->getPatch(i)->nbEmigrant;
    b += _popPtr->getPatch(i)->nbImigrant;
    c += _popPtr->getPatch(i)->nbPhilopat;
  }
  message("immigrate: %f, emigrants: %i, imigrants: %i)\n",(double)b/(b+c), a, b);
#endif
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::setReproductiveStageProportions
// ----------------------------------------------------------------------------------------
void LCE_Breed_Disperse::setReproductiveStageProportions ()
{
  double sum;
  age_idx age;

  // count number of reproductive adults in each patch, used when choosing parents
  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); i++) {

    for(unsigned int j = 0; j < _repro_class.size(); j++) {

      age = age_idx(_repro_class[j]);

      //calculate the relative contribution of each age class to total reproduction in each patch

      _repro_ind[FEM][i][j] = (_popPtr->size(FEM, age, i)) * _LeslieMatrix->get(0, _repro_class[j]);
      _repro_ind[MAL][i][j] = (_popPtr->size(MAL, age, i)); // males don't have fecundities
    }

    //sum N*m
    sum = std::accumulate(_repro_ind[FEM][i].begin(), _repro_ind[FEM][i].end(), 0.0);

    // calculate proportions for reproduction (see below), divide each element by the sum
    std::transform(_repro_ind[FEM][i].begin(), _repro_ind[FEM][i].end(),_repro_ind[FEM][i].begin(),
        [sum](double i){return i/sum;});
//        std::bind2nd(divides<double>(), sum)); // deprecated since C++11

    //same for males
    sum = std::accumulate(_repro_ind[MAL][i].begin(), _repro_ind[MAL][i].end(), 0.0);

    std::transform(_repro_ind[MAL][i].begin(), _repro_ind[MAL][i].end(), _repro_ind[MAL][i].begin(),
        [sum](double i){return i/sum;});
//        std::bind2nd(divides<double>(), sum)); // deprecated since C++11

  }
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::do_breed_disperse
// ----------------------------------------------------------------------------------------
void LCE_Breed_Disperse::do_breed_disperse()
{
  Patch* patch;
  unsigned int nfem, nmal;

  //set proportions of reproductive adults in stages
  setReproductiveStageProportions();

  // check if offspring remain while shouldn't
  if(_popPtr->size(OFFSx) != 0 and _LeslieMatrix->get(0, 0) == 0)  {

    warning("offspring containers are not empty at time of breeding, flushing.\n");

    _popPtr->flush(OFFSx);
  }


  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); i++) {

    patch = _popPtr->getPatch(i);

    nfem = (this->* _get_numFemOffspring)(patch);
    nmal = (this->* _get_numMalOffspring)(patch);

    for(unsigned int j = 0; j < nfem; j++)
      patch->add(FEM, OFFSx, 
          LCE_Breed_base::makeOffspring( (this->*_make_offspring)(FEM, patch, i) )
      );

    //the colonizer counter must be set now, not in get_parent(),
    //otherwise colonizers will be counted twice, once per parent
    if(patch->size(ADULTS) == 0) patch->nbKolonisers += nfem;

    for(unsigned int j = 0; j < nmal; j++) 
      patch->add(MAL, OFFSx, 
          LCE_Breed_base::makeOffspring( (this->*_make_offspring)(MAL, patch, i) )
      );

    if(patch->size(ADULTS) == 0) patch->nbKolonisers += nmal;

  }
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::do_breed_disperse_propagule
// ----------------------------------------------------------------------------------------
void LCE_Breed_Disperse::do_breed_disperse_propagule()
{
  this->setIsland_PropagulePool_Matrix();
  do_breed_disperse();
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::exponentialGrowth
// ----------------------------------------------------------------------------------------
unsigned int LCE_Breed_Disperse::exponentialGrowth (Patch* patch, sex_t SEX)
{
  static unsigned int t0 = 0;
  unsigned int idx = patch->getID();
  unsigned int cGen = _popPtr->getCurrentGeneration();
  unsigned int nGen;
  unsigned int N;

  if(t0 == 0) { //mark starting generation of growth model, when first call (in first patch)
    t0 = cGen; //cGen can be zero
  }

  if(t0 == cGen) { // that means we have just set t0, in some other patch probably
    _NatT0.set(0, idx, patch->size(SEX, ADULTS)); //record initial patch size
  }

  if(patch->size(SEX, ADULTS) < patch->get_K(SEX)) {

    nGen = cGen - t0 + 1; //we're calculating the size at time = t+1

    N = _NatT0.get(0, idx)*exp(_growthRates[idx]*nGen);

  } else {
    N = patch->get_K(SEX);
  }

  return N;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::mate_random
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_Disperse::mate_random(sex_t SEX, Patch *patch, unsigned int LocalPatch)
{
  return _popPtr->makeNewIndividual(get_parent(FEM, FEM, patch, LocalPatch), 
      get_parent(MAL, MAL, patch, LocalPatch),
      SEX, LocalPatch);
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::mate_random_hermaphrodite
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_Disperse::mate_random_hermaphrodite(sex_t SEX, Patch *patch,
    unsigned int LocalPatch)
{
  return _popPtr->makeNewIndividual(get_parent(FEM, FEM, patch, LocalPatch), 
      get_parent(FEM, _dispersing_sex, patch, LocalPatch),
      FEM, LocalPatch);
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::mate_selfing
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_Disperse::mate_selfing(sex_t SEX, Patch *patch, unsigned int LocalPatch)
{
  Individual *mom, *dad;
  //  cout << "--- choosing mum in patch "<<LocalPatch<<" ("<<patch->size(FEM, ADLTx)<<")\n";
  mom = get_parent(FEM, FEM, patch, LocalPatch);

  //  cout << "--- choosing dad \n";

  if( RAND::Uniform() > this->getMatingProportion() ) {
    //random mating
    do{
      dad = get_parent(FEM, _dispersing_sex, patch, LocalPatch);
    }while(dad == mom && _popPtr->getPatchNbr() != 1 && patch->size(FEM, _reproductive_adults) != 1);

  } else
    dad = mom;

  //  cout << "--- make offspring by crossing "<<mom->getID()<<" with "<<dad->getID()<<endl;


  return _popPtr->makeNewIndividual(mom, dad, FEM, LocalPatch);
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::mate_full_selfing
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_Disperse::mate_full_selfing(sex_t SEX, Patch *patch, unsigned int LocalPatch)
{
  Individual *mom = get_parent(FEM, FEM, patch, LocalPatch);

  return _popPtr->makeNewIndividual(mom, mom, FEM, LocalPatch);
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::mate_cloning
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_Disperse::mate_cloning(sex_t SEX, Patch *patch, unsigned int LocalPatch)
{    
  return LCE_Breed_base::breed_cloning(get_parent(FEM, FEM, patch, LocalPatch), NULL, LocalPatch);
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Disperse::get_parent
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_Disperse::get_parent(sex_t SEX, sex_t DispSex, Patch *local_patch,
    unsigned int LocalPatch)
{
  unsigned int SourcePatch;
  Patch* src_patch;
  age_idx current_age;

  current_age = age_idx( _repro_class[0] ); // get first stage class that can reproduce

  //draw the patch of origin of that parent
  do {

    SourcePatch = LCE_Disperse_base::getMigrationPatchBackward(DispSex, LocalPatch);
    src_patch = _popPtr->getPatchPtr(SourcePatch);
    
  } while (src_patch->size( SEX, _reproductive_adults ) == 0); //redraw if source patch is empty
  
  if(LocalPatch != SourcePatch) {

    src_patch->nbEmigrant++;

    if( local_patch->size(ADULTS) != 0 ) // only count immigrants, not colonizers
      local_patch->nbImigrant++;

  } else
    src_patch->nbPhilopat++;


  // get the stage class of the parent ...............................................

  if(_repro_class.size() == 1){
    // if only one stage class is reproducing  -> pick this stage class for parent

    current_age = age_idx( _repro_class[0] );

    return src_patch->get(SEX, current_age, RAND::Uniform( src_patch->size( SEX, current_age ) ) );

  } else {

    // if multiple stage classes are reproducing
    // -> pick stage classes in proportion to their total fecundities (= num ind * fecundity)

    double sum = 0, random = RAND::Uniform();
    unsigned int AimedStage = 0;

//    RAND::Multinomial(_repro_class.size(), 1, &_repro_ind[SEX][SourcePatch][0], &AimedStage); //not a good idea

    if(random > 0.999999) random = 0.999999;           // this to avoid overflows when random == 1

    sum =  _repro_ind[SEX][SourcePatch][0];

    while (random > sum && AimedStage < _repro_class.size()) {

      AimedStage++;

      sum +=  _repro_ind[SEX][SourcePatch][AimedStage];

    }

    current_age = age_idx( _repro_class[AimedStage] );

    return src_patch->get(SEX, current_age, RAND::Uniform( src_patch->size( SEX, current_age ) ) );

  }

  return NULL;

}
/*_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/*/

//                              LCE_Breed_Selection_Disperse

// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection_Disperse::LCE_Breed_Selection_Disperse
// ----------------------------------------------------------------------------------------
LCE_Breed_Selection_Disperse::LCE_Breed_Selection_Disperse()
:  LifeCycleEvent("breed_selection_disperse",""), _breed_selection_disperse(0)
{
  add_parameter("breed_selection_disperse_fitness_threshold", DBL, false, true, 0, 1, 
      new ParamUpdater<LCE_Breed_Selection_Disperse> (&LCE_Breed_Selection_Disperse::setParameters) );
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection_Disperse::setParameters
// ----------------------------------------------------------------------------------------
bool LCE_Breed_Selection_Disperse::setParameters()
{
  if(!LCE_Breed_Disperse::setParameters()) return false;
//  if(!LCE_Selection_base::setBaseParameters()) return false; // the selection parameters prefix should be set by Breed_Selection cstor
  if(!LCE_Breed_Selection::setParameters()) return false;

  if(get_parameter("breed_selection_disperse_fitness_threshold")->isSet())
    _base_fitness = get_parameter_value("breed_selection_disperse_fitness_threshold");
  else
    _base_fitness = 0.05;

  unsigned int model = this->getDispersalModel();
  if(model == 2)
    _breed_selection_disperse = &LCE_Breed_Selection_Disperse::breed_selection_disperse_propagule;
  else
    _breed_selection_disperse = &LCE_Breed_Selection_Disperse::breed_selection_disperse;

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection_Disperse::LCE_Breed_Selection_Disperse
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection_Disperse::execute()
{
#ifdef _DEBUG_
  message("LCE_Breed_Selection_Disperse::execute (mean fitness(adlt): %f, fit scale: %f, offsprg nb: %i, adlt nb: %i, "
      ,getMeanFitness(ADLTx), _scaling_factor, _popPtr->size( OFFSPRG ), _popPtr->size( ADULTS ));
  fflush(stdout);
#endif

  //  _scaling_factor = (_mean_fitness >= _base_fitness ? 1 : _base_fitness/_mean_fitness);

  if(_npatch != _popPtr->getPatchNbr()) {
    _npatch = _popPtr->getPatchNbr();
    if(!updateDispMatrix()) fatal("LCE_Breed_Selection_Disperse::execute: bailing out\n");
  }

  LCE_Selection_base::resetCounters();
  LCE_Disperse_base::reset_counters();

  // check if change in local optima for quantitative traits
  LCE_Selection_base::checkChangeLocalOptima();

  //the values within the Leslie matrix may change during a simulation
  _LeslieMatrix = _popPtr->getLeslieMatrix();

  setMatingPool(); //has to be rechecked each time if the leslie matrix was modified

  //check if offspring already present
  if(_popPtr->size(OFFSPRG) != 0 and _LeslieMatrix->get(0, 0) == 0) {

    warning("offspring containers not empty at time of breeding, flushing.\n");

    _popPtr->flush(OFFSx);

  }

  //set proportions of reproductive adults in stages
  setReproductiveStageProportions();

  // do the job!
  (this->*_breed_selection_disperse)();

  // update fitness stat counters
  LCE_Selection_base::setMeans();

#ifdef _DEBUG_
  unsigned int a = 0, b = 0, c = 0;
  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); i++){
    a += _popPtr->getPatch(i)->nbEmigrant;
    b += _popPtr->getPatch(i)->nbImigrant;
    c += _popPtr->getPatch(i)->nbPhilopat;
  }
  message("immigrate: %f, emigrants: %i, imigrants: %i, mean fitness(offsprg): %f)\n",(double)b/(b+c), a, b, _mean_fitness);
#endif
}  
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection_Disperse::breed_selection_disperse
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection_Disperse::breed_selection_disperse ()
{
  Patch* patch;
  unsigned int nfem, nmal;
  unsigned int patchNbr = _popPtr->getPatchNbr();
  age_idx current_age;

  for(unsigned int i = 0; i < patchNbr; i++) {

    patch = _popPtr->getPatch(i);

    //get the mean fitness of the reproductive adults, pooled together under same selection conditions
    _mean_fitness = getMeanPatchFitnessMultiStage(_reproductive_adults, 0, i);
    _scaling_factor = (_mean_fitness >= _base_fitness ? 1 : _base_fitness/_mean_fitness);

    nfem = (this->* _get_numFemOffspring)(patch);
    nmal = (this->* _get_numMalOffspring)(patch);

    do_breed(FEM, nfem, patch, i);
    do_breed(MAL, nmal, patch, i);

    // viability selection --------------------------------------------------------------------------
    //cycle through the other stages under selection, except OFFSx (stage 0)
    for(int j = 1 ; j < _stage_selected->getNbCols();j++) {

      current_age = age_idx(_stage_selected->get(0,j));

      (this->*_setScalingFactor)(j, i); // depends on fitness model option

      LCE_Selection_base::doViabilitySelection(FEM, current_age, patch, i, j);

      LCE_Selection_base::doViabilitySelection(MAL, current_age, patch, i, j);

    }
  }
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection_Disperse::do_breed
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection_Disperse::breed_selection_disperse_propagule ()
{
  this->setIsland_PropagulePool_Matrix();
  breed_selection_disperse();
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection_Disperse::do_breed
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection_Disperse::do_breed(sex_t SEX, unsigned int size, Patch* patch,
    unsigned int cpatch)
{
  Individual *new_ind;

  for(unsigned int j = 0; j < size; j++) {

    do{

      new_ind = makeOffspringWithSelection( (this->*_make_offspring)(SEX, patch, cpatch), cpatch, OFFSx);

    } while (new_ind == NULL);

    patch->add(SEX, OFFSx, new_ind);
  }
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::loadStatServices
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection_Disperse::loadStatServices ( StatServices* loader )
{
  LCE_Selection_base::loadStatServices(loader);
}

/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */

//                                 LCE_Breed_Selection

// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection
// ----------------------------------------------------------------------------------------
LCE_Breed_Selection::LCE_Breed_Selection ( ) : LifeCycleEvent("breed_selection",""), 
_breed_selection(0), _nb_trait(0), _setOffsprgScaledSurvival(0), _setReproFemaleScaledFitness(0)
{
  LCE_Selection_base::addParameters("breed_selection",  //the parameter name prefix
      new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_fit_model),
      new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_sel_model),
      new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_local_optima),
      new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_param_rate_of_change));

  add_parameter("breed_selection_fecundity_fitness", BOOL, false, false, 0, 0, 0);

}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::setParameters
// ----------------------------------------------------------------------------------------
bool LCE_Breed_Selection::setParameters ( )
{
  if( !LCE_Breed_base::setParameters( ) ||  !LCE_Selection_base::setBaseParameters( ) )
    return false;

  if(this->getMatingSystem() == 5)
    return error("LCE \"breed_selection\" does not support mating system 5 - cloning\n");

  //check which stage is under selection
  //parameters for stage 0 MUST be set
  if(_stage_selected->get(0,0) != 0)
    return error("In LCE \"breed_selection\", selection parameters for stage 0 MUST be specified.\n");

  // option for selection on fecundity:
  if(get_parameter("breed_selection_fecundity_fitness")->isSet()){

    _breed_selection = &LCE_Breed_Selection::do_breed_selection_FecFitness ;

  } else {
    _breed_selection = &LCE_Breed_Selection::do_breed_selection_OffSurvival ;
  }

  // default fitness model:
  _setOffsprgScaledSurvival = &LCE_Breed_Selection::setOffsprgScaledSurvival_absolute;
  _setReproFemaleScaledFitness = &LCE_Breed_Selection::setReproFemaleScaledFitness_absolute;

  //check if "fitness" model is relative:
  if( ! _is_absolute ) {  //in LCE_Selection_Base

    if( !_is_local ) {
      error("LCE breed_selection relative fitness model is set to \"global\", please use \"local\" options.\n");
      return error("LCE breed_selection only computes relative fitness locally within patches.\n");
    }

    // reset the method used with offspring survival
    _breed_selection = &LCE_Breed_Selection::do_breed_selection_Off_RelativeSurvival;

    //check which scaling is required
    if(_paramSet->isSet(_prefix + "_fitness_model")) {

      string fit_model = _paramSet->getArg(_prefix + "_fitness_model");

      if(fit_model == "relative_local"){

        _setOffsprgScaledSurvival = &LCE_Breed_Selection::setOffsprgScaledSurvival_mean;
        _setReproFemaleScaledFitness = &LCE_Breed_Selection::setReproFemaleScaledFitness_mean;

      } else if(fit_model == "relative_max_local") {

        _setOffsprgScaledSurvival = &LCE_Breed_Selection::setOffsprgScaledSurvival_max;
        _setReproFemaleScaledFitness = &LCE_Breed_Selection::setReproFemaleScaledFitness_max;
      }
    }
  }

  return setNonSelectedTraitTable();
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::setNonSelectedTraitTable
// ----------------------------------------------------------------------------------------
bool LCE_Breed_Selection::setNonSelectedTraitTable() {

  _nb_trait = _popPtr->getIndividualProtoype()->getTraitNumber();

  _nonSelectedTraitIndices.clear();

#ifdef _DEBUG_
  cout << "@@@@ " << _nb_trait - _TraitIndices.size()<< " trait(s) not under selection\n";
#endif

  if (_nb_trait > _TraitIndices.size()) { //means we have some traits not under selection

    for (unsigned int i = 0; i < _nb_trait; ++i) {
      //check which trait is not under selection and add its index into the vector
      if (find(_TraitIndices.begin(), _TraitIndices.end(), i) == _TraitIndices.end()) {
        _nonSelectedTraitIndices.push_back(i);
      }
    }

    if(_nonSelectedTraitIndices.size() != _nb_trait - _TraitIndices.size())
      return error("LCE_Breed_Selection::setParameters:: trait not under selection was not found at initialization\n");

  }
  
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::breed
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::execute ()
{
  Patch* current_patch;
  age_idx current_age;

  // check if change in local optima for quantitative traits
  LCE_Selection_base::checkChangeLocalOptima();

  //the values within the Leslie matrix may change during a simulation

  _LeslieMatrix = _popPtr->getLeslieMatrix();

  setMatingPool(); //has to be rechecked each time if the leslie matrix was modified

  //check if offspring already present
  if(_popPtr->size(OFFSPRG) != 0 and _LeslieMatrix->get(0, 0) == 0) {

    warning("offspring containers not empty at time of breeding, flushing.\n");

    _popPtr->flush(OFFSx);

  }

#ifdef _DEBUG_
  message("LCE_Breed_Selection::execute\n");
#endif

  _scaling_factor = 1;
  _mean_fitness = 0;

  LCE_Selection_base::resetCounters();

  // perform selection on stage 0: can be fecundity or survival selection
  for(unsigned int home = 0; home < _popPtr->getPatchNbr(); home++) {

    current_patch = _popPtr->getPatch(home);

    if( !checkMatingCondition(current_patch) ) continue;

    (this->* _breed_selection) (current_patch, home); //this is fecundity selection

  }//end_for home

  // viability selection --------------------------------------------------------------------------
  //cycle through the other stages under selection, except OFFSx (stage 0)
  for(unsigned int p = 0; p < _popPtr->getPatchNbr(); p++) {

    current_patch = _popPtr->getPatch(p);

    //here we skip the first stage, which is stage 0, unless something wrong happened (checked at init)
    for(int i = 1 ; i < _stage_selected->getNbCols();i++) {

      current_age = age_idx(_stage_selected->get(0,i));

      (this->*_setScalingFactor)(i, p);

      LCE_Selection_base::doViabilitySelection(FEM, current_age, current_patch, p, i);

      LCE_Selection_base::doViabilitySelection(MAL, current_age, current_patch, p, i);

    }
  }

  LCE_Selection_base::setMeans(); //this is still up to date but mixes fitness in multiple stages

#ifdef _DEBUG_
  message("   (offsprg nb: %i adults nb: %i, mean fitness: %f)\n",_popPtr->size(OFFSPRG),
      _popPtr->size(ADULTS), _mean_fitness);
#endif

}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::do_breed_selection_OffSurvival
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::do_breed_selection_OffSurvival (Patch* patch, unsigned int patchID)
{
  unsigned int nbBaby;
  Individual* FatherPtr;
  Individual* MotherPtr;
  Individual* NewOffsprg;
  age_idx current_age;

  //cycle only through reproductive classes
  for(unsigned int j = 0; j < _repro_class.size(); ++j){

    current_age = static_cast<age_idx> ( _repro_class[j] );

//        cout<<"current age "<<current_age<<"; size = "
//            <<patch->size(FEM, current_age)<<"(F) "<<patch->size(MAL, current_age)<<"(M)"<<endl;

    for(unsigned int size = patch->size(FEM, current_age), indexOfMother = 0;
        indexOfMother < size;
        indexOfMother++)
      {

      MotherPtr = patch->get(FEM, current_age, indexOfMother);

        nbBaby = (unsigned int)MotherPtr->setFecundity( getFecundity(_LeslieMatrix->get(0, current_age)) );

        //-----------------------------------------------------------------------
        while(nbBaby != 0) {

          FatherPtr = this->getFatherPtr(patch, MotherPtr, indexOfMother);

          NewOffsprg = makeOffspringWithSelection( LCE_Breed_base::do_breed(MotherPtr, FatherPtr, patchID), patchID, OFFSx );

          if(NewOffsprg != NULL)
            patch->add(NewOffsprg->getSex(), OFFSx, NewOffsprg);

          nbBaby--;

        }//END__WHILE

      }//END__for females

  }//END__for age class
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::do_breed_selection_OffSurvival
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::do_breed_selection_Off_RelativeSurvival (Patch* patch, unsigned int patchID)
{
  unsigned int nbBaby;
  Individual* FatherPtr;
  Individual* MotherPtr;
  Individual* NewOffsprg;
  age_idx current_age;
  vector<Individual*> newBrood;

  //cycle only through reproductive classes
  for(unsigned int j = 0; j < _repro_class.size(); ++j){

    current_age = static_cast<age_idx> ( _repro_class[j] );

    for(unsigned int size = patch->size(FEM, current_age), indexOfMother = 0;
        indexOfMother < size;
        indexOfMother++)
      {

      MotherPtr = patch->get(FEM, current_age, indexOfMother);

        nbBaby = (unsigned int)MotherPtr->setFecundity( getFecundity(_LeslieMatrix->get(0, current_age)) );

        //-----------------------------------------------------------------------
        while(nbBaby != 0) {

          FatherPtr = this->getFatherPtr(patch, MotherPtr, indexOfMother);

          NewOffsprg = makeOffspringSelectedTraitNoSelection( LCE_Breed_base::do_breed(MotherPtr, FatherPtr, patchID));

          newBrood.push_back( NewOffsprg );

          nbBaby--;

        }//END__WHILE

      }//END__for females

  }//END__for age class

  // we need to calculate the relative survival rates of the new brood only (don't account for pre-existing offspring)
  valarray< double > survival(0.0, newBrood.size());
  unsigned int cat;

  // compute survival rates according to fitness model option
  (this->*_setOffsprgScaledSurvival)(newBrood, survival, patchID);

  // check for individual survival
  for (unsigned int i = 0; i < newBrood.size(); ++i) {

    NewOffsprg = newBrood[i];

    cat = NewOffsprg->getPedigreeClass();

    _fitness[cat] += survival[i];
    _ind_cntr[cat]++;

    //check for survival
    if( RAND::Bernoulli2(survival[i]) ){

      //survivor!
      //compute inheritance and mutation of the traits not under selection:
      for(unsigned int t = 0; t < _nonSelectedTraitIndices.size(); t++)
        NewOffsprg->createTrait(_nonSelectedTraitIndices[t], LCE_Breed_base::doInheritance(), true);

      //add it to the patch
      patch->add(NewOffsprg->getSex(), OFFSx, NewOffsprg);

      _survival[cat]++;

      NewOffsprg->getMother()->DidHaveABaby(cat);
      if(cat!=4) NewOffsprg->getFather()->DidHaveABaby(cat);

    } else {

      //this one dies
      _popPtr->recycle(NewOffsprg);

      NewOffsprg->getMother()->addMating(cat);
      if(cat!=4) NewOffsprg->getFather()->addMating(cat);

    }

  }
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::do_breed_selection_OffSurvival
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::do_breed_selection_FecFitness (Patch* patch, unsigned int patchID)
{
  unsigned int nbBaby;
  //double mean_fec;
  Individual* FatherPtr;
  Individual* MotherPtr;
  Individual* NewOffsprg;
  unsigned int cat;

  age_idx current_age;

  unsigned int numReproFem = patch->size(FEM, _reproductive_adults);
  unsigned int reproFemCntr = 0;

  // array to store the scaled fitness values of ALL reproducing females
  // the female ordering in this array is the same as in the loops below,
  // females are taken one after the other in the same order
  valarray< double > femFitness(0.0, numReproFem);

  //cycle only through reproductive classes
  for(unsigned int j = 0; j < _repro_class.size(); ++j){

    current_age = static_cast<age_idx> ( _repro_class[j] );

    (this->*_setReproFemaleScaledFitness)(femFitness, numReproFem, patch); //fitness may be relative local only, on absolute

    for(unsigned int size = patch->size(FEM, current_age), indexOfMother = 0;
        indexOfMother < size && reproFemCntr < numReproFem;
        indexOfMother++, reproFemCntr++) {

      MotherPtr = patch->get(FEM, current_age, indexOfMother);

      //update counters
      cat = MotherPtr->getPedigreeClass();
      _fitness[cat] += femFitness[ reproFemCntr ];
      _ind_cntr[cat]++;
      _survival[cat]++;

      // selection on fecundity is taken from the selection parameters of stage 0
      nbBaby = (unsigned int)MotherPtr->setFecundity( getFecundity(_LeslieMatrix->get(0, current_age) * femFitness[ reproFemCntr ]) );

      //-----------------------------------------------------------------------
      while(nbBaby != 0) {

        FatherPtr = this->getFatherPtr(patch, MotherPtr, indexOfMother); //fitness of father doesn't count here

        NewOffsprg = LCE_Breed_base::makeOffspring( LCE_Breed_base::do_breed(MotherPtr, FatherPtr, patchID) );

        patch->add(NewOffsprg->getSex(), OFFSx, NewOffsprg);

        nbBaby--;

      }//_END__WHILE;
    }//end_for indexOfMother
  }

}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::makeOffspringWithSelection
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_Selection::makeOffspringWithSelection (Individual* ind,
    unsigned int natalpatch, unsigned int stage)
{
  if(!ind->getMother() || !ind->getFather())
    fatal("found NULL parent ptr in offspring (LCE_Breed_Selection::makeOffspringWithSelection)\n");
  
  //do inheritance of the traits under selection:
  for(unsigned int i = 0; i < _TraitIndices.size(); ++i)
    ind->createTrait(_TraitIndices[i], LCE_Breed_base::doInheritance(), true);

  //check if the individual survives
  double fitness = getFitness(ind, natalpatch, stage);
  unsigned int cat = ind->getPedigreeClass();

  _fitness[cat] += fitness;
  _ind_cntr[cat]++;

  if(RAND::Uniform() > fitness ) { // here the fitness can only be absolute
    //this one dies
    _popPtr->recycle(ind);

    ind->getMother()->addMating(cat);
    if(cat!=4) ind->getFather()->addMating(cat);

    // EXIT now, with null ptr
    return NULL;

  } else {
    //it survived, we'll return it

    //update counters
    _survival[cat]++;

    ind->getMother()->DidHaveABaby(cat);
    if(cat!=4) ind->getFather()->DidHaveABaby(cat);
  }

  //compute inheritance and mutation of the traits not under selection:
  for(unsigned int i = 0; i < _nonSelectedTraitIndices.size(); i++)
      ind->createTrait(_nonSelectedTraitIndices[i], LCE_Breed_base::doInheritance(), true);

  return ind;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::makeOffspringWithSelection
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_Selection::makeOffspringSelectedTraitNoSelection (Individual* ind)
{
  if(!ind->getMother() || !ind->getFather())
    fatal("found NULL parent ptr in offspring (LCE_Breed_Selection::makeOffspringWithSelection)\n");

  //do inheritance of the traits under selection:
  for(unsigned int i = 0; i < _TraitIndices.size(); ++i)
    ind->createTrait(_TraitIndices[i], LCE_Breed_base::doInheritance(), true);

  return ind;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::setOffsprgScalingFactor_absolute
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::setOffsprgScaledSurvival_absolute (vector< Individual* >& brood, valarray< double >& survival,
    unsigned int natalpatch)
{
  _scaling_factor = 1;
  for(unsigned int i = 0; i < brood.size(); ++i) {
    survival[i] = getFitness(brood[i], natalpatch, 0);
  }
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::setOffsprgScalingFactor_mean
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::setOffsprgScaledSurvival_mean     (vector< Individual* >& brood, valarray< double >& survival,
    unsigned int natalpatch)
{
  _scaling_factor = 1; //we want the raw fitness before re-scaling
  for(unsigned int i = 0; i < brood.size(); ++i) {
    survival[i] = getFitness(brood[i], natalpatch, 0);
  }

  double sum = survival.sum();
  double mean = sum/survival.size();

  survival /= mean;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::setOffsprgScalingFactor_max
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::setOffsprgScaledSurvival_max      (vector< Individual* >& brood, valarray< double >& survival,
    unsigned int natalpatch)
{
  _scaling_factor = 1; //we want the raw fitness before re-scaling
  for(unsigned int i = 0; i < brood.size(); ++i) {
    survival[i] = getFitness(brood[i], natalpatch, 0);
  }

  double max = survival.max();

  survival /= max;

}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::setOffsprgScalingFactor_sum
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::setOffsprgScaledSurvival_sum      (vector< Individual* >& brood, valarray< double >& survival,
    unsigned int natalpatch)
{
  _scaling_factor = 1; //we want the raw fitness before re-scaling
  for(unsigned int i = 0; i < brood.size(); ++i) {
    survival[i] = getFitness(brood[i], natalpatch, 0);
  }

  double sum = survival.sum();

  survival /= sum;

}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::setOffsprgScalingFactor_sum
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::setReproFemaleScaledFitness_absolute (valarray< double > &survival, unsigned int numReproFem, Patch* patch)
{
  _scaling_factor = 1; //we want the raw fitness before re-scaling
  for(unsigned int i = 0; i < numReproFem; ++i) {
    survival[i] = getFitness(patch->get(FEM, _reproductive_adults, i), patch->getID(), 0);
  }

}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::setOffsprgScalingFactor_sum
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::setReproFemaleScaledFitness_mean     (valarray< double > &survival, unsigned int numReproFem, Patch* patch)
{
  _scaling_factor = 1; //we want the raw fitness before re-scaling
  for(unsigned int i = 0; i < numReproFem; ++i) {
    survival[i] = getFitness(patch->get(FEM, _reproductive_adults, i), patch->getID(), 0);
  }

  double sum = survival.sum();
  double mean = sum/survival.size();

  survival /= mean;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::setOffsprgScalingFactor_sum
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::setReproFemaleScaledFitness_max      (valarray< double > &survival, unsigned int numReproFem, Patch* patch)
{
  _scaling_factor = 1; //we want the raw fitness before re-scaling
  for(unsigned int i = 0; i < numReproFem; ++i) {
    survival[i] = getFitness(patch->get(FEM, _reproductive_adults, i), patch->getID(), 0);
  }

  double max = survival.max();

  survival /= max;

}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::setOffsprgScalingFactor_sum
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::setReproFemaleScaledFitness_sum      (valarray< double > &survival, unsigned int numReproFem, Patch* patch)
{
  _scaling_factor = 1; //we want the raw fitness before re-scaling
  for(unsigned int i = 0; i < numReproFem; ++i) {
    survival[i] = getFitness(patch->get(FEM, _reproductive_adults, i), patch->getID(), 0);
  }

  double sum = survival.sum();

  survival /= sum;

}
// ----------------------------------------------------------------------------------------
// LCE_Breed_Selection::loadStatServices
// ----------------------------------------------------------------------------------------
void LCE_Breed_Selection::loadStatServices ( StatServices* loader )
{
  LCE_Selection_base::loadStatServices(loader);
}


