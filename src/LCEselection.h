/**  $Id: LCEselection.h,v 1.9.2.4 2017-03-15 13:45:36 fred Exp $
*
*  @file LCEselection.h
*  Nemo2
*
*   Copyright (C) 2006-2011 Frederic Guillaume
*   frederic.guillaume@env.ethz.ch
*
*   This file is part of Nemo
*
*   Nemo is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   Nemo is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*  created on @date 09.10.2007
*
*  @author fred
*/

#ifndef LCE_SELECTION_H
#define LCE_SELECTION_H

#include <cmath>
#include "lifecycleevent.h"
#include "filehandler.h"
#include "datatable.h"
#ifdef HAS_GSL
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#endif

class LCE_SelectionSH;
class LCE_SelectionFH;
//CLASS LCE_Selection_base
//
/**Base class performing (viability) selection.
   Implements several types of selection models, from fixed inbreeding depression-type model to 
   Gaussian stabilizing selection on an adaptive landscape.
   This class is the base-class for the composite LCEs breed_selection and breed_selection_disperse.
*/
class LCE_Selection_base : public virtual LifeCycleEvent
{
  ///@name Gaussian selection variables
  ///@{

  /**Selection matrices for each stage under selection, also patch specific.*/
  vector< vector< TMatrix*> > _selection_matrix;

  /**Selection matrices converted to GSL matrices. One for each stage under selection, also patch specific.*/
  vector< vector< gsl_matrix*> > _gsl_selection_matrix;

  /**Two vectors used for the individual fitness calculation.*/
  gsl_vector *_diffs, *_res1;

  /**The matrix holding the trait phenotypic optima across patches, with row = patch, col = trait.*/
  TMatrix *_local_optima;

  /**Matrix holding the rate of per-generation change of the local optima.*/
  TMatrix _rate_of_change_local_optima;
  //env change parameters
  bool _do_change_local_opt;
  bool _rate_of_change_is_std;
  unsigned int _set_std_rate_at_generation;
  int _std_rate_reference_patch;

  /**Array to temporarily hold the phenotypic values of an individual, is never allocated.*/
  double *_phe;
  ///@}

  ///@name Fixed selection (lethal-equivalents-based) parameters
  ///@{

  /**Array of pedigree values used in the fixed-selection model.*/
  double _Fpedigree[5];
  /**Absolute fitness values of the five pedigree class for the fixed selection model (lethal equivalents model).
    Fitness is computed as:  _FitnessFixModel[i] = _base_fitness * exp( -_letheq * _Fpedigree[i] ); */
  double _FitnessFixModel[5];

  ///@}

  /**The StatHandler associated class is a friend.*/
  friend class LCE_SelectionSH;
  friend class LCE_SelectionFH;
  friend class LCE_Breed_Selection;

protected:
  
  /**Parameter name prefix.*/
  string _prefix;

  /**Number of traits under selection.*/
  unsigned int _nb_trait;

  /**Number of quantitative traits under selection for the Gaussian and Quadratic selection models.*/
  int _selectTraitDimension;
  
  double _letheq, _base_fitness;
  double _mean_fitness, _max_fitness, _scaling_factor;
  bool _is_local, _is_absolute;
  
  /**Fitness counters, one for each pedigree class.*/
  double _fitness[5], _survival[5], _ind_cntr[5]; 
  
  /**Environmental variance.*/ //@TODO make environmental variance patch specific or remove it!
  double _eVariance;

  /**The list of trait types under selection.*/
  vector< trait_t > _Traits;

  /**The indices of the traits under selection.*/
  vector< unsigned int > _TraitIndices;

  /**The selection models associated with each trait under selection.*/
  vector< string > _SelectionModels;  

  /**Stores which stage is under selection.*/
  TMatrix* _stage_selected;

  /**Stores the stage-specific selection matrices for the Gaussian model.*/
  TMatrix* _selection_stage_variance;

  ///@name Pointers to functions, set in LCE_Selection_base::setParameters
  ///@{
  /**A vector containing pointers to fitness function related to each trait under selection.
   * The arguments must be: individual pointer, patch, trait, stage*/
  vector< double (LCE_Selection_base::* ) (Individual*, unsigned int, unsigned int, unsigned int) > _getRawFitness;

  /**Pointer to the function returning the individual fitness. 
   May point to LCE_Selection_base::getFitnessAbsolute or LCE_Selection_base::getFitnessRelative.
   The arguments must be: ind, patch, stage.*/
  double (LCE_Selection_base::* _getFitness) (Individual*, unsigned int, unsigned int);

  /**Pointer to the function used to set the fitness scaling factor when fitness is relative.
   May point to LCE_Selection_base::setScalingFactorGlobal, LCE_Selection_base::setScalingFactorLocal,
   or LCE_Selection_base::setScalingFactorAbsolute (returns 1).*/
  void (LCE_Selection_base::* _setScalingFactor) (unsigned int, unsigned int);
  
   /**Pointer to the function used to change the local phenotypic optima or its rate of change.
   May point to LCE_Selection_base::changeLocalOptima or LCE_Selection_base::set_std_rate_of_change.*/
  void (LCE_Selection_base::* _setNewLocalOptima) (void);

  ///@}

  LCE_SelectionSH* _stater;
  LCE_SelectionFH* _writer;
    
public:
    
  LCE_Selection_base ( );
  
  virtual ~LCE_Selection_base ( );
  
  void reset_selection_matrix_containers();

  TMatrix* getAgeClassesSelected () {return _stage_selected;}

  void addParameters     (string prefix, ParamUpdaterBase* fitMod_updtr, ParamUpdaterBase* selMod_updtr, ParamUpdaterBase* opt_updtr, ParamUpdaterBase* rchge_updtr);
  bool setBaseParameters ();
  bool setSelectionMatrix();
  bool setSelectionMatrixMultiStage();
  bool setLocalOptima ();
  bool set_sel_model  ();
  bool set_fit_model  ();

  // functions to set and modify the local optima and their rate of change
  bool set_local_optima ();
  bool set_param_rate_of_change();
  void set_std_rate_of_change();
  void addPhenotypicSD (unsigned int deme, double *stDev);
  void changeLocalOptima ();
  void checkChangeLocalOptima();


  /**Resets the fitness counters.*/
  void resetCounters  ()
  {
    for(unsigned int i = 0; i < 5; i++) {
      _fitness[i] = 0;
      _survival[i] = 0;
      _ind_cntr[i] = 0;
    }
  }
  /**Computes the average fitness of each pedigree class.
     Called after selection has been done in the whole population.
     @param tot_ind count of the total number of individuals produced*/
  void setMeans ()
  {
    _mean_fitness = 0;
    unsigned int tot = 0;
    for(unsigned int i = 0; i < 5; i++) tot += _ind_cntr[i];
    for(unsigned int i = 0; i < 5; i++) {
      _mean_fitness += _fitness[i];
      _fitness[i] /= _ind_cntr[i];
      _survival[i] /= _ind_cntr[i];
      _ind_cntr[i] /= tot;
    }
    _mean_fitness /= tot;
  }
  
  
  /**Computes the mean fitness of the whole population for a given age class.
    @param age the age-class index*/
  double getMeanFitness         (unsigned int selected_stage_idx);
  
  /**Computes the mean fitness in a given patch for a given age class.
    @param age the age-class index
    @param p the patch index*/
  double getMeanPatchFitness    (unsigned int selected_stage_idx, unsigned int p);
  
  /**Computes the mean fitness in a given patch for multiple stages pooled together under the same selection conditions.
   * Used when selection is on fecundity of the reproductive adults.
    @param AGE the age-class flags for all stages considered (e.g. reproductive adults)
    @param selected_stage_idx the index of the selection coefficients in the _stage_selected matrix
    @param p the patch index*/
  double getMeanPatchFitnessMultiStage    (age_t AGE, unsigned int selected_stage_idx, unsigned int p);

  /**Computes the maximum fitness value of the whole population for a given age class.
   @param age the age-class index*/
  double getMaxFitness         (unsigned int selected_stage_idx);
  
  /**Computes the maximum fitness value in a given patch for a given age class.
   @param age the age-class index
   @param p the patch index*/
  double getMaxPatchFitness    (unsigned int selected_stage_idx, unsigned int p);

  /**Sets the _mean_fitness variable to the value of the mean population fitness.
    @param age the age-class index*/
  double setMeanFitness         (unsigned int selected_stage_idx) { return (_mean_fitness = getMeanFitness(selected_stage_idx));}

  /**Calls the fitness function according to the fitness model for a given trait type.
    The fitness model can be "absolute", "relative_local" or "relative_global".
    @param ind the focal indvidual, we want to know its fitness
    @param patch the index of the patch of the focal individual*/
  double getFitness (Individual* ind, unsigned int patch, unsigned int selected_stage_idx)
  {
      return (this->*_getFitness)(ind, patch, selected_stage_idx);
  }

  /**Returns the fitness of an individual in the fixed selection model.*/
  double getFitnessFixedEffect (Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx)
  {
    return _FitnessFixModel[ ind->getPedigreeClass() ];
  }
  
  /**Returns the fitness of an individual following the direct selection model.*/
  double getFitnessDirect (Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx)
  { 
    return *(double*)ind->getTraitValue(trait);
  }
  
  /**Quadratic fitness surface, approximates the Gaussian model for weak selection and/or small deviation from the optimum.*/
  double getFitnessUnivariateQuadratic ( Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx);

  /**Returns the fitness of an individual following the Gaussian selection model with one trait under selection.*/
  double getFitnessMultivariateGaussian (Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx);
  
  /**Returns the fitness of an individual following the Gaussian selection model with several traits under selection.*/
  double getFitnessUnivariateGaussian (Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx);
  
  /**Returns the fitness of an individual following the Gaussian selection model with one trait under selection and environmental variance.*/
  double getFitnessMultivariateGaussian_VE (Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx);
  
  /**Returns the fitness of an individual following the Gaussian selection model with several traits under selection and environmental variance.*/
  double getFitnessUnivariateGaussian_VE (Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx);
  
  
  /**Returns the raw fitness of the individual, without adjustment (absolute fitness).
    Calls the fitness function according to the right selection model.
    @param ind the focal indvidual, we want to know its fitness
    @param patch the index of the patch of the focal individual*/
  double getFitnessAbsolute (Individual* ind, unsigned int patch, unsigned int selected_stage_idx);
//  {
//    return (this->*_getRawFitness)(ind, patch);
//  }

//  double getFitnessAbsoluteAge (Individual* ind, unsigned int patch, unsigned int selected_stage_idx);
//  {
//    return (this->*_getRawFitnessAge)(ind, patch, selected_stage_idx);
//  }


  /**Returns the relative fitness of the individual, adjusted by a scaling factor.
    The scaling factor is set according to the relative fitness model (local or global)
    outside of this function.
    If the scaling factor is one, this is the absolute fitness of the individual.
    Calls the fitness function according to the right selection model.
    @param ind the focal indvidual, we want to know its fitness
    @param patch the index of the patch of the focal individual
    @param selected_stage_idx the index?*/
  double getFitnessRelative (Individual* ind, unsigned int patch, unsigned int selected_stage_idx)
  {
    return getFitnessAbsolute(ind, patch, selected_stage_idx) * _scaling_factor;
  }

  /**Sets the fitness scaling factor equal to the inverse of the mean local patch fitness.
    @param age the age-class index
    @param p the focal patch index*/
  void   setScalingFactorLocal  (unsigned int selected_stage_idx, unsigned int p);

  /**Sets the fitness scaling factor equal to the inverse of the mean population fitness.
    Function exits on p != 0; the mean population fitness is computed only once in the execute()
    procedure, at the beginning of the patch loop.
    @param age the age-class index
    @param p the focal patch index*/
  void   setScalingFactorGlobal (unsigned int selected_stage_idx, unsigned int p);

  /**Sets the fitness scaling factor equal to the inverse of the maximum local patch fitness value.
   @param age the age-class index
   @param p the focal patch index*/
  void   setScalingFactorMaxLocal  (unsigned int selected_stage_idx, unsigned int p);
  
  /**Sets the fitness scaling factor equal to the inverse of the maximum population fitness value.
   Function exits on p != 0; the mean population fitness is computed only once in the execute()
   procedure, at the beginning of the patch loop.
   @param age the age-class index
   @param p the focal patch index*/
  void   setScalingFactorMaxGlobal (unsigned int selected_stage_idx, unsigned int p);

  /**Resets the fitness scaling factor equal to one.
    @param age the age-class index
    @param p the focal patch index*/
  void   setScalingFactorAbsolute (unsigned int selected_stage_idx, unsigned int p)
  { _scaling_factor = 1; }

  /**Calculates the scaled fitness in a patch for a given selected_stage_idx and sex. Returns an array of scaled fitness
   *  values such that sum(scaled_fitness) =1
   * @param sex the sex container index
   * @param selected_stage_idx the stage index in the individual container
   * @param patch the pointer to the focal patch
   * @param N number of individual fitness values to process
   * @param scaled_fitness the recipient array of scaled fitness values
   */
  void getScaledPatchFitness (sex_t sex, unsigned int selected_stage_idx, Patch* patch, unsigned int N, double scaled_fitness[]);

  /**Selectively removes individuals in the population depending on their fitness.
    Calls the fitness function for each individual. Updates the fitness counters.
    The fitness scaling factor is set outside this function, in the execute() procedure.
    @param SEX the sex class
    @param AGE the age-class index
    @param patch the focal patch
    @param p the focal patch index
    @param stage the index of the stage under selection in the table of selection matrices*/
  void   doViabilitySelection   (sex_t SEX, age_idx AGE, Patch* patch, unsigned int p, unsigned int selected_stage_idx);

  ///@name Implementations
  ///@{
//  virtual bool setParameters ();
  virtual void execute ();
  virtual void loadStatServices (StatServices* loader);
  virtual void loadFileServices (FileServices* loader);
//  virtual LifeCycleEvent* clone () {return new LCE_Selection_base();}
  virtual age_t removeAgeClass   () {return NONE;}
  virtual age_t addAgeClass      () {return NONE;}
  virtual age_t requiredAgeClass () {return OFFSPRG;}
  ///@}
};

class LCE_Viability_Selection : public virtual LCE_Selection_base
{

public:
  LCE_Viability_Selection();

  virtual ~LCE_Viability_Selection(){}

  ///@name Implementations
  ///@{
  virtual bool setParameters () {return LCE_Selection_base::setBaseParameters();}
  virtual LifeCycleEvent* clone () {return new LCE_Viability_Selection();}
  ///@}
};

/**StatHandler class for the LCE_Selection class. Records the fitness stats.*/
class LCE_SelectionSH : public EventStatHandler< LCE_Selection_base, LCE_SelectionSH >
{

  DataTable< double > _phenoTable;
  unsigned int _table_set_gen, _table_set_age, _table_set_repl;
  TMatrix* _stages;

public:

  LCE_SelectionSH (LCE_Selection_base* event) :
    EventStatHandler< LCE_Selection_base, LCE_SelectionSH > (event),
    _table_set_gen(999999), _table_set_age(999999), _table_set_repl(999999), _stages(0)
    {}

  virtual ~LCE_SelectionSH() {}

  virtual bool setStatRecorders (string& token);

  void   addMeanPerPatch (age_t AGE);
  void   addVarPerPatch  (age_t AGE);
  void   setDataTable    (age_t AGE);
  void   setAdultTable   ( ) {setDataTable(ADULTS);}
  void   setOffsprgTable ( ) {setDataTable(OFFSPRG);}


  double getMeanFitness  () {return _SHLinkedEvent->_mean_fitness;}
  double getFitness  (unsigned int i) {return _SHLinkedEvent->_fitness[i];}
  double getSurvival (unsigned int i) {return _SHLinkedEvent->_survival[i];}
  double getPedProp  (unsigned int i) {return _SHLinkedEvent->_ind_cntr[i];}
  double getMeanPatchFitness (unsigned int i, unsigned int age_pos);
  double getMeanPatchFitness (unsigned int i);
  double getVarPatchFitness (unsigned int i, unsigned int age_pos);
};
class LCE_SelectionFH : public virtual EventFileHandler< LCE_Selection_base >
{
  
  public:
  
  LCE_SelectionFH(LCE_Selection_base *event) : EventFileHandler< LCE_Selection_base >(event, ".fit") { }
  
  virtual ~LCE_SelectionFH() {}
  
  virtual void FHwrite();
  
  virtual void FHread(string& filename) {}
  
  void print(ofstream& FH, sex_t SEX, age_idx AGE, unsigned int i, Patch* patch, unsigned int ntraits);
};

#endif

