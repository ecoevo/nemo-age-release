/** $Id: LCEdisperse.h,v 1.6.2.5 2017-06-09 09:04:16 fred Exp $
 *
 *  @file LCEdisperse.h
 *  Nemo2
 *
 *  Copyright (C) 2006-2015 Frederic Guillaume
 *   frederic.guillaume@ieu.uzh.ch
 *
 *   This file is part of Nemo
 *
 *   Nemo is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Nemo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *  
 *  Created on @date 07.08.2004
 *  @author fred
 */

#ifndef LCEDISPERSE_H
#define LCEDISPERSE_H
#include <vector>
#include "lifecycleevent.h"
#include "param.h"

class Patch;
/**The base class of the dispersal LCEs, all events move offspring to the post-dispersal patch containers.
 * Stores the dispersal matrices and dispersal model parameters and interface.
 */
class LCE_Disperse_base: public virtual LifeCycleEvent
{
  
  int    _disp_model;
  double _disp_propagule_prob;
  vector<unsigned int> _PropaguleTargets;
  double _fem_rate, _mal_rate;
  bool _isForward;
  bool _isByNumber;
  
  /**The sex-specific dispersal matrices, [0] for males, [1] for females, might be used as connectivity matrix as well*/
  TMatrix* _DispMatrix[2]; 

  /**Parameter name prefix (dispersal, seed_disp, breed_disp, etc.)*/
  string _prefix;
  
  friend class LCE_Disperse_ConstDisp;
  friend class LCE_Disperse_EvolDisp;
  
protected:
  
  unsigned int _npatch;
  unsigned int _nstages;

  vector<Patch*>            _postDispPop;
  vector< vector<double> >  _reducedDispMat[2];
  vector< vector<double> >  _reducedDispMatProba[2];
  deque< unsigned int >     _stages;

  
public:
  
  LCE_Disperse_base();
  
  /**Deallocates the disp matrix.*/
  virtual ~LCE_Disperse_base(); 
  
  bool setBaseParameters            (string prefix);
  void setParamPrefix               (string prefix) {_prefix = prefix;}
  void addParameters                (string prefix, ParamUpdaterBase* updater);
  void setPostDispPop               ();
  void resetPostDispPop             ();
  void swapPostDisp                 ();
  void setIndentityDispMatrix       (TMatrix* mat);
  
  ///@name Dispersal Matrix
  ///@{
  void set_isForward                  (bool val) {_isForward = val;}
  bool checkForwardDispersalMatrix    (TMatrix* mat);
  bool checkBackwardDispersalMatrix   (TMatrix* mat);
  void allocateDispMatrix             (sex_t sex, unsigned int dim);
  bool setDispMatrix                  ();
  bool updateDispMatrix               ();
  bool setReducedMatricesBySex        (sex_t SEX, Param& connectivity, Param& rate);
  bool setReducedDispMatrix           ();
  bool setIsland_MigrantPool_Matrix   ();
  bool setIsland_PropagulePool_Matrix ();
  bool setSteppingStone1DMatrix       ();
  bool setLatticeMatrix               ();
  bool setBasicLatticeMatrix          (int rows, int cols, double phi_mal, double phi_fem, double disp_mal, double disp_fem);
  bool setLatticeTorrusMatrix         (int rows, int cols, double disp_mal, double disp_fem, TMatrix* grid);
  bool setLatticeAbsorbingMatrix      ();
  bool setLatticeReflectingMatrix     (int rows, int cols, TMatrix* grid);
  ///@}  
  unsigned int getMigrationPatchForward  (sex_t SEX, unsigned int LocalPatch);
  unsigned int getMigrationPatchBackward (sex_t SEX, unsigned int LocalPatch);
  void setPropaguleTargets ( );
  void reset_counters ( );

  ///@name Accessors
  ///@{
  bool isForward                  ( ) {return _isForward;}
  bool isByNumber                 ( ) {return _isByNumber;}
  unsigned int getDispersalModel  ( ) {return _disp_model;}
  double       getPropaguleProb   ( ) {return _disp_propagule_prob;}
  unsigned int getPropaguleTarget (unsigned int home) {return _PropaguleTargets[home];}
  ///@}
  ///@name Implementations
  ///@{
  virtual void loadFileServices  ( FileServices* loader ) {}
  virtual void loadStatServices  ( StatServices* loader ) {}
  virtual age_t removeAgeClass   () {return NONE;}
  virtual age_t addAgeClass      () {return NONE;}
  virtual age_t requiredAgeClass () {return OFFSPRG;}
  ///@}
};

/**Dispersal event with constant dispersal rates.
 * Sets and uses the dispersal matrices according to the dispersal model chosen. 
 * Dispersal models implemented so far are:
 * <ul> 
 * <li>1: Island Model with migrant pool
 * <li>2: Island Model with propagule pool
 * <li>3: Stepping stone model in 1 dimension (ring population)
 * <li>4: Lattice model (stepping stone in 2D)
 * </ul>
 **/
class LCE_Disperse_ConstDisp: public virtual LCE_Disperse_base 
{
  void (LCE_Disperse_ConstDisp::* doMigration) (void);
  void (LCE_Disperse_ConstDisp::* doPatchMigration) (sex_t SEX, unsigned int local_patch, age_idx age);
  
public:
	
  LCE_Disperse_ConstDisp ();
  virtual ~LCE_Disperse_ConstDisp(){}

  bool setParameters (string prefix);
  
  void Migrate ( );
  void Migrate_propagule ( );
  void MigratePatch (sex_t SEX, unsigned int LocalPatch, age_idx age);
  void MigratePatch_AbsorbingBorder (sex_t SEX, unsigned int LocalPatch, age_idx age);
  void MigratePatchByNumber         (sex_t SEX, unsigned int LocalPatch, age_idx age);
  ///@name Implementations
  ///@{
  virtual bool setParameters () {return setParameters("dispersal");}
  virtual void execute ();
  virtual LifeCycleEvent* clone () {return new LCE_Disperse_ConstDisp();}
  ///@}
};



class LCE_SeedDisp : public LCE_Disperse_ConstDisp {
  

public:
  LCE_SeedDisp ();
  virtual ~LCE_SeedDisp(){}
  virtual bool setParameters () {return LCE_Disperse_ConstDisp::setParameters("seed_disp");}
  virtual LifeCycleEvent* clone () {return new LCE_SeedDisp();}
};


#endif //LCEDISPERSE_H
