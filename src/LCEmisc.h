/**  $Id: LCEmisc.h,v 1.7.2.8 2017-06-09 11:07:51 fred Exp $
 *
 *  @file LCEmisc.h
 *  Nemo-age
 *
 *   Copyright (C) 2006-2020
 *   guillaume.fred@gmail.com
 *
 *   This file is part of Nemo
 *
 *   Nemo is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Nemo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  created on @date 06.16.2005
 *
 *  @author fred
 */

#ifndef LCEMISC_H
#define LCEMISC_H

#include "lifecycleevent.h"



// LCE_Aging
//
/**Removes all adults from the patches and randomly moves the offspring to the adults age class.
 * Patch regulation is performed at the same time, leaving the patch at carrying capacity (if enough
 * offspring individuals were present).
 * This is the only LCE that actually removes the adults from the patches.
 * Also checks whether the patch is filled and sets the extinction flag accordingly.*/
class LCE_Aging: public LifeCycleEvent
{
public:

  LCE_Aging( ) : LifeCycleEvent("aging","") {}

  virtual ~LCE_Aging( ) { }

  //implementations:
  virtual bool setParameters () {return true;}
  virtual void execute ();
  virtual LCE_Aging* clone ( ) {return new LCE_Aging();}
  virtual void loadFileServices ( FileServices* loader ) {}
  virtual void loadStatServices ( StatServices* loader ) {}
  virtual age_t removeAgeClass ( ) {return OFFSPRG;}
  virtual age_t addAgeClass ( ) {return ADULTS;}
  virtual age_t requiredAgeClass () {return OFFSPRG;}
};

// LCE_Aging_multi
//
/**Performs aging with overlapping generations.
 * Uses the Leslie matrix to decide on individual survival during aging.
 * Some parameters are specific to an older, now unused model.
 * Will need clean-up!!*/
class LCE_Aging_Multi: public LifeCycleEvent
{

  TMatrix* _survival;

public:

  LCE_Aging_Multi( ) : LifeCycleEvent("aging_multi",""),_survival(0) {}
  //  {add_parameter("regulation_before_aging",BOOL,false,false,0,0,0);}

  virtual ~LCE_Aging_Multi( ) {}

  void AgeOrDie (Patch* patch, sex_t SEX, age_idx age, double survival_rate);
  void stageTransition (Patch* patch, sex_t SEX, unsigned int stage, age_idx age, double survival_rate,
      double transition_rate, double sigma, double gamma);
  void offsprgTransition (Patch* patch, sex_t SEX, double survival_rate, double transition_rate,
      double sigma, double gamma);

  //implementations:
  virtual bool setParameters ();
  virtual void execute ();

  virtual LCE_Aging_Multi* clone ( ) {return new LCE_Aging_Multi();}

  virtual void loadFileServices ( FileServices* loader ) {}
  virtual void loadStatServices ( StatServices* loader ) {}
  virtual age_t removeAgeClass ( ) {return NONE;}
  virtual age_t addAgeClass ( ) {return ALL;}
  virtual age_t requiredAgeClass () {return ALL;}
};

#endif //LCEMISC_H


