/** $Id: metapop.cc,v 1.11.2.7 2017-06-09 11:07:51 fred Exp $
 *
 *  @file metapop.cc
 *  Nemo2
 *
 *  Copyright (C) 2006-2020 Frederic Guillaume
 *   frederic.guillaume@ieu.uzh.ch
 *
 *   This file is part of Nemo
 *
 *   Nemo is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Nemo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Created on @date 07.08.2004
 *
 *  @author fred
 */

#include <time.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cmath>
#include <dirent.h>
#include <map>
#include "Uniform.h"
#include "output.h"
#include "metapop.h"
#include "individual.h"
#include "lifecycleevent.h"
#include "simulation.h"
#include "simenv.h"
#include "tstring.h"

using namespace std;

//extern MPIenv *_myenv;

// ----------------------------------------------------------------------------------------
// constructor
// ----------------------------------------------------------------------------------------
Metapop::Metapop() : _mpimgr(0), _statHandler(), _writer(0),_loader(), _source(0),
    _source_preserve(0), _source_load(0),_source_replicates(0), _source_replicate_digits(0),
    _source_start_at_replicate(1), _source_generation(0), _source_load_periodicity(0),
    _patchNbr(0), _patchK(0), _patchKfem(0), _patchKmal(0), _hasInitPatchSize(0), _transition_by_age(0),
    _generations(0), _replicates(0),
    _currentGeneration(0), _currentReplicate(0), _currentAge(NONE), _requiredAge(0)
{
  set_paramset("population", true, this);

  ParamUpdater<Metapop>* upd = new ParamUpdater<Metapop>( &Metapop::updatePopulationParameters );
  add_parameter("patch_capacity",INT,false,false,0,0, upd);
  add_parameter("patch_nbfem",INT,false,false,0,0, upd);
  add_parameter("patch_nbmal",INT,false,false,0,0, upd);
  add_parameter("patch_number",INT,false,false,0,0, upd);

  add_parameter("patch_init_stage_size", MAT, false, false, 0,0,0);

  upd = new ParamUpdater<Metapop>( &Metapop::setAgeStructureParameters );
  //age structure will not change during a run, too complicated
  add_parameter("pop_age_structure",MAT,false,false,0,0);
  add_parameter("pop_transition_matrix",MAT,false,false,0,0, upd);
  add_parameter("pop_transition_by_age", STR, false, false, 0, 0); 

  add_parameter("pop_output", BOOL, false, false, 0, 0);
  add_parameter("pop_output_dir", STR, false, false, 0, 0);
  add_parameter("pop_output_logtime", INT, false, false, 0, 0);
  add_parameter("pop_output_sample_size", INT, false, false, 0, 0);

  // duplicates parameter in LCE_Breed, to set the transition matrix
  add_parameter("mean_fecundity", DBL, false, false, 0, 0);

  upd = new ParamUpdater<Metapop>( &Metapop::setSourceParameters );
  add_parameter("source_pop",STR,false,false,0,0, upd);
  add_parameter("source_file_type",STR,false,false,0,0, upd);
  add_parameter("source_preserve",BOOL,false,false,0,0, upd);
  add_parameter("source_replicates",INT,false,false,0,0, upd);
  add_parameter("source_replicate_digit",INT,false,false,0,0, upd);
  add_parameter("source_start_at_replicate",INT,false,false,0,0, upd);
  add_parameter("source_generation",INT,false,false,0,0, upd);
  add_parameter("source_fill_stage",STR,false,false,0,0, upd);
}
// ----------------------------------------------------------------------------------------
// destructor
// ----------------------------------------------------------------------------------------
Metapop::~Metapop()
{
#ifdef _DEBUG_
  message("Metapop::~Metapop\n");
#endif
  clear();
}
// ----------------------------------------------------------------------------------------
// init
// ----------------------------------------------------------------------------------------
bool Metapop::init( )
{
  if( !(_paramSet->isSet()) ) {
    return error("parameters in \"population\" are not properly set!\n");
  }

  if(!setParameters()) return false;

  buildPatchArray();

  //empty and clean the RecyclingPOOL, safer...
  purgeRecyclingPOOL();

  return true;
}
// ----------------------------------------------------------------------------------------
// setParameters
// ----------------------------------------------------------------------------------------
bool Metapop::setParameters()
{
  if(!setAgeStructureParameters()) return false;
  if(!setPopulationParameters()) return false;
  if(!setSourceParameters()) return false;
  return true;
}
// ----------------------------------------------------------------------------------------
// setPopulationParameters
// ----------------------------------------------------------------------------------------
bool Metapop::setSourceParameters()
{
  if(_paramSet->isSet("source_pop")) {

    _source_load = true;
    _source_name = _paramSet->getArg("source_pop");
    _source_preserve = _paramSet->isSet("source_preserve");
    _source_replicates = _paramSet->isSet("source_replicates") ? 
        (unsigned int)_paramSet->getValue("source_replicates") : 0;

    _source_replicate_digits = _paramSet->isSet("source_replicate_digit") ? 
        (unsigned int)_paramSet->getValue("source_replicate_digit") : 1;

    _source_start_at_replicate = _paramSet->isSet("source_start_at_replicate") ? 
        (unsigned int)_paramSet->getValue("source_start_at_replicate") : 1;

    _source_generation = _paramSet->isSet("source_generation") ? 
        (unsigned int)_paramSet->getValue("source_generation") : 0;

    if(_paramSet->isSet("source_file_type"))
      _source_filetype = _paramSet->getArg("source_file_type");
    else
      _source_filetype = ".bin";

    _source_required_age = _paramSet->getArg("source_fill_stage");

    if(_source_required_age.length() == 0) {

      _requiredAge = NONE;  // no target stage set

    } else if(_source_required_age == "OFFSPRG" || _source_required_age == "offspring"){

      _requiredAge = OFFSPRG;

    } else if(_source_required_age=="ADULTS" || _source_required_age=="adults") {

      _requiredAge = ADULTS;

    } else if(_source_required_age=="ALL" || _source_required_age=="all") {

      _requiredAge = ALL;

    } else if(tstring::isanumber(_source_required_age)) { //an integer is passed?

      _requiredAge = age_t(tstring::str2uint(_paramSet->getArg("source_fill_stage")));

      cout<<"--- source_fill_stage has value:"<<_requiredAge<<" ("<<_paramSet->getValue("source_fill_stage")<<")"<<endl;

    } else {
      return error("parameter \"source_fill_stage\" should receive an integer value or options \"offspring\", \"adults\", or \"all\".\n");
    }

  } else {
    _source = NULL;
    _source_preserve = false;
    _source_load = false;
    _source_replicates = 0;
    _source_generation = 0;
    _source_name = "";
    _source_filetype = ".bin";
  }
  return true;
}

// ----------------------------------------------------------------------------------------
// setAgeStructureParameters
// ----------------------------------------------------------------------------------------
bool Metapop::setAgeStructureParameters ()
{

  //age structure, will tell the number of individual containers to add to each patch:
  if(_paramSet->isSet("pop_age_structure")) {

    _paramSet->getMatrix("pop_age_structure", &_age_structure);

    if(_age_structure.getNbRows() != 1) {

      return error("\"pop_age_structure\" must be a vector!\n");;
    }

    if(_age_structure.get(0,0) != 0){

      return error("the first entry of \"pop_age_structure\" must be 0!\n");

    }

  } else {
    //by default we set two age classes: offspring and adults
    double val[2] = {0,1};
    _age_structure.reset(1,2,&val[0]);
  }

  // determines the behavior when stage transitions are enforced by the age of an individual:
  if(_paramSet->isSet("pop_transition_by_age")) { 

    _transition_by_age = true;

  } else {

    _transition_by_age = false;

  }

  //-----------------------------------------------------------------
  //population growth and regulation with age structure:

  //transition matrix:
  if(_paramSet->isSet("pop_transition_matrix")) {

    if(_age_structure.length() == 0) {

      return error("cannot set \"pop_transition_matrix\" when \"pop_age_structure\" isn't specified!\n");

    }

    _paramSet->getMatrix("pop_transition_matrix", &_LeslieMatrix);


    if(_age_structure.getNbCols() != _LeslieMatrix.getNbCols() ) {

      return error("\"pop_transition_matrix\" must have same number of columns as the age structure vector!\n");

    }

    if(_LeslieMatrix.getNbRows() != _LeslieMatrix.getNbCols() ) {

      return error("\"pop_transition_matrix\" must be a square matrix!\n");

    }

    // check if max. number of stages is exceeded (correct?)
    if(_LeslieMatrix.getNbCols() > 32){

      return error("in \"pop_transition_matrix\" only 32 stages are allowed at maximum!\n");

    }

    if(_LeslieMatrix.getNbCols() < 2){

      return error("the smallest \"pop_transition_matrix\" must be a 2x2 matrix!\n");
    }

    // check if transition matrix was set correctly
    double colSum=0;
    //first column, stage 0
    for(unsigned int r = 0; r < _LeslieMatrix.getNbRows(); r++){

      colSum += _LeslieMatrix.get(r,0) ;

      if(_LeslieMatrix.get(r,0)>1 || _LeslieMatrix.get(r,0)<0){

        return error("in \"pop_transition_matrix\" all transition rates (except fecundities) must lay between 0 and 1!\n");

      }

      if(r > 1 && _LeslieMatrix.get(r,0)>0){

        return error("in \"pop_transition_matrix\" individuals are not allowed to skip stages (yet)!\n");

      }

      if(r == 1 && _LeslieMatrix.get(r,0) == 0)
        return error("transition rates to next stages in \"pop_transition_matrix\" must be different from zero.\n");
    }//end for stage 0

    if(colSum>1 || colSum<0){

      return error("in \"pop_transition_matrix\" the sum of transition rates in each column (except fecundities) must lay between 0 and 1 !\n");

    }

    // check submatrix of adult stages
    for(unsigned int c = 1; c < _LeslieMatrix.getNbCols(); c++){

      colSum=0;

      for(unsigned int r = 1; r < _LeslieMatrix.getNbRows(); r++) {  //skip row 0 (fecundities)

        colSum += _LeslieMatrix.get(r,c) ;

        if(_LeslieMatrix.get(r,c) > 1 || _LeslieMatrix.get(r,c) < 0){

          return error("in \"pop_transition_matrix\" all transition rates (except fecundities) must lay between 0 and 1!\n");

        }

        if(r > (c+1) && _LeslieMatrix.get(r,c) > 0){

          return error("in \"pop_transition_matrix\" individuals are not allowed to skip stages!\n");

        }

        if(r == (c+1) && _LeslieMatrix.get(r,c) == 0){

          return error("transition rates to next stages in \"pop_transition_matrix\" must be different from zero.\n");

        }

        if(r < c && _LeslieMatrix.get(r,c) > 0){

          return error("in \"pop_transition_matrix\" individuals are not allowed to regress to previous stages!\n");

        }

      } //end for r, end column check

      if(colSum>1 || colSum<0){

        return error("in \"pop_transition_matrix\" the sum of transition rates in each column (except fecundities) must lay between 0 and 1 !\n");

      }

    }//end for c

  } else { //no transition matrix set in input

    double val[4] = {0,2.7,1,0};

    if( get_parameter("mean_fecundity")->isSet() ) {

      val[1] = get_parameter_value("mean_fecundity");

    }

    //by default, offspring have fecundity 0 and survival 1, adults have fecundity 2.7 and survival 0

    _LeslieMatrix.reset(2,2,&val[0]);
  }

  return true;
}
// ----------------------------------------------------------------------------------------
// setPopulationParameters
// ----------------------------------------------------------------------------------------
bool Metapop::setPopulationParameters ()
{

  //population structure:
  if(_paramSet->isSet("patch_number")) 
    _patchNbr = (unsigned int)_paramSet->getValue("patch_number");
  else _patchNbr = 0;

  if(_paramSet->isSet("patch_capacity")) {

    if( _paramSet->isMatrix("patch_capacity") ) {

      setPatchCapacities("patch_capacity");

    } else if( !(_paramSet->isSet("patch_number")) ) {

      return error("param \"patch_number\" is missing!\n");;

    } else {
      _patchK = (unsigned int)_paramSet->getValue("patch_capacity");
      _patchKfem = _patchKmal = _patchK/2;
      setPatchCapacities();
    }

  } else if(_paramSet->isSet("patch_nbfem") && _paramSet->isSet("patch_nbmal")) {

    if( !(_paramSet->isMatrix("patch_nbfem")) && !(_paramSet->isMatrix("patch_nbmal")) ) {

      if( !(_paramSet->isSet("patch_number")) ) {

        return error("param \"patch_number\" is missing!\n");;

      } else {
        _patchKfem = (unsigned int)_paramSet->getValue("patch_nbfem");
        _patchKmal = (unsigned int)_paramSet->getValue("patch_nbmal");
        _patchK = _patchKfem + _patchKmal;
        setPatchCapacities();
      }

    } else {

      if( !(_paramSet->isMatrix("patch_nbfem")) && _paramSet->isMatrix("patch_nbmal") ) {

        _patchKfem = (unsigned int)_paramSet->getValue("patch_nbfem");

        setPatchCapacities(MAL,"patch_nbmal");

      } else if( _paramSet->isMatrix("patch_nbfem") && !(_paramSet->isMatrix("patch_nbmal")) ) {

        _patchKmal = (unsigned int)_paramSet->getValue("patch_nbmal");

        setPatchCapacities(FEM,"patch_nbfem");

      } else

        setPatchCapacities("patch_nbfem","patch_nbmal");
    }

  } else {

    return error("population parameters are not properly set!\n");;
  }

  // number of individuals per patch and stage can be set specifically
  if(get_parameter("patch_init_stage_size")->isSet()){

    if(!setInitPatchSize()) return false;

  }  else
    _hasInitPatchSize = false;

  return true;
}
// ----------------------------------------------------------------------------------------
// setPatchCapacities
// ----------------------------------------------------------------------------------------
bool Metapop::setInitPatchSize()
{
  TMatrix tmp;

  _paramSet->getMatrix("patch_init_stage_size", &tmp);

  if(tmp.ncols() != _age_structure.ncols())
    return error("patch_init_stage_size must has as many elements per row as the number of stages in pop_age_structure\n");

  unsigned int nstage = tmp.ncols();
  unsigned int nrow = tmp.nrows();

  _patchInitSize.reset(_patchNbr, nstage);

  for(unsigned int i = 0; i < _patchNbr; ++i) {
    for (unsigned int j = 0; j < nstage; ++j) {
      _patchInitSize.set(i, j, tmp.get(i % nrow, j) );
      _patchInitSize.set(i, j, tmp.get(i % nrow, j) );
    }
  }

  _hasInitPatchSize = true;

  return true;
}
// ----------------------------------------------------------------------------------------
// updatePopulationParameters
// ----------------------------------------------------------------------------------------
bool Metapop::updatePopulationParameters ()
{
  if(!setPopulationParameters()) return false;

  updatePatchArray();

  //if we added patches, they will be empty
  //fusion of existing pop is not possible here, 
  //the individuals are flushed when deleting the patches

  if(!setAgeStructureParameters()) return false;

  return true;
}
// ----------------------------------------------------------------------------------------
// loadFileServices
// ----------------------------------------------------------------------------------------
void Metapop::loadFileServices ( FileServices* loader )
{

  if(get_parameter("pop_output")->isSet()){

    if(!get_parameter("pop_output_logtime")->isSet())
      fatal("parameter \"pop_output_logtime\" is missing when pop_output is set\n");


    if(_writer == 0) _writer = new MPFileHandler();

    int size = 0;

    if(get_parameter("pop_output_sample_size")->isSet())
      size = get_parameter_value("pop_output_sample_size");

    _writer->setOption(size);

    Param* param = get_parameter("pop_output_logtime");

    if(param->isMatrix()) {

      TMatrix temp;
      param->getMatrix(&temp);
      _writer->set_multi(true, true, 1, &temp, get_parameter("pop_output_dir")->getArg());

    } else   //  rpl_per, gen_per, rpl_occ, gen_occ, rank (0), path, self-ref
      _writer->set(true, true, 1, (param->isSet() ? (int)param->getValue() : 0),
          0, get_parameter("pop_output_dir")->getArg());

    loader->attach(_writer);

  } else if(_writer) {
    delete _writer;
    _writer = NULL;
  }
}
// ----------------------------------------------------------------------------------------
// buildPatchArray
// ----------------------------------------------------------------------------------------
void Metapop::buildPatchArray()
{
  resizePatchArray();

  //set the population capacities:
  for(unsigned int i = 0; i < _patchNbr; ++i) {
    _vPatch[i]->init(_age_structure.length(),
        (unsigned int)_patchSizes.get(FEM,i),
        (unsigned int)_patchSizes.get(MAL,i), i);
  }
}
// ----------------------------------------------------------------------------------------
// resizePatchArray
// ----------------------------------------------------------------------------------------
void Metapop::resizePatchArray()
{
  //reset the right number of patches for the new simulation
  if(_vPatch.size() > _patchNbr) {
    while(_vPatch.size() > _patchNbr) {
      delete _vPatch[0];
      _vPatch.pop_front();
    }
  } else while(_vPatch.size() < _patchNbr) _vPatch.push_back(new Patch());
}
// ----------------------------------------------------------------------------------------
// updatePatchArray
// ----------------------------------------------------------------------------------------
void Metapop::updatePatchArray()
{
  //remove or add patches as needed:
  resizePatchArray();
  //reset the patch capacities and patch ID:
  updatePatchState();
}
// ----------------------------------------------------------------------------------------
// updatePatchState
// ----------------------------------------------------------------------------------------
void Metapop::updatePatchState()
{
  //reset the patch capacities and patch ID:
  for(unsigned int i = 0; i < _patchNbr; ++i) {
    _vPatch[i]->setID(i);
    _vPatch[i]->set_K((unsigned int)_patchSizes.get(FEM,i) + (unsigned int)_patchSizes.get(MAL,i));
    _vPatch[i]->set_KFem((unsigned int)_patchSizes.get(FEM,i));
    _vPatch[i]->set_KMal((unsigned int)_patchSizes.get(MAL,i));
  }
}
// ----------------------------------------------------------------------------------------
// setPatchCapacities
// ----------------------------------------------------------------------------------------
void Metapop::setPatchCapacities()
{
  _patchSizes.reset(2, _patchNbr);

  for(unsigned int i = 0; i < _patchNbr; ++i) {
    _patchSizes.set(FEM, i, _patchKfem);
    _patchSizes.set(MAL, i, _patchKmal);
  }
}
// ----------------------------------------------------------------------------------------
// setPatchCapacities
// ----------------------------------------------------------------------------------------
void Metapop::setPatchCapacities(string param)
{
  double* size_array;
  unsigned int Knum;
  TMatrix popK;

  _paramSet->getMatrix(param, &popK);

  Knum = popK.length();

  if(_patchNbr == 0 || _patchNbr < Knum)  _patchNbr = Knum;

  _patchSizes.reset(2, _patchNbr);

  size_array = popK.get();

  for(unsigned int i = 0; i < _patchNbr; ++i) {
    _patchSizes.set(FEM, i, (unsigned int)size_array[i % Knum]/2);
    _patchSizes.set(MAL, i, (unsigned int)size_array[i % Knum]/2);
  }
}
// ----------------------------------------------------------------------------------------
// setPatchCapacities
// ----------------------------------------------------------------------------------------
void Metapop::setPatchCapacities(sex_t SEX, string param)
{
  double* size_array;
  unsigned int size_, Knum;
  TMatrix popK;

  _paramSet->getMatrix(param, &popK);

  Knum = popK.length();

  if(_patchNbr == 0 || _patchNbr < Knum)  _patchNbr = Knum;

  _patchSizes.reset(2, _patchNbr);

  size_array = popK.get();

  if(SEX == FEM)
    size_ = _patchKmal;
  else
    size_ = _patchKfem;

  for(unsigned int i = 0; i < _patchNbr; ++i) {
    _patchSizes.set(SEX, i, (unsigned int)size_array[i % Knum]);
    _patchSizes.set(!SEX, i, size_);
  }
}
// ----------------------------------------------------------------------------------------
// setPatchCapacities
// ----------------------------------------------------------------------------------------
void Metapop::setPatchCapacities(string paramfem, string parammal)
{
  double *size_fem, *size_mal;
  unsigned int KFnum, KMnum;
  TMatrix popKfem, popKmal;

  _paramSet->getMatrix(paramfem,&popKfem);
  _paramSet->getMatrix(parammal,&popKmal);

  KFnum = popKfem.length();
  KMnum = popKmal.length();

  if(_patchNbr == 0 && KFnum != KMnum){
    warning("not same number of elements in females and males capacity matrices!\n");
    warning("setting the number of populations from size of longest capacity array (= %i).\n",
        max(KFnum, KMnum));
  }
  if(_patchNbr < max(KFnum, KMnum)) _patchNbr = max(KFnum, KMnum);

  _patchSizes.reset(2, _patchNbr);

  size_fem = popKfem.get();
  size_mal = popKmal.get();

  for(unsigned int i = 0; i < _patchNbr; ++i) {
    _patchSizes.set(FEM, i, (unsigned int)size_fem[i % KFnum]);
    _patchSizes.set(MAL, i, (unsigned int)size_mal[i % KMnum]);
  }   
}
// ----------------------------------------------------------------------------------------
// store_data
// ----------------------------------------------------------------------------------------
void Metapop::store_data ( BinaryStorageBuffer* saver )
{
  unsigned char separator[2] = {'@','P'};
  unsigned int stage_num = getNumAgeClasses();
  unsigned int size_collect;
  age_idx age;

  //store the data, begin with pop separator and number of patches:
  saver->store(&separator, 2 * sizeof(unsigned char));

  saver->store(&_patchNbr, sizeof(unsigned int));

  //store the Patch sizes

  for(unsigned int i = 0; i < _patchNbr; ++i) {

    for (int j = 0; j < stage_num; ++j) {

      age = static_cast<age_idx>(j);

      size_collect = size(FEM, age, i);
      saver->store(&size_collect, sizeof(unsigned int));

      size_collect = size(MAL, age, i);
      saver->store(&size_collect, sizeof(unsigned int));
    }
  }

  int byte_count = saver->getTotByteRecorded();

  //record all individual informations: IDs, matings, etc.
  for(unsigned int i = 0; i < _patchNbr; ++i) {

    for (int j = 0; j < stage_num; ++j) {

      age = static_cast<age_idx>(j);

      for(unsigned int k = 0; k < _vPatch[i]->size(FEM, age); ++k)
        _vPatch[i]->get(FEM, age, k)->store_data(saver);

      for(unsigned int k = 0; k < _vPatch[i]->size(MAL, age); ++k)
        _vPatch[i]->get(MAL, age, k)->store_data(saver);
    }
  }

#ifdef _DEBUG_
  message("Metapop::store_data :stored %ikB of individual data (%i individuals)\n",
          (saver->getTotByteRecorded()-byte_count)/1024, size());
#endif

  //records the trait sequences:
  map<trait_t, TraitPrototype *> traits = this->getTraitPrototypes();
  map<trait_t, TraitPrototype *>::iterator tt = traits.begin();
  //  trait_t type;
  separator[1] = 'T'; //trait separator = '@T'

  byte_count = saver->getTotByteRecorded();

  while(tt != traits.end()) {
    saver->store(&separator, 2 * sizeof(unsigned char));
    //store the trait type:
    saver->store((void*)tt->first.c_str(), TRAIT_T_MAX); 

    //then ask the prototype to store its data:
    tt->second->store_data(saver);
    //store the traits data:
    store_trait(tt->second->get_index(), saver);

    tt++;
  }
#ifdef _DEBUG_
  message("Metapop::store_data :stored %ikB of traits data\n",
      (saver->getTotByteRecorded()-byte_count)/1024);
#endif

}
// ----------------------------------------------------------------------------------------
// store_trait
// ----------------------------------------------------------------------------------------
void Metapop::store_trait (int trait_idx, BinaryStorageBuffer* saver)
{  
  age_idx age;

  for(unsigned int i = 0; i < _patchNbr; ++i) {

    for (int j = 0; j < getNumAgeClasses(); ++j) {

      age = static_cast<age_idx>(j);

      for(unsigned int k = 0; k < _vPatch[i]->size(FEM, age); ++k) {
        _vPatch[i]->get(FEM, age, k)->getTrait(trait_idx)->store_data(saver);
      }

      for(unsigned int k = 0; k < _vPatch[i]->size(MAL, age); ++k) {
        _vPatch[i]->get(MAL, age, k)->getTrait(trait_idx)->store_data(saver);
      }
    }
  }
}
// ----------------------------------------------------------------------------------------
// retrieve_data
// ----------------------------------------------------------------------------------------
bool Metapop::retrieve_data ( BinaryStorageBuffer* loader )
{
#ifdef _DEBUG_
  message("Metapop::retrieve_data, %iB of data read so far\n",loader->getBytesOut()); 
#endif
  unsigned int dummy_int;
  unsigned char separator[2];
  unsigned int stage_num = getNumAgeClasses();
  unsigned int *size;
  TMatrix Sizes(_patchNbr, stage_num*2);
  age_idx age;

  // read separator
  loader->read(&separator, 2 * sizeof(unsigned char));

  if(separator[0] != '@' || separator[1] != 'P')
    return error("Binary file appears corrupted:\n\
 >>>> Metapop::retrieve_data::wrong population seprarator\n");

  // read patch number
  loader->read(&dummy_int, sizeof(unsigned int));

  if(dummy_int != _patchNbr)
    return error("Population in binary file differs from simulation settings:\n\
 >>>> Metapop::retrieve_data:number of Patch differ from parameter value\n");

  //get the Patch sizes
  size = new unsigned int [stage_num*2];

  for(unsigned int i = 0; i < _patchNbr; ++i) {

    loader->read(size, stage_num*2 * sizeof(unsigned int));

    for (int j = 0; j < stage_num*2; ++j) {

      Sizes.set(i, j, size[j]);
    }
  }

  delete [] size;

  // read in the individual data
  Individual* ind;
  unsigned int bytes_cnt = loader->getBytesOut();

  unsigned int pos;

  for(unsigned int i = 0; i < _patchNbr; ++i) {

    pos = 0;

    for (int j = 0; j < stage_num; ++j) {

      age = static_cast<age_idx>(j);

      //start with the females in this stage
      for (int k = 0; k < Sizes.get(i, pos); ++k) {

        ind = this->getNewIndividual();
        ind->retrieve_data(loader);
        _vPatch[i]->add(FEM, age, ind);
      }

      pos++; //move to the males

      for (int k = 0; k < Sizes.get(i, pos); ++k) {

        ind = this->getNewIndividual();
        ind->retrieve_data(loader);
        _vPatch[i]->add(MAL, age, ind);
      }
      pos++;
    }
    assert(pos == stage_num*2);

  }

#ifdef _DEBUG_
  message("Metapop::retrieve_data::retrieved %ikB of ind data (%i individuals)\n",
      (loader->getBytesOut()-bytes_cnt)/1024, this->size());
#endif

  //retrieve traits sequences
  map<trait_t, TraitPrototype *>::iterator tt = _protoTraits.begin();

  char trait_name[6] = {'\0','\0','\0','\0','\0','\0'};

  unsigned int trait_cntr = 0;

  loader->read(&separator, 2 * sizeof(unsigned char));

  if(separator[0] != '@' || separator[1] != 'T') {
    error("Binary file appears corrupted:\n >>>> Metapop::retrieve_data::wrong trait seprarator\n");
    return false;
  }

  bytes_cnt = loader->getBytesOut();

  do {
    //get the trait type:
    loader->read(&trait_name[0], TRAIT_T_MAX);

    string dummy_trait(trait_name);

    //get the prototype:
    tt = _protoTraits.find(dummy_trait);

    if( tt == _protoTraits.end() )
      return error("Trait(s) in binary file differ from simulation settings:\n\
 >>>> Metapop::retrieve_data::trait in file not present in prototype\n");


    //then ask the prototype to retrieve its data:
    tt->second->retrieve_data(loader);

#ifdef _DEBUG_
    message("%iB of trait data read so far (trait %s)\n",loader->getBytesOut()-bytes_cnt,tt->first.c_str()); 
#endif

    //get the traits data:
    read_trait(tt->second->get_index(), &Sizes, loader);

    trait_cntr++;

    //get the next separator, should be present if right number of bytes have been read
    loader->read(&separator, 2 * sizeof(unsigned char));


  } while (separator[0] == '@' && separator[1] == 'T') ;

#ifdef _DEBUG_
  message("Metapop::retrieve_data::retrieved %li of trait data (out of %li)\n",
      (loader->getBytesOut()-bytes_cnt), loader->getBytesOut());
#endif

  if(trait_cntr != _protoTraits.size())
    return error("Trait(s) in binary file differ from simulation settings:\n\
 >>>> Metapop::retrieve_data::some traits are missing from binary file\n");


  if(separator[0] != '@') 
    return error("Binary file appears corrupted:\n\
 >>>> Metapop::retrieve_data::separator not found at end of pop record!\n");

#ifdef _DEBUG_
  message("Metapop::retrieve_data::retrieved %li of data (%li in buffer)\n",
      loader->getBytesOut(), loader->getBuffLength());
#endif

  return true;
}
// ----------------------------------------------------------------------------------------
// read_trait
// ----------------------------------------------------------------------------------------
void Metapop::read_trait (int trait_idx, TMatrix *Sizes, BinaryStorageBuffer* loader)
{ 
  unsigned int pos, stage_num = getNumAgeClasses();
  age_idx age;

  for(unsigned int i = 0; i < _patchNbr; ++i) {

    pos = 0;

    for (int j = 0; j < stage_num; ++j) {

      age = static_cast<age_idx>(j);

      //start with the females in this stage
      for (int k = 0; k < Sizes->get(i, pos); ++k) {

        _vPatch[i]->get(FEM, age, k)->getTrait(trait_idx)->retrieve_data(loader);

      }

      pos++; //move to the males

      for (int k = 0; k < Sizes->get(i, pos); ++k) {

        _vPatch[i]->get(MAL, age, k)->getTrait(trait_idx)->retrieve_data(loader);
      }
      pos++;
    }
  }

}
// ----------------------------------------------------------------------------------------
// setPopulation
// ----------------------------------------------------------------------------------------
void Metapop::setPopulation(unsigned int currentReplicate, unsigned int replicates)
{
#ifdef _DEBUG_
  message("\n+++Metapop::setPopulation: ");
  fflush(stdout);
#endif

  _currentReplicate = currentReplicate;

  //reset the population parameters, they may have changed during the life cycle
  setPopulationParameters();
  //reset the patch array, flush the patches and reset the patch capacities.
  buildPatchArray();

  _currentAge = NONE;

  //find the age class required to start the life cycle with:
  if(_requiredAge == NONE) 
    _requiredAge = SIMenv::MainSim->getFirstRequiredAgeInLifeCycle();

#ifdef _DEBUG_
  message("required age is: %i ", _requiredAge);
  fflush(stdout);
#endif

  //load first generation from a source population
  if(_source_load) {    

    //delete ind in recycler, needed when dealing with large source pop and lots of replicates:
    purgeRecyclingPOOL();

    _source_load_periodicity = (unsigned int)ceil((double)replicates/_source_replicates);

    // load the population from the input file
    // a new metapop object will be created to hold the individuals
    // from the source
    // _loader.extractPop() will be called
    // some current parameters may be reset to match parameter values in the source pop

    loadSourcePopulation();

    // once the data is loaded in the _source Metapop, we can use it to fill in the current Metapop:

    if(_source_preserve) {
      
      //load in preserve mode
      // i.e. make a copy of the saved pop
      // the population structure in the source is preserved
      // patches are loaded sequentially using the same patch index as in the source
      // the number of individuals loaded per patch will match that of the source

      setPopulationFromSourceInPreserveMode();

    } else {
      
      // the individuals are put in a big bag, destroying the existing structure
      // individuals will be randomly drawn from this bag with replacement

      setPopulationFromSource();

    }

    //clean up the source loading metapop to free up memory
    if(_source_filetype == ".bin") {

      _loader.clear();
      _source = NULL;

    } else {

      if(_source) delete _source;
      _source = NULL;

    }

  } else { //not source_load

    for(unsigned int i = 0; i < _patchNbr; ++i)
      _vPatch[i]->setNewGeneration( _requiredAge, this );

    _currentAge = _requiredAge;

  }

#ifdef _DEBUG_
  message("+++loaded age is: %i \n", _currentAge);
#endif

  if(_currentAge == NONE) warning("Metapop::setPopulation: generation 0 is empty!!\n");
}
// ----------------------------------------------------------------------------------------
// setPopulationFromSourceInPreserveMode
// ----------------------------------------------------------------------------------------
void Metapop::setPopulationFromSourceInPreserveMode()
{
  Patch* src_patch;
  age_t source_age;

  // set the number of patches to load relative to the number present in the source
  unsigned int num_patch = (unsigned int)min((double)_vPatch.size(), (double)_source->getPatchNbr());

  if(_requiredAge != NONE && _source->size(_requiredAge) == 0 && _source->size() != 0) {

    source_age = _source->getCurrentAge();

    warning("required age %i not present in source, using age class %i instead (preserve mode).\n"
        ,_requiredAge,source_age);

    for(unsigned int i = 0; i < num_patch; ++i) {

      src_patch = _source->getPatchPtr(i);

      fillPatchFromSource(FEM, src_patch, _vPatch[i], source_age);
      fillPatchFromSource(MAL, src_patch, _vPatch[i], source_age);
    }

  } else {

    if (_requiredAge == NONE) _requiredAge = ALL;

    for(unsigned int i = 0; i < num_patch; ++i) {

      src_patch = _source->getPatchPtr(i);

      fillPatchFromSource(FEM, src_patch, _vPatch[i], _requiredAge);
      fillPatchFromSource(MAL, src_patch, _vPatch[i], _requiredAge);

//      if(_requiredAge & ADULTS) {
//        fillPatchFromSource(FEM, src_patch, _vPatch[i], ADULTS);
//        fillPatchFromSource(MAL, src_patch, _vPatch[i], ADULTS);
//      }
    }
  }

  if(size( ADULTS ) != 0)
    _currentAge |= ADULTS;

  if(size( OFFSPRG ) != 0)
    _currentAge |= OFFSPRG;

#ifdef _DEBUG_
  message("+++pop set from source: offspring: %i f, %i m; adults: %i f, %i m\n", size(FEM,OFFSPRG),
      size(MAL,OFFSPRG), size(FEM,ADULTS), size(MAL,ADULTS));
#endif

} 
// ----------------------------------------------------------------------------------------
// fillPatchFromSource, only for preserve mode
// ----------------------------------------------------------------------------------------
void Metapop::fillPatchFromSource(sex_t SEX, Patch* src, Patch* patch, age_t AGE)
{
  //  unsigned int age, age_last_class = _age_structure.get(0, getNumAgeClasses()-1);
  age_idx age_class; //, idx_last_class = static_cast<age_idx>(getNumAgeClasses()-1);
  Individual* new_ind;
  unsigned int stage_num = getNumAgeClasses();
  unsigned int MASK = 1;

  for (unsigned int i = 0; i < stage_num; ++i) {

    if(MASK & AGE) {

      age_class = age_idx(i);

      for(unsigned int j = 0; j < src->size(SEX, age_class); ++j) {

        new_ind = getNewIndividual(); //this correctly sets pointers to TProto's, and params

        (*new_ind) = (*src->get(SEX, age_class, j)); //this should only copy genes of the traits

        patch->add(SEX , age_class, new_ind );
      }
    }
    MASK <<= 1;
  }
} 
// ----------------------------------------------------------------------------------------
// setPopulationFromSource, not in preserve mode
// ----------------------------------------------------------------------------------------
void Metapop::setPopulationFromSource()
{
  ///loads a pop in non-preserve mode, i.e. use saved pop as a source of new individuals
  ///individuals are drawn without replacement, sex is preserved, not age.
  ///only the first adult age class is filled, at carrying capacity
  deque< Individual* > src_fem_pool;
  deque< Individual* > src_mal_pool;

  if(_source->size(_requiredAge) != 0) {

    _source->getAllIndividuals(_requiredAge, src_fem_pool, src_mal_pool);

  } else if(_source->size(ALL) != 0){

    warning("source population does not contain individuals in the required stage(s), using all available stages instead.\n");

    _source->getAllIndividuals(ALL, src_fem_pool, src_mal_pool);

  } else  //total size of source has been checked before, source shouldn't be empty here
    fatal("source population is empty!\n");

  unsigned int stage_num = getNumAgeClasses();
  unsigned int MASK = 1;
  age_idx stage;

  for (unsigned int i = 0; i < stage_num; ++i) {

    if( MASK & _requiredAge ){

      stage = age_idx(i);

      fillPopulationFromSource(stage, FEM, src_fem_pool);
      fillPopulationFromSource(stage, MAL, src_mal_pool);
    }

    MASK <<= 1;
  }

  src_fem_pool.clear();
  src_mal_pool.clear();

  _currentAge |= _requiredAge;
}
// ----------------------------------------------------------------------------------------
// fillPopulationFromSource, filling in non-preserve mode
// ----------------------------------------------------------------------------------------
void Metapop::fillPopulationFromSource(age_idx age_x, sex_t SEX, deque<Individual*>& src_pool)
{
  unsigned int Ktot = 0;
  unsigned int ind_pos ;
  Individual* new_ind;

  for(unsigned int i = 0; i < _patchNbr; ++i) 
    Ktot += _vPatch[i]->get_K(SEX);

  if(src_pool.size() < Ktot)
    warning("Number of %s %s in source metapop (%i) not enough to fill current metapop (%i).\n",
        (SEX ? "female" : "male"), (age_x == OFFSx ? "offspring" : "adults"),
        src_pool.size(), Ktot);

  for(unsigned int i = 0; i < _patchNbr; ++i) {

    for(unsigned int j = 0, psize = _vPatch[i]->get_K(SEX);
        j < psize && src_pool.size() != 0; ++j) 
    {

      new_ind = getNewIndividual();

      ind_pos = RAND::Uniform( src_pool.size() );

      (*new_ind) = *(src_pool[ ind_pos ]);

      new_ind->setAge((unsigned short)age_x);

      _vPatch[i]->add( SEX, age_x, new_ind );

      src_pool.erase( src_pool.begin() + ind_pos );

    }
  }

}
// ----------------------------------------------------------------------------------------
// loadSourcePopulation
// ----------------------------------------------------------------------------------------
void Metapop::loadSourcePopulation( )
{
  std::string filename;

  if(_source_replicates != 0) {
    //find the replicate string of the source file
    ostringstream rpl;
    rpl.fill('0');
    rpl.width(_source_replicate_digits);

    if(_source_replicates >= _replicates)
      rpl << _currentReplicate + _source_start_at_replicate - 1;
    else if (_source_replicates != 1)
      //less replicates in source, reload the same repl every `source_replicates'
      rpl << ((_currentReplicate-1) / _source_load_periodicity) + 1;
    else
      rpl << 1; //a bit dumb but just in case...

    filename = _source_name + "_" + rpl.str() + _source_filetype;

  } else {

    filename = _source_name;

  }

  if(_source_filetype == ".bin")
    loadPopFromBinarySource(filename);
  else
    loadPopFromTraitFile(filename);

  if(_source->size() == 0) {

    fatal("source population is empty!\n");

  } else {

    if(_source->size(OFFSPRG) != 0)
      _source->setCurrentAge( (_source->getCurrentAge() | OFFSPRG) );

    if(_source->size(ADULTS)  != 0)
      _source->setCurrentAge( (_source->getCurrentAge() | ADULTS)  );
  }

#ifdef _DEBUG_
  message("+++source pop loaded: offspring: %i f, %i m; adults: %i f, %i m\n", _source->size(FEM,OFFSPRG),
      _source->size(MAL,OFFSPRG), _source->size(FEM,ADULTS), _source->size(MAL,ADULTS));
#endif
}
// ----------------------------------------------------------------------------------------
// loadPopFromBinarySource
// ----------------------------------------------------------------------------------------
void Metapop::loadPopFromBinarySource( string &filename )
{
  Individual  *new_ind, *src_tmp;

  // EXTRACT DATA FROM SOURCE -------------------------------------------------------------

  // here the data is put in the _source Metapop, it is not yet copied to fill the current pop

  if(!(_source = _loader.extractPop(filename,_source_generation,SIMenv::MainSim, this)))
    fatal("Metapop::loadPopFromBinarySource:could not extract pop from binary file \"%s\"\n",filename.c_str());

#ifdef _DEBUG_
  message("+++Metapop::loadPopFromBinarySource::data read from %s\n",filename.c_str());
#endif

  // PERFORM CHECKS -----------------------------------------------------------------------

  //check trait parameters; we want to have equal genetic architecture for the traits
  new_ind = getNewIndividual();
  src_tmp = _source->getNewIndividual();

  if( (*new_ind) != (*src_tmp) ) {
    fatal("loading population from binary file failed: trait genetic architecture differs.\n");
  }

  delete new_ind;
  delete src_tmp;

  if(_source_preserve) {

//    //basic checks:
//    if(_source->getPatchNbr() != _patchNbr)
//      fatal("loading population from source file failed: preserve mode forbids difference in number of patches\n");

    if(_source->getNumAgeClasses() != getNumAgeClasses())
      fatal("loading population from binary file failed: preserve mode forbids difference in stage/age structure\n");

  }
}

// ----------------------------------------------------------------------------------------
// loadPopFromTraitFile
// ----------------------------------------------------------------------------------------
void Metapop::loadPopFromTraitFile( string &filename )
{
  if(_source != NULL) delete _source;

  _source = new Metapop();
  _source->setMPImanager( _mpimgr );

  //the population's parameters from current params
  _source->set_paramsetFromCopy((*this->get_paramset()));

  //we need this to be able to create new individuals in the source:
  _source->init( );
  _source->makePrototype(this->getTraitPrototypes());

  //now read data from genotype file:
  FileServices *FS = SIMenv::MainSim->get_FileServices();

  FileHandler* file = FS->getReader(_source_filetype);

  if(!file)
    fatal("no file reader exists with source file extension \"%s\".\n",_source_filetype.c_str());

  //attach the source pop to the reader
  file->set_pop_ptr(_source);

  //read the genotypes from files into the source metapop object
  file->FHread(filename);

  if(_source->size() == 0)
    fatal("source population in file \"%s\" is empty.\n",filename.c_str());
}
// ----------------------------------------------------------------------------------------
//  Metapop::reset
 /** Resets each Patch, all individuals are moved to the POOL */
// ----------------------------------------------------------------------------------------
void Metapop::reset()
{
  unsigned int i;

  for(i = 0; i < _patchNbr; ++i) {
    _vPatch[i]->flush(this);
    _vPatch[i]->reset_counters();
  }
}
// ----------------------------------------------------------------------------------------
// clear
// ----------------------------------------------------------------------------------------
void Metapop::clear()
{
#ifdef _DEBUG_
  message("Metapop::clear\n");
#endif

  for(unsigned int i = 0; i < _vPatch.size(); ++i) delete _vPatch[i];

  _vPatch.clear();

  _patchNbr = 0;

  purgeRecyclingPOOL();

}
// ----------------------------------------------------------------------------------------
// setCurrentAge
// ----------------------------------------------------------------------------------------
void Metapop::setCurrentAge ( LifeCycleEvent* LCE )
{
  _currentAge ^= LCE->removeAgeClass();
  _currentAge |= LCE->addAgeClass();
}
// ----------------------------------------------------------------------------------------
// getAllIndividuals
// ----------------------------------------------------------------------------------------
void Metapop::getAllIndividuals(age_t AGE, deque<Individual*>& fem_pool, deque<Individual*>& mal_pool)
{

  for(unsigned int i = 0; i < _vPatch.size(); ++i) {

    for(unsigned int j = 0, psize = _vPatch[i]->size(MAL, AGE); j < psize; ++j)
      mal_pool.push_back( _vPatch[i]->get(MAL, AGE, j) );

    for(unsigned int j = 0, psize = _vPatch[i]->size(FEM, AGE); j < psize; ++j)
      fem_pool.push_back( _vPatch[i]->get(FEM, AGE, j) );

  }

}
// ----------------------------------------------------------------------------------------
// show_up
// ----------------------------------------------------------------------------------------
void Metapop::show_up()
{
  message("Metapop:\n");
  message("nbre. of patches: %i(%i)\n", _vPatch.size(),_patchNbr);
  message("population size : %i\n", size());
  message("K = %i, K_fem = %i, K_mal = %i\n",_patchK, _patchKfem, _patchKmal);
  message("patch capacities: \n");
  _patchSizes.show_up();
  message("Patches:\n");
  for(unsigned int i = 0; i < _vPatch.size(); i++)
    _vPatch[i]->show_up();
}
//------------------------------------------------------------------------------------------
//
//                                       MPFileHandler
//
//------------------------------------------------------------------------------------------
void MPFileHandler::FHwrite()
{
  if (!_pop->isAlive()) return;


  std::string filename = get_filename();

  std::ofstream FILE (filename.c_str(), ios::out);

  if(!FILE) fatal("could not open \"%s\" output file!!\n",filename.c_str());

  FILE << "ID dad mum sex age stage home pop\n";

  Patch* patch;

  if(_patch_sample_size == 0) {

    for(unsigned int i = 0; i < _pop->getPatchNbr(); ++i) {

      patch = _pop->getPatch(i);

      for(unsigned int k = 0; k < _pop->getNumAgeClasses(); ++k) {

        for(unsigned int S = 0; S < 2; ++S){

          printNoSample((sex_t)S, static_cast<age_idx> (k), patch, FILE);

        }
      }
    }

  } else {

    for(unsigned int i = 0; i < _pop->getPatchNbr(); ++i) {

      patch = _pop->getPatch(i);

      for(unsigned int k = 0; k < _pop->getNumAgeClasses(); ++k) {

        createAndPrintSample(static_cast<age_idx> (k), patch, FILE);

      }
    }
  }

  FILE.close();

}
// ------------------------------------------------------------------------------------------
// MPFileHandler::printNoSample
// ------------------------------------------------------------------------------------------
void MPFileHandler::printNoSample (sex_t SEX, age_idx AGE, Patch* patch, ofstream& FH )
{
  Individual* ind;
  for(unsigned int j = 0; j < patch->size(SEX, AGE); ++j) {

    ind = patch->get(SEX, AGE, j);

    FH<< ind->getID() << " " << ind->getFatherID() << " " << ind->getMotherID()
			        << " " << SEX << " " << ind->getAge() << " " << AGE << " " << ind->getHome()+1
			        << " " << patch->getID()+1 << endl;
  }
}
// ------------------------------------------------------------------------------------------
// MPFileHandler::createAndPrintSample
// ------------------------------------------------------------------------------------------
void MPFileHandler::createAndPrintSample (age_idx AGE, Patch* patch, ofstream& FH)
{
  Individual* ind;
  unsigned int size_fem = patch->size(FEM, AGE);
  unsigned int size_mal = patch->size(MAL, AGE);
  unsigned int sample_size;

  int* sample = NULL;

  //Females -----------------------------------------------------------------------------
  sample_size = (_patch_sample_size < (int)size_fem ? _patch_sample_size : size_fem);

  if( sample_size != 0) {

    sample = new int[sample_size];

    RAND::Sample(0, size_fem, sample_size, sample, false);

    for(unsigned int j = 0; j < sample_size; ++j) {

      ind = patch->get(FEM, AGE, sample[j]);

      FH<< ind->getID() << " " << ind->getFatherID() << " " << ind->getMotherID()
				        << " " << FEM << " " << ind->getAge() << " " << AGE << " " << ind->getHome()+1
				        << " " << patch->getID()+1 << endl;
    }
  }

  //Males -------------------------------------------------------------------------------
  sample_size = (_patch_sample_size < (int)size_mal ? _patch_sample_size : size_mal);

  if( sample_size != 0) {

    if(sample) delete [] sample;

    sample = new int[sample_size];

    RAND::Sample(0, size_mal, sample_size, sample, false);

    for(unsigned int j = 0; j < sample_size; ++j) {

      ind = patch->get(MAL, AGE, sample[j]);

      FH<< ind->getID() << " " << ind->getFatherID() << " " << ind->getMotherID()
				        << " " << MAL << " " << ind->getAge() << " " << AGE << " " << ind->getHome()+1
				        << " " << patch->getID()+1 << endl;
    }
  }

  if(sample) delete [] sample;
}
