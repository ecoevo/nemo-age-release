/** $Id: $
 *
 *  @file LCEregulation.cc
 *  Nemo-age
 *
 *   Copyright (C) 2020
 *   guillaume.fred@gmail.com
 *
 *   This file is part of Nemo
 *
 *   Nemo is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Nemo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  created on @date 16.06.2005
 *
 *  @author fred
 */


#include "LCEregulation.h"
#include "output.h"
#include "Uniform.h"

/*_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/*/

//                                   LCE_Regulation

// ----------------------------------------------------------------------------------------
// LCE_Regulation::setParameters
// ----------------------------------------------------------------------------------------
bool LCE_Regulation::setParameters ()
{

  if(! get_parameter("regulation_by_competition")->isSet() &&
      ! get_parameter("regulation_carrying_capacity")->isSet())
    return error("regulation mode not set for LCE regulation\n");

  _regulate_ptrs.clear();

  // defaults:
  _competition_regulation = 0;
  _age_flag = 4294967294; // = ADULTS

  _num_stage = _popPtr->getNumAgeClasses();

  string model = get_parameter("regulation_by_competition_model")->getArg();



  // set the parameters for regulation by competition
  if(get_parameter("regulation_by_competition")->isSet()) {

    if(!_competition_regulation) _competition_regulation = new TMatrix(); // holds patch-specific regulation coefficients

    if(!get_parameter("regulation_by_competition")->isMatrix()) {

      _competition_regulation->reset(1,_popPtr->getPatchNbr());

      _competition_regulation->assign(get_parameter_value("regulation_by_competition"));

    } else {

      get_parameter("regulation_by_competition")->getMatrix(_competition_regulation);

      if(_competition_regulation->getNbCols()!=_popPtr->getPatchNbr()){

          return error("the column number of \"_competition_regulation\" is wrong. As many cols as number of patches");

      }

      if(_competition_regulation->getNbRows()> 1 ){

          return error("the number of rows of \"regulation_by_competition\" is wrong. Only a single row is allowed.");

      }

    }

    if(get_parameter("regulation_by_competition_affected_age")->isSet()) {

      // regulation can act on different age classes, it acts only on the offspring by default

      if(!get_parameter("regulation_by_competition_affected_age")->isMatrix()) {

        _age_target = age_idx((unsigned int)get_parameter_value("regulation_by_competition_affected_age"));

        if(_age_target > _popPtr->getNumAgeClasses() -1)
          return error("parameter \"regulation_by_competition_affected_age\" targets a non-existing age class\n");


        // set the competition model, is Berverton-Holt by default

        if(get_parameter("regulation_by_competition_model")->isSet()){

          if(model == "Beverton-Holt"){

            _regulate_ptrs.push_back( &LCE_Regulation::regulatePatch );

          } else if(model == "Ricker"){

            _regulate_ptrs.push_back( &LCE_Regulation::regulatePatch_Ricker );

          } else
            return error("the specified \"regulation_by_competition_model\" does not exist. It must be Ricker or Beverton-Holt! \n");

        // set to default if not set in the config file:
        } else
          _regulate_ptrs.push_back( &LCE_Regulation::regulatePatch );


        // the affected age is a matrix, which means that multiple stages are regulated by competition
      } else {

        if(!_affected_age) _affected_age = new TMatrix(); // holds the matrix of age classes affected by regulation

        get_parameter("regulation_by_competition_affected_age")->getMatrix(_affected_age);

        for(unsigned int i = 0; i < _affected_age->getNbCols(); ++i){

          if(_affected_age->get(0,i) > _popPtr->getNumAgeClasses() -1)
            return error("parameter \"regulation_by_competition_affected_age\" targets more stages than present in the age structure\n");

        }

        // set the competition function, use the multi-age version of the regulation function
        if(get_parameter("regulation_by_competition_model")->isSet()){

          if(model=="Beverton-Holt"){

            _regulate_ptrs.push_back( &LCE_Regulation::regulatePatch_multiAge );

          } else if(model=="Ricker"){

            _regulate_ptrs.push_back( &LCE_Regulation::regulatePatch_multiAge_Ricker );

          } else
            return error("the specified \"regulation_by_competition_model\" does not exist. Ricker or Beverton-Holt! \n");


        } else // is Beverton-Holt by default
          _regulate_ptrs.push_back( &LCE_Regulation::regulatePatch_multiAge );

      }

    } else { // affected age is not set

      _age_target = OFFSx;

      if(get_parameter("regulation_by_competition_model")->isSet()){

        if(model=="Beverton-Holt"){

          _regulate_ptrs.push_back( &LCE_Regulation::regulatePatch );

        } else if(model=="Ricker"){

          _regulate_ptrs.push_back( &LCE_Regulation::regulatePatch_Ricker );

        } else
          return error("the specified \"regulation_by_competition_model\" does not exist. Ricker or Beverton-Holt! \n");

      } else
        _regulate_ptrs.push_back( &LCE_Regulation::regulatePatch );

    }

    // the competition is exerted on the targeted age class by individuals in all other age-classes,
    // we need to set the age flag corresponding to those age-classes if different from all adult
    // age-classes; age flag 4294967294 = 0xFFFFFFFE, is all age classes except the offspring
    // (the max is 32 stages, see the definition of the ADULTS age flag in types.h)

    if(get_parameter("regulation_by_competition_count_age_flag")->isSet()) {

      _age_flag = (unsigned int)get_parameter_value("regulation_by_competition_count_age_flag");

      if(_age_flag > 4294967295) //should in principle be OK to include all stages, including offspring
        return(error("in \"regulation_by_competition\", the age flag is > 4294967295\n"));

    }


  } // end regulation_by_competition

  // for ceiling regulation, we add the pointer to the set of regulation function
  if (get_parameter("regulation_carrying_capacity")->isSet()) {

    _regulate_ptrs.push_back( &LCE_Regulation::regulatePatchCeiling );

    if(_stage_size_proportions) delete _stage_size_proportions;
    _stage_size_proportions = new double [_num_stage];

    if(_stage_size_rmultinom) delete _stage_size_rmultinom;
    _stage_size_rmultinom = new unsigned int [_num_stage];

  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Regulation::execute
// ----------------------------------------------------------------------------------------
void LCE_Regulation::execute ()
{
#ifdef _DEBUG_
  message("LCE_Regulation::execute (Patch nb: %i, offsprg: %i, adults: %i ",_popPtr->getPatchNbr()
          ,_popPtr->size( OFFSPRG ), _popPtr->size( ADULTS ));
#endif
  Patch* patch;

//  _num_stage = _popPtr->getNumAgeClasses(); // won't change during a simulation

  unsigned int pop_size;

  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); i ++) {

    patch = _popPtr->getPatch(i);

    pop_size = patch->size(_age_flag); //total adult size matters for the strength of competition

    //if(!pop_size) continue; //go to next patch if this one is empty

    for (unsigned int i = 0; i < _regulate_ptrs.size(); ++i) {
      (this->* _regulate_ptrs[i])(patch, FEM, pop_size);
      (this->* _regulate_ptrs[i])(patch, MAL, pop_size);
    }

  }

#ifdef _DEBUG_
  message("after: offsprg: %i, adults: %i)\n",_popPtr->size( OFFSPRG ), _popPtr->size( ADULTS ));
#endif

}
// ----------------------------------------------------------------------------------------
// LCE_Regulation::regulatePatch
// ----------------------------------------------------------------------------------------
void LCE_Regulation::regulatePatch (Patch* patch, sex_t sex, unsigned int pop_size)
{
  unsigned int num_surviving, num_affected = patch->size(sex, _age_target);
  double compet;

  if(!num_affected) return; //exit if sex-age class container is empty

  //option by default: beverton holt regulation

  if(_competition_regulation->get(0,patch->getID()) != 0) {

    compet = 1/(1 + pop_size * _competition_regulation->get(0,patch->getID()));

    num_surviving = RAND::Binomial2(compet,num_affected);

    while(patch->size(sex, _age_target) > num_surviving){

      _popPtr->recycle( patch->remove(sex, _age_target,
                                      RAND::Uniform( patch->size(sex, _age_target) ) ) );

    }
  }

}//end function
// ----------------------------------------------------------------------------------------
// LCE_Regulation::regulatePatch_Ricker
// ----------------------------------------------------------------------------------------
void LCE_Regulation::regulatePatch_Ricker (Patch* patch, sex_t sex, unsigned int pop_size){

  unsigned int num_surviving, num_affected = patch->size(sex, _age_target);
  double compet, inside;

  inside = pop_size * _competition_regulation->get(0,patch->getID());
  compet = exp(-1 * inside);

  if(!num_affected) return; //exit if sex-age class container is empty

  if(_competition_regulation->get(0,patch->getID()) != 0) {

    num_surviving = RAND::Binomial2(compet,num_affected);

    while(patch->size(sex, _age_target) > num_surviving){

      _popPtr->recycle( patch->remove(sex, _age_target,
                                      RAND::Uniform( patch->size(sex, _age_target) ) ) );
    }
  }
}//end function

// ----------------------------------------------------------------------------------------
// LCE_Regulation::regulatePatch_multiAge
// ----------------------------------------------------------------------------------------
void LCE_Regulation::regulatePatch_multiAge (Patch* patch, sex_t sex, unsigned int pop_size){

  unsigned int num_affected, num_surviving;
  double compet;

  for(unsigned int i = 0; i < _affected_age->getNbCols(); ++i){

    _age_target = age_idx( _affected_age->get(0,i));

    num_affected = patch->size(sex, _age_target);

    if(!num_affected) continue; //exit if sex-age class container is empty

    //option by default: beverton holt regulation

    if(_competition_regulation->get(0,patch->getID()) != 0) {

      compet = 1/(1 + pop_size * _competition_regulation->get(0,patch->getID()));

      num_surviving = RAND::Binomial2(compet,num_affected);

      while(patch->size(sex, _age_target) > num_surviving){

        _popPtr->recycle( patch->remove(sex, _age_target, RAND::Uniform( patch->size(sex, _age_target) ) ) );

      }
    }
  }
}//end function

// ----------------------------------------------------------------------------------------
// LCE_Regulation::regulatePatch_multiAge_Ricker (Matrix Population Models, Caswell, p. 506)
// ----------------------------------------------------------------------------------------
void LCE_Regulation::regulatePatch_multiAge_Ricker (Patch* patch, sex_t sex, unsigned int pop_size){

  unsigned int num_affected, num_surviving;
  double compet, inside;
  inside = pop_size * _competition_regulation->get(0,patch->getID());
  compet = exp(-1 * inside);

  for(unsigned int i = 0; i < _affected_age->getNbCols(); ++i){             // loop through each age class regulated

    _age_target = age_idx( _affected_age->get(0,i));         // regulated age class

    num_affected = patch->size(sex, _age_target);            // ind number of this age class

    if(!num_affected) continue; //exit if sex-age class container is empty

    if(_competition_regulation->get(0,patch->getID()) != 0) {

      num_surviving = RAND::Binomial2(compet,num_affected);

      while(patch->size(sex, _age_target) > num_surviving){

        _popPtr->recycle( patch->remove(sex, _age_target, RAND::Uniform( patch->size(sex, _age_target) ) ) );

      }
    }
  }
}//end function

// ----------------------------------------------------------------------------------------
// LCE_Regulation::regulatePatchCeiling
// ----------------------------------------------------------------------------------------
void LCE_Regulation::regulatePatchCeiling (Patch* patch, sex_t sex, unsigned int pop_size)
{
  // adjust to carrying capacity randomly removing individuals
  _K = patch->get_K(sex);

  age_idx age;

  unsigned int tot_size = patch->size(sex, ALL); // current size of patch

  if(_K == 0) {

    if(tot_size != 0) {

      patch->flush(sex, _popPtr);

    }

    patch->set_isExtinct(true); // patch considered extinct if later filled with immigrants = colonizers
  }

  else

  {

    // randomly remove individual, drawing proportionally to the stage size

//    float prop;
    unsigned int size;
    unsigned int num_to_remove;
    unsigned int diff;

    if( tot_size > _K ) {

      diff = tot_size - _K; // how many need to be removed

      for (unsigned int i = 0; i < _num_stage; ++i) {

        age = age_idx(i);

        size = patch->size(sex, age); // current size of stage

        _stage_size_proportions[i] = (double)size/tot_size; // proba to be removed by random regulation

      }  // end for stage

      // randomly draw the number of individuals to remove in each stage from a multinomial distribution:
      // the numbers returned may exceed the actual number of individuals in a stage
      // a check is added in the second loop to avoid removing when patch size is down to zero
      RAND::Multinomial(_num_stage, diff, _stage_size_proportions, _stage_size_rmultinom);

      for (unsigned int i = 0; i < _num_stage; ++i) {

        age = age_idx(i);

        for (int j = 0; j < _stage_size_rmultinom[i] &&  patch->size(sex, age) > 0; ++j) {

          _popPtr->recycle( patch->remove(sex, age, RAND::Uniform( patch->size(sex, age) ) ) );

        }

      } // end for remove

    } // end if

  } // end if _K == 0

}//end function
