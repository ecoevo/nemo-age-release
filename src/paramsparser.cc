/** $Id: paramsparser.cc,v 1.15 2018/09/12 12:17:21 fred Exp $
*
*  @file paramsparser.cc
*  Nemo2
*
*   Copyright (C) 2006-2015 Frederic Guillaume
*   frederic.guillaume@ieu.uzh.ch
*
*   This file is part of Nemo
*
*   Nemo is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   Nemo is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*  created on @date 09.05.2005
* 
*  @author fred
*/

#include <iostream>
#include <iomanip>
#include <sstream>
#include <streambuf>
#include <fstream>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <time.h>
#include <algorithm>
#include "paramsparser.h"
#include "output.h"
#include "tstring.h"
#include "tmatrix.h"
#include "Uniform.h"
#include <cmath>
#include <cfloat>

#define EOL '\n'

using namespace std;


//----------------------------------------------------------------------------------------
// ParamsParser
// ----------------------------------------------------------------------------------------
ParamsParser::ParamsParser(const char* name)
{
  _sname = name;

  _macroMap["rep"] = &ParamsParser::rep;
  _macroMap["seq"] = &ParamsParser::seq;
  _macroMap["tempseq"] = &ParamsParser::tempseq;
  _macroMap["c"] = &ParamsParser::concat;
  _macroMap["q"] = &ParamsParser::quote;
  _macroMap["runif"] = &ParamsParser::runif;
  _macroMap["rnorm"] = &ParamsParser::rnorm;
  _macroMap["rpois"] = &ParamsParser::rpoiss;
  _macroMap["rlognorm"] = &ParamsParser::rlognorm;
  _macroMap["rgamma"] = &ParamsParser::rgamma;
  _macroMap["rbernoul"] = &ParamsParser::rbernoul;
  _macroMap["matrix"] = &ParamsParser::matrix;
  _macroMap["diag"] = &ParamsParser::diag_matrix;
  _macroMap["smatrix"] = &ParamsParser::sym_matrix;
}
//----------------------------------------------------------------------------------------
// getParameters
// ----------------------------------------------------------------------------------------
map< string, string >& ParamsParser::getParameters(const char* stream_name)
{
#ifdef _DEBUG_
  cout<<"ParamsParser::getParameters:"<<endl;
#endif  
  if(stream_name == NULL) {

    if(_sname == NULL)

      fatal("ParamsParser::getParameters::no stream attached to the parser!\n");

    else if( !(read(_sname)) ) // read the input text stream and extract parameters and their arguments

      fatal("ParamsParser::getParameters::failed from file \"%s\"\n",_sname);

  } else if( !(read(stream_name)) )  // read the input text stream and extract parameters and their arguments

    fatal("ParamsParser::getParameters::failed from file \"%s\"\n",stream_name);
  
  if(stream_name != NULL) _sname = stream_name;
  
  // pairs of parameter name & values have been recorder in _inputParams map
  return _inputParams;
}
//----------------------------------------------------------------------------------------
// getParsedParameters
// ----------------------------------------------------------------------------------------
map< string, vector< string > >& ParamsParser::getParsedParameters(const char* stream_name)
{
#ifdef _DEBUG_
  cout<<"ParamsParser::getParsedParameters:"<<endl;
#endif  
  // read the input parameters from the input text stream
  getParameters(stream_name);
  
  // parse the input text stream
  parse();
  
  return _parsedParams;
}
//----------------------------------------------------------------------------------------
// parse
// ----------------------------------------------------------------------------------------
void ParamsParser::parse()
{
#ifdef _DEBUG_
  cout<<"ParamsParser::parse:"<<endl;
#endif  

  // empty container map of parameters parsed into key - value(s) pairs
  _parsedParams.clear();
  
  // local holder of the arguments read in the input stream, contains whole lines from input
  vector< string > argvect;
  string arg;

  // go through all parameters previously read in the input text stream and parse param values into multiple values
  map<string, string>::iterator param = _inputParams.begin();

  while(param != _inputParams.end()) {

    // first, check and replace macros:
    param->second = replaceMacro(param->second);
    
    argvect.clear();

    if((param->first == "stats") || (param->first == "stat") )

      argvect.push_back(param->second);  //this one cannot be a sequential parameter

    else { //other params:

      getArguments(param->second, argvect);

    }

    _parsedParams[param->first] = argvect;

#ifdef _DEBUG_
    message(" %s (", param->first.c_str());
    unsigned int nb=argvect.size()   ;
    for(unsigned int i=0 ;i<nb; ++i){
      cout<<argvect[i]<<" ";
    }
    message("| %i args)\n", nb);
#endif

    param++;
  }
}
//----------------------------------------------------------------------------------------
// ParamsParser::getArguments
// ----------------------------------------------------------------------------------------
void ParamsParser::getArguments (string& arg_str, vector< string >& arg_vect)
{
  istringstream IN, BLOCK;
  string arg, block_elmnt;
  vector<string> block_args;
  int dummy_line = 0;
  char c;
  
  IN.str(arg_str);
  
  while(IN.get(c) && IN.good() && !IN.eof()){
    
    if(isspace(c)) {
      StreamParser::removeSpaceAndComment(IN, dummy_line);
      continue;
    }
    
    if(c == '{' || c == '(' || c == '[' || c == '\"')
    {
      getBlockArgument(IN, c, arg); //because it may span more than one line

//      // treat the block as a new stream to look for macros and do the replacements
//      BLOCK.str(arg);
//
//      block_args.clear();
//
//      while(BLOCK.good() && !BLOCK.eof()) {
//
//        BLOCK>>block_elmnt;
//
//        block_elmnt = replaceMacro(BLOCK, block_elmnt);
//
//        block_args.push_back(block_elmnt);
//      }
//
//      // concatenate the elements back together:
//      arg.clear();
//      for (int i = 0; i < block_args.size(); ++i) {
//        arg += block_args[i] + " ";
//      }
    } 
    else //not a block argument
    {
      IN.putback(c);
      IN>>arg; //this stops if a whitespace is found, thus not including macro arguments if a macro

//      arg = replaceMacro(IN, arg);
    }


    arg_vect.push_back( arg );
  }
}
//----------------------------------------------------------------------------------------
// ParamsParser::getBlockArgument
// ----------------------------------------------------------------------------------------
void ParamsParser::getBlockArgument (istream& IN, char& start_c, string& arg)
{
  char e = 0;
  int dummy_line = 0;

  switch (start_c) {
    case '{': e = '}';
      break;
    case '(': e = ')';
      break;
    case '[': e = ']';
      break;
    default:
      break;
  }
      
  arg = StreamParser::readUntilCharacter(IN, dummy_line, start_c, (const char)e);
    
//    cout<<"added arg to vector: "<<out<<" next char='"<<IN.peek()<<"'\n";
}
//----------------------------------------------------------------------------------------
// ParamsParser::replaceMacro
// ----------------------------------------------------------------------------------------
string ParamsParser::replaceMacro(const string& arg)
{
  // we now need to check if a macro is part of the argument string
  // this is detected if the arg string holds a "(" as in "macro_name( "
  // or "(" is within the whole string as in "macro_name(arg1"
  // or "(" is within a block argument, we skip the first char to account for that last case

  string out_arg;

  if( arg.find_first_of('(', 1) != string::npos )
  {
    // there might be a macro, get it

    out_arg = parseMacroFunctionBlock(arg);

  }
  else  // no macro
  {
    out_arg = arg;
  }

  return out_arg;
}
//----------------------------------------------------------------------------------------
// ParamsParser::getMacroFunctionBlock
// ----------------------------------------------------------------------------------------
string ParamsParser::parseMacroFunctionBlock(const string& in_arg)
{
  string front, tail, rest, out_arg;
  string name;

//  cout<<"parseMacroFunctionBlock <"<< in_arg <<">\n";

  size_t opar_pos, cpar_pos, first_macro_ch;

  // extract the name of the macro, first find the opening parenthesis
  opar_pos = in_arg.find_first_of('(', 1);

  //cut the string at the opening par to look for the macro name in the front part
  front = in_arg.substr(0, opar_pos);

  //cut the string after the opening par to look for macro arguments and more macros in the rest
  rest = in_arg.substr(opar_pos+1,  in_arg.size() - opar_pos - 1);

  //a macro name can only start after the last delimiter in front of it
  first_macro_ch = front.find_last_of("(){}[], =\t");
  // '=' is included because arguments to macros can be named and assigned a call to a nested macro
  // eg: tempseq(at=seq(),seq=seq())

  //the open par may not be part of a macro, or there might not be any delim in front, check this:
  if(first_macro_ch == string::npos)
    first_macro_ch = 0; // no delim before name, start at first pos in string
  else
    first_macro_ch++;  //skip the last delimiter

  // check if we have a macro name in front and extract it
  if(opar_pos > first_macro_ch)
  {
    name = in_arg.substr(first_macro_ch, opar_pos - first_macro_ch);
    // keep what was in front of the macro name;
    front = in_arg.substr(0, first_macro_ch);
  }
  else // first_macro_ch == opar_pos; no name, no macro, skip
  {    // this corresponds to cases where multiple blocks are provided in input:
       // param_name (... macro1(...),...) (... macro2(...), ...)
       // the second block causes problems if we lose the brackets on the way

    name = "";

    // keep what was in front of the false macro block, including the opening '(';
    front = in_arg.substr(0, first_macro_ch+1);
  }

  out_arg += front;

  // before reading the arguments of the macro function call, check for nested macros, recursively
  if(rest.find_first_of('(', 1) != string::npos)
  {
    //recursion
    //replace the macro name by the result of the call to the nested macro in the macro_args vect
    rest = ( parseMacroFunctionBlock(rest) );

  }
  //no more nested block, close recursion and return arguments

//  cout<< " processing: "<<rest<<endl;

  // find the limit of the  macro args
  cpar_pos = rest.find_first_of(')');

  if ( cpar_pos != string::npos)
  {
    // record what is left after the macro args, excluding the closing macro ')'
    if( cpar_pos < rest.size()-1 ){

      tail = rest.substr(cpar_pos+1, rest.size() - cpar_pos - 1);
    }
    else
      tail = "";
  }
  else
  {
    fatal("Error while parsing argument string \"%s\" for macros, closing bracket not found\n", (rest+tail).c_str());
  }

  rest = rest.substr(0, cpar_pos); // this is the string containing the args to the macro

  if(name == "")
  {

//    cout<< " not calling macro: name =\"\"; arg string is <"<<rest<<">"<<"\n";

    out_arg += rest + ")" + tail; //need to put back the closing ')' because not a macro

  }
  else
  {

//    cout<< " calling macro: "<<name<<"("<<rest<<")"<<"\n";

    // call the macro and paste the result into the output string
    out_arg += callMacro(name, rest) + tail;

  }

//  cout<< "   returning "<<out_arg<<endl;
  // return the processed string
  return out_arg;
}
//----------------------------------------------------------------------------------------
// ParamsParser::getMacroFunctionBlock
// ----------------------------------------------------------------------------------------
string ParamsParser::callMacro(const string& name, const string& argstr)
{
  string out;
#ifdef _DEBUG_
  message(" ^^^^ calling macro: %s(%s)\n",name.c_str(), argstr.c_str());
#endif

  auto elmnt = _macroMap.find(name);

  if( elmnt == _macroMap.end())
  {
    fatal("macro \"%s(%s)\" could not be mapped to a known function.\n", name.c_str(), argstr.c_str());
  }

  out = (this->*(elmnt->second))(argstr);

#ifdef _DEBUG_
  message(" ^^^^     returning: %s\n",out.c_str());
#endif

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::getMacroArgs
// ----------------------------------------------------------------------------------------
vector<string> ParamsParser::getMacroArgs (const string& argstr, const int min_arg,
    const size_t max_arg, const string macro_name, const string syntax, bool lastArgIsSeparatorChar)
{

//  cout<<" getMacroArgs:: argstr="<<argstr<<endl;

  vector<string> macro_args = tstring::splitExcludeEnclosedDelimiters(argstr, ',');

  if(macro_args.size() > max_arg ){
    fatal("macro \"%s\" can have max %i arguments: %s\n", macro_name.c_str(), max_arg, syntax.c_str());
  }
  else if (macro_args.size() < min_arg) {
    fatal("macro \"%s\" must have at least %i arguments: %s\n", macro_name.c_str(), min_arg, syntax.c_str());
  }

  size_t last_arg = macro_args.size() -1;

  string sep = ","; // default sep

  // treat the last argument specially if it is a separator char
  // or add the default separator char to the arg vector if not specified in input
  if(lastArgIsSeparatorChar) {

    // make sure it was specified in input
    if(macro_args.size() > min_arg) {
    // check if separator is given in arg (we'll assume it is the last arg...)
       if(argstr.rfind("sep") != string::npos) {

        // get the separator char from the last character string
        sep = getMacroSepParamChar(macro_args[ last_arg ], macro_name);

        macro_args[ last_arg ] = sep; // we replace the string "sep=..." with the char

      } else
        macro_args.push_back(sep); //default
    } else
      macro_args.push_back(sep); //default
  }

//  auto print = [this](const string& s){cout<<" getMacroArgs:: "<<s<<endl;};
//
//  for_each(macro_args.cbegin(), macro_args.cend(), print);

  return macro_args;
}
//----------------------------------------------------------------------------------------
// ParamsParser::getMacroSepParamChar
// ----------------------------------------------------------------------------------------
string ParamsParser::getMacroSepParamChar (const string& sep_in, const string macro_name)
{

  if( tstring::isanumber(sep_in))
    fatal("last argument \"sep\" of macro \"%s\" must be a character string in \"%s\"\n", macro_name.c_str(), sep_in.c_str());

  vector<string> sep_args = tstring::splitExcludeEnclosedDelimiters(sep_in, '=');

  string sep;

  // check format of sep argument specification: sep="c", or "c"
  if(sep_args.size() == 0)
    fatal("wrong format of \"sep\" argument to macro \"%s\" in \"%s\"\n", macro_name.c_str(), sep_in.c_str());

  if(sep_args[0] == "sep") {

    if(sep_args.size() > 2){
      fatal("argument \"sep\" of macro \"%s\" must receive only one value in \"%s\" \n", macro_name.c_str(), sep_in.c_str());
    }

    sep = sep_args[1];

  } else {
    sep = sep_args[0];
  }

  if(sep[0] != '"') {
    fatal("argument to \"sep\" of macro \"%s\" must be enclosed with two \" in \"%s\" \n", macro_name.c_str(), sep_in.c_str());
  }

  sep = tstring::removeEnclosingChar(sep, '"', '"');

  return sep;
}
//----------------------------------------------------------------------------------------
// ParamsParser::getMacroParamValue
// ----------------------------------------------------------------------------------------
string ParamsParser::getMacroParamValue (const string& str_in, const string& par_name, const string& macro_name)
{
  vector<string> param_args = tstring::splitExcludeEnclosedDelimiters(str_in, '=');

  if(param_args[0] != par_name)
    fatal("expected \"%s\" as input param to macro \"%s\", received \"%s\"\n", par_name.c_str(), macro_name.c_str(), str_in.c_str());

  return param_args[1];
}
//----------------------------------------------------------------------------------------
// ParamsParser::c
// ----------------------------------------------------------------------------------------
string ParamsParser::concat (const string& argstr)
{

  string syntax="c(... , sep=\",\")";
  string out;

  size_t max_arg = INT64_MAX;

  vector<string> macro_args = getMacroArgs(argstr, 2, max_arg, "c", syntax, true);

  string sep = macro_args.back();

  int last = macro_args.size() - 2; //omit separator at last position

  // stick each element together, un-quoting them, if needed
  for (int i = 0; i < last; ++i) {
    out += tstring::removeEnclosingChar(macro_args[i], '"', '"', true) + sep;
  }
  // add last element without separator
  out += tstring::removeEnclosingChar(macro_args[last], '"', '"', true);


  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::quote
// ----------------------------------------------------------------------------------------
string ParamsParser::quote (const string& argstr)
{

  string syntax="q(... , sep=\",\")";
  string out;

  size_t max_arg = INT64_MAX;

  vector<string> macro_args = getMacroArgs(argstr, 1, max_arg, "q", syntax, true);

  string sep = macro_args.back();

  int last = macro_args.size() - 2; //omit separator at last position

  for (int i = 0; i < last; ++i) {
    out += macro_args[i] + sep;
  }
  // add last element without separator
  out += macro_args[last];

  return "\"" + out + "\"";
}
//----------------------------------------------------------------------------------------
// ParamsParser::rep
// ----------------------------------------------------------------------------------------
string ParamsParser::rep (const string& argstr)
{

  string syntax="rep(x, n, each=1, sep=\",\")";

  int max_arg = 4;

  vector<string> macro_args = getMacroArgs(argstr, 2, max_arg, "rep", syntax, true);

//  auto print = [this](const string& s){cout<<" "<<s<<endl;};
//
//  for_each(macro_args.cbegin(), macro_args.cend(), print);

  auto num_args = macro_args.size();

  // ---- get the string to repeat ----
  string what = macro_args[0];

  //removing enclosing "" if added:
  what = tstring::removeEnclosingChar(what, '"', '"', true);

  // ---- the second argument is the number of repetition ----
  if ( !tstring::isanumber(macro_args[1]) ) {
    fatal("rep(): second argument must be a number in: rep(x, n, each=1, sep=\",\")\n");
  }

  int num = tstring::str2int(macro_args[1]);

  // ---- looking for the "each" argument ----
  auto has_each = [] (const string& s) {return (s.find("each") != string::npos);};

  auto argPos = find_if(macro_args.begin(), macro_args.end(), has_each);

  string arg;

  if( argPos != macro_args.end() )
    arg = getMacroParamValue(*argPos, "each", syntax);
  else
    arg = "1";  // default value if argument is absent, mean "each" must be named

  int each = tstring::str2int(arg);

  // the separator char has been set at the back by default
  string sep;

  sep = macro_args[num_args-1];


  // processing the input in case where "each" has been specified
  vector<string> elmnts;

  if(argPos != macro_args.end())
    elmnts  = tstring::split(tstring::removeEnclosingChar(macro_args[0], '"', '"', true), ',');
  else
    elmnts.push_back(what);

  string out;

  for(int i = 0; i < num; ++i) {
    for(auto e = 0; e < elmnts.size(); ++e) {
      for(int j = 0; j < each; ++j) {
        out += elmnts[e] + sep;
      }
    }
  }
  // remove last separator
  if(sep == ",")
    out = tstring::removeLastCharOf(out, ',');

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::seq
// ----------------------------------------------------------------------------------------
string ParamsParser::seq (const string& argstr)
{
  string syntax="seq(from, to, by, sep=\",\")";

  int max_arg = 4;

  vector<string> macro_args = getMacroArgs(argstr, 3, max_arg, "seq", syntax, true);

  string sep = macro_args[ 3 ];

  for (int i = 0; i < 3; ++i) {

    if(!tstring::isanumber(macro_args[i]))
      fatal("seq(): argument %i must be a number in: seq(%s)\n", i+1, argstr.c_str());

  }

  // now create the sequence of numbers:
  string out;

  int index = 0;
  double current = tstring::str2dble(macro_args[0]);
  double to = tstring::str2dble(macro_args[1]);
  double i = tstring::str2dble(macro_args[2]);

  double sign = 1.0; // indicate whether the sequence is increasing (+1.0) or decreasing (-1.0)

  // check if the sequence is decreasing
  if(current > to) {

    sign = -1.0;

    if(i > 0)
      fatal("seq(): third argument \"by\" must be a negative number when from > to in: seq(%s).\n", argstr.c_str());

//      i *= sign; // must be negative when decrement
  }

  // check the sign of the increment if increasing sequence
  if(current < to && i < 0) {

    fatal("seq(): third argument \"by\" must be a positive number when from < to in: seq(%s).\n", argstr.c_str());

//    i *= -1.0; //make it positive
  }
  // loop until reaching "to"
  while(sign*(current-to) < 1.0e-15) { // to avoid precision errors, it's a workaround, setting epsilon to a fixed value

    out += tstring::dble2str(current);

    current += i;

    // check next value to place the separator thus avoiding adding a separator after the last value
    if(sign*(current-to) < 1.0e-15)
      out += sep;
  };

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::tempseq
// ----------------------------------------------------------------------------------------
string ParamsParser::tempseq (const string& argstr)
{
  string syntax = "tempseq(at, seq) ";

  int max_arg = 2, min_arg = 2;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "tempseq", syntax, false);

  //get arg values by name:
  string arg;
  vector<string>::const_iterator argPos;


  // look for a named argument

  // check if arg "at" is named ( at="..." )
  auto has_at = [] (const string& s) {return (s.find("at") != string::npos);};

  argPos = find_if(macro_args.begin(), macro_args.end(), has_at);

  if( argPos != macro_args.end() )
    arg = getMacroParamValue(*argPos, "at", syntax);
  else
    arg = macro_args[0];  // default assignment by position

  vector<string> at_time = tstring::split(tstring::removeEnclosingChar(arg, '"', '"', false), ',');

  if(at_time[0] != "0")
    fatal("tempseq(): argument \"at\" must start with value \'0\' (for parameter's initial value at generation 0) in: tempseq(%s).\n",argstr.c_str());

  // check if arg "seq" is named:
  auto has_seq = [] (const string& s) {return (s.find("seq") != string::npos);};

  argPos = find_if(macro_args.begin(), macro_args.end(), has_seq);

  if( argPos != macro_args.end() )
    arg = getMacroParamValue(*argPos, "seq", syntax);
  else
    arg = macro_args[1];  // default assignment by position

  vector<string> values = tstring::split(tstring::removeEnclosingChar(arg, '"', '"', false), ',');

  //------- process input and build output string ---------
  size_t num_gen = at_time.size();
  size_t num_val = values.size();

  if(num_val < num_gen)
    fatal("tempseq(): argument \"seq\" contains less values than \"at\" in: tempseq(%s).\n",argstr.c_str());

  if(num_val > num_gen)
    fatal("tempseq(): argument \"seq\" contains more values than \"at\" in: tempseq(%s)\n",argstr.c_str());

  string out = "(";

  for(auto i = 0; i < num_gen-1; ++i) {

    out += "@g" + at_time[i] + " " + values[ i % num_val] + ", ";
  }

  out += "@g" + at_time[num_gen-1] + " " + values[ (num_gen-1) % num_val] + ")";

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::matrix
// ----------------------------------------------------------------------------------------
string ParamsParser::matrix (const string& argstr)
{
  string syntax = "matrix(x, nrow, ncol)";

  int max_arg = 3, min_arg = 3;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "matrix", syntax, false);

  if(macro_args[0][0] != '"' && macro_args[0].back() != '"')
    fatal("matrix(): first argument must be quoted with \" in: matrix(%s)\n",argstr.c_str());

  vector<string> elmnts = tstring::split(tstring::removeEnclosingChar(macro_args[0], '"', '"', false), ',');

  unsigned int nrow = tstring::str2uint(macro_args[1]);
  unsigned int ncol = tstring::str2uint(macro_args[2]);

  if(elmnts.size() < nrow*ncol)
    fatal("matrix(): not enough elements provided, got %i, must be rows*columns = %i\n", elmnts.size(), nrow*ncol);

  if(elmnts.size() > nrow*ncol)
    fatal("matrix(): too many elements provided, got %i, must be rows*columns = %i\n", elmnts.size(), nrow*ncol);

  // start writing the matrix into the output string
  string out("{");

  for (unsigned int r = 0, e = 0; r < nrow; ++r) {

    out += "\n{";

    for (unsigned int c = 0; c < ncol - 1 && e < elmnts.size(); ++c, ++e) {

      out += elmnts[e] + ",";
    }

    out += elmnts[e++] + "}";

  }

  // close the matrix block
  out += "}";

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::diag_matrix
// ----------------------------------------------------------------------------------------
string ParamsParser::diag_matrix               (const string& argstr)
{
  string syntax = "diag(x, n)";

  int max_arg = 2, min_arg = 1;
  string out;
  string off_diag = "0";
  size_t nrow;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "diag", syntax, false);

  // the size is inferred from splitting the string provided
  vector<string> elmnts = tstring::split(tstring::removeEnclosingChar(macro_args[0], '"', '"', true), ',');

  if( macro_args.size() == 1) {

    nrow = elmnts.size();

  } else {
    // already checked that it's not >2 arguments passed

    nrow = size_t(tstring::str2uint(macro_args[1]));

    // the value may contain several elements that might need to be repeated:
    if(elmnts.size() > 1 && elmnts.size() != nrow) {

      fatal("diag(): the number of elements to use for the diag matrix is different from parameter \"n\" in: diag(%s)\n\
          use quoted rep() if you intend to repeat a pattern, eg: diag(q(rep(..., n1)),n2)\n", syntax.c_str());

    } else if (elmnts.size() == 1){

       // build the vector of elements used below

      for(unsigned int i = 0; i < nrow-1; ++i)
        elmnts.push_back(elmnts[0]);
    }
  }

  out += "{";

  for (unsigned int r = 0, c, e = 0; r < nrow; ++r) {

    out += "\n{";

    for (c = 0; c < nrow - 1 && e < elmnts.size(); ++c) {

      if(c == r)
        out += elmnts[e++] + ",";
      else
        out += off_diag + ",";
    }

    if(c == r)
      out += elmnts[e++] + "}";
    else
      out += off_diag + "}";

  }

  // close the matrix block
  out += "}";

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::sym_matrix
// ----------------------------------------------------------------------------------------
string ParamsParser::sym_matrix                (const string& argstr)
{
  string syntax = "smatrix(x, nrow, diag=0)";

  // first arg is row-oriented
  int max_arg = 3, min_arg = 2;
  string out;
  size_t nrow;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "smatrix", syntax, false);

  // the size is inferred from splitting the string provided
  vector<string> upper = tstring::split(tstring::removeEnclosingChar(macro_args[0], '"', '"', true), ',');

  if(!tstring::isanumber(macro_args[1]))
    fatal("smatrix(): secong argument \"nrow\" is not a number in: smatrix(%s)\n", argstr.c_str());
  else
    nrow = tstring::str2int(macro_args[1]);

  size_t num_el = nrow*(nrow-1)/2;

  //fill "upper" if only one value was passed, that value will be copied
  if( upper.size() == 1) {
    string val = upper[0];
    upper.assign(num_el, val);
  }

  // check size
  if( upper.size() != nrow*(nrow-1)/2 )
    fatal("smatrix(): number of elements (%i) passed doesn't match with the number of rows (%i) specified in: smatrix(%s)\n", upper.size(), nrow, argstr.c_str());

  // look for diagonal elements
  auto is_diag = [] (const string& s) {return (s.find("diag") != string::npos);};

  vector<string>::const_iterator diagPos = find_if(macro_args.begin(), macro_args.end(), is_diag);

  vector<string> diag;
  string diag_arg;

  if( diagPos != macro_args.end() ) // user specified the diagonal elements
  {
    diag_arg = *diagPos;
    //remove the front string "diag=" and get the argument value
    diag_arg = getMacroParamValue(diag_arg, "diag", syntax);

    diag = tstring::split(tstring::removeEnclosingChar(diag_arg, '"', '"', true), ',');

    if(diag.size() == 1) {
      string val = diag[0];
      diag.assign(nrow, val);
    }
    else if (diag.size() != nrow) {
      fatal("smatrix(): number of elements to \"diag\" doesn't match with the matrix size in: smatrix(%s)\n", argstr.c_str());
    }

  } else
    diag.assign(nrow, "0");

  // create the matrix

  TMatrix mat(nrow, nrow);

  for (unsigned int r = 0, c, e = 0, d = 0; r < nrow ; ++r) {

    for (c = r; c < nrow && e < upper.size(); ++c) {

      if(c == r)
        mat.set(r, c, tstring::str2dble(diag[d++]));
      else
        mat.set(r, c, tstring::str2dble(upper[e++]));
    }

    // catch the last diagonal element of the matrix, after having copied all off-diagonal elements
    if(c == r)
      mat.set(r, c, tstring::str2dble(diag[d++]));
  }

  // fill the lower triangle:
  for (unsigned int r = nrow-1; r > 0 ; --r) {

    for(unsigned int c = 0; c < r; ++c)

      mat.set(r, c, mat.get(c,r));

  }

  return mat.to_string();
}
//----------------------------------------------------------------------------------------
// ParamsParser::runif
// ----------------------------------------------------------------------------------------
string ParamsParser::runif (const string& argstr)
{
  string syntax = "runif(n, min=0, max=1, sep=\",\")";

  int max_arg = 4, min_arg = 1;
  double min = 0.0, max = 1.0;
  unsigned int num_deviates;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "runif", syntax, true);

  // get and check the number of deviates we need to draw
  if(!tstring::isanumber(macro_args[0]))
    fatal("runif(): first argument must be a positive integer in: runif(%s)\n", argstr.c_str());

  num_deviates = tstring::str2uint(macro_args[0]);

  if(!(num_deviates > 0))
    fatal("runif(): first argument must be a positive integer in: runif(%s)\n", argstr.c_str());


//  map<string, double> dev_param; dev_param["min"] = 0.0; dev_param["max"] = 1.0;
//
//  auto get_param_value = [&](const string& str_in){
//    vector<string> param_args = tstring::splitExcludeEnclosedDelimiters(str_in, '=');
//
//  };

  // get the min and max values, if present
  if(macro_args.size()-1 > 1){

    // omit last arg which is always the separator character

    for (unsigned int i = 1; i < macro_args.size()-1; ++i) {

      vector<string> param_args = tstring::splitExcludeEnclosedDelimiters(macro_args[i], '=');

      if(param_args[0] == "min") {

        min = tstring::str2dble(param_args[1]);

      } else if(param_args[0] == "max") {

        max = tstring::str2dble(param_args[1]);

      } else if(param_args.size() == 1) {

        // the "min=" and "max=" can be omitted

        if(i == 1)
          min = tstring::str2dble(param_args[0]);
        else if (i == 2)
          max = tstring::str2dble(param_args[0]);

      }else {

        fatal("runif() syntax is: %s\n",syntax.c_str());
      }
    }
  }

  string sep = macro_args.back();

  // compute the random deviates
  string out;

  auto draw = [min, max](){return tstring::dble2str((max-min)*RAND::Uniform() + min);};

  for(unsigned int i = 0; i < num_deviates -1; ++i)
    out += draw() + sep;

  out += draw();

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::rnorm
// ----------------------------------------------------------------------------------------
string ParamsParser::rnorm (const string& argstr)
{
  string syntax = "rnorm(n, mean=0, sd=1, sep=\",\")";

  int max_arg = 4, min_arg = 1;
  double mean = 0.0, sd = 1.0;
  unsigned int num_deviates;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "rnorm", syntax, true);

  // get and check the number of deviates we need to draw
  if(!tstring::isanumber(macro_args[0]))
    fatal("rnorm(): first argument must be a positive integer in: rnorm(%s)\n", argstr.c_str());

  num_deviates = tstring::str2uint(macro_args[0]);

  if(!(num_deviates > 0))
    fatal("rnorm(): first argument must be a positive integer in: rnorm(%s)\n", argstr.c_str());

  // get the mean and sd values, if present
  if(macro_args.size()-1 > 1){

    // omit last arg which is always the separator character

    for (unsigned int i = 1; i < macro_args.size()-1; ++i) {

      vector<string> param_args = tstring::splitExcludeEnclosedDelimiters(macro_args[i], '=');

      if(param_args[0] == "mean") {

        mean = tstring::str2dble(param_args[1]);

      } else if(param_args[0] == "sd") {

        sd = tstring::str2dble(param_args[1]);

      } else if(param_args.size() == 1) {

        // the "mean=" and "sd=" can be omitted

        if(i == 1)
          mean = tstring::str2dble(param_args[0]);
        else if (i == 2)
          sd = tstring::str2dble(param_args[0]);

      } else {

        fatal("rnorm() syntax is: %s\n",syntax.c_str());
      }

    }
  }

  string sep = macro_args.back();

  // compute the random deviates
  string out;

  auto draw = [mean, sd](){return tstring::dble2str(mean + RAND::Gaussian(sd));};

  for(unsigned int i = 0; i < num_deviates -1; ++i)
    out += draw() + sep;

  out += draw();

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::rpoiss
// ----------------------------------------------------------------------------------------
string ParamsParser::rpoiss (const string& argstr)
{
  string syntax = "rpois(n, mean=1, sep=\",\")";

  int max_arg = 3, min_arg = 1;
  double mean = 1.0;
  unsigned int num_deviates;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "rpois", syntax, true);

  // get and check the number of deviates we need to draw
  if(!tstring::isanumber(macro_args[0]))
    fatal("rpois(): first argument must be a positive integer in: rpois(%s)\n", argstr.c_str());

  num_deviates = tstring::str2uint(macro_args[0]);

  if(!(num_deviates > 0))
    fatal("rpois(): first argument must be a positive integer in: rpois(%s)\n", argstr.c_str());


  // get the mean value, if present
  // we omit last arg macro_args[2] which is always the separator character
  if(macro_args.size() == 3){

    vector<string> param_args = tstring::splitExcludeEnclosedDelimiters(macro_args[1], '=');

    if(param_args[0] == "mean") {

      mean = tstring::str2dble(param_args[1]);

    } else if(param_args.size() == 1) {

      // the string "mean=" can be omitted by user

      mean = tstring::str2dble(param_args[0]);

    } else {

      fatal("rpois() syntax is: %s\n",syntax.c_str());
    }
  }

  string sep = macro_args.back();

  // compute the random deviates
  string out;

  auto draw = [mean](){return tstring::dble2str(RAND::Poisson(mean));};

  for(unsigned int i = 0; i < num_deviates -1; ++i)
    out += draw() + sep;

  out += draw();

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::rbernoul
// ----------------------------------------------------------------------------------------
string ParamsParser::rbernoul (const string& argstr)
{
  string syntax = "rbernoul(n, p=0.5, sep=\",\")";

  int max_arg = 3, min_arg = 1;
  double p = 0.5;
  unsigned int num_deviates;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "rbernoul", syntax, true);

  // get and check the number of deviates we need to draw
  if(!tstring::isanumber(macro_args[0]))
    fatal("rbernoul(): first argument must be a positive integer in: rbernoul(%s)\n", argstr.c_str());

  num_deviates = tstring::str2uint(macro_args[0]);

  if(!(num_deviates > 0))
    fatal("rbernoul(): first argument must be a positive integer in: rbernoul(%s)\n", argstr.c_str());

  // get the mean value, if present
  // we omit last arg macro_args[2] which is always the separator character
  if(macro_args.size() == 3){

    vector<string> param_args = tstring::splitExcludeEnclosedDelimiters(macro_args[1], '=');

    if(param_args[0] == "p") {

      p = tstring::str2dble(param_args[1]);

    } else if(param_args.size() == 1) {

      // the string "mean=" can be omitted by user

      p = tstring::str2dble(param_args[0]);

    } else {

      fatal("rbernoul() syntax is: %s\n",syntax.c_str());
    }
  }

  string sep = macro_args.back();

  // compute the random deviates
  string out;

  auto draw = [p](){return tstring::dble2str(RAND::Bernoulli(p));};

  for(unsigned int i = 0; i < num_deviates -1; ++i)
    out += draw() + sep;

  out += draw();

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::rexp
// ----------------------------------------------------------------------------------------
string ParamsParser::rexp (const string& argstr)
{
  string syntax = "rexp(n, mean=1, sep=\",\")";

  int max_arg = 3, min_arg = 2;
  double mean=1.0;
  unsigned int num_deviates;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "rexp", syntax, true);

  // get and check the number of deviates we need to draw
  if(!tstring::isanumber(macro_args[0]))
    fatal("rexp(): first argument must be a positive integer in: rexp(%s)\n", argstr.c_str());

  num_deviates = tstring::str2uint(macro_args[0]);

  if(!(num_deviates > 0))
    fatal("rexp(): first argument must be a positive integer in: rexp(%s)\n", argstr.c_str());


  // get the mean value, if present
  // we omit last arg macro_args[2] which is always the separator character
  if(macro_args.size() == 3){

    vector<string> param_args = tstring::splitExcludeEnclosedDelimiters(macro_args[1], '=');

    if(param_args[0] == "mean") {

      mean = tstring::str2dble(param_args[1]);

    } else if(param_args.size() == 1) {

      // the string "mean=" can be omitted by user

      mean = tstring::str2dble(param_args[0]);

    } else {

      fatal("rexp() syntax is: %s\n",syntax.c_str());
    }
  }

  string sep = macro_args.back();

  // compute the random deviates
  string out;

  auto draw = [mean](){return tstring::dble2str(RAND::Exponential(mean));};

  for(unsigned int i = 0; i < num_deviates -1; ++i)
    out += draw() + sep;

  out += draw();

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::rgamma
// ----------------------------------------------------------------------------------------
string ParamsParser::rgamma (const string& argstr)
{
  string syntax = "rgamma(n, a, b, sep=\",\")";

  // a and b params are mandatory, with no default

  int max_arg = 4, min_arg = 3;
  double a = 0.0, b = 1.0;
  unsigned int num_deviates;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "rgamma", syntax, true);

  // get and check the number of deviates we need to draw
  if(!tstring::isanumber(macro_args[0]))
    fatal("rgamma(): first argument must be a positive integer in: rgamma(%s)\n", argstr.c_str());

  num_deviates = tstring::str2uint(macro_args[0]);

  if(!(num_deviates > 0))
    fatal("rgamma(): first argument must be a positive integer in: rgamma(%s)\n", argstr.c_str());


  // omit last arg which is always the separator character

  for (unsigned int i = 1; i < macro_args.size()-1; ++i) {

    vector<string> param_args = tstring::splitExcludeEnclosedDelimiters(macro_args[i], '=');

    if(param_args[0] == "a") {

      a = tstring::str2dble(param_args[1]);

    } else if(param_args[0] == "b") {

      b = tstring::str2dble(param_args[1]);

    } else if(param_args.size() == 1) {

      // the strings "a=" or "b=" can be omitted by the user
      // we assume meaning by position

      if(i == 1)
        a = tstring::str2dble(param_args[0]);
      else if (i == 2)
        b = tstring::str2dble(param_args[0]);

    } else {

      fatal("rgamma() syntax is: %s.\n",syntax.c_str());
    }
  }

  if(a < 0)
    fatal("rgamma(): shape parameter \"a\" must be positive in %s.\n",syntax.c_str());

  if(b < 0)
    fatal("rgamma(): rate parameter \"b\" must be positive in %s.\n",syntax.c_str());


  string sep = macro_args.back();

  // compute the random deviates
  string out;

  auto draw = [a, b](){return tstring::dble2str(RAND::Gamma(a,b));};

  for(unsigned int i = 0; i < num_deviates -1; ++i)
    out += draw() + sep;

  out += draw();

  return out;
}
//----------------------------------------------------------------------------------------
// ParamsParser::rlognorm
// ----------------------------------------------------------------------------------------
string ParamsParser::rlognorm (const string& argstr)
{
  string syntax = "rlognorm(n, mean, sd, sep=\",\")";

  // a and b params are mandatory, with no default

  int max_arg = 4, min_arg = 3;
  double a = 0.0, b = 1.0;
  unsigned int num_deviates;

  vector<string> macro_args = getMacroArgs(argstr, min_arg, max_arg, "rlognorm", syntax, true);

  // get and check the number of deviates we need to draw
  if(!tstring::isanumber(macro_args[0]))
    fatal("rlognorm(): first argument must be a positive integer in: rlognorm(%s)\n", argstr.c_str());

  num_deviates = tstring::str2uint(macro_args[0]);

  if(!(num_deviates > 0))
    fatal("rlognorm(): first argument must be a positive integer in: rlognorm(%s)\n", argstr.c_str());


  // omit last arg which is always the separator character

  for (unsigned int i = 1; i < macro_args.size()-1; ++i) {

    vector<string> param_args = tstring::splitExcludeEnclosedDelimiters(macro_args[i], '=');

    if(param_args[0] == "mean") {

      a = tstring::str2dble(param_args[1]);

    } else if(param_args[0] == "sd") {

      b = tstring::str2dble(param_args[1]);

    } else if(param_args.size() == 1) {

      // the strings "mean=" or "sd=" can be omitted by the user
      // we assume meaning by position

      if(i == 1)
        a = tstring::str2dble(param_args[0]);
      else if (i == 2)
        b = tstring::str2dble(param_args[0]);

    } else {

      fatal("rlognorm() syntax is: %s\n",syntax.c_str());
    }
  }

  string sep = macro_args.back();

  // compute the random deviates
  string out;

  auto draw = [a, b](){return tstring::dble2str(RAND::LogNormal(a,b));};

  for(unsigned int i = 0; i < num_deviates -1; ++i)
    out += draw() + sep;

  out += draw();

  return out;
}
//----------------------------------------------------------------------------------------
// StreamParser::read
// ----------------------------------------------------------------------------------------
bool StreamParser::read(const char* stream)
  {
    int linecnt = 0;    
    //output string to collect parameter arguments
    string args;
    //string to store parameter name
    string key;
    
    ParamsParser::reset_inputParams();
    
    //--------------------------------------------------------------------------------
    char c = 0, eof = '\0';
    //put the char stream into a string
    string input_stream(stream);
    //add the terminating character to the string:
    input_stream += eof;
    
    //check if LF 
    if(input_stream.find_first_of('\r') != string::npos) {
      //guess if NL is CRLF as in DOS files:
      if(input_stream.find_first_of('\n') == input_stream.find_first_of('\r') + 1)
        replaceCR(input_stream, ' '); //remove CR
      else
        replaceCR(input_stream); //replaces CR with LF
    }
    
    //initiate the input stringstream:
    istringstream IN(input_stream);

    //--------------------------------------------------------------------------------
    //read the file line by line
    while(IN.good() && !IN.eof()) {
      
      linecnt++;
      
      // remove the forgoing space of the line, returns false if EOL is reached
      if(!removeSpaceAndComment(IN, linecnt)) continue;

      //--------------------------------------------------------------------------------
      //read the parameter name and record it in 'key':
      
      key="";
      while(IN.get(c) && IN.good() && !IN.eof() && c != EOL && !isspace(c)){
        //whitespace is the split char between a parameter and its argument
        //this basically does the same as IN>>key, but we have to check for comments:
        if(c == '#'){
          removeComment(IN, linecnt);
          break;
        }
        
        key += c;
      } //__end_while_key__
      
      if(c == EOL || !removeSpaceAndComment(IN, linecnt) ) {
        if(c != eof){
          if(key.size() != 0) {//boolean param
            args = "1";
            ParamsParser::add_inputParam(key, args);
          }
#ifdef _DEBUG_
          message("  line %i: %s (1)\n", linecnt, key.c_str());
#endif 
        }
          continue;
      }
                      
#ifdef _DEBUG_
      message("  line %i: %s (", linecnt, key.c_str());
#endif
            
      //--------------------------------------------------------------------------------
      //read the arguments:
      args = "";
      while( readArguments(IN, linecnt, args) );
      
      //remove any trailling space on the argument string
      int i;
      for(i=(int)args.size()-1; i>=0; --i){
        if(!isspace(args[i])) break;
      }
      args[i+1]='\0';
      
      //--------------------------------------------------------------------------------
      // store the key - args pair in the _inputParam table
      ParamsParser::add_inputParam(key, args.c_str());
      
#ifdef _DEBUG_
      message("%s)\n", args.c_str());
#endif
      //reset the stream state, is changed by operator >>
      IN.clear(ios::goodbit);
            
    }//__END__WHILE__
    
    return true;
  }
//----------------------------------------------------------------------------------------
// StreamParser::removeSpaceAndComment
//----------------------------------------------------------------------------------------
/**Removes whitespace char on a line until a non-ws or EOL is reached.
* Returns false if EOL or EOF is reached or true otherwise.
*/
bool StreamParser::removeSpaceAndComment(istream & IN, int& l_count, bool keepLast)
{
  char c;
  
  while(IN.get(c) && IN.good() && !IN.eof() && c != EOL) {
        
    if(!isspace(c)){
      
      if(c=='#') return removeComment(IN, l_count); // recursively remove comment
      
      IN.putback(c); //this is a parameter: put the character back
      
      if(keepLast) {
        //get back one char:
        IN.unget();
        IN.get(c); //read it again and check if whitespace:
        if(isspace(c)) IN.putback(c);
      }
      
      return true;
    }
  }
  return false;
}
//----------------------------------------------------------------------------------------
// StreamParser::removeComment
//----------------------------------------------------------------------------------------
/**Recusively removes comments until the end of a line/of the file, or of a block comment is reached.
*  
* #: commented line (removed until the end of the line is reached)
* #/ ... /#: a block comment (may span several lines)
* Consecutive lines of comments are also removed, even if not part of a block comment.
* Note: this function always returns false, unless something remains on a line after a 
* block comment (i.e. if removeSpaceAndComment() returns true)
*/
bool StreamParser::removeComment (istream & IN, int& l_count)
{
  char c;
  
  //remember: we enter the loop with c = '#'
  //check if next char is the start of a block comment:
  bool isBlock = (IN.peek() == '/');
  bool prevIsComment = true;
  
  while(IN.get(c) && IN.good() && !IN.eof()){
    
    //break if EOL && next line not a comment, continue otherwise:
    if (c == EOL){
      //check if next line is also a comment, start the loop again:
      if (IN.peek() == '#') {
        
        IN.get(c);
        prevIsComment = true;
        ++l_count;
        continue;
        
      //continue if within a block comment:
      } else if( isBlock ) {
        ++l_count;
        continue;
      //next line not a comment, get out of the loop:
      } else
        return false;
    }
    
    //check if we had the block str '#/' within a commented line
    if (c == '/'){ 
      
      if(IN.peek() == '#') {
        IN.get(c);
        
        if(isBlock)
          //block termination string '/#', remove trailing space
          //note: the string '#/#' is also considered as a terminating string
          return removeSpaceAndComment(IN, l_count);
        
      } else if(prevIsComment) { 
        //we've got '#/', a block comment may start anywhere on a comment line
        isBlock = true;
      }
    }
    
    if(c == '#') prevIsComment = true;
    else prevIsComment = false;
    
  } //__END_WHILE__
  
  return false;
}
//----------------------------------------------------------------------------------------
// StreamParser::readArguments
//----------------------------------------------------------------------------------------
bool StreamParser::readArguments(istream & IN, int& l_count, string& args)
{
  char c;
  
  //read one byte at a time
  while(IN.get(c) && IN.good() && !IN.eof() && c != EOL){

    if(c == '\\')
      eatLine(IN, l_count);
    //block delimiters: the block will be parsed in Param
    else if(c == '{')
      args += readUntilCharacter(IN, l_count, c, '}');
    else if(c == '(')
      args += readUntilCharacter(IN, l_count, c, ')');
    else if(c == '[')
      args += readUntilCharacter(IN, l_count, c, ']');
    else if(c == '\"')
      args += readUntilCharacter(IN, l_count, c, '\"');
    //comment delimiter:
    else if(c == '#') {
      if(!removeComment(IN, l_count))
        return false; // reached end of line

    //argument in external file
    } else if (c == '&') {
      
      //read the file name
      string file;

      while(IN.get(c) && IN.good() && !IN.eof() && !isspace(c)){

        if(c=='#'){      // check if a comment is included
          IN.putback(c);                                       // comment
          if(!removeSpaceAndComment(IN, l_count)) break;
        }
        file += c;
      }

      if(c==EOL || isspace(c)) IN.putback(c);

      //put the file name back into the arg, will be processed later
      args += "&" + file + " ";

    } else
      args += c;
    
  }//end while read args

  if(c == EOL || IN.eof())  return false;
  
  if(!IN.good())  //this may happen if an external file doesn't end with a \n
    fatal("problem reading input file; line %i appears corrupted, skipping line\n",l_count);
  
  return true; 
}
//----------------------------------------------------------------------------------------
// StreamParser::readUntilCharacter
//----------------------------------------------------------------------------------------
string StreamParser::readUntilCharacter(istream &IN, int& l_count, const char start_c, const char end_c)
{
  string out;
  char c;
  bool closed = false;
    
  out += start_c;

  //cout << "start block : "<<start_c<<endl;
  while (IN.get(c) && IN.good() && !IN.eof() ) {
    
    if(c == EOL) //a block can span several lines
    
      ++l_count;
    
    else if(c == end_c) {
      
      out += c;
      //remove trailing spaces but keep last one, it's the arg seperator
      if(!removeSpaceAndComment(IN, l_count, true)) {
        IN.putback(EOL); //we've reached eol, put it back
      }
      
      closed = true;
      break;
    
    } else if(c == start_c) {//nested blocks

      out += readUntilCharacter(IN, l_count, start_c, end_c);
    
    } else if(c == '\\') {//line continuation within a block
      
      eatLine(IN, l_count);
      
    } else if(c == '#') {
    
      if(!removeComment(IN, l_count)) l_count++;
    
    } else out += c;
  } //__end_while__
  
  if(!closed) fatal("missing closing character '%c' on line %i.\n", end_c, l_count);

  return out;
}
//----------------------------------------------------------------------------------------
// StreamParser::read
// ----------------------------------------------------------------------------------------
void StreamParser::eatLine (istream & IN, int& l_count)
{
  char c;
  while(IN.get(c) && IN.good() && c != EOL && !IN.eof());
  ++l_count;
}
//----------------------------------------------------------------------------------------
// StreamParser::removeCR
// ----------------------------------------------------------------------------------------
void StreamParser::replaceCR ( string& stream, const char rpl )
{
  size_t pos = 0;
  
  while ( (pos = stream.find_first_of('\r', pos)) != string::npos) {
    stream[pos] = rpl;
  }
}

