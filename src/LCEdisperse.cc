/** $Id: LCEdisperse.cc,v 1.14.2.8 2017-06-09 09:04:16 fred Exp $
 *
 *  @file LCEdisperse.cc
 *  Nemo2
 *
 *   Copyright (C) 2006-2015 Frederic Guillaume
 *   frederic.guillaume@ieu.uzh.ch
 *
 *   This file is part of Nemo
 *
 *   Nemo is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Nemo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *  
 *  Created on @date 08.07.2004
 *  @author fred
 */

#include <stdlib.h>
#include <string>
#include <map>
#include <iterator>
#include <cmath>
#include "LCEdisperse.h"
#include "metapop.h"
#include "individual.h"
#include "Uniform.h"

using namespace std;
// ------------------------------------------------------------------------------

//                             LCE_Disperse_base

// ----------------------------------------------------------------------------------------
LCE_Disperse_base::LCE_Disperse_base () 
: LifeCycleEvent("", ""), _disp_model(-1), _disp_propagule_prob(-1.0), _PropaguleTargets()
, _fem_rate (-1), _mal_rate(-1), _isForward(1), _isByNumber(0), _npatch(0), _nstages(1)
{
  _DispMatrix[0] = NULL;
  _DispMatrix[1] = NULL;
}
// ----------------------------------------------------------------------------------------
LCE_Disperse_base::~LCE_Disperse_base ()
{
  if(NULL != _DispMatrix[0])
    delete _DispMatrix[0];
  
  if(NULL != _DispMatrix[1])
    delete _DispMatrix[1];
  
  for (unsigned int i = 0; i < _postDispPop.size(); ++i) {
    if(_postDispPop[i]) delete _postDispPop[i]; //!! careful when deleting individuals here...
  }
  _postDispPop.clear();
}
// ----------------------------------------------------------------------------------------
void LCE_Disperse_base::addParameters (string prefix, ParamUpdaterBase* updater)
{
  add_parameter(prefix + "_model",INT,false,true,1,4,updater);
  add_parameter(prefix + "_border_model",INT,false,true,1,3,updater);
  add_parameter(prefix + "_lattice_range",INT,false,true,1,2,updater);
  add_parameter(prefix + "_lattice_rows",INT,false,false,0,0,updater);
  add_parameter(prefix + "_lattice_columns",INT,false,false,0,0,updater);
  add_parameter(prefix + "_propagule_prob",DBL,false,true,0,1,updater);
  add_parameter(prefix + "_matrix",MAT,false,false,0,0,updater);
  add_parameter(prefix + "_matrix_fem",STR,false,false,0,0,updater);
  add_parameter(prefix + "_matrix_mal",STR,false,false,0,0,updater);
  add_parameter(prefix + "_rate",DBL,false,true,0,1,updater);
  add_parameter(prefix + "_rate_fem",DBL,false,true,0,1,updater);
  add_parameter(prefix + "_rate_mal",DBL,false,true,0,1,updater);
  add_parameter(prefix + "_connectivity_matrix",MAT,false,false,0,0,updater);
  add_parameter(prefix + "_reduced_matrix",MAT,false,false,0,0,updater);
  add_parameter(prefix + "_connectivity_matrix_fem",MAT,false,false,0,0,updater);
  add_parameter(prefix + "_reduced_matrix_fem",MAT,false,false,0,0,updater);
  add_parameter(prefix + "_connectivity_matrix_mal",MAT,false,false,0,0,updater);
  add_parameter(prefix + "_reduced_matrix_mal",MAT,false,false,0,0,updater);
  add_parameter(prefix + "_stage", INT, false,false,0,0,0);

  add_parameter(prefix + "_by_number", BOOL,false,false,0,0,0);
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setParameters
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::setBaseParameters(string prefix)
{
  _prefix = prefix;
  
  _npatch = _popPtr->getPatchNbr();
  
  setPostDispPop();
  
  _disp_model = (int)_paramSet->getValue(prefix + "_model");
  
  _disp_propagule_prob = _paramSet->getValue(prefix + "_propagule_prob");
  
  if(get_parameter(prefix + "_by_number")->isSet())
    _isByNumber = true;
  else
    _isByNumber = false;

  // STAGE VALUES -----------------------------------------------------------------------

  TMatrix tmpmat;
  Param* param = get_parameter(prefix + "_stage");

  _stages.clear();

  if(param->isSet()){

    if(param->isMatrix()) {

      param->getMatrix(&tmpmat);

      if(tmpmat.ncols() > _popPtr->getNumAgeClasses())
        return error("\"%s_stage\" has more elements than the number of stages in \"pop_age_structure\"\n");

      _nstages = tmpmat.ncols();

      for (unsigned int i = 0; i < _nstages; ++i) {

        if(tmpmat.get(0,i) < 0 || tmpmat.get(0,i) > _popPtr->getNumAgeClasses()-1)
          return error("elements in \"%s_stage\" are out of bounds, must be between 0 and %i (included)\n",
              prefix.c_str(), _popPtr->getNumAgeClasses()-1);

        _stages.push_back(tmpmat.get(0,i));
      }

    }
    else {

      _nstages = 1;

      if(param->getValue() < 0 || param->getValue() > _popPtr->getNumAgeClasses()-1)
        return error("value of \"%s_stage\" is out of bounds, must be between 0 and %i (included)\n",
            prefix.c_str(), _popPtr->getNumAgeClasses()-1);

      _stages.push_back(param->getValue());
    }
  }
  else { // default stage is 0
    _nstages = 1;
    _stages.push_back(0);
  }

  // SET MATRICES FROM INPUT MODELS --------------------------------------------------------

  if(_DispMatrix[0]) delete _DispMatrix[0];
  _DispMatrix[0] = NULL;

  if(_DispMatrix[1]) delete _DispMatrix[1];
  _DispMatrix[1] = NULL;

  //skip this if there is only one patch
  if(_npatch == 1) {
    warning("not setting the dispersal LCE whith only one patch in the population\n");
    return false;
  }
  //  if(_nstages > 1)
//    return setBaseParametersMultiStage();

  // single-stage parameters
  // connectivity matrix is specified in input --------------------------------------------
  if( (_paramSet->isSet(prefix + "_connectivity_matrix")     && _paramSet->isSet(prefix + "_reduced_matrix") ) ||
      (_paramSet->isSet(prefix + "_connectivity_matrix_mal") && _paramSet->isSet(prefix + "_reduced_matrix_mal") ) ||
      (_paramSet->isSet(prefix + "_connectivity_matrix_fem") && _paramSet->isSet(prefix + "_reduced_matrix_fem")) )
  {
    
    _disp_model=0;
        
    // both sex identical matrices
    if(_paramSet->isSet(prefix + "_connectivity_matrix")     && _paramSet->isSet(prefix + "_reduced_matrix")){
      if( ! setReducedMatricesBySex(FEM, (*get_parameter(prefix + "_connectivity_matrix")), (*get_parameter(prefix + "_reduced_matrix"))) )
        return false;
      if( ! setReducedMatricesBySex(MAL, (*get_parameter(prefix + "_connectivity_matrix")), (*get_parameter(prefix + "_reduced_matrix"))) )
         return false;
    }
    
    // female specific matrices
    if(_paramSet->isSet(prefix + "_connectivity_matrix_fem")     && _paramSet->isSet(prefix + "_reduced_matrix_fem")){
      if( ! setReducedMatricesBySex(FEM, (*get_parameter(prefix + "_connectivity_matrix_fem")), (*get_parameter(prefix + "_reduced_matrix_fem"))) )
        return false;
    }

    // male specific matrices    
    if(_paramSet->isSet(prefix + "_connectivity_matrix_mal")     && _paramSet->isSet(prefix + "_reduced_matrix_mal")){
      if( ! setReducedMatricesBySex(MAL, (*get_parameter(prefix + "_connectivity_matrix_mal")), (*get_parameter(prefix + "_reduced_matrix_mal"))) )
        return false;
      }
      
    if( (_paramSet->isSet(prefix + "_connectivity_matrix_fem") && !_paramSet->isSet(prefix + "_connectivity_matrix_mal")  ) ||
        (_paramSet->isSet(prefix + "_connectivity_matrix_mal") && !_paramSet->isSet(prefix + "_connectivity_matrix_fem")  ))
      return error("both \"%s_connectivity_matrix\" and \"%s_reduced_matrix\" must be set for each sex together\n", prefix.c_str(), prefix.c_str());
    
#ifdef _DEBUG_
    cout << "=== female reduced dispersal matrix ===\n";
    cout << "    stage = "<<_stages[0]<<endl;

    for (unsigned int i = 0; i < _npatch; ++i) {
      cout << "  [";
      for (unsigned int k = 0; k < _reducedDispMat[FEM][i].size(); k++) {
        cout << _reducedDispMatProba[FEM][i][k] << " [" << _reducedDispMat[FEM][i][k] <<"] ";
      }
      cout<<"]\n";
    }

    cout << "=== male reduced dispersal matrix ===\n";
    cout << "    stage = "<<_stages[0]<<endl;
    for (unsigned int i = 0; i < _npatch; ++i) {
      cout << "  [";
      for (unsigned int k = 0; k < _reducedDispMat[MAL][i].size(); k++) {
        cout << _reducedDispMatProba[MAL][i][k] << " [" << _reducedDispMat[MAL][i][k] <<"] ";
      }
      cout<<"]\n";

    }
#endif

  }
  
  // dispersal matrix is specified in input -----------------------------------------------
  else if ( (_paramSet->isSet(prefix + "_connectivity_matrix") && 
             !_paramSet->isSet(prefix + "_reduced_matrix")) || 
            (!_paramSet->isSet(prefix + "_connectivity_matrix") && 
             _paramSet->isSet(prefix + "_reduced_matrix")))
    
  {
    return error("both \"%s_connectivity_matrix\" and \"%s_reduced_matrix\" must be set together\n", prefix.c_str(), prefix.c_str());
  }
  
  // connectivity or reduced matrices not given in input
  else
  
  {
    
    if(_paramSet->isSet(prefix + "_matrix")) {

      _DispMatrix[0] = new TMatrix();
      
      _paramSet->getMatrix(prefix + "_matrix",_DispMatrix[0]);
      
      //same dispersal matrix for males and females
      _DispMatrix[1] = new TMatrix(*_DispMatrix[0]);
      
    } else {

      if(_paramSet->isSet(prefix + "_matrix_fem")) {
        
        _DispMatrix[FEM] = new TMatrix();
        
        _paramSet->getMatrix(prefix + "_matrix_fem",_DispMatrix[FEM]);
        
      }
      
      if(_paramSet->isSet(prefix + "_matrix_mal")) {
        
        _DispMatrix[MAL] = new TMatrix();
        
        _paramSet->getMatrix(prefix + "_matrix_mal",_DispMatrix[MAL]);
        
      }
    }
    
    if( _paramSet->isSet(prefix + "_matrix") || 
       ( _paramSet->isSet(prefix + "_matrix_fem") && _paramSet->isSet(prefix + "_matrix_mal") )  )
    {

      if(  ( _paramSet->isSet(prefix + "_rate") ||
           ( _paramSet->isSet(prefix + "_rate_fem") &&  _paramSet->isSet(prefix + "_rate_mal")) )
           || _paramSet->isSet(prefix + "_model") )
        warning("parameter \"dispersal_matrix\" takes precedence over parameters \"dispersal_rate\" and \"dispersal_model\"\n");
      
      _disp_model = 0;
      
      if(_DispMatrix[FEM]) {

        //check if we want the Identity matrix
        if(_DispMatrix[FEM]->length() == 1 && _DispMatrix[FEM]->get(0,0) == 1)
          setIndentityDispMatrix(_DispMatrix[FEM]);

        //check dimensions
        if(_DispMatrix[FEM]->length() != _npatch*_npatch)
          return error("the size of the female dispersal matrix is not equal to patch_number X patch_number (%i[%i,%i] != %i)!\n",
                _DispMatrix[FEM]->length(),_DispMatrix[FEM]->getNbRows(),_DispMatrix[FEM]->getNbCols(),_npatch*_npatch);

        //check that migration rates sum to 1 per row or column
        if(!_isByNumber) {
        if(_isForward) {
          if(!checkForwardDispersalMatrix(_DispMatrix[FEM])) return false;
        } else {
          if(!checkBackwardDispersalMatrix(_DispMatrix[FEM])) return false;
        }

        }
      }
      
      if(_DispMatrix[MAL]) {

        //check if we want the Identity matrix
        if(_DispMatrix[MAL]->length() == 1 && _DispMatrix[MAL]->get(0,0) == 1)
          setIndentityDispMatrix(_DispMatrix[MAL]);

        //check dimensions
        if(_DispMatrix[MAL]->length() != _npatch*_npatch)
          return error("the size of the male dispersal matrix is not equal to patch_number X patch_number (%i[%i,%i] != %i)!\n",
                _DispMatrix[MAL]->length(),_DispMatrix[MAL]->getNbRows(),_DispMatrix[MAL]->getNbCols(),_npatch*_npatch);

        //check that migration rates sum to 1 per row or column
        if(!_isByNumber) {
        if(_isForward) {
          if(!checkForwardDispersalMatrix(_DispMatrix[MAL])) return false;
        } else {
          if(!checkBackwardDispersalMatrix(_DispMatrix[MAL])) return false;
        }
      }
      }
      
      setReducedDispMatrix();

    // dispersal rate is given in input ---------------------------------------------------
    } else {

      if(!_paramSet->isSet(prefix + "_model")) return error("Dispersal model is not set!\n");

      if(_paramSet->isSet(prefix + "_rate")) 
        
      {
        _fem_rate = _mal_rate = _paramSet->getValue(prefix + "_rate");
        
        if(!setDispMatrix()) return false;
      }
      
      else if(  _paramSet->isSet(prefix + "_rate_fem") &&  _paramSet->isSet(prefix + "_rate_mal")  ) 
        
      {
        _fem_rate = _paramSet->getValue(prefix + "_rate_fem");
        
        _mal_rate = _paramSet->getValue(prefix + "_rate_mal");
        
        if(!setDispMatrix()) return false;
      }
      
      else {
        return error("Dispersal rate parameters not set!\n");
      }
      
    }  
    
  }
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setReducedMatricesBySex
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::setReducedMatricesBySex (sex_t SEX, Param& connectivity, Param& rate)
{
  string sex = (SEX == FEM ? "female" : "male");

  connectivity.getVariableMatrix(&_reducedDispMat[SEX]);

  rate.getVariableMatrix(&_reducedDispMatProba[SEX]);

  if(_reducedDispMat[SEX].size() != _npatch)
    return error("The %s connectivity dispersal matrix must have as many rows as the number of patches in the population\n", sex.c_str());

  if(_reducedDispMatProba[SEX].size() != _npatch)
    return error("The %s reduced dispersal rate matrix must have as many rows as the number of patches in the population\n", sex.c_str());

  if (_reducedDispMat[SEX].size() != _reducedDispMatProba[SEX].size())
    return error("The %s connectivity and reduced dispersal matrices don't have same number of rows\n", sex.c_str());

  // check match between the two matrices
  for (unsigned int i = 0; i < _reducedDispMat[SEX].size(); ++i) {

    if (_reducedDispMat[SEX][i].size() != _reducedDispMatProba[SEX][i].size())
      return error("Row %i of the %s connectivity and reduced dispersal matrices are not of same size\n", i+1, sex.c_str());

    double row_sum = 0;

    // we here deduce 1 from the patch ID in the connectivity matrix
    // this is because IDs are given in range [1 - num patch] in input (in principle)
    for (unsigned int j = 0; j < _reducedDispMat[SEX][i].size(); ++j) {

      _reducedDispMat[SEX][i][j]--; //remove 1 to make sure that the indexes start with 0, not 1!!!! @TODO check for range first!!!

      row_sum += _reducedDispMatProba[SEX][i][j];
    }

    if(!_isByNumber && (row_sum < 0.999999 || row_sum > 1.000001))
      return error("the elements of row %i of the %s reduced dispersal matrix do not sum to 1!\n",i+1, sex.c_str());

  }
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setIndentityDispMatrix
// ----------------------------------------------------------------------------------------
void LCE_Disperse_base::setIndentityDispMatrix (TMatrix* mat)
{
  mat->reset(_npatch, _npatch);
  mat->assign(0);
  for(unsigned int i = 0; i < _npatch; ++i) mat->set(i, i, 1);
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::updateDispMatrix
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::updateDispMatrix()
{
  //called by the 'execute' function when a change in patch number is detected

  if ( getDispersalModel() == 0 )
    return error("cannot update the dispersal matrix provided in input when the number of population changes.\n");
  
  return setDispMatrix();
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setPostDispPop
// ----------------------------------------------------------------------------------------
void LCE_Disperse_base::setPostDispPop ( )
{
  Patch* new_patch;
  
  if (_postDispPop.size() != 0) {
    resetPostDispPop();
  }
  
  for (unsigned int i = 0; i < _npatch; ++i) {

    new_patch = new Patch();

    //init containers with same num stages as in the whole pop to avoid complex re-indexing
    new_patch->init(_popPtr->getNumAgeClasses(), _popPtr->getPatchKFem(), _popPtr->getPatchKMal(), i);

    _postDispPop.push_back(new_patch);
  }
  
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setPostDispPop
// ----------------------------------------------------------------------------------------
void LCE_Disperse_base::resetPostDispPop ( )
{
  for (unsigned int i = 0; i < _postDispPop.size(); ++i) {
    if(_postDispPop[i]->size() != 0) _postDispPop[i]->clear(); //active pointers are lost.
    delete _postDispPop[i];//clear must be called before delete otherwise individuals are lost
  }
  _postDispPop.clear();
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::swapPostDisp
// ----------------------------------------------------------------------------------------
void LCE_Disperse_base::swapPostDisp ( )
{
  Patch *patch;
  age_idx age;

  for(unsigned int s = 0; s < _nstages; ++s) {

    age = age_idx(_stages[s]);

    for(unsigned int i = 0; i < _npatch; i++) {
      patch = _popPtr->getPatch(i);
      patch->slice(FEM, age, age, (*_postDispPop[i]), 0, _postDispPop[i]->size(FEM, age));
      patch->slice(MAL, age, age, (*_postDispPop[i]), 0, _postDispPop[i]->size(MAL, age));
      _postDispPop[i]->clear(FEM, age);
      _postDispPop[i]->clear(MAL, age);
    }
  }
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::reset_counters
// ----------------------------------------------------------------------------------------
void LCE_Disperse_base::reset_counters()
{
  for(unsigned int i = 0; i < _npatch; i++) {
    _popPtr->getPatch(i)->reset_counters();
  }
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::allocateDispMatrix
// ----------------------------------------------------------------------------------------
void LCE_Disperse_base::allocateDispMatrix (sex_t sex, unsigned int dim)
{
  if(_DispMatrix[sex] != NULL)
    _DispMatrix[sex]->reset(dim,dim);
  else
    _DispMatrix[sex] = new TMatrix(dim,dim);
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::checkDispMatrix
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::checkForwardDispersalMatrix (TMatrix* mat)
{
  double cntr;
  
  for(unsigned int i = 0; i < mat->getNbRows(); ++i) {
    cntr = 0;
    for(unsigned int j = 0; j < mat->getNbCols(); ++j)
      cntr += mat->get(i,j);
    if(cntr < 0.999999 || cntr > 1.000001) {
      error("the elements of row %i of the dispersal matrix do not sum to 1!\n",i+1);
      error("sum of row %i is: %f\n",i+1, cntr);
      return false;
    }
  }
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::checkDispMatrix
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::checkBackwardDispersalMatrix (TMatrix* mat)
{
  double cntr;

  for(unsigned int i = 0; i < mat->getNbCols(); ++i) {
    cntr = 0;
    for(unsigned int j = 0; j < mat->getNbRows(); ++j)
      cntr += mat->get(j,i);
    if(cntr < 0.999999 || cntr > 1.000001) {
      error("The elements of column %i of the dispersal matrix do not sum to 1!\n",i+1);
      error("sum of row %i is: %f\n",i+1, cntr);
      return false;
    }
  }
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setPropaguleTargets
// ----------------------------------------------------------------------------------------
void LCE_Disperse_base::setPropaguleTargets ( )
{
  unsigned int nb_patch = _popPtr->getPatchNbr();
  unsigned int tmp_array[nb_patch];
  unsigned int table_index, target_patch;
  unsigned int size, last;
  
  //shuffling algorithm:
  do {
    for(unsigned int i = 0; i < nb_patch; ++i)
      tmp_array[i] = i;
    
    size = nb_patch;
    
    for(unsigned int orig_patch = 0; orig_patch < nb_patch-1; ++orig_patch) {
      
      do{
        
        table_index = RAND::Uniform( size );
        
        target_patch = tmp_array[ table_index ];
        
      }while(target_patch == orig_patch);
      
      size--;
      
      last = tmp_array[size];
      
      tmp_array[table_index] = last;
      
      tmp_array[size] = target_patch;
    }
    //do this until the last element left is not the last patch:
  }while (tmp_array[0] == nb_patch-1);
  
  _PropaguleTargets.assign(nb_patch,0);
  
  unsigned int reverse_i = nb_patch;
  
  //we read the shuffled array in reverse order:
  for(unsigned int i=0; i < _PropaguleTargets.size(); i++) {
    _PropaguleTargets[i] = tmp_array[--reverse_i];
    
#ifdef _DEBUG_
    cout<<" -- Patch "<<i<<" : assigned Patch "<<_PropaguleTargets[i]<<endl;
#endif
    
  }
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setDispMatrix
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::setDispMatrix ()
{  

  switch ( getDispersalModel() ) {
    case 1:
      if( !setIsland_MigrantPool_Matrix() ) return false;
      break;
    case 2:
      if( !setIsland_PropagulePool_Matrix() ) return false;
      break;
    case 3:
      if( !setSteppingStone1DMatrix() ) return false;
      break;
    case 4:
      if( !setLatticeMatrix() ) return false;
      break;
    default:
      return error("Dispersal model '%i' not yet implemented\n",getDispersalModel());
  }

  if(_isForward) {
    if(!checkForwardDispersalMatrix(_DispMatrix[FEM])) return false;
    if(!checkForwardDispersalMatrix(_DispMatrix[MAL])) return false;
  } else {
    if(!checkBackwardDispersalMatrix(_DispMatrix[FEM])) return false;
    if(!checkBackwardDispersalMatrix(_DispMatrix[MAL])) return false;
  }

  return setReducedDispMatrix();
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setIsland_MigrantPool_Matrix()  (set the Island dispersal matrix)
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::setIsland_MigrantPool_Matrix()
{
#ifdef _DEBUG_
  cout<<"setIsland_MigrantPool_Matrix(_npatch="<<_npatch<<", _mal_rate="
      <<_mal_rate<<", _fem_rate="<<_fem_rate<<")"<<endl;
#endif
  allocateDispMatrix(MAL, _npatch);
  allocateDispMatrix(FEM, _npatch);
  
  TMatrix* mmat = _DispMatrix[MAL];
  TMatrix* fmat = _DispMatrix[FEM];
  double pmal = 1 - _mal_rate;
  double pfem = 1 - _fem_rate;
  double mmal = _mal_rate/(_npatch-1);
  double mfem = _fem_rate/(_npatch-1);
  
  for (unsigned int i=0; i<_npatch; ++i){
    for (unsigned int j=0; j<_npatch; ++j){
      mmat->set(i,j, mmal);
      fmat->set(i,j, mfem);
    }
  }
  
  for (unsigned int i=0; i<_npatch; ++i){
    mmat->set(i,i, pmal);
    fmat->set(i,i, pfem);
  }
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setIsland_PropagulePool_Matrix()  (set the Island dispersal matrix)
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::setIsland_PropagulePool_Matrix()
{
 #ifdef _DEBUG_
  cout<<"setIsland_PropagulePool_Matrix(_npatch="<<_npatch<<", _mal_rate="
      <<_mal_rate<<", _fem_rate="<<_fem_rate<<")"<<endl;
#endif

  allocateDispMatrix(MAL, _npatch);
  allocateDispMatrix(FEM, _npatch);

  
  if( !_paramSet->isSet(_prefix + "_propagule_prob") ) {
    error("Missing parameter \"dispersal_propagule_prob\" with dispersal model 2!\n");
    return false;
  }
  
  setPropaguleTargets();

  double propagulePHI = getPropaguleProb();
  double c1 = (1 - _fem_rate), c2 = (_fem_rate*propagulePHI),
	c3 = (_fem_rate*(1.0 - propagulePHI)/(_npatch-2));
  
  TMatrix* mmat = _DispMatrix[MAL];
  TMatrix* fmat = _DispMatrix[FEM];
  
  for (unsigned int i=0; i < _npatch; ++i){
		
    fmat->set(i, i, c1);
    
    for (unsigned int j=i+1; j < _npatch; ++j){
      fmat->set(i, j, c3);
      fmat->set(j, i, c3);
    }
    fmat->set(i, getPropaguleTarget(i), c2);
  }
  
  c1 = (1 - _mal_rate);
  c2 = (_mal_rate*propagulePHI);
  c3 = (_mal_rate*(1.0 - propagulePHI)/(_npatch-2));
  
  for (unsigned int i=0; i < _npatch; ++i){
    
    mmat->set(i, i, c1);
    
    for (unsigned int j=i+1; j< _npatch; ++j) {
      mmat->set(i, j, c3);
      mmat->set(j, i, c3);
    }
    mmat->set(i, getPropaguleTarget(i), c2);
  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setSteppingStone1DMatrix
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::setSteppingStone1DMatrix()
{
#ifdef _DEBUG_
  message("setSteppingStone1DMatrix()\n");
#endif
  int border_model = (unsigned int)get_parameter_value(_prefix + "_border_model");
  
  //check for the border model, the extra patch is the absorbing patch
  if(border_model == 3) {
    allocateDispMatrix(MAL, _npatch+1);
    allocateDispMatrix(FEM, _npatch+1);
  } else {    
    allocateDispMatrix(MAL, _npatch);
    allocateDispMatrix(FEM, _npatch);
  }
  
  TMatrix* mmat = _DispMatrix[MAL];
  TMatrix* fmat = _DispMatrix[FEM];
  
  //philopatry:
  double pmal = 1 - _mal_rate, pfem = 1 - _fem_rate;
  //migration:
  double mmal = _mal_rate/2, mfem = _fem_rate/2;
  
  fmat->assign(0);
  mmat->assign(0);
  
  //diagonal:
  for (unsigned int i = 0; i < _npatch; ++i){
    fmat->set(i, i, pfem);
    mmat->set(i, i, pmal);
  }
  //around the diagonal
  for (unsigned int i = 0; i < _npatch-1; ++i){
    fmat->set(i, i+1, mfem);
    fmat->set(i+1, i, mfem);
    mmat->set(i, i+1, mmal);
    mmat->set(i+1, i, mmal);
  }
  
  if(border_model == 3) {
    
    //absorbing boders
    //emigrants who leave the population are put in the sink patch
    fmat->set(0, _npatch, mfem);
    mmat->set(0, _npatch, mmal);
    fmat->set(_npatch -1, _npatch, mfem);
    mmat->set(_npatch -1, _npatch, mmal);
    fmat->set(_npatch, _npatch, 1);
    mmat->set(_npatch, _npatch, 1);
    
    if(!_isForward) {
      fmat->transpose();  mmat->transpose(); //this creates artificial immigration from sink
      //need to reset border patches as immigration from sink not allowed:
      fmat->set(0, 0, pfem + mfem); //the proportion not comming from the sink must be added
      fmat->set(_npatch, 0, 0);  //no immigration from sink
      fmat->set(_npatch-1, _npatch-1, pfem + mfem);
      fmat->set(_npatch, _npatch-1, 0);

      mmat->set(0, 0, pmal + mmal);
      mmat->set(_npatch, 0, 0);
      mmat->set(_npatch-1, _npatch-1, pmal + mmal);
      mmat->set(_npatch, _npatch-1, 0);
    }
    
  } else if (border_model == 2) {
    //reflective borders,
    //emigrants that cannot leave stay in place
    fmat->set(0, 0, pfem+mfem);
    mmat->set(0, 0, pmal+mmal);
    fmat->set(_npatch -1, _npatch -1, pfem+mfem);
    mmat->set(_npatch -1, _npatch -1, pmal+mmal);

    //no need to transpose for backward migration as the matrix is symmetrical
    
  } else { //is a torus by default
    //the 2 last elements, this is a ring population!
    fmat->set(0, _npatch -1, mfem);
    mmat->set(0, _npatch -1, mmal);
    fmat->set(_npatch -1, 0, mfem);
    mmat->set(_npatch -1, 0, mmal);
  }
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setLatticeMatrix
// ----------------------------------------------------------------------------------------
/**Sets the dispersal matrices for the Lattice dispersal model.*/
bool LCE_Disperse_base::setLatticeMatrix()
{
#ifdef _DEBUG_
  message("setLatticeMatrix()\n");
#endif
  if(!_paramSet->isSet(_prefix + "_border_model"))
    return error("Missing parameter \"dispersal_border_model\" with dispersal model 4!\n");

  if(!_paramSet->isSet(_prefix + "_lattice_range") )
    return error("Missing parameter \"dispersal_lattice_range\" with dispersal model 4!\n");

  int rows, cols;

  if( !_paramSet->isSet(_prefix + "_lattice_rows") && !_paramSet->isSet(_prefix + "_lattice_columns") ) {

    rows = cols = (int) sqrt((double)_npatch);

    if( rows*cols != (int)_npatch )
      return error("The number of patches is not a square number in the lattice dispersal model\n");

  } else {

    rows = _paramSet->getValue(_prefix + "_lattice_rows");

    cols = _paramSet->getValue(_prefix + "_lattice_columns");

    if(rows == -1) return error("number of rows of the dispersal lattice is not specified\n");
    if(cols == -1) return error("number of columns of the dispersal lattice is not specified\n");

    if( rows*cols != (int)_npatch ){
      error("The size of the dispersal matrix in the lattice model is not equal to the number of patches,\n");
      return error("parameters %s_lattice_rows and %s_lattice_columns must be set.\n", _prefix.c_str(), _prefix.c_str());
    }
  }
  
  /**Each matrix has 'patch number' x 'patch number' cells unless the lattice model is the absorbing
   boundaries model where we add the sink patch.*/
  if((unsigned int)get_parameter_value(_prefix + "_border_model") == 3) {
    allocateDispMatrix(MAL, _npatch+1);
    allocateDispMatrix(FEM, _npatch+1);
  } else {
    allocateDispMatrix(MAL, _npatch);
    allocateDispMatrix(FEM, _npatch);
  }
  
  /**The "dispersal_lattice_range" parameter defines the number of neighbouring patches to disperse into.
   Option 1 sets this number to 4 (left and right, up and down patches) whereas option 2 allows to disperse to the 8 neighbouring
   patches, including the patches on the diagonals.*/
  unsigned int range = (unsigned int)get_parameter_value(_prefix + "_lattice_range");
  //philopatry:
  double pmal = 1 - _mal_rate, pfem = 1 - _fem_rate;
  //migration:
  double mmal = _mal_rate/(range == 1 ? 4 : 8), mfem = _fem_rate/(range == 1 ? 4 : 8);

  setBasicLatticeMatrix(rows, cols, pmal, pfem, mmal, mfem);
  
  
  return true;

} 
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setBasicLatticeMatrix
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::setBasicLatticeMatrix(int rows, int cols, double phi_mal, double phi_fem,
                                              double disp_mal, double disp_fem)
{
  TMatrix* mmat = _DispMatrix[MAL];
  TMatrix* fmat = _DispMatrix[FEM];

  //init:
  fmat->assign(0.0);
  mmat->assign(0.0);

  TMatrix grid(rows, cols);

  int p = 0;
  for (int i = 0; i < rows; ++i) {
    for (int j = 0; j < cols ; ++j) {
      grid.set(i, j, p++);
    }
  }
  assert( p == rows*cols );

  //diagonal:
  for (unsigned int i = 0; i < _npatch; ++i){
    fmat->set(i, i, phi_fem);
    mmat->set(i, i, phi_mal);
  }

  int connect;

  for (int x = 0; x < rows; ++x) {
    for (int y = 0; y < cols; ++y) {

      p = grid.get(x, y);

      connect = p + 1; //patch to the right

      if (connect < (x + 1)*cols) { //stay on the same row
        fmat->set(p, connect, disp_fem);
        mmat->set(p, connect, disp_mal);
      }

      connect = p - 1; //patch to the left

      if (connect >= x*cols ) {
        fmat->set(p, connect, disp_fem);
        mmat->set(p, connect, disp_mal);
      }

      connect = p + cols; //patch one row up

      if (connect < (int)_npatch ) {
        fmat->set(p, connect, disp_fem);
        mmat->set(p, connect, disp_mal);
      }

      connect = p - cols; //patch one row down

      if (connect >= 0 ) {
        fmat->set(p, connect, disp_fem);
        mmat->set(p, connect, disp_mal);
      }

      //diagonal steps:
      if((unsigned int)get_parameter_value(_prefix + "_lattice_range") == 2) {

        connect = p + cols; //patches one row up

        if (connect < (int)_npatch ) { //we are not on the last row

          if (connect + 1 < (x + 2)*cols) { //do not reach past the right border
            fmat->set(p, connect + 1, disp_fem);
            mmat->set(p, connect + 1, disp_mal);
          }

          if (connect - 1 >= (x + 1)*cols) { //do not reach past the left border
            fmat->set(p, connect - 1, disp_fem);
            mmat->set(p, connect - 1, disp_mal);
          }
        }

        connect = p - cols; //patches one row down

        if (connect >= 0) { //we are not on the first row

          if (connect + 1 < (x + 1)*cols) {
            fmat->set(p, connect + 1, disp_fem);
            mmat->set(p, connect + 1, disp_mal);
          }

          if (connect - 1 >= (x - 1)*cols) {
            fmat->set(p, connect - 1, disp_fem);
            mmat->set(p, connect - 1, disp_mal);
          }
        }
      } //lattice range == 2

    } //y
  } //x

  switch((unsigned int)get_parameter_value(_prefix + "_border_model")) {
    case 1:
      return setLatticeTorrusMatrix(rows, cols, disp_mal, disp_fem, &grid);
    case 2:
      return setLatticeReflectingMatrix(rows, cols, &grid);
    case 3:
      return setLatticeAbsorbingMatrix();
    default:
      error("parameter \"%s_border_model\" accepts only three values: [1,2,3]\n", _prefix.c_str());
      break;
  }

  return false;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setLatticeTorrusMatrix
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::setLatticeTorrusMatrix(int rows, int cols, double disp_mal, double disp_fem, TMatrix* grid)
{
  TMatrix* mmat = _DispMatrix[MAL];
  TMatrix* fmat = _DispMatrix[FEM];

  int x, y;

  x = 0; //first row, connect with upper row
  for (y = 0; y < cols; ++y) {
    fmat->set( grid->get(x, y), grid->get( rows-1, y ), disp_fem);
    mmat->set( grid->get(x, y), grid->get( rows-1, y ), disp_mal);
  }

  x = rows - 1; //last row, connect with bottom row
  for (y = 0; y < cols; ++y) {
    fmat->set( grid->get(x, y), grid->get( 0, y ), disp_fem);
    mmat->set( grid->get(x, y), grid->get( 0, y ), disp_mal);
  }

  y = 0; //left column, connect with right-most column
  for (x = 0; x < rows; ++x) {
    fmat->set( grid->get(x, y), grid->get( x, cols - 1 ), disp_fem);
    mmat->set( grid->get(x, y), grid->get( x, cols - 1 ), disp_mal);
  }

  y = cols - 1; //right-most column, connect with left-most column
  for (x = 0; x < rows; ++x) {
    fmat->set( grid->get(x, y), grid->get( x, 0 ), disp_fem);
    mmat->set( grid->get(x, y), grid->get( x, 0 ), disp_mal);
  }

  //connect along diagonals

  if((int)get_parameter_value(_prefix + "_lattice_range") == 2) {


    //CORNERS:

    //first patch on first row, wrap around to top right border (last patch)
    fmat->set( grid->get(0, 0), grid->get( rows - 1, cols - 1 ), disp_fem);
    mmat->set( grid->get(0, 0), grid->get( rows - 1, cols - 1 ), disp_mal);
    //last patch to first patch
    fmat->set( grid->get( rows - 1, cols - 1 ), grid->get(0, 0), disp_fem);
    mmat->set( grid->get( rows - 1, cols - 1 ), grid->get(0, 0), disp_mal);

    //last patch on first row, wrap around to top left border
    fmat->set( grid->get(0, cols - 1), grid->get( rows - 1, 0 ), disp_fem);
    mmat->set( grid->get(0, cols - 1), grid->get( rows - 1, 0 ), disp_mal);
    //third corner to second corner
    fmat->set( grid->get( rows - 1, 0 ), grid->get(0, cols - 1), disp_fem);
    mmat->set( grid->get( rows - 1, 0 ), grid->get(0, cols - 1), disp_mal);


    //BORDERS:

    x = 0;//we are on the first row

    for (y = 0; y < cols - 1; ++y) {
      //diagonal step to the right, move to the other side of the grid
      fmat->set( grid->get(x, y), grid->get( rows-1, y + 1 ), disp_fem);
      mmat->set( grid->get(x, y), grid->get( rows-1, y + 1 ), disp_mal);
    }
    for (y = 1; y < cols; ++y) {
      //diagonal step to the left, move to the other side of the grid
      fmat->set( grid->get(x, y), grid->get( rows-1, y - 1 ), disp_fem);
      mmat->set( grid->get(x, y), grid->get( rows-1, y - 1 ), disp_mal);
    }

    x = rows - 1; //last row

    for (y = 0; y < cols - 1; ++y) {
      //diagonal step to the right, move to the first row
      fmat->set( grid->get(x, y), grid->get( 0, y + 1 ), disp_fem);
      mmat->set( grid->get(x, y), grid->get( 0, y + 1 ), disp_mal);
    }
    for (y = 1; y < cols; ++y) {
      //diagonal step to the left, move to the first row
      fmat->set( grid->get(x, y), grid->get( 0, y - 1 ), disp_fem);
      mmat->set( grid->get(x, y), grid->get( 0, y - 1 ), disp_mal);
    }

    //in-between rows, connect first and last columns
    for (x = 0; x < rows; ++x) {

      if(x > 0) {
        //diagonal step down, to previous row
        y = 0; //first column
        fmat->set( grid->get(x, y), grid->get( x - 1, cols - 1 ), disp_fem);
        mmat->set( grid->get(x, y), grid->get( x - 1, cols - 1 ), disp_mal);
        y = cols - 1; //last column
        fmat->set( grid->get(x, y), grid->get( x - 1, 0 ), disp_fem);
        mmat->set( grid->get(x, y), grid->get( x - 1, 0 ), disp_mal);
      }
      if(x < rows - 1){
        //diagonal step up, to next row
        y = 0;
        fmat->set( grid->get(x, y), grid->get( x + 1, cols - 1 ), disp_fem);
        mmat->set( grid->get(x, y), grid->get( x + 1, cols - 1 ), disp_mal);
        y = cols - 1;
        fmat->set( grid->get(x, y), grid->get( x + 1, 0 ), disp_fem);
        mmat->set( grid->get(x, y), grid->get( x + 1, 0 ), disp_mal);
      }
    }
  }
//the matrix is symmetrical no need to transpose for backward migration
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setLatticeReflectingMatrix
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::setLatticeReflectingMatrix(int rows, int cols, TMatrix* grid)
{
  TMatrix* mmat = _DispMatrix[MAL];
  TMatrix* fmat = _DispMatrix[FEM];


  vector<unsigned int> border_cells;

  for (unsigned int i = 0; i < _npatch; ++i) {

    for (int j = 0; j < cols; ++j)
      border_cells.push_back( j ); //first row

    for (int j = 1; j < rows - 1; ++j) { //intermediate rows, only the two border cells
      border_cells.push_back( j*cols );
      border_cells.push_back( (j + 1)*cols - 1 );
    }

    for (unsigned int j = cols*(rows-1); j < _npatch; ++j)
      border_cells.push_back( j ); //last row

  }

  double sum;
  // individuals who can't disperse past the border stay in place:
  for (unsigned int i = 0; i < border_cells.size(); ++i) {
    //sum of dispersal rates:
    sum = fmat->rowSum( border_cells[i] ) - fmat->get(border_cells[i], border_cells[i]);
    //difference gives 1 - m:
    fmat->set(border_cells[i], border_cells[i], 1 - sum);

    sum = mmat->rowSum( border_cells[i] ) - mmat->get(border_cells[i], border_cells[i]);
    mmat->set(border_cells[i], border_cells[i], 1 - sum);
  }
  
  if(!_isForward) {  fmat->transpose();  mmat->transpose(); }
  
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setLatticeAbsorbingMatrix
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_base::setLatticeAbsorbingMatrix()
{
  TMatrix* mmat = _DispMatrix[MAL];
  TMatrix* fmat = _DispMatrix[FEM];
  fmat->set(_npatch, _npatch, 1.0);
  mmat->set(_npatch, _npatch, 1.0);

  double sum;
  
  if(_isForward) {
  //set the absorbing patch probs to 1 - sum(row)
    for(unsigned int i = 0; i < _npatch; ++i) {
      sum = 0;
      for (unsigned int j = 0; j < _npatch; ++j) {
        sum += fmat->get(i, j);
      }
      fmat->set(i, _npatch, 1.0 - sum);
    }

    for(unsigned int i = 0; i < _npatch; ++i) {
      sum = 0;
      for (unsigned int j = 0; j < _npatch; ++j) {
        sum += mmat->get(i, j);
      }
      mmat->set(i, _npatch, 1.0 - sum);
    }
  //backward migration:
  }else {
    //the missing immigrant rate from non-existing patches must be added to the "philopatric" rate
    for(unsigned int i = 0; i < _npatch; ++i) {
      sum = 0;
      for (unsigned int j = 0; j < _npatch; ++j) {
        sum += fmat->get(j, i);
      }
      fmat->plus(i, i, 1.0 - sum);
    }

    for(unsigned int i = 0; i < _npatch; ++i) {
      sum = 0;
      for (unsigned int j = 0; j < _npatch; ++j) {
        sum += mmat->get(j, i);
      }
      mmat->plus(i, i, 1.0 - sum);
    }
  }
  
  return true;
}

// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::setReducedDispMatrix
// ----------------------------------------------------------------------------------------
/** The reduced dispersal matrix contains the indices of the patches to which each patch is
 connected. The connected patches are further ordered in descending order of the migration rates.
 The migration rates are stored in a reduced dispersal matrix. The original dispersal matrices
 are cleared (de-allocated).
 This offers a double speed-up compared to the classical method.
*/
bool LCE_Disperse_base::setReducedDispMatrix()
{
  unsigned int border_model = (unsigned int)get_parameter_value(_prefix + "_border_model");
  unsigned int num_patch = (border_model == 3 ? _npatch + 1 : _npatch);

  //multimap automatically orders the key values in ascending order
  multimap<double, unsigned int> ordered_rates_fem, ordered_rates_mal;

  typedef multimap<double, unsigned int>::const_iterator CI;

#ifdef _DEBUG_
  message("== Dispersal matrices ==\n");
    _DispMatrix[FEM]->show_up();
  _DispMatrix[MAL]->show_up();
  message("== setting reduced dispersal matrices ==\n");
#endif

  // purge the connectivity and reduced dipsersal matrices
  for (unsigned int sex = 0; sex < 2; sex++) {
    _reducedDispMat[sex].clear();
    _reducedDispMatProba[sex].clear();
  }
  // build them from full dispersal matrices given in input
  for (unsigned int i = 0; i < num_patch; ++i) {
    
    _reducedDispMat[0].push_back(vector<double>());
    _reducedDispMat[1].push_back(vector<double>());
    
    _reducedDispMatProba[0].push_back(vector<double>());
    _reducedDispMatProba[1].push_back(vector<double>());
    
    ordered_rates_fem.clear();
    ordered_rates_mal.clear();

    if(_isForward) {
      
      //make pairs: {disp proba, patch connected}, will be ordered by dispersal probabilities:
      for (unsigned int j = 0; j < num_patch; ++j)
        if(_DispMatrix[MAL]->get(i, j) != 0) ordered_rates_mal.insert(make_pair(_DispMatrix[MAL]->get(i, j), j));


      for (unsigned int j = 0; j < num_patch; ++j)
        if(_DispMatrix[FEM]->get(i, j) != 0) ordered_rates_fem.insert(make_pair(_DispMatrix[FEM]->get(i, j),j));


    } else {
      
      //backward migration matrices are read column-wise
      for (unsigned int j = 0; j < num_patch; ++j)
        if(_DispMatrix[MAL]->get(j, i) != 0) ordered_rates_mal.insert(make_pair(_DispMatrix[MAL]->get(j, i),j));

      for (unsigned int j = 0; j < num_patch; ++j)
        if(_DispMatrix[FEM]->get(j, i) != 0) ordered_rates_fem.insert(make_pair(_DispMatrix[FEM]->get(j, i),j));
    }

    
    if(ordered_rates_fem.size() == 1) {

      _reducedDispMat[FEM][i].push_back(ordered_rates_fem.begin()->second);

      _reducedDispMatProba[FEM][i].push_back(ordered_rates_fem.begin()->first);

    } else {

      //store the patch indices in reverse order of the migration rates:
      //store the dispersal rates as well in a separate reduced matrix
      CI p;

  
      for (p = --ordered_rates_fem.end(); p != ordered_rates_fem.begin(); --p) {
        _reducedDispMat[FEM][i].push_back(p->second);
        _reducedDispMatProba[FEM][i].push_back(p->first);
      }

      _reducedDispMat[FEM][i].push_back(ordered_rates_fem.begin()->second);
      _reducedDispMatProba[FEM][i].push_back(ordered_rates_fem.begin()->first);

    }
    
    //same for the male dispersal matrices
    if(ordered_rates_mal.size() == 1) {
      
      _reducedDispMat[MAL][i].push_back(ordered_rates_mal.begin()->second);

      _reducedDispMatProba[MAL][i].push_back(ordered_rates_mal.begin()->first);
    
    } else {

      CI p;
      for (p = --ordered_rates_mal.end(); p != ordered_rates_mal.begin(); --p) {
        _reducedDispMat[MAL][i].push_back(p->second);
        _reducedDispMatProba[MAL][i].push_back(p->first);
      }
      
      _reducedDispMat[MAL][i].push_back(ordered_rates_mal.begin()->second);
      _reducedDispMatProba[MAL][i].push_back(ordered_rates_mal.begin()->first);

    }
  }

  //we can now get rid of the dispersal matrices...
  for (unsigned int sex = 0; sex < 2; sex++) {
    delete _DispMatrix[sex];
    _DispMatrix[sex] = NULL;
  }

#ifdef _DEBUG_
    cout << "=== female reduced dispersal matrix ===\n";
    cout << "    stage = "<<_stages[0]<<endl;
  for (unsigned int i = 0; i < num_patch; ++i) {
    cout << "  ["<<i<<": ";
    for (unsigned int k = 0; k < _reducedDispMat[FEM][i].size(); k++) {
      cout <<"m="<< _reducedDispMatProba[FEM][i][k] << " -> " << _reducedDispMat[FEM][i][k]<<", ";
    }
    cout<<"]\n";
  }

    cout << "=== male reduced dispersal matrix ===\n";
    cout << "    stage = "<<_stages[0]<<endl;
  for (unsigned int i = 0; i < num_patch; ++i) {
    cout << "  ["<<i<<": ";
    for (unsigned int k = 0; k < _reducedDispMat[MAL][i].size(); k++) {
      cout <<"m="<< _reducedDispMatProba[MAL][i][k] << " -> " << _reducedDispMat[MAL][i][k]<<", ";
    }
    cout<<"]\n";

  }
#endif

  return true;
}

// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::Migrate
// ----------------------------------------------------------------------------------------
unsigned int LCE_Disperse_base::getMigrationPatchForward (sex_t SEX, unsigned int LocalPatch)
{
  double sum = 0, random = RAND::Uniform();
  unsigned int AimedPatch = 0;

  if(random > 0.999999) random = 0.999999;//this to avoid overflows when random == 1

  sum = _reducedDispMatProba[SEX][LocalPatch][AimedPatch];

  while (random > sum) {

    AimedPatch++;

    sum += _reducedDispMatProba[SEX][LocalPatch][AimedPatch];

  }
  //return the patch ID stored in the connectivity matrix:
  return _reducedDispMat[SEX][LocalPatch][AimedPatch];
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_base::Migrate
// ----------------------------------------------------------------------------------------
unsigned int LCE_Disperse_base::getMigrationPatchBackward (sex_t SEX, unsigned int LocalPatch)
{
  double sum = 0, random = RAND::Uniform();
  unsigned int SourcePatch = 0;
  
  if(random > 0.999999) random = 0.999999;//this to avoid overflows when random == 1

  sum = _reducedDispMatProba[SEX][LocalPatch][SourcePatch];

  while (random > sum) {
    SourcePatch++;
    sum += _reducedDispMatProba[SEX][LocalPatch][SourcePatch];
  }
    
  return _reducedDispMat[SEX][LocalPatch][SourcePatch];
}
// ------------------------------------------------------------------------------

//                             LCE_Disperse_ConstDisp

// ----------------------------------------------------------------------------------------
// LCE_Disperse_ConstDisp
// ----------------------------------------------------------------------------------------
LCE_Disperse_ConstDisp::LCE_Disperse_ConstDisp () 
: LifeCycleEvent ("disperse",""), doMigration(0), doPatchMigration(0)
{
  
  ParamUpdater< LCE_Disperse_ConstDisp > * updater = 
  new ParamUpdater< LCE_Disperse_ConstDisp > (&LCE_Disperse_ConstDisp::setParameters);
  
  LCE_Disperse_base::addParameters("dispersal", updater);
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_ConstDisp::setParameters
// ----------------------------------------------------------------------------------------
bool LCE_Disperse_ConstDisp::setParameters(string prefix)
{
  
  if(!LCE_Disperse_base::setBaseParameters(prefix)) return false;
  
  switch ( getDispersalModel() ) {
    case 0: //dispersal matrix given in input
    case 1:
    case 3:
    case 4:
      doMigration = &LCE_Disperse_ConstDisp::Migrate;
      break;
    case 2:
      doMigration = &LCE_Disperse_ConstDisp::Migrate_propagule;
      break;
    default:
      return error("\nDispersal model '%i' not yet implemented\n",getDispersalModel());
  }
  
  switch ((int)get_parameter_value(prefix + "_border_model")) {
    case -1:
    case 1:
    case 2:
      doPatchMigration = &LCE_Disperse_ConstDisp::MigratePatch;
      break;
    case 3:
      doPatchMigration = &LCE_Disperse_ConstDisp::MigratePatch_AbsorbingBorder;
      break;
    default:
      return error("\nDispersal border model '%i' not yet implemented\n",
            (int)get_parameter_value(prefix + "_border_model"));
  }
  
  //if number of migrants is specified in the matrix rather than dispersal probabilities:
  if(isByNumber())
    doPatchMigration = &LCE_Disperse_ConstDisp::MigratePatchByNumber;

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_ConstDisp::execute
// ----------------------------------------------------------------------------------------
void LCE_Disperse_ConstDisp::execute ()
{
#ifdef _DEBUG_
  message("LCE_Disperse_ConstDisp::execute (Patch nb: %i offsprg nb: %i adlt nb: %i "
          ,_popPtr->getPatchNbr(), _popPtr->size( OFFSPRG ), _popPtr->size( ADULTS ));
#endif

  //check whether the number of patches changed during simulation:
  if(_npatch != _popPtr->getPatchNbr()) {

    _npatch = _popPtr->getPatchNbr();

    if(!updateDispMatrix()) fatal("bailing out\n");
  }

  reset_counters();

  (this->*doMigration)();
  
#ifdef _DEBUG_
  unsigned int c = 0;
  for(unsigned int i = 0; i < _npatch; i++)
    c += _popPtr->getPatch(i)->nbEmigrant;
  message("emigrants nb: %i)\n",c);
#endif
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_ConstDisp::Migrate
// ----------------------------------------------------------------------------------------
void LCE_Disperse_ConstDisp::Migrate ()
{
  Patch *patch;
  age_idx age;
  
  for(unsigned int i = 0; i < _npatch; i++) {

    patch = _popPtr->getPatch(i);

    for(unsigned int j = 0; j < _nstages; ++j) {

      age = age_idx(_stages[j]);
      
      (this->*doPatchMigration)(FEM, i, age);

      (this->*doPatchMigration)(MAL, i, age);

      //set coloniser counter
      if(patch->get_isExtinct())

        patch->nbKolonisers = _postDispPop[i]->size(age);

      else

        patch->nbKolonisers = -1; //value used in stat_demo to check for colonization

    }//end for stages
  }//end for nb_patch
  
  //put the individuals back into the offspring container
  swapPostDisp();
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_ConstDisp
// ----------------------------------------------------------------------------------------
void LCE_Disperse_ConstDisp::Migrate_propagule ()
{
  setIsland_PropagulePool_Matrix();
  Migrate();
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_ConstDisp::MigratePatch
// ----------------------------------------------------------------------------------------
void LCE_Disperse_ConstDisp::MigratePatch (sex_t SEX, unsigned int LocalPatch, age_idx age)
{
  Patch* patch = _popPtr->getPatch(LocalPatch);
  unsigned int AimedPatch, at;
  unsigned int Limit = _npatch -1;
  Individual* ind;

  while( patch->size(SEX, age) != 0 ) {
    
    do{
      AimedPatch = getMigrationPatchForward(SEX, LocalPatch);
    }while ( AimedPatch > Limit );
    
    at = RAND::Uniform(patch->size(SEX, age));

    ind = patch->remove(SEX, age, at);
    
    _postDispPop[AimedPatch]->add(SEX, age, ind);
       
       
    if(LocalPatch != AimedPatch) {
    
      patch->nbEmigrant++;
      
      _popPtr->getPatch(AimedPatch)->nbImigrant++;
      
    } else
    
      patch->nbPhilopat++;
       
  }//end while
}
// ----------------------------------------------------------------------------------------
// LCE_Disperse_ConstDisp::MigratePatchByNumber
// ----------------------------------------------------------------------------------------
void LCE_Disperse_ConstDisp::MigratePatchByNumber (sex_t SEX, unsigned int LocalPatch, age_idx age)
{
  Patch* patch = _popPtr->getPatch(LocalPatch);
  unsigned int AimedPatch, at;
  unsigned int Limit = _npatch -1;
  Individual* ind;

  for(unsigned int i = 0; i < _reducedDispMat[SEX][LocalPatch].size(); ++i) {

    AimedPatch = _reducedDispMat[SEX][LocalPatch][i];

    // move as many individual as we can until either the specified number or the patch size is reached
    for(unsigned int m = _reducedDispMatProba[SEX][LocalPatch][i]; m > 0 && patch->size(SEX, age) > 0; --m) {

      // take an individual at random:
      at = RAND::Uniform(patch->size(SEX, age));

      ind = patch->remove(SEX, age, at);

      _postDispPop[AimedPatch]->add(SEX, age, ind);

      if(LocalPatch != AimedPatch) {

        patch->nbEmigrant++;

        _popPtr->getPatch(AimedPatch)->nbImigrant++;

      } else

        patch->nbPhilopat++;
    }//end for num migrants

  }//end for num connected patches

  // there might be individuals still in the patch, we keep them there for now
}
// ----------------------------------------------------------------------------------------
// MigratePatch_AbsorbingBorder
// ----------------------------------------------------------------------------------------
void LCE_Disperse_ConstDisp::MigratePatch_AbsorbingBorder (sex_t SEX, unsigned int LocalPatch, age_idx age)
{   
  Patch* patch = _popPtr->getPatch(LocalPatch);
  unsigned int AimedPatch, at;
  Individual* ind;
  
  while( patch->size(SEX, age) != 0 ) {
   
    do{
      AimedPatch = getMigrationPatchForward(SEX, LocalPatch);
    }while ( AimedPatch > _npatch );

    // take an individual at random:
    at = RAND::Uniform(patch->size(SEX, age));
    
    if(AimedPatch == _npatch) {
      _popPtr->recycle( patch->get(SEX, age, at) );
      patch->remove(SEX, age, at);
    } else {
      ind = patch->remove(SEX, age, at);
      _postDispPop[AimedPatch]->add(SEX, age, ind);
    }
    
    if(LocalPatch != AimedPatch) {
      patch->nbEmigrant++;
      if(AimedPatch < _npatch)
        _popPtr->getPatch(AimedPatch)->nbImigrant++;
    } else
      patch->nbPhilopat++;
    
  }//end while
}
// ----------------------------------------------------------------------------------------

//                             LCE_SeedDisp/

// ----------------------------------------------------------------------------------------
LCE_SeedDisp::LCE_SeedDisp () : LifeCycleEvent ("seed_disperse","")
{
  ParamUpdater<LCE_SeedDisp> * updater =
  new ParamUpdater<LCE_SeedDisp> (&LCE_SeedDisp::setParameters);
  
  set_event_name("seed_disperse"); //this resets the paramset, erases the params added by LCE_Disperse_ConstDisp

  addParameters("seed_disperse", updater);

//  get_paramset()->show_up();
}

