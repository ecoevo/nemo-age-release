/**  $Id: fileservices.cc,v 1.9.2.1 2014-04-29 18:26:23 fred Exp $
*
*  @file fileservices.cc
*  Nemo2
*
*   Copyright (C) 2006-2015 Frederic Guillaume
*   frederic.guillaume@ieu.uzh.ch
*
*   This file is part of Nemo
*
*   Nemo is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   Nemo is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*  created on @date 16.06.2005
*
*  @author fred
*/

#include <iostream>
#include <iomanip>
#include <sstream>
#include <time.h>
#include <cmath>
#include "simcomponent.h"
#include "fileservices.h"
#include "filehandler.h"
#include "metapop.h"
#include "output.h"
#include "Uniform.h"

using namespace std;

extern MPIenv *_myenv;

 FileServices::FileServices() : _popPtr(0), _rep_filename(""), _basename(""), _root_dir(""),
    _patch_sample_size(0), _patch_sample_age(0), _sample_pop(0), _sampled_at_replicate(-1),
    _sampled_at_generation(-1)
{
  _logWriter = new FHLogWriter();
  attach(_logWriter);
}

FileServices::~FileServices (){
  if(_logWriter) delete _logWriter;
}
// ----------------------------------------------------------------------------------------
// attach
// ----------------------------------------------------------------------------------------
void FileServices::attach ( Handler* H )
{
//  Service::attach(FH);
  FileHandler* FH = dynamic_cast<FileHandler*> (H);
  _writers.push_back(FH);
  
  if(FH->get_isInputHandler()) attach_reader(FH);
  
  FH->set_service(this);
}
// ----------------------------------------------------------------------------------------
// attach_reader
// ----------------------------------------------------------------------------------------
void FileServices::attach_reader ( FileHandler* FH )
{
  _readers.push_back(FH);
  FH->set_service(this);
}
// ----------------------------------------------------------------------------------------
// load
// ----------------------------------------------------------------------------------------
void FileServices::load ( SimComponent* sc ) {sc->loadFileServices(this);}
// ----------------------------------------------------------------------------------------
// init
// ----------------------------------------------------------------------------------------
bool FileServices::init (list< ParamSet* >&  params)
{
  char yn;
  bool ok;
  file_it HIT;

  _params = params;
  
  _logWriter->set(0, 0, 0, 0, 0, "");

  //first, build path and filename for each file handler
  HIT = _writers.begin();

  if(_myenv->isMaster()) message("    outputs: %s{",_root_dir.c_str());
  
  while(HIT != _writers.end()) {
    if(_myenv->isMaster())  message("%s%s*%s",
            (*HIT)->get_path().c_str(),
            ((*HIT)->get_path().size() !=0? "/":""),
            (*HIT)->get_extension().c_str());
    (*HIT)->init();
    HIT++;
    if(HIT != _writers.end() && _myenv->isMaster()) message(", ");
  }
  if(_myenv->isMaster()) message("}\n");

  //then check if any file already exists
check:

  ok = true;
  HIT = _writers.begin();
  vector<string> local_files, Xing_files;

  //we have to skip the first writer, it is the log-writer (always overwritten)
  while(++HIT != _writers.end()) {

    Xing_files = (*HIT)->ifExist();

    if( Xing_files.size() != 0 ) {
      local_files.insert(local_files.end(), Xing_files.begin(), Xing_files.end() );
      ok = false;
    } else
      ok &= true;
  }

  if(!ok) {

//    warning("simulation filename \"%s\" already used on disc!", getBaseFileName().c_str());

    for(unsigned int i=0; i < local_files.size(); ++i)
      warning("\"%s\" exists", local_files[i].c_str());

    //file mode is set during simulation setup, in SimRunner::init()
    if(_mode == 0) { //"run" & "silent_run"

#if defined(_R_OUTPUT_) || defined(LOW_VERBOSE) || defined(USE_MPI)
    yn = 's';
    message("\nPlease choose an other base filename or move the existing file(s)\n");
#else

      message("\nDo you want to overwrite all the files that use it ? (y/n/s(kip)): ");
      cin>>yn;
#endif

    } else if( _mode == 1 ) { //"overwrite"

      warning("Overwriting existing files.\n\n");
      yn = 'y';

    } else if( _mode == 2 ) { //"skip"

      message("\nPlease choose another base filename or move the existing file(s)\n");
      yn = 's';

    } else if( _mode == 3 || _mode == 4) { // "dryrun" & "create_init"

      //dryrun, pretend overwriting, silently
      yn = 'y';      

    } else {
      fatal("Run mode not properly set, check simulation parameter \"run_mode\".\n");
    }


    switch(yn) {
      case 'y':
        break;
      case 's':
        message("Skipping this simulation\n");
        return false;
      default: {
        message("Please give a new output filename : ");
        cin>>_basename;
        goto check;
      }
    }
  }
  
  return true;
}
// ----------------------------------------------------------------------------------------
// notify
// ----------------------------------------------------------------------------------------
void FileServices::notify()
{
  for (file_it file = _writers.begin(); file != _writers.end(); ++file) {
    (*file)->update();
  }
}
// ----------------------------------------------------------------------------------------
// getSampledPop
// ----------------------------------------------------------------------------------------
Metapop* FileServices::getSampledPop ()
{
  // if the sampling parameters haven't been set, we return the main pop ptr
  if(_patch_sample_size == 0 && _patch_sample_age == 0)
    return _popPtr;

  // if we already created a population sample, we return it:
  if( _popPtr->getCurrentReplicate() == _sampled_at_replicate &&
      _popPtr->getCurrentGeneration() == _sampled_at_generation )
  {
    return _sample_pop;
  }

#ifdef _DEBUG_
  message("FileServices::setting sub-sampled pop (size = %i, age = %i): ", _patch_sample_size, _patch_sample_age);
#endif


  // else, population hasn't been sampled yet, create sample:

  Patch *sample_patch, *src_patch;

  //first time it is called, the pointer should be NULL upon instantiation of FileServices
  if(!_sample_pop) {

    _sample_pop = new Metapop();

  } else {

    // sampled pop already exists, we empty it (without deleting the individuals)

    // remove the patches :
    for(unsigned int i = 0; i < _sample_pop->getPatchArraySize(); ) {
      // removePatch() resizes the array, i must stay = 0 to mimic pop_front() until array is empty
      sample_patch = _sample_pop->removePatch(i);
      sample_patch->clear(); // this resets all container's size to 0 without deleting content
      delete sample_patch;   // delete the patch, without deleting individuals because containers are seen as empty
    }

  }
  // check the age flag:
  if( _patch_sample_age != 0) {

    // check if current pop age flag is compatible with user's choice:
    // reset it to match with current age structure
    if( _popPtr->getCurrentAge() != _patch_sample_age)
      _patch_sample_age &= _popPtr->getCurrentAge();

  } else { // the age flag wasn't set by the user

    _patch_sample_age = _popPtr->getCurrentAge();

  }

  //need to set the age flag of the population
  _sample_pop->setCurrentAge( _patch_sample_age);

  // stop here if age flag is null after checking:
  if( _patch_sample_age == 0 )
    fatal("The population cannot be sub-sampled for the age-class chosen (parameter \"files_sample_age\")\n             Check your parameter file or contact the developers\n");

#ifdef _DEBUG_
  message("after check, sampling age is %i; ", _patch_sample_age);
#endif

  // set age structure in sampled pop
  TMatrix *age_strct = _sample_pop->getAgeStructure();
  // copy from main pop
  age_strct->copy(*_popPtr->getAgeStructure());

  //dble check
//  assert(_sample_pop->getAgeStructure()->length() == _popPtr->getAgeStructure()->length());

  _sample_pop->setPatchNbr(_sample_pop->getPatchArraySize());
  _sample_pop->setPatchSizes(*_popPtr->getPatchCapacities());


  // add patches to match current parameters and patch sizes (discard empty patches)

  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); ++i) {

    if( _popPtr->size(_patch_sample_age, i) == 0 ) continue; //skip empty patches

    src_patch = _popPtr->getPatch(i);
    // allocate new patches
    sample_patch = new Patch();
    sample_patch->setID(i);
    sample_patch->set_K(src_patch->get_K());
    sample_patch->set_KFem(src_patch->get_KFem());
    sample_patch->set_KMal(src_patch->get_KMal());
    // allocate Individual containers and set age structure:
    sample_patch->reset_containers(_sample_pop->getNumAgeClasses());

    // sub sample within patch, among offspring:
    if( _patch_sample_age & OFFSPRG) {

      subSamplePatch(FEM, OFFSx, src_patch, sample_patch);
      subSamplePatch(MAL, OFFSx, src_patch, sample_patch);
    }

    // sub sample within patch, among adults:

    if( _patch_sample_age & ADULTS) {

      for(unsigned int i = 1; i < _popPtr->getNumAgeClasses(); ++i) {

        subSamplePatch(FEM, static_cast<age_idx>(i), src_patch, sample_patch);
        subSamplePatch(MAL, static_cast<age_idx>(i), src_patch, sample_patch);
      }

    }

    _sample_pop->addPatch( sample_patch );

  }

  _sample_pop->setPatchNbr(_sample_pop->getPatchArraySize());
  _sample_pop->setPatchSizes(*_popPtr->getPatchCapacities());

#ifdef _DEBUG_
  message("sampled pop is: \n");
  _sample_pop->show_up();
#endif


  _sampled_at_replicate = _popPtr->getCurrentReplicate();
  _sampled_at_generation = _popPtr->getCurrentGeneration();

  return _sample_pop;
}
// ----------------------------------------------------------------------------------------
// subSamplePatch
// ----------------------------------------------------------------------------------------
void FileServices::subSamplePatch(sex_t SEX, age_idx AGE, Patch* source_patch, Patch* patch)
{
  int* sample = NULL;
  int size = (int)source_patch->size(SEX, AGE), sample_size;
  Individual *ind = 0;

  sample_size = (_patch_sample_size <  size ? _patch_sample_size : size);

  if( patch->size(SEX, AGE) != 0) fatal("FileServices::subSamplePatch::receiver patch container is not empty before sampling\n");

  if( sample_size != 0) {

    sample = new int[sample_size];

    RAND::Sample(0, size, sample_size, sample, false);

    for(unsigned int j = 0; j < sample_size; ++j) {

      ind = source_patch->get(SEX, AGE, sample[j]);

      patch->add(SEX, AGE, ind);
    }

    delete [] sample;
  }
}
// ----------------------------------------------------------------------------------------
// reset
// ----------------------------------------------------------------------------------------
void FileServices::reset()
{
  //Service::reset(); 
  _writers.clear(); _writers.push_back(_logWriter);
  _readers.clear();
}
// ----------------------------------------------------------------------------------------
// log_simparams
// ----------------------------------------------------------------------------------------
void FileServices::log_simparams() 
{
  if(_mode == 4)
    _logWriter->createInitFile(_params);
  else
    _logWriter->save_simparams(_params);
}
// ----------------------------------------------------------------------------------------
// log
// ----------------------------------------------------------------------------------------
void FileServices::log (string message) 
{
  _logWriter->log_message(message);
}
// ----------------------------------------------------------------------------------------
// getFirstReplicateFileName
// ----------------------------------------------------------------------------------------
string& FileServices::getFirstReplicateFileName ()
{
  ostringstream rpl;

  rpl.fill('0');
  rpl.width( (int)log10((double)_popPtr->getReplicates()) + 1);
  rpl<< max( (unsigned)1, _myenv->workerRank() );

  _rep_filename = _basename + "_" + rpl.str();

  return _rep_filename;
}
// ----------------------------------------------------------------------------------------
// getReplicateFileName
// ----------------------------------------------------------------------------------------
string& FileServices::getReplicateFileName ()
{
  _rep_filename = _basename + "_" + getReplicateCounter();

  return _rep_filename;
}
// ----------------------------------------------------------------------------------------
// getGenerationReplicateFileName
// ----------------------------------------------------------------------------------------
string FileServices::getGenerationReplicateFileName () 
{
  string name = _basename + "_" + getGenerationCounter() + "_" + getReplicateCounter();
  return name;
}
// ----------------------------------------------------------------------------------------
// getBaseFileName
// ----------------------------------------------------------------------------------------
string& FileServices::getBaseFileName () 
{
  return _basename;
}
// ----------------------------------------------------------------------------------------
// getRootDir
// ----------------------------------------------------------------------------------------
std::string& FileServices::getRootDir ()
{
  return _root_dir;
}
// ----------------------------------------------------------------------------------------
// getReplicateCounter
// ----------------------------------------------------------------------------------------
string FileServices::getReplicateCounter ()
{
  ostringstream rpl;
  
  rpl.fill('0');
  rpl.width( (int)log10((double)_popPtr->getReplicates()) + 1);
  rpl<<_popPtr->getCurrentReplicate();
  
  return rpl.str();
}
// ----------------------------------------------------------------------------------------
// getGenerationCounter
// ----------------------------------------------------------------------------------------
string FileServices::getGenerationCounter ()
{
  ostringstream gen;
  
  gen.fill('0');
  gen.width( (int)log10((double)_popPtr->getGenerations()) + 1);
  gen<<_popPtr->getCurrentGeneration();
  
  return gen.str();
}
// ----------------------------------------------------------------------------------------
// setBasename
// ----------------------------------------------------------------------------------------
void FileServices::setBasename (string name) 
{
    _basename = name;
}
// ----------------------------------------------------------------------------------------
// setRootDir
// ----------------------------------------------------------------------------------------
void FileServices::setRootDir (string name) 
{
  _root_dir = name;
 
  if(_root_dir.size() != 0 && _root_dir[_root_dir.length()-1] != '/')
	_root_dir += "/";
  
}
// ----------------------------------------------------------------------------------------
// getReader
// ----------------------------------------------------------------------------------------
FileHandler* FileServices::getReader (string& type)
{
  file_it file = getFirstReader(), last = getLastReader() ;
    
  for(;file != last; file++) {
    if( (*file)->get_extension().compare( type ) == 0) {
      return (*file);
    }
  }
  return 0;
}
