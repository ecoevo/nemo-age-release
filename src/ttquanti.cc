/** $Id: ttquanti.cc,v 1.23.2.10 2017-03-16 10:20:40 fred Exp $
 *
 *  @file ttquanti.cc
 *  Nemo2
 *
 *   Copyright (C) 2006-2020 Frederic Guillaume
 *   frederic.guillaume@ieu.uzh.ch
 *
 *   This file is part of Nemo
 *
 *   Nemo is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Nemo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  created on @date 14.11.2005
 * 
 *  @author fred
 */

#include <sstream>
#include <fstream>
#include <string.h>
#include <cmath>
#include <algorithm>
#include "ttquanti.h"
#include "filehandler.h"
#include "output.h"
#include "Uniform.h"
#include "tstring.h"
#include "utils.h"
#ifdef HAS_GSL
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#endif

void store_quanti_trait_values (Patch* patch, unsigned int patchID, unsigned int size, unsigned int *cntr,
                                sex_t SEX, age_t AGE, DataTable<double> *ptable, DataTable<double> *gtable,
                                unsigned int nTrait, unsigned int TraitIndex);


// ------------------------------------------------------------------------------

//                             TProtoQuanti

// ----------------------------------------------------------------------------------------
// cstor
// ----------------------------------------------------------------------------------------
TProtoQuanti::TProtoQuanti() :
_nb_locus(0),
_nb_traits(0),
_seq_length(0),
_allele_model(0),
_allele_value(0),
_mutation_matrix(0),
_gsl_mutation_matrix(0),
_evect(0),
_eval(0),
_effects_multivar(0),
_ws(0),
_genomic_mutation_rate(0),
_mutation_correlation(0),
_mutation_sigma(0),
_init_value(0),
_doInitMutation(0),
_all_chooser(0),
_locusByteSize(0),
_sizeofLocusType(sizeof(double)),
_eVariance(0),
_mutation_func_ptr(0),
_stats(0),
_writer(0),
_reader(0),
_freqExtractor(0)
{
  set_paramset("quantitative_trait", false, this);
  
  add_parameter("quanti_traits",INT,true,false,0,0);
  add_parameter("quanti_loci",INT,true,false,0,0);
  add_parameter("quanti_allele_model",STR,false,false,0,0);
  add_parameter("quanti_allele_value",DBL,false,false,0,0);
  add_parameter("quanti_init_value",MAT,false,false,0,0);
  add_parameter("quanti_init_model",INT,false,true,0,4);
  add_parameter("quanti_environmental_variance",MAT,false,false,0,0);

  add_parameter("quanti_mutation_rate",DBL,true,true,0,1, 0);
  add_parameter("quanti_mutation_variance",DBL,false,false,0,0, 0);
  add_parameter("quanti_mutation_correlation",DBL,false,false,0,0, 0);
  add_parameter("quanti_mutation_covariance",DBL,false,false,0,0, 0);
  add_parameter("quanti_mutation_matrix",MAT,false,false,0,0, 0);
  
  //genetic map parameters:
  TTProtoWithMap::addGeneticMapParameters("quanti");
  
  add_parameter("quanti_output",STR,false,false,0,0);
  add_parameter("quanti_logtime",INT,false,false,0,0);
  add_parameter("quanti_dir",STR,false,false,0,0);
  
  add_parameter("quanti_freq_output",BOOL,false,false,0,0);
  add_parameter("quanti_freq_logtime",INT,false,false,0,0);
}
// ----------------------------------------------------------------------------------------
// copy cstor
// ----------------------------------------------------------------------------------------
TProtoQuanti::TProtoQuanti(const TProtoQuanti& T) : 
_nb_locus(T._nb_locus),
_nb_traits(T._nb_traits),
_seq_length(T._seq_length),
_allele_model(T._allele_model),
_mutation_matrix(0),
_gsl_mutation_matrix(0),
_evect(0),
_eval(0),
_effects_multivar(0),
_ws(0),
_genomic_mutation_rate(T._genomic_mutation_rate),
_mutation_correlation(T._mutation_correlation),
_mutation_sigma(0),
_init_value(0),
_doInitMutation(0),
_allele_value(0),
_all_chooser(0),
_locusByteSize(0),
_sizeofLocusType(sizeof(double)),
_eVariance(0),
_mutation_func_ptr(0),
_stats(0),
_writer(0),
_reader(0),
_freqExtractor(0)
{ 
  _locusByteSize = T._nb_traits * sizeof(double);
  _paramSet = new ParamSet( *(T._paramSet) ) ;
}
// ----------------------------------------------------------------------------------------
// dstor
// ----------------------------------------------------------------------------------------
TProtoQuanti::~TProtoQuanti ()
{
  reset_mutation_pointers();
  
  if(_stats != NULL){delete _stats; _stats = NULL;}
  if(_writer != NULL){delete _writer; _writer = NULL;}
  if(_reader) delete _reader;
  if(_freqExtractor != NULL){delete _freqExtractor; _freqExtractor = NULL;}
  if(_all_chooser) {delete [] _all_chooser; _all_chooser = NULL;}
  if(_init_value) {delete[] _init_value;_init_value = NULL;}
}
// ----------------------------------------------------------------------------------------
// setParameters
// ----------------------------------------------------------------------------------------
bool TProtoQuanti::setParameters()
{  
  if(get_parameter("quanti_mutation_covariance")->isSet()) {
    return error("\"quanti_mutation_covariance\" is deprecated, use \"quanti_mutation_correlation\" instead.\n");
  }

  // reset all allelic effect tables and pointer to mutation parameters etc.:
  // table sizes may change with new parameters, free memory before updating table sizes
  reset_mutation_pointers();


  // Mandatory parameters:

  // number of quantitative traits
  _nb_traits = (unsigned int)get_parameter_value("quanti_traits");
  
  // number of loci
  _nb_locus = (unsigned int)get_parameter_value("quanti_loci");
  
  // store a few important numbers

  //total nbre of values for both traits in a haploid genome
  _seq_length = _nb_traits * _nb_locus; //mutations are pleiotropic!!!

  // size of data used to store pleiotropic allelic values at a locus
  _locusByteSize = _nb_traits * sizeof(double);

  // memory size of a single allele value
  _sizeofLocusType = sizeof(double);
  
  //  temporary TMatrix for parameters read-in
  TMatrix tmp_mat;

  //---------------------------------------------------------------------------------------------
  // ENVIRONMENTAL VARIANCE

  _eVariance.clear();

  if(get_parameter("quanti_environmental_variance")->isSet()){

    get_parameter("quanti_environmental_variance")->getMatrix(&tmp_mat);

    double var;

    for(unsigned int i = 0; i < _nb_traits; i++) {

      var = tmp_mat.get(0,i);

      if( var < 0 )
        return error("\"quanti_environmental_variance\" has a negative value for trait %i!\n", i+1 );
      else
      _eVariance.push_back( sqrt(var) ); // store the SD to save time later
  
    }
    _set_value_func_ptr = &TTQuanti::set_value_VE;

  } else {

    _set_value_func_ptr = &TTQuanti::set_value_noVE;
  }
  
  //---------------------------------------------------------------------------------------------
  // INITIAL VALUES:
  if(_init_value != NULL) {
    delete [] _init_value;
    _init_value = NULL;
  }  
  
  if(get_parameter("quanti_init_value")->isSet()) {
    
    get_parameter("quanti_init_value")->getMatrix(&tmp_mat);
    
    if(tmp_mat.getNbRows() != 1 || tmp_mat.getNbCols() != _nb_traits) {
      return error("\"quanti_init_value\" must be a vector of length equal to the number of traits!\n");
    }
    
    _init_value = new double [_nb_traits];
    
    for(unsigned int i = 0; i < _nb_traits; i++)
      _init_value[i] = tmp_mat.get(0,i);
    
  }
  else {
    
    _init_value = new double [_nb_traits];
    
    for(unsigned int i = 0; i < _nb_traits; i++)
      _init_value[i] = 0.0;
  }
  
  if(get_parameter("quanti_init_model")->isSet()) {
    
    _doInitMutation = (unsigned int)get_parameter_value("quanti_init_model");
    
  } else {
    _doInitMutation = 1;
  }
  
  //---------------------------------------------------------------------------------------
  // ALLELE MODELS AND MUTATIONS

  if( !setMutationParameters() ) return false;

  // SET POINTER TO MUTATE FUNCTION

  if(_genomic_mutation_rate == 0) {

    _mutation_func_ptr = &TProtoQuanti::mutate_nill;

  } else if(_allele_model == 1 || _allele_model == 3) {// no "house-of-cards" models

    _mutation_func_ptr = &TProtoQuanti::mutate_noHC;

  } else if(_allele_model == 2) {

    _mutation_func_ptr = &TProtoQuanti::mutate_diallelic_HC;

  } else
    _mutation_func_ptr = &TProtoQuanti::mutate_HC;
  
  // SET POINTERS TO MUTATION EFFECTS FUNCTIONS
  
  if(_nb_traits == 1) { // univariate models
    
    if (_allele_model == 1 || _allele_model == 2) {

      _getMutationValues = &TProtoQuanti::getMutationEffectUnivariateDiallelic;
    
  } else {
    
      _getMutationValues = &TProtoQuanti::getMutationEffectUnivariateGaussian;
  
  }

  } else if (_nb_traits == 2) { // bivariate models
  
    if (_allele_model == 1 || _allele_model == 2) {

      _getMutationValues = &TProtoQuanti::getMutationEffectBivariateDiallelic;
    } else {
      _getMutationValues = &TProtoQuanti::getMutationEffectBivariateGaussian;
    }
    
  } else if (_nb_traits > 2) { // multivariate models, only for non-diallelic loci
    
    if (_allele_model > 2) {
    
      _getMutationValues = &TProtoQuanti::getMutationEffectMultivariateGaussian;
    
    } else {
      fatal("in \"quanti\" trait, the di-allelic model is only allowed for max. 2 quantitative traits.");
    }
    
  }
  
  //---------------------------------------------------------------------------------------
  // RECOMBINATION

  //bypass the genetic map for free recombination to save significant time:
  //this is done whenever the recombination rate is set to 0.5

  if( TTProtoWithMap::isRecombinationFree("quanti")) { //test all possible options to set recomb rate = 0.5

    TTProtoWithMap::_recombRate = 0.5;

        _inherit_fun_ptr = &TProtoQuanti::inherit_free;

   //necessary for the inherit_free function:
        if(_all_chooser) delete [] _all_chooser;
        _all_chooser = new bool[_nb_locus];

  } else {
   
    // the loci positions will be registered in the genetic map
    if( !setGeneticMapParameters("quanti") ) return false;

    _inherit_fun_ptr = &TProtoQuanti::inherit_low;
  }

  //---------------------------------------------------------------------------------------
  return true; 
}
// ----------------------------------------------------------------------------------------
// setMutationParameters
// ----------------------------------------------------------------------------------------
bool TProtoQuanti::setMutationParameters ()
{  
  // set the genomic mutation rate
  _genomic_mutation_rate = get_parameter_value("quanti_mutation_rate") * 2 * _nb_locus;  
  
  // set the mutational correlation, for pleiotropic loci:
  if(get_parameter("quanti_mutation_correlation")->isSet())
    _mutation_correlation = get_parameter_value("quanti_mutation_correlation");
  else
    _mutation_correlation = 0;
  
  //checking allelic model
  if (get_parameter("quanti_allele_model")->isSet()) {
    
    string model = get_parameter("quanti_allele_model")->getArg();
    
    //---------------------------------------------------------------------------------------
    // DIALLELIC MODELS
    if (model == "diallelic") {
      
      if(get_parameter("quanti_init_value")->isSet()){

        error("\"quanti_allele_model diallelic\" needs to be initialized with \"quanti_init_freq\" instead of \"quanti_init_value\". \n");

      }

      _allele_model = 1;
      
      return setDiallelicMutationModel ();
      
    } else if (model == "diallelic_HC") {
      
      if(get_parameter("quanti_init_value")->isSet()){

        error("\"quanti_allele_model diallelic_HC\" needs to be initialized with \"quanti_init_freq\" instead of \"quanti_init_value\". \n");

      }

      _allele_model = 2;
      
      return setDiallelicMutationModel ();
    //---------------------------------------------------------------------------------------
    // CONTIUUM OF ALLELE MODELS
      
    } else if (model == "continuous") {
      
      _allele_model = 3;
      
      return setContinuousMutationModel ();
      
    } else if (model == "continuous_HC") {
      
      _allele_model = 4;
      
      return setContinuousMutationModel ();
      
    } else {

      return error("\"quanti_allele_model\" has options \"diallelic[_HC]\" or \"continuous[_HC]\" only. \n");

    }
    
  } 
  else { //default model: 'continuous"
    _allele_model = 3;
    return setContinuousMutationModel ();
  }
  
  return true;
}
//---------------------------------------------------------------------------------------------
// setDiallelicMutationModel
//---------------------------------------------------------------------------------------------
bool TProtoQuanti::setDiallelicMutationModel ()
{
  if (!get_parameter("quanti_allele_value")->isSet()) {
    return error("in \"quanti\" trait, \"quanti_allele_value\" is not set for the diallelic model.\n");
    
  } else {
    
    assert(_allele_value == NULL); 
    //should be the case as reset_mutation_pointers has been called in setMutationParameters
    
    // allele_value is a matrix nb_locus x 2 values

    // IMPORTANT:: This interface doesn't allow for trait-specific allelic values!!
    
    _allele_value = new double* [_nb_locus];
    
    for (unsigned i = 0; i < _nb_locus; ++i) _allele_value[i] = new double [2];
    
    if (get_parameter("quanti_allele_value")->isMatrix()) { //locus-specific allelic values
      
      TMatrix tmp;
      
      get_parameter("quanti_allele_value")->getMatrix(&tmp);
      
      if (tmp.ncols() != _nb_locus) {
        warning("\"quanti_allele_value\" has less values than the number of loci, allelic values will be recycled among loci.\n");
      }

      unsigned int nval = tmp.ncols();
      
      if (tmp.nrows() == 1) { // store + and - allelic values
        
        if(nval < _nb_locus) { // recycle allelic values among loci
          for (unsigned i = 0; i < _nb_locus; ++i) {
            _allele_value[i][0] = tmp.get(0, i % nval);
            _allele_value[i][1] = -_allele_value[i][0];
          }
        } else {
        for (unsigned i = 0; i < _nb_locus; ++i) {
          _allele_value[i][0] = tmp.get(0, i);
          _allele_value[i][1] = -_allele_value[i][0];
        }
        
        }

      } else if (tmp.nrows() == 2) { // store A and a allelic values

        if(nval < _nb_locus) { // recycle allelic values among loci

          for (unsigned i = 0; i < _nb_locus; ++i) {
            _allele_value[i][0] = tmp.get(0, i % nval);
            _allele_value[i][1] = tmp.get(1, i % nval);
          }

        } else {
        
        for (unsigned i = 0; i < _nb_locus; ++i) {
          _allele_value[i][0] = tmp.get(0, i);
          _allele_value[i][1] = tmp.get(1, i);
          }
        }
        
      } else {
        return error("\"quanti_allele_value\" must get a matrix with a max. of 2 rows (and num. columns = num. loci).\n");
      }
      
      
    } else { //not a matrix, therefore no locus-specific allelic values
      
      double val = get_parameter_value("quanti_allele_value");
      for (unsigned i = 0; i < _nb_locus; ++i) {
        _allele_value[i][0] = val;
        _allele_value[i][1] = -_allele_value[i][0];
      }
      
    }
    
  }
  
  return true;
}
//---------------------------------------------------------------------------------------------
// setContinuousMutationModel
//---------------------------------------------------------------------------------------------
bool TProtoQuanti::setContinuousMutationModel ()
{
  unsigned int dims[2];
  // the pointer has been freed previously in TProtoQuanti::setMutationParameters
  // we double-check with assert
  assert(_mutation_matrix == NULL);
  assert(_gsl_mutation_matrix == NULL);
  assert(_mutation_sigma == NULL);

  //setting the mutation variance-covariance matrix  
  if(get_parameter("quanti_mutation_variance")->isSet()) {
    
    if(get_parameter("quanti_mutation_matrix")->isSet()) {
      warning("both \"quanti_mutation_variance\" and \"quanti_mutation_matrix\" are set, using the matrix only!\n");
    } else {
      
      // the variance, and covariance are passed as single values,
      // we set the mutation-matrix from those values, copied to all traits
      _mutation_sigma = new double [_nb_traits];
      
      double sigma = sqrt( get_parameter_value("quanti_mutation_variance") );
      
      for(unsigned int i = 0; i < _nb_traits; i++)  _mutation_sigma[i] = sigma;
      
      // pleiotropic loci for more than 1 trait, we need the full mutation matrix:
      if(_nb_traits > 1) {

        //setting the mutation matrix
        _mutation_matrix = new TMatrix();
        
        _gsl_mutation_matrix = gsl_matrix_alloc(_nb_traits, _nb_traits);
        
        double covar, var = get_parameter_value("quanti_mutation_variance");
        
        covar = _mutation_correlation * var;
        
        // copy the trait mutational variance into the GSL matrix (diogonal)
        for(unsigned int i = 0; i < _nb_traits; i++)
          gsl_matrix_set(_gsl_mutation_matrix, i, i, var);
        
        // copy the trait mutational co-variance into the GSL matrix (off-diogonal & symmetric)
        for(unsigned int i = 0; i < _nb_traits - 1; i++)  
          for(unsigned int j = i + 1 ; j < _nb_traits; j++) {
            gsl_matrix_set(_gsl_mutation_matrix, i, j, covar);
            gsl_matrix_set(_gsl_mutation_matrix, j, i, covar);
          }

        // copy the GSL matrix into the TMatrix:
        _mutation_matrix->set_from_gsl_matrix(_gsl_mutation_matrix);
      }
    }
  }
  
  if(get_parameter("quanti_mutation_matrix")->isSet()) {

    // full mutation matrix given in input:
    
    _mutation_matrix = new TMatrix();
    
    get_parameter("quanti_mutation_matrix")->getMatrix(_mutation_matrix);
    
    _mutation_matrix->get_dims(&dims[0]);
    
    if( dims[0] != _nb_traits || dims[1] != _nb_traits) {
      return error("\"quanti_mutation_matrix\" must be a square matrix of size = \"quanti_traits\" x \"quanti_traits\"\n");
    }
    
    _gsl_mutation_matrix = gsl_matrix_alloc(dims[0], dims[1]);
    
    _mutation_matrix->get_gsl_matrix(_gsl_mutation_matrix);
    
    _mutation_sigma = new double [_nb_traits];
    
    for(unsigned int i = 0; i < _nb_traits; i++) 
      _mutation_sigma[i] = sqrt(_mutation_matrix->get(i, i));
    
  }
  else
    if(!get_parameter("quanti_mutation_variance")->isSet()) {

    return error("\"quanti_mutation_matrix\" or \"quanti_mutation_variance\" must be specified!\n");
  }

  //some more initializations for multi-variate case, important to draw mutation from multi-variate Gaussian
  if(_mutation_matrix) {
    
    _evect = gsl_matrix_alloc(_nb_traits, _nb_traits);
    _eval = gsl_vector_alloc(_nb_traits);
    
    // M-matrix eigen-decomposition:
    set_mutation_matrix_decomposition();
    
    if(_nb_traits == 2) {
    
      _mutation_correlation = _mutation_matrix->get(0,1) / sqrt(_mutation_matrix->get(0,0)*_mutation_matrix->get(1,1));
    
    } else if(_nb_traits > 2) {
      _effects_multivar = gsl_vector_alloc(_nb_traits);
      _ws = gsl_vector_alloc(_nb_traits);
    }
#ifdef _DEBUG_
    message("-- Mutation matrix:\n");
    _mutation_matrix->show_up();
    message("-- MMatrix decomposition:\n");
    for(unsigned int i = 0; i < _nb_traits; i++)
      cout<<gsl_vector_get(_eval,i)<<" ";
    cout<<endl;
    if(_nb_traits == 2) message("-- mutation correlation: %f\n",_mutation_correlation);
#endif
  }
  
  return true;
}
// ----------------------------------------------------------------------------------------
// set_mutation_matrix_decomposition
// ----------------------------------------------------------------------------------------
void TProtoQuanti::set_mutation_matrix_decomposition()
{
  gsl_matrix *E = gsl_matrix_alloc(_nb_traits,_nb_traits);
  gsl_matrix_memcpy(E,_gsl_mutation_matrix);
  gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc (_nb_traits);
  gsl_eigen_symmv (E, _eval, _evect, w);
  gsl_eigen_symmv_free (w);
  gsl_matrix_free(E);
#ifdef _DEBUG_
  message("-- Mutation matrix eigenvalues:\n");
  for(unsigned int i = 0; i < _nb_traits; i++)
    cout<<gsl_vector_get(_eval,i)<<" ";
  cout<<endl;
#endif
  double eval;
  //take square root of eigenvalues, will be used in Gaussian as stdev
  for(unsigned int i = 0; i < _nb_traits; i++) {
    eval = gsl_vector_get(_eval,i);
    eval = (eval < 0.000001 ? 0 : eval);
    gsl_vector_set( _eval, i, sqrt(eval) );
  }
}
// ----------------------------------------------------------------------------------------
// getMutationEffectMultivariateGaussian
// ----------------------------------------------------------------------------------------
double* TProtoQuanti::getMutationEffectMultivariateGaussian (unsigned int loc)
{
  RAND::MultivariateGaussian(_eval, _evect, _ws, _effects_multivar);
  return _effects_multivar->data;
}
// ----------------------------------------------------------------------------------------
// getMutationEffectBivariateGaussian
// ----------------------------------------------------------------------------------------
double* TProtoQuanti::getMutationEffectBivariateGaussian   (unsigned int loc)
{
  RAND::BivariateGaussian(_mutation_sigma[0], _mutation_sigma[1], _mutation_correlation,
                          &_effects_bivar[0], &_effects_bivar[1]);
  return &_effects_bivar[0];
}
// ----------------------------------------------------------------------------------------
// getMutationEffectUnivariateGaussian
// ----------------------------------------------------------------------------------------
double* TProtoQuanti::getMutationEffectUnivariateGaussian   (unsigned int loc)
{
  _effects_bivar[0] = RAND::Gaussian(_mutation_sigma[0]);
  return &_effects_bivar[0];
}
// ----------------------------------------------------------------------------------------
// getMutationEffectUnivariateDiallelic
// ----------------------------------------------------------------------------------------
double* TProtoQuanti::getMutationEffectUnivariateDiallelic   (unsigned int loc)
{
  _effects_bivar[0] = _allele_value[loc][ RAND::RandBool() ];
  return &_effects_bivar[0];
}
// ----------------------------------------------------------------------------------------
// getMutationEffectBivariateDiallelic
// ----------------------------------------------------------------------------------------
double* TProtoQuanti::getMutationEffectBivariateDiallelic   (unsigned int loc)
{

  bool pos = RAND::RandBool();

  _effects_bivar[0] = _allele_value[loc][pos];
  _effects_bivar[1] = _allele_value[loc][ (RAND::Uniform() < _mutation_correlation ? 
                                           pos : RAND::RandBool()) ];

  return &_effects_bivar[0];
}
// ----------------------------------------------------------------------------------------
// reset_mutation_pointers
// ----------------------------------------------------------------------------------------
void TProtoQuanti::reset_mutation_pointers()
{
  if(_allele_value) {
    for(unsigned int i = 0; i < _nb_locus; ++i)
      delete [] _allele_value[i];
    delete [] _allele_value;
    _allele_value = NULL;
  }
  
  if(_mutation_matrix != NULL) {
    delete _mutation_matrix;
    _mutation_matrix = NULL;
  }
  
  if(_gsl_mutation_matrix != NULL) {
    gsl_matrix_free(_gsl_mutation_matrix);
    _gsl_mutation_matrix = NULL;
  }
  
  if(_mutation_sigma != NULL){
    delete [] _mutation_sigma;
    _mutation_sigma = NULL;
  }
  
  if(_evect != NULL) {
    gsl_matrix_free(_evect);
    _evect = NULL;
  }
  
  if(_eval != NULL) {
    gsl_vector_free(_eval);
    _eval = NULL;
  }
  
  if(_effects_multivar != NULL) {
    gsl_vector_free(_effects_multivar);
    _effects_multivar = NULL;
  }
  
  if(_ws) {
    gsl_vector_free(_ws);
    _ws = 0;
  }
  
}
// ----------------------------------------------------------------------------------------
// set_init_values
// ----------------------------------------------------------------------------------------
void TProtoQuanti::set_init_values (const double *values, unsigned int nval)
{
  assert(nval == _nb_traits);

  if(!_init_value) _init_value = new double[_nb_traits];

  for(unsigned int i = 0; i < nval; ++i)
    _init_value[i] = values[i];
}
// ----------------------------------------------------------------------------------------
// inherit
// ----------------------------------------------------------------------------------------
inline void TProtoQuanti::inherit (sex_t SEX, double* seq, double** parent)
{
  (this->*_inherit_fun_ptr)(SEX, seq, parent);
}
// ----------------------------------------------------------------------------------------
// inherit
// ----------------------------------------------------------------------------------------
inline void TProtoQuanti::inherit_free (sex_t SEX, double* seq, double** parent)
{
  unsigned int bloc;
  assert(_all_chooser);
  for(unsigned int i = 0; i < _nb_locus; ++i)
    _all_chooser[i] = RAND::RandBool();
  
  for(unsigned int i = 0; i < _nb_locus; ++i) {
    
    bloc = i*_nb_traits;
    
    memcpy(&seq[bloc], &parent[ _all_chooser[i] ][bloc], _locusByteSize);
    
  }
}
// ----------------------------------------------------------------------------------------
// inherit
// ----------------------------------------------------------------------------------------
inline void TProtoQuanti::inherit_low (sex_t SEX, double* seq, double** parent)
{
  unsigned int prevLoc = 0, chrm_bloc = 0, prev_bloc, cpy_bloc;
  bool flipper;
  
  //the table containing the loci at which x-overs happen
  vector< unsigned int >& recTable = _map.getRecLoci(SEX, _mapIndex);
  
  //the table containing which homologous chromosome copy we start with, for each chromosome
  vector< bool > & firstRecPos = _map.getFirstRecPosition(SEX);
  
  //number of x-overs
  unsigned int nbRec = recTable.size();
  
  //  cout << "TProtoQuanti::inherit; sex="<<SEX<<"; nb Rec = "<<nbRec;//<<endl;
  
  // c is the chromosome number
  // stride is the number of loci considered so-far, sum of chromosome loci
  // rec is the number of x-over done so-far
  for(unsigned int c = 0, stride = 0, rec = 0; c < _numChromosome; ++c) {
    
    //the copy of the chromosome with which with start
    flipper = firstRecPos[c];
    
    //number of loci to consider, including previous chromosomes
    chrm_bloc = stride + _numLociPerChrmsm[c];
    
    //last locus at which a x-over happened, will be first position on current chromosome
    prevLoc = stride;
    
    //    cout<<"chrm "<<c<<" side="<<firstRecPos[c]<<endl;
    
    // copy blocs of loci between x-over points on current chromosome
    // skip it if locus is not on this chromosome but a latter one
    for(; recTable[rec] < chrm_bloc && rec < nbRec; rec++) {
      
      // start position of the bloc of values to copy (_nb_traits values per locus)
      prev_bloc = prevLoc * _nb_traits;
      
      // size of the bloc to copy
      cpy_bloc = (recTable[rec] - prevLoc) * _nb_traits;
      
      //      cout<<"copy seq from "<<prevLoc<<"("<<prev_bloc<<") to "<<recTable[rec]
      //      <<"("<<(recTable[rec] - prevLoc)<<" loc) ("<<cpy_bloc*_locusByteSize<<"B) side "<<flipper<<endl;
      
      memcpy(&seq[prev_bloc], &parent[flipper][prev_bloc], cpy_bloc * _sizeofLocusType);
      
      //update the starting locus to the next recombination point
      prevLoc = recTable[rec];
      
      //switch side for next bloc to copy on this chromosome
      flipper = !flipper;
    }
    
    prev_bloc = prevLoc * _nb_traits;
    
    cpy_bloc = (chrm_bloc - prevLoc) * _nb_traits;
    
    //    cout << "copy end of chrmsm from "<<prevLoc<<" to "<<chrm_bloc
    //         <<"("<<(chrm_bloc - prevLoc)<<" loc) on side "<<flipper<<endl;
    
    //copy what's left between the last x-over point and the end of the chrmsme
    memcpy(&seq[prev_bloc], &parent[flipper][prev_bloc], cpy_bloc * _sizeofLocusType);
    
    stride += _numLociPerChrmsm[c];
  }
  //  cout<<" done"<<endl;
}
// ----------------------------------------------------------------------------------------
// mutate
// ----------------------------------------------------------------------------------------
inline void TProtoQuanti::mutate (TTQuanti* ind)
{
  (this->*_mutation_func_ptr)(ind);
}
// ----------------------------------------------------------------------------------------
// mutate
// ----------------------------------------------------------------------------------------
inline void TProtoQuanti::mutate_nill (TTQuanti* ind)
{
  //nill
}
// ----------------------------------------------------------------------------------------
// mutate_noHC
// ----------------------------------------------------------------------------------------
inline void TProtoQuanti::mutate_noHC (TTQuanti* ind)
{
  unsigned int NbMut = (unsigned int)RAND::Poisson(_genomic_mutation_rate);
  unsigned int mut_locus, mut_all, pos;
  double *effects;

  while(NbMut != 0) {
    mut_locus = RAND::Uniform(_nb_locus);
    effects = (this->*_getMutationValues)(mut_locus);
    mut_all = RAND::RandBool();
    pos = mut_locus*_nb_traits;

    for(unsigned int i = 0; i < _nb_traits; i++) {
      ind->set_allele( pos + i, mut_all, ind->get_allele_value(pos + i, mut_all) + effects[i]);
    }

    NbMut--;
  }

}
// ----------------------------------------------------------------------------------------
// mutate_diallelic_HC
// ----------------------------------------------------------------------------------------
inline void TProtoQuanti::mutate_diallelic_HC (TTQuanti* ind)
{
  unsigned int NbMut = (unsigned int)RAND::Poisson(_genomic_mutation_rate);
  unsigned int mut_locus, mut_all, block, pos;

  while(NbMut != 0) {

    mut_locus = RAND::Uniform(_nb_locus);
    mut_all = RAND::RandBool();

    block = mut_locus*_nb_traits;

    for(unsigned int i = 0; i < _nb_traits; i++) {
      pos = block + i;
      ind->set_allele( pos, mut_all, -1.0 * ind->get_allele_value(pos, mut_all));
    }

    NbMut--;
  }

}
// ----------------------------------------------------------------------------------------
// mutate_HC
// ----------------------------------------------------------------------------------------
inline void TProtoQuanti::mutate_HC (TTQuanti* ind)
{
  unsigned int NbMut = (unsigned int)RAND::Poisson(_genomic_mutation_rate);
  unsigned int mut_locus, mut_all, pos;
  double *effects;

  while(NbMut != 0) {
    mut_locus = RAND::Uniform(_nb_locus);
    effects = (this->*_getMutationValues)(mut_locus);
    mut_all = RAND::RandBool();
    pos = mut_locus*_nb_traits;

    for(unsigned int i = 0; i < _nb_traits; i++)
      ind->set_allele( pos + i, mut_all, effects[i]);

    NbMut--;
  }
}
// ----------------------------------------------------------------------------------------
// hatch
// ----------------------------------------------------------------------------------------
TTQuanti* TProtoQuanti::hatch()
{
  TTQuanti* kid = new TTQuanti();
    
  kid->set_proto(this);

  return kid;
}
// ----------------------------------------------------------------------------------------
// loadFileServices
// ----------------------------------------------------------------------------------------
void TProtoQuanti::loadFileServices  (FileServices* loader)
{ 
  int logtime = 0;
  //writer
  if(get_parameter("quanti_output")->isSet()) {
    
    if(_writer == NULL) _writer = new TTQuantiFH(this);
    
    _writer->setOutputOption(get_parameter("quanti_output")->getArg());
    
    if(!get_parameter("quanti_logtime")->isSet())
    	fatal("parameter \"quanti_logtime\" is missing!\n");

    Param* param = get_parameter("quanti_logtime");
    
    logtime = (param->isSet() ? (int)param->getValue() : 0);

    if(param->isMatrix()) {
      
      TMatrix temp;
      param->getMatrix(&temp);
      _writer->set_multi(true, true, 1, &temp, get_parameter("quanti_dir")->getArg());
      
    } else   //  rpl_per, gen_per, rpl_occ, gen_occ, rank (0), path, self-ref      
      _writer->set(true, true, 1, logtime, 0, get_parameter("quanti_dir")->getArg(),this);
    
    loader->attach(_writer);
    
  } else if(_writer != NULL) {
    delete _writer;
    _writer = NULL;
  }
  
  //freq extractor
  if(get_parameter("quanti_freq_output")->isSet()) {
    
    string model = get_parameter("quanti_allele_model")->getArg();
    
    if(model != "diallelic" && model != "diallelic_HC") {
    
      error("option \"quanti_freq_output\" only works with di-allelic quantitative loci, ignoring option.\n");
    
    }else {

      if(_freqExtractor == NULL) _freqExtractor = new TTQFreqExtractor(this);

      _freqExtractor->resetTable();

      Param* param = get_parameter("quanti_freq_logtime");

      logtime = (param->isSet() ? (int)param->getValue() : 0);

      //save multiple generations in a single file:
      _freqExtractor->set(true, true, 1, logtime, 0, get_parameter("quanti_dir")->getArg(),this);

      loader->attach(_freqExtractor);
    }
    
  } else if(_freqExtractor != NULL) {
    delete _freqExtractor;
    _freqExtractor = NULL;
  }
  
  //load the reader:
  if(_reader) delete _reader;
  _reader = new TTQuantiFH(this);
  _reader->set_isInputHandler(true);
  loader->attach_reader(_reader);

}
// ----------------------------------------------------------------------------------------
// loadStatServices
// ----------------------------------------------------------------------------------------
void TProtoQuanti::loadStatServices  (StatServices* loader)
{
  //allocate the stat handler
  if(_stats == NULL)
    _stats = new TTQuantiSH(this);
  
  if(_stats != NULL) {
    loader->attach(_stats);
  }
}
// ----------------------------------------------------------------------------------------

//                             TTQuanti

// ----------------------------------------------------------------------------------------
// operator=
// ----------------------------------------------------------------------------------------
TTQuanti& TTQuanti::operator= (const TTrait& T)
{  
  const TTQuanti& TQ = dynamic_cast<const TTQuanti&>(T);

  assert(*this == TQ);  // to make sure that the sequences are of same length

  if(this != &TQ) {

    init();

    memcpy(_sequence[0], TQ._sequence[0], _myProto->_seq_length*sizeof(double));
    memcpy(_sequence[1], TQ._sequence[1], _myProto->_seq_length*sizeof(double));

    set_value();
  }
  
  return *this;
}
// ----------------------------------------------------------------------------------------
// operator==
// ----------------------------------------------------------------------------------------
bool TTQuanti::operator== (const TTrait& T)
{ 
  if(this->get_type().compare(T.get_type()) != 0) return false;
  
  const TTQuanti& TQ = dynamic_cast<const TTQuanti&>(T);
  
  if(this != &TQ) {
    if(_myProto->_nb_locus != TQ._myProto->_nb_locus) return false;
    if(_myProto->_nb_traits != TQ._myProto->_nb_traits) return false;
    if(_myProto->_seq_length != TQ._myProto->_seq_length) return false;
  }
  return true;
}
// ----------------------------------------------------------------------------------------
// operator!=
// ----------------------------------------------------------------------------------------
bool TTQuanti::operator!= (const TTrait& T)
{
  if(!((*this) == T) )
    return true;
  else
    return false;
}
// ----------------------------------------------------------------------------------------
// get_allele_value
// ----------------------------------------------------------------------------------------
inline double TTQuanti::get_allele_value (int loc, int all)
{  
  return (loc < (int)_myProto->_seq_length && all < 2 ? _sequence[all][loc] : 0);
}
// ----------------------------------------------------------------------------------------
// set_allele_value
// ----------------------------------------------------------------------------------------
inline void TTQuanti::set_allele_value (unsigned int locus, unsigned int allele, double value)
{ 
  assert(locus < _myProto->_nb_locus && allele < 2);
  _sequence[allele][locus] = value;
}
// ----------------------------------------------------------------------------------------
// init
// ----------------------------------------------------------------------------------------
inline void TTQuanti::init ()
{  
  reset();

  _sequence = new double*[2];
  _sequence[0] = new double [ _myProto->_seq_length];
  _sequence[1] = new double [ _myProto->_seq_length];
  
  _phenotypes = new double [_myProto->_nb_traits];

}
// ----------------------------------------------------------------------------------------
// set_sequence
// ----------------------------------------------------------------------------------------
void TTQuanti::set_sequence(void** seq)
{
  init();
  memcpy(_sequence[0], seq[0], _myProto->_seq_length*sizeof(double));
  memcpy(_sequence[1], seq[1], _myProto->_seq_length*sizeof(double));
}
// ----------------------------------------------------------------------------------------
// init_sequence
// ----------------------------------------------------------------------------------------
inline void TTQuanti::init_sequence ()
{
  unsigned int pos;
  //options:
  //0: no variation, init value = (trait value)/(2*_nb_locus)
  //1: init value = (trait value)/(2*_nb_locus) + 1 mutation/locus
  //2: init value = (trait value)/(2*_nb_locus) + 1 mutation/locus+make parts ==> mean trait value doesn't change
  //3: init value = (trait value + random deviate N(0,sdVm))/(2*_nb_locus) + 1 mutation/locus+make parts
  //4: no variation and initialize with opposite effect alleles at alternating loci.
  
  //decide what initial value to use
  
  //Note: the initial values may have been set individually by LCE_quanti
  //      it wouldn't make sense then to store the init values in the prototype 
  //      because init values are set patch-specific by LCE_quanti
  //      Problem: without LCE_quanti, the original init values must not change from one replicate
  //      to the other, so we use a local variable
  
  double my_init[_myProto->_nb_traits];

  if(_myProto->_doInitMutation == 3) {
    
    double sdVm;
    
    for(unsigned int j = 0; j < _myProto->_nb_traits; j++) {
      //      sdVm = sqrt(4*_nb_locus*_mut_rate*_myProto->get_trait_var(j)); //trait variance = 2Vm
      sdVm = 0.25;
      my_init[j] = (_myProto->_init_value[j] + RAND::Gaussian(sdVm)) / (2*_myProto->_nb_locus);
    }
    
  } else {
    
    for(unsigned int j = 0; j < _myProto->_nb_traits; j++) my_init[j] = _myProto->_init_value[j] / (2*_myProto->_nb_locus);
    
  }
  
  if(_myProto->_allele_model < 3) { //for the di-allelic models

    if (_myProto->_doInitMutation != 4){
      for(unsigned int i = 0; i < _myProto->_nb_locus; i++) {
        pos = i * _myProto->_nb_traits;
        for(unsigned int j = 0; j < _myProto->_nb_traits; j++) {

          _sequence[0][pos] = _myProto->_allele_value[i][0]; //set with the positive allele value
          _sequence[1][pos] = _myProto->_allele_value[i][0];
          pos++;


        }
      }

    } else {
      //this is intended for a diallelic model with no initial variance

      for(unsigned int i = 0; i < _myProto->_nb_locus; i++) {
        pos = i * _myProto->_nb_traits;
        for(unsigned int j = 0; j < _myProto->_nb_traits; j++) {
          if (i % 2 == 0){
            _sequence[0][pos] = _myProto->_allele_value[i][0]; //set with the positive allele value
            _sequence[1][pos] = _myProto->_allele_value[i][0];
            pos++;
          } else {
            _sequence[0][pos] = _myProto->_allele_value[i][0]*-1; //set with the negative allele value
            _sequence[1][pos] = _myProto->_allele_value[i][0]*-1;
            pos++;
          }
        }
      }

    }
  } else {

    //set the allele values from the trait value
    for(unsigned int i = 0; i < _myProto->_nb_locus; i++) {
      pos = i * _myProto->_nb_traits;
      for(unsigned int j = 0; j < _myProto->_nb_traits; j++) {
        _sequence[0][pos] = my_init[j];
        _sequence[1][pos] = my_init[j];
        pos++;
      }
    }
  }

  //add random effects to allele values if needed
  if(_myProto->_doInitMutation != 0 && _myProto->_doInitMutation != 4) {
    
    double *mut1, *mut2;
    unsigned int L;
    
    for(unsigned int i = 0; i < _myProto->_nb_locus; i++) {
      
      pos = i * _myProto->_nb_traits;
      
      mut1 = (_myProto->*_myProto->_getMutationValues)(i);
      mut2 = (_myProto->*_myProto->_getMutationValues)(i);
      
      if(_myProto->_allele_model < 3) { //diallelic models

        for(unsigned int j = 0; j < _myProto->_nb_traits; j++) {
          _sequence[0][pos] = mut1[j]; 
          _sequence[1][pos] = mut2[j]; 
          pos++;
        }
        
      } else {

        for(unsigned int j = 0; j < _myProto->_nb_traits; j++) {
          _sequence[0][pos] += mut1[j]; 
          _sequence[1][pos] += mut2[j]; 
          pos++;
        }
        
        if(_myProto->_doInitMutation > 1) { // the make-parts algorithm
          
          //select a random locus
          do{
            L = RAND::Uniform(_myProto->_nb_locus);
          }while(L == i);
          
          pos = L * _myProto->_nb_traits;
          //subtract the previous random deviates from that locus
          for(unsigned int j = 0; j < _myProto->_nb_traits; j++) {
            _sequence[0][pos] -= mut1[j]; 
            _sequence[1][pos] -= mut2[j]; 
            pos++;
          }
        }
      }
    }
  } 
  
}
// ----------------------------------------------------------------------------------------
// reset
// ----------------------------------------------------------------------------------------
inline void TTQuanti::reset ()
{
  if(_sequence != NULL) {
    delete [] _sequence[0]; 
    delete [] _sequence[1];
    delete [] _sequence; 
    _sequence = NULL;
  }
  if(_phenotypes != NULL) delete [] _phenotypes;
  _phenotypes = NULL;
}
// ----------------------------------------------------------------------------------------
// inherit
// ----------------------------------------------------------------------------------------
inline void TTQuanti::inherit (TTrait* mother, TTrait* father)
{
  double** mother_seq = (double**)mother->get_sequence();
  double** father_seq = (double**)father->get_sequence();
  
  _myProto->inherit(FEM, _sequence[FEM], mother_seq);
  
  _myProto->inherit(MAL, _sequence[MAL], father_seq);
}
// ----------------------------------------------------------------------------------------
// mutate
// ----------------------------------------------------------------------------------------
inline void TTQuanti::mutate()
{
  _myProto->mutate(this);
  }
  
// ----------------------------------------------------------------------------------------
// set_value
// ----------------------------------------------------------------------------------------
inline void TTQuanti::set_value ()
{
  (this->*_myProto->_set_value_func_ptr)();
}
// ----------------------------------------------------------------------------------------
// set_value
// ----------------------------------------------------------------------------------------
inline void TTQuanti::set_value_noVE ()
{
  unsigned int loc;
  
  for(unsigned int i = 0; i < _myProto->_nb_traits; ++i)
    _phenotypes[i] = 0;
    
  for(unsigned int j = 0; j < _myProto->_nb_locus; ++j) {
    loc = j * _myProto->_nb_traits;
    for(unsigned int i = 0; i < _myProto->_nb_traits; ++i) {
      _phenotypes[i] += _sequence[0][loc] + _sequence[1][loc];
      loc++;
    }
  }
} 
// ----------------------------------------------------------------------------------------
// set_value
// ----------------------------------------------------------------------------------------
inline void TTQuanti::set_value_VE ()
{
  unsigned int loc;
  
  for(unsigned int i = 0; i < _myProto->_nb_traits; ++i)
    _phenotypes[i] = 0;
  
  for(unsigned int j = 0; j < _myProto->_nb_locus; ++j) {
    loc = j * _myProto->_nb_traits;
    for(unsigned int i = 0; i < _myProto->_nb_traits; ++i) {
      _phenotypes[i] += _sequence[0][loc] + _sequence[1][loc];
      loc++;
    }
  } 
  
  for(unsigned int i = 0; i < _myProto->_nb_traits; ++i) {
    _phenotypes[i] += RAND::Gaussian( _myProto->_eVariance[i] ); // variances have been square-rooted before
  }

}
// ----------------------------------------------------------------------------------------
// set_phenotype
// ----------------------------------------------------------------------------------------
void TTQuanti::set_phenotype (unsigned int trait, double value)
{
  assert(trait < _myProto->_nb_traits && _phenotypes != NULL);

  _phenotypes[trait] = value;
}
// ----------------------------------------------------------------------------------------
// get_genotype
// ----------------------------------------------------------------------------------------
double TTQuanti::get_genotype (unsigned int trait)
{
  unsigned int loc;
  double genotype = 0;
  
  for(unsigned int j = 0; j < _myProto->_nb_locus; ++j) {
    loc = j * _myProto->_nb_traits + trait;
    genotype += _sequence[0][loc] + _sequence[1][loc];
  } 
  return genotype;
}
// ----------------------------------------------------------------------------------------
// store_data
// ----------------------------------------------------------------------------------------
inline void TTQuanti::store_data ( BinaryStorageBuffer* saver )
{
  saver->store(_sequence[0],_myProto->_seq_length*sizeof(double));
  saver->store(_sequence[1],_myProto->_seq_length*sizeof(double));
}
// ----------------------------------------------------------------------------------------
// retrieve_data
// ----------------------------------------------------------------------------------------
inline bool TTQuanti::retrieve_data ( BinaryStorageBuffer* reader )
{
  reader->read(_sequence[0],_myProto->_seq_length*sizeof(double));
  reader->read(_sequence[1],_myProto->_seq_length*sizeof(double));
  return true;
}
// ----------------------------------------------------------------------------------------
// show_up
// ----------------------------------------------------------------------------------------
void TTQuanti::show_up  ()
{
  message("\
          Trait's type: QUANTI\n\
          traits: %i\n\
          loci: %i\n\
          seq length: %i\n",_myProto->_nb_traits,_myProto->_nb_locus
          ,_myProto->_seq_length);
  
  for(unsigned int i = 0; i < _myProto->_nb_traits; i++)
    message("phenotype %i: %f\n",i+1,_phenotypes[i]);
  
  message("_sequence: \n0:");
  unsigned int loc;
  for(unsigned int i = 0; i < _myProto->_nb_traits; ++i) {
    message("\nt%i:",i+1);
    for(unsigned int j = 0; (j < _myProto->_nb_locus); ++j) {
      loc = j * _myProto->_nb_traits + i;
      message("%.3f,",_sequence[0][loc]);
    }
  }
  message("\n1:");
  for(unsigned int i = 0; i < _myProto->_nb_traits; ++i) {
    message("\nt%i:",i+1);
    for(unsigned int j = 0; (j < _myProto->_nb_locus); ++j) {
      loc = j * _myProto->_nb_traits + i;
      message("%.3f,",_sequence[1][loc]);
    }
  }
  message("\n");
  
}  
// ------------------------------------------------------------------------------

//                             TTQuantiSH

// ----------------------------------------------------------------------------------------
// 
// ----------------------------------------------------------------------------------------
void TTQuantiSH::resetPtrs ( )
{  
  
  if(_G != NULL) gsl_matrix_free(_G);
  if(_eval != NULL) gsl_vector_free(_eval);
  if(_evec != NULL) gsl_matrix_free(_evec);
  if(_ws != NULL)  gsl_eigen_symmv_free (_ws);
  
  if(_meanP != NULL) delete [] _meanP;
  if(_meanG != NULL) delete [] _meanG;
  if(_Va != NULL) delete [] _Va;
  if(_Vb != NULL) delete [] _Vb;
  if(_Vp != NULL) delete [] _Vp;
  if(_covar != NULL) delete [] _covar;
  _covar = 0;
  if(_eigval != NULL) delete [] _eigval;
  
  
  if(_eigvect) {
    for(unsigned int i=0; i < _nb_trait; ++i) delete [] _eigvect[i];
    delete [] _eigvect;
  }
  if(_pVa) {
    for(unsigned int i=0; i < _nb_trait; ++i) delete [] _pVa[i];
    delete [] _pVa;
  }
  if(_pVp) {
    for(unsigned int i=0; i < _nb_trait; ++i) delete [] _pVp[i];
    delete [] _pVp;
  }
  if(_pmeanP) {
    for(unsigned int i=0; i < _nb_trait; ++i) delete [] _pmeanP[i];
    delete [] _pmeanP;
  }
  if(_pmeanG) {
    for(unsigned int i=0; i < _nb_trait; ++i) delete [] _pmeanG[i];
    delete [] _pmeanG;
  }
  if(_peigval) {
    for(unsigned int i=0; i < _nb_trait; ++i) delete [] _peigval[i];
    delete [] _peigval;
  }
  
  if(_pcovar != NULL) {  
    for(unsigned int i = 0; i < (_nb_trait*(_nb_trait - 1)/2); i++) delete [] _pcovar[i];
    delete [] _pcovar;
    _pcovar = 0;
  }
  
  if(_peigvect != NULL) {
    for(unsigned int i = 0; i < _patchNbr; i++) delete [] _peigvect[i];
    delete []  _peigvect;
    _peigvect = 0;
  }
  
}
// ----------------------------------------------------------------------------------------
// init
// ----------------------------------------------------------------------------------------
void TTQuantiSH::init()
{  
  StatHandlerBase::init();
  
  _eVar = (!_SHLinkedTrait->get_env_var().empty());
  
  resetPtrs(); //deallocate anything that was previously allocated
  
  if(_patchNbr != _pop->getPatchNbr())
    _patchNbr = _pop->getPatchNbr();
  
  if(_nb_trait != _SHLinkedTrait->get_nb_traits())
    _nb_trait = _SHLinkedTrait->get_nb_traits();
  
  _G = gsl_matrix_alloc(_nb_trait,_nb_trait);
  _eval = gsl_vector_alloc (_nb_trait);
  _evec = gsl_matrix_alloc (_nb_trait, _nb_trait);
  _ws = gsl_eigen_symmv_alloc (_nb_trait);
  
  _meanP = new double [_nb_trait];
  _meanG = new double [_nb_trait];
  _Va = new double [_nb_trait];
  _Vb = new double [_nb_trait];
  _Vp = new double [_nb_trait];
  _eigval = new double [_nb_trait];
  
  _eigvect = new double* [_nb_trait];
  _pVa = new double* [_nb_trait];
  _pVp = new double* [_nb_trait];
  _pmeanP = new double* [_nb_trait];
  _pmeanG = new double* [_nb_trait];
  _peigval = new double* [_nb_trait];
  
  for(unsigned int i=0; i < _nb_trait; ++i) {
    
    _eigvect[i]  = new double [_nb_trait];
    
    _pVa[i] = new double [_patchNbr];
    _pVp[i] = new double [_patchNbr];
    _pmeanP[i] = new double [_patchNbr];
    _pmeanG[i] = new double [_patchNbr];
    _peigval[i] = new double [_patchNbr];
    
  }
  
  _peigvect = new double* [_patchNbr];
  for(unsigned int i = 0; i < _patchNbr; i++)     
    _peigvect[i] = new double [_nb_trait*_nb_trait]; 
  
  
  if(_nb_trait > 1) {
    
    _covar = new double [_nb_trait*(_nb_trait -1)/2];
    
    _pcovar = new double* [_nb_trait*(_nb_trait - 1)/2];

    for(unsigned int i = 0; i < _nb_trait*(_nb_trait - 1)/2; i++)
      _pcovar[i] = new double [_patchNbr];
    
  }
  
}
// ----------------------------------------------------------------------------------------
// setStatsRecorders
// ----------------------------------------------------------------------------------------
bool TTQuantiSH::setStatRecorders(std::string& token)
{
#ifdef _DEBUG_
  message("-TTQuantiSH::setStatRecorders ");
#endif   
  string age_tag = token.substr(0,token.find_first_of("."));
  string sub_token;
  age_t AGE = ALL;
  
  if (age_tag.size() != 0 && age_tag.size() != string::npos) {
    
    if (age_tag == "adlt") AGE = ADULTS;
    
    else if (age_tag == "off") AGE = OFFSPRG;
    
    else age_tag = "";
    
  } else {
    age_tag = "";
  }
  
  if (age_tag.size() != 0) 
    sub_token = token.substr(token.find_first_of(".") + 1, string::npos);
  else
    sub_token = token;
    
  if(sub_token == "quanti") {
    addQuanti(AGE);
  } else if(sub_token == "quanti.eigen") {  //based on Vb; among-patch (D) matrix
    addEigen(AGE);
  } else if(sub_token == "quanti.eigenvalues") {  //based on Vb; among-patch (D) matrix
    addEigenValues(AGE);
  } else if(sub_token == "quanti.eigenvect1") {  //based on Vb; among-patch (D) matrix
    addEigenVect1(AGE);
  } else if(sub_token == "quanti.patch") {
    addQuantiPerPatch(AGE);
  } else if(sub_token == "quanti.mean.patch") {
    addAvgPerPatch(AGE);
  } else if(sub_token == "quanti.var.patch") {
    addVarPerPatch(AGE);
  } else if(sub_token == "quanti.covar.patch") {
    addCovarPerPatch(AGE);
  } else if(sub_token == "quanti.eigen.patch") {
    addEigenPerPatch(AGE);
  } else if(sub_token == "quanti.eigenvalues.patch") {
    addEigenValuesPerPatch(AGE);
  } else if(sub_token == "quanti.eigenvect1.patch") {
    addEigenVect1PerPatch(AGE);
  } else if(sub_token == "quanti.skew.patch") {
    addSkewPerPatch(AGE);
  } else
    return false;
  
  return true;
}
// ----------------------------------------------------------------------------------------
// addQuanti
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addQuanti (age_t AGE)
{
  if (AGE == ALL) {
    addQuanti(ADULTS);
    addQuanti(OFFSPRG);
    return;
  }
  
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  string name = suffix + "q";
  string t1, t2;
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats : &TTQuantiSH::setOffsprgStats);
  
  add("",suffix + "q1",AGE,0,0,0,&TTQuantiSH::getMeanPhenot,0,setter);
  
  for(unsigned int i = 1; i < _nb_trait; i++) {
    t1 = tstring::int2str(i+1);
    add("", name + t1,AGE,i,0,0,&TTQuantiSH::getMeanPhenot,0,0);
  } 
  
  //Va
  for(unsigned int i = 0; i < _nb_trait; i++) {
    t1 = tstring::int2str(i+1);
    add("", name + t1 +".Va",AGE,i,0,0,&TTQuantiSH::getVa,0,0);
  }
  
  //Vp
  if(_eVar) {
    for(unsigned int i = 0; i < _nb_trait; i++) {
      t1 = tstring::int2str(i+1);
      add("", name + t1 +".Vp",AGE,i,0,0,&TTQuantiSH::getVp,0,0);
    }
  }

  //Vb
  if(_pop->getPatchNbr() > 1){
    for(unsigned int i = 0; i < _nb_trait; i++) {
      t1 = tstring::int2str(i+1);
      add("", name + t1 +".Vb",AGE,i,0,0,&TTQuantiSH::getVb,0,0);
    }

    //Qst
    for(unsigned int i = 0; i < _nb_trait; i++) {
      t1 = tstring::int2str(i+1);
      add("", name + t1 +".Qst",AGE,i,0,0,&TTQuantiSH::getQst,0,0);
    }
  }
  
  if (_nb_trait > 1) {
    unsigned int c = 0;
    for(unsigned int t = 0; t < _nb_trait; ++t) {
      for(unsigned int v = t + 1; v < _nb_trait; ++v) {
        t1 = tstring::int2str((t+1)*10+(v+1));
        add("", name + t1 +".cov", AGE, c++, 0,0,&TTQuantiSH::getCovar,0,0);
      }
    }
  }
}
// ----------------------------------------------------------------------------------------
// addEigen
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addEigen (age_t AGE)
{
  if(_nb_trait < 2) {
    warning("not recording G-matrix eigen stats with only one \"quanti\" trait!\n");
    return;
  }
  
  if (AGE == ALL) {
    addEigen(ADULTS);
    addEigen(OFFSPRG);
    return;
  }
  
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats : &TTQuantiSH::setOffsprgStats);
  
  add("", suffix + "q.eval1",AGE,0,0,0,&TTQuantiSH::getEigenValue, 0, setter); //this one calls the setter
  
  for(unsigned int t = 1; t < _nb_trait; ++t)
    add("", suffix + "q.eval" + tstring::int2str(t+1),AGE,t,0,0,&TTQuantiSH::getEigenValue, 0, 0);
  
  for(unsigned int t = 0; t< _nb_trait; ++t)
    for(unsigned int v = 0; v < _nb_trait; ++v)
      add("", suffix + "q.evect" + tstring::int2str((t+1)*10+(v+1)),AGE,t,v,0,0,&TTQuantiSH::getEigenVectorElt,0);
  
}
// ----------------------------------------------------------------------------------------
// addEigenValues
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addEigenValues (age_t AGE)
{
  if(_nb_trait < 2) {
    warning("not recording G-matrix eigen stats with only one \"quanti\" trait!\n");
    return;
  }
  
  if (AGE == ALL) {
    addEigen(ADULTS);
    addEigen(OFFSPRG);
    return;
  }
  
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats : &TTQuantiSH::setOffsprgStats);
  
  add("", suffix + "q.eval1",AGE,0,0,0,&TTQuantiSH::getEigenValue, 0, setter);
  
  for(unsigned int t = 1; t < _nb_trait; ++t)
    add("", suffix + "q.eval" + tstring::int2str(t+1),AGE,t,0,0,&TTQuantiSH::getEigenValue, 0, 0);
  
  
}
// ----------------------------------------------------------------------------------------
// addEigenVect1 : save only the first eigenvector
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addEigenVect1 (age_t AGE)
{
  if(_nb_trait < 2) {
    warning("not recording G-matrix eigen stats with only one \"quanti\" trait!\n");
    return;
  }
  
  if (AGE == ALL) {
    addEigen(ADULTS);
    addEigen(OFFSPRG);
    return;
  }
  
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats : &TTQuantiSH::setOffsprgStats);
  
  add("", suffix + "q.evect11",AGE,0,0,0,0,&TTQuantiSH::getEigenVectorElt,setter);
  
  for(unsigned int v = 1; v < _nb_trait; ++v)
    add("", suffix + "q.evect1" + tstring::int2str(v+1),AGE,0,v,0,0,&TTQuantiSH::getEigenVectorElt,0);
  
}
// ----------------------------------------------------------------------------------------
// addQuantiPerPatch
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addQuantiPerPatch (age_t AGE)
{
  
  if (AGE == ALL) {
    addQuantiPerPatch(ADULTS);
    addQuantiPerPatch(OFFSPRG);
    return;
  }
  
  addAvgPerPatch(AGE);
  addVarPerPatch(AGE);
  addCovarPerPatch(AGE);
//  addEigenPerPatch(AGE);
  
}
// ----------------------------------------------------------------------------------------
// addAvgPerPatch
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addAvgPerPatch (age_t AGE)
{
  if (AGE == ALL) {
    addAvgPerPatch(ADULTS);
    addAvgPerPatch(OFFSPRG);
    return;
  }
  
  unsigned int patchNbr = _pop->getPatchNbr();
  
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  string name;
  string patch;
  string t1;
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats : &TTQuantiSH::setOffsprgStats);
  
  add("Mean phenotype of trait 1 in patch 1", suffix + "q1.p1",  AGE, 0, 0, 0, 0,
      &TTQuantiSH::getMeanPhenotPerPatch, setter);
  
  for(unsigned int p = 0; p < patchNbr; p++) {
    for(unsigned int i = 0; i < _nb_trait; i++) {
      if(p == 0 && i == 0) continue;
      name = "Mean phenotype of trait " + tstring::int2str(i+1) + " in patch " + tstring::int2str(p+1);
      t1 = "q" + tstring::int2str(i+1);
      patch = ".p" + tstring::int2str(p+1);
      add(name, suffix + t1 + patch, AGE, i, p, 0, 0, &TTQuantiSH::getMeanPhenotPerPatch, 0);
    } 
  }
  
}
// ----------------------------------------------------------------------------------------
// addVarPerPatch
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addVarPerPatch (age_t AGE)
{
  if (AGE == ALL) {
    addVarPerPatch(ADULTS);
    addVarPerPatch(OFFSPRG);
    return;
  }
  
  unsigned int patchNbr = _pop->getPatchNbr();
  
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  string name;
  string patch;
  string t1;
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats :
                                        &TTQuantiSH::setOffsprgStats);
  
  add("Genetic variance of trait 1 in patch 1", suffix + "Va.q1.p1",  AGE, 0, 0, 0, 0,
      &TTQuantiSH::getVaPerPatch, setter);
  
  for(unsigned int p = 0; p < patchNbr; p++) {
    for(unsigned int i = 0; i < _nb_trait; i++) {
      if(p == 0 && i == 0) continue;
      name = "Genetic variance of trait " + tstring::int2str(i+1) + " in patch " + tstring::int2str(p+1);
      t1 = "q" + tstring::int2str(i+1);
      patch = ".p" + tstring::int2str(p+1);
      add(name, suffix + "Va." + t1 + patch, AGE, i, p, 0, 0, &TTQuantiSH::getVaPerPatch, 0);
    }
  }
  
  if(_eVar) {
    for(unsigned int p = 0; p < patchNbr; p++) {
      for(unsigned int i = 0; i < _nb_trait; i++) {
        name = "Phenotypic variance of trait " + tstring::int2str(i+1) + " in patch " + tstring::int2str(p+1);
        t1 = "q" + tstring::int2str(i+1);
        patch = ".p" + tstring::int2str(p+1);
        add(name, suffix + "Vp." + t1 + patch, AGE, i, p, 0, 0, &TTQuantiSH::getVpPerPatch, 0);
      }
    }
  }
}
// ----------------------------------------------------------------------------------------
// addCovarPerPatch
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addCovarPerPatch (age_t AGE)
{
  if(_nb_trait < 2) {
//    warning("not recording traits covariance with only one \"quanti\" trait!");
    return;
  }
  
  if (AGE == ALL) {
    addCovarPerPatch(ADULTS);
    addCovarPerPatch(OFFSPRG);
    return;
  }
  
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  string patch;
  string cov;
  unsigned int patchNbr = _pop->getPatchNbr();
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats : &TTQuantiSH::setOffsprgStats);
  
  add("Genetic covariance of trait 1 and trait 2 in patch 1", suffix + "cov.q12.p1",  AGE, 0, 0, 0, 0,
      &TTQuantiSH::getCovarPerPatch, setter);
  
  unsigned int c;
  for(unsigned int p = 0; p < patchNbr; p++) {
    patch = ".p" + tstring::int2str(p+1);
    c = 0;
    for(unsigned int t = 0; t < _nb_trait; ++t) {
      for(unsigned int v = t + 1; v < _nb_trait; ++v){
        if(p==0 && t==0 && v==1) {c++; continue;}
        cov = tstring::int2str((t+1)*10+v+1);
        add("", suffix + "cov.q" + cov + patch,  AGE, c++, p, 0, 0, &TTQuantiSH::getCovarPerPatch, 0);
      }
    }
  }
  
}
// ----------------------------------------------------------------------------------------
// addEigenPerPatch
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addEigenPerPatch (age_t AGE)
{
  if(_nb_trait < 2) {
    warning("not recording G-matrix eigen stats with only one \"quanti\" trait!\n");
    return;
  }
  
  if (AGE == ALL) {
    addEigenPerPatch(ADULTS);
    addEigenPerPatch(OFFSPRG);
    return;
  }
  
  unsigned int patchNbr = _pop->getPatchNbr();
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  string patch;
  unsigned int pv =0;
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats : &TTQuantiSH::setOffsprgStats);
  
  
  add("First G-matrix eigenvalue in patch 1", suffix + "qeval1.p1",  AGE, 0, 0, 0, 0,
      &TTQuantiSH::getEigenValuePerPatch, setter);
  
  for(unsigned int p = 0; p < patchNbr; ++p) {
    patch = ".p" + tstring::int2str(p+1);
    for(unsigned int t = 0; t < _nb_trait; ++t) {
      if(p==0 && t==0) continue;
      add("", suffix + "qeval" + tstring::int2str(t+1) + patch,  AGE, t, p, 0, 0, &TTQuantiSH::getEigenValuePerPatch,0);
    }
  }
  for(unsigned int p = 0; p < patchNbr; ++p) {
    patch = ".p" + tstring::int2str(p+1);
    pv = 0;
    for(unsigned int t = 0; t < _nb_trait; ++t)
      for(unsigned int v = 0; v < _nb_trait; ++v)
        add("", suffix + "qevect" + tstring::int2str((t+1)*10+v+1) + patch,  AGE, p, pv++, 0, 0, &TTQuantiSH::getEigenVectorEltPerPatch,0);
  }
  
}
// ----------------------------------------------------------------------------------------
// addEigenValuesPerPatch
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addEigenValuesPerPatch (age_t AGE)
{
  if(_nb_trait < 2) {
    warning("not recording G-matrix eigen stats with only one \"quanti\" trait!\n");
    return;
  }
  
  if (AGE == ALL) {
    addEigenPerPatch(ADULTS);
    addEigenPerPatch(OFFSPRG);
    return;
  }
  
  unsigned int patchNbr = _pop->getPatchNbr();
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  string patch;
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats : &TTQuantiSH::setOffsprgStats);
  
  add("First G-matrix eigenvalue in patch 1", suffix + "qeval1.p1",  AGE, 0, 0, 0, 0,
      &TTQuantiSH::getEigenValuePerPatch, setter);
  
  for(unsigned int p = 0; p < patchNbr; ++p) {
    patch = ".p" + tstring::int2str(p+1);
    for(unsigned int t = 0; t < _nb_trait; ++t) {
      if(p==0 && t==0) continue;
      add("", suffix + "qeval" + tstring::int2str(t+1) + patch,  AGE, t, p, 0, 0, &TTQuantiSH::getEigenValuePerPatch,0);
    }
  }  
}
// ----------------------------------------------------------------------------------------
// addEigenVect1PerPatch
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addEigenVect1PerPatch (age_t AGE)
{
  if(_nb_trait < 2) {
    warning("not recording G-matrix eigen stats with only one \"quanti\" trait!\n");
    return;
  }
  
  if (AGE == ALL) {
    addEigenPerPatch(ADULTS);
    addEigenPerPatch(OFFSPRG);
    return;
  }
  
  unsigned int patchNbr = _pop->getPatchNbr();
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  string patch;
  unsigned int pv =0;
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats : 
                                        &TTQuantiSH::setOffsprgStats);
  
  
  add("First G-matrix eigenvector in patch 1", suffix + "qevect11.p1",  AGE, 0, 0, 0, 0,
      &TTQuantiSH::getEigenVectorEltPerPatch, setter);
  
  for(unsigned int p = 0; p < patchNbr; ++p) {
    patch = ".p" + tstring::int2str(p+1);
    pv = 0;
    //    for(unsigned int t = 0; t < _nb_trait; ++t)
    for(unsigned int v = 0; v < _nb_trait; ++v){
      if(p==0 && v==0) {pv++; continue;}
      add("", suffix + "qevect1" + tstring::int2str(v+1) + patch,  AGE, p, pv++, 0, 0, &TTQuantiSH::getEigenVectorEltPerPatch,0);
    }
  }
}
// ----------------------------------------------------------------------------------------
// addSkewPerPatch
// ----------------------------------------------------------------------------------------
void TTQuantiSH::addSkewPerPatch(age_t AGE)
{
  if (AGE == ALL) {
    addSkewPerPatch(ADULTS);
    addSkewPerPatch(OFFSPRG);
    return;
  }
  
  unsigned int patchNbr = _pop->getPatchNbr();
  
  string suffix = (AGE == ADULTS ? "adlt.":"off.");
  string name;
  string patch;
  string t1;
  
  void (TTQuantiSH::* setter) (void) = (AGE == ADULTS ?
                                        &TTQuantiSH::setAdultStats :
                                        &TTQuantiSH::setOffsprgStats);
  
  add("Genetic skew of trait 1 in patch 1", suffix + "Sk.q1.p1",  AGE, 0, 0, 0, 0,
      &TTQuantiSH::getSkewPerPatch, setter);
  
  for(unsigned int p = 0; p < patchNbr; p++) {
    for(unsigned int i = 0; i < _nb_trait; i++) {
      if(p == 0 && i == 0) continue;
      name = "Genetic skew of trait " + tstring::int2str(i+1) + " in patch " + tstring::int2str(p+1);
      t1 = "q" + tstring::int2str(i+1);
      patch = ".p" + tstring::int2str(p+1);
      add(name, suffix + "Sk." + t1 + patch, AGE, i, p, 0, 0, &TTQuantiSH::getSkewPerPatch, 0);
    }
  }
  
}
// ----------------------------------------------------------------------------------------
// getSkewPerPatch
// ----------------------------------------------------------------------------------------
double TTQuantiSH::getSkewPerPatch (unsigned int i, unsigned int p)
{
  double skew = 0;
  
  double *phenot = _phenoTable.getClassWithinGroup(i, p);
  unsigned int patch_size = _phenoTable.size(i, p);  
  
  for(unsigned int k = 0; k < patch_size; ++k)
    skew += pow( phenot[k] - _pmeanP[i][p], 3);  //the mean has been set by setStats()
  
  return skew / patch_size;
}
// ----------------------------------------------------------------------------------------
// setDataTables
// ----------------------------------------------------------------------------------------
void TTQuantiSH::setDataTables(age_t AGE) 
{
  unsigned int **sizes;
  unsigned int nb_patch = _pop->getPatchNbr();
  
  sizes = new unsigned int * [_nb_trait];
  
  for(unsigned int i = 0; i < _nb_trait; ++i) {
    sizes[i] = new unsigned int [nb_patch];
    for(unsigned int j = 0; j < nb_patch; ++j)
      sizes[i][j] = _pop->size(AGE, j);
  }
  
  _phenoTable.update(_nb_trait, nb_patch, sizes);
  _genoTable.update(_nb_trait, nb_patch, sizes);
  
  for(unsigned int i = 0; i < _nb_trait; ++i)
    delete [] sizes[i];
  delete [] sizes;
  
  Patch* patch;
  //age_idx age = (AGE == ADULTS ? ADLTx : OFFSx);
  
  for(unsigned int i = 0, n; i < nb_patch; i++) {
    
    patch = _pop->getPatch(i);
    
    n=0;
    
    if ((patch->size(MAL, AGE)+patch->size(FEM, AGE)) != _phenoTable.size(0,i)) {
      fatal("problem while recording quanti trait values; table size doesn't match patch size.\n");
    }
    store_quanti_trait_values(patch, i, patch->size(MAL, AGE), &n, MAL, AGE, &_phenoTable, &_genoTable,
                       _nb_trait, _SHLinkedTraitIndex);
    
    store_quanti_trait_values(patch, i, patch->size(FEM, AGE), &n, FEM, AGE, &_phenoTable, &_genoTable,
                       _nb_trait, _SHLinkedTraitIndex);
    
    if (n != _phenoTable.size(0,i) || n != _genoTable.size(0,i)) {
      fatal("problem while recording quanti trait values; size counter doesn't match table size.\n");
    }
  }
}
// ----------------------------------------------------------------------------------------
// store_trait_values
// ----------------------------------------------------------------------------------------
void store_quanti_trait_values (Patch* patch, unsigned int patchID, unsigned int size, unsigned int *cntr,
                         sex_t SEX, age_t AGE, DataTable<double> *ptable, DataTable<double> *gtable,
                         unsigned int nTrait, unsigned int TraitIndex)
{
  double *phe;
  TTQuanti *trait;
  
  for(unsigned int j = 0; j < size; ++j) {
    
    trait = dynamic_cast<TTQuanti*> (patch->get(SEX, AGE, j)->getTrait( TraitIndex ));
    
    phe = (double*)trait->getValue();
    
    for(unsigned int k = 0; k < nTrait; k++){
      ptable->set( k, patchID, (*cntr), phe[k]);
      gtable->set( k, patchID, (*cntr), trait->get_genotype(k));
    }
    (*cntr)++;
  }
  
}
// ----------------------------------------------------------------------------------------
// setStats
// ----------------------------------------------------------------------------------------
void TTQuantiSH::setStats (age_t AGE)
{  
  if(_table_set_age == AGE 
     && _table_set_gen == _pop->getCurrentGeneration()
     && _table_set_repl == _pop->getCurrentReplicate())
    return;
  
  unsigned int pop_size = _pop->size(AGE);
  unsigned int patch_size;
  double *phenot1, *genot1, *genot2;
  
  unsigned int nb_patch = _pop->getPatchNbr();
  
  if(nb_patch < _patchNbr) {
    warning("increase in patch number detected (in Quanti Stat Handler),");
    warning("stats for quanti trait will not be recorded in new patches, patch identity may have changed.\n");
    _patchNbr = nb_patch; // record stat only in remaining patches
  }
    
  if(nb_patch > _patchNbr)  nb_patch = _patchNbr;  //tables have been allocated for _patchNbr patches
    
  setDataTables(AGE);
  
#ifdef HAS_GSL
  unsigned int c = 0; //covariance position counter
  unsigned int pv = 0; //eigenvector position counter
  //within deme stats:
  for(unsigned int j = 0; j < nb_patch; j++) { 
    
    patch_size = _pop->size(AGE, j);
    
    for(unsigned int t=0; t < _nb_trait; t++) {
      
      phenot1 = _phenoTable.getClassWithinGroup(t,j);
      genot1  = _genoTable.getClassWithinGroup(t,j);
      
      _pmeanP[t][j] = my_mean (phenot1, patch_size );
      _pmeanG[t][j] = my_mean (genot1, patch_size );
      _pVp[t][j] = my_variance_with_fixed_mean (phenot1, patch_size, _pmeanP[t][j]);
      _pVa[t][j] = my_variance_with_fixed_mean (genot1,  patch_size, _pmeanG[t][j]);
           
    }
    
    if(_nb_trait > 1) { 
      c = 0;
      //    calculate the covariances and G, need to adjust dimensions in class declaration
      for(unsigned int t1 = 0; t1 < _nb_trait; t1++) {
        //      set the diagonal elements of G here
        
        gsl_matrix_set(_G, t1, t1, _pVa[t1][j]);
        
        for(unsigned int t2 = t1 + 1; t2 < _nb_trait; t2++) {
          
          genot1  = _genoTable.getClassWithinGroup(t1,j);
          genot2  = _genoTable.getClassWithinGroup(t2,j);
          
          _pcovar[c][j] = gsl_stats_covariance_m (genot1, 1, genot2, 1, patch_size,
                                                  _pmeanG[t1][j], _pmeanG[t2][j]);
          
          gsl_matrix_set(_G, t1, t2, _pcovar[c][j]);
          gsl_matrix_set(_G, t2, t1, _pcovar[c++][j]);
        }
      }
      
      gsl_eigen_symmv (_G, _eval, _evec, _ws);
      gsl_eigen_symmv_sort (_eval, _evec, GSL_EIGEN_SORT_VAL_DESC);
      
      pv = 0;
      
      for(unsigned int t = 0; t < _nb_trait; t++) {
        _peigval[t][j] = gsl_vector_get (_eval, t);
        for(unsigned int v = 0; v < _nb_trait; v++)
          _peigvect[j][pv++] = gsl_matrix_get (_evec, v, t); //read eigenvectors column-wise
      }
    }
  }
  
  double meanGamong1, meanGamong2;
  c = 0; //reset covariance positioner
  //among demes stats:
  for(unsigned int t1 = 0; t1 < _nb_trait; t1++) {
    
    phenot1 = _phenoTable.getGroup(t1);
    genot1  = _genoTable.getGroup(t1);
    
    _meanP[t1] = my_mean (phenot1, pop_size ); //grand mean, pooling all individuals
    _meanG[t1] = my_mean (genot1, pop_size ); //grand mean, pooling all individuals
    
    _Vp[t1] = my_mean_no_nan (_pVp[t1], nb_patch); //mean within patch variance
    _Va[t1] = my_mean_no_nan (_pVa[t1], nb_patch); //mean within patch variance
    
    meanGamong1 = my_mean_no_nan (_pmeanG[t1], nb_patch); //mean of within patch mean genotypic values
    
    _Vb[t1] = my_variance_with_fixed_mean_no_nan (_pmeanG[t1], nb_patch,  meanGamong1); //variance of patch means
    
    gsl_matrix_set(_G, t1, t1, _Vb[t1]); //_G here becomes the D-matrix, the among-deme (Difference) covariance matrix 
    
    for(unsigned int t2 = t1 + 1; t2 < _nb_trait; t2++) {
      
      meanGamong2 = my_mean (_pmeanG[t2], nb_patch);
      
      _covar[c] = my_covariance_no_nan(_pmeanG[t1], _pmeanG[t2], nb_patch, nb_patch);

      gsl_matrix_set(_G, t1, t2, _covar[c]);
      gsl_matrix_set(_G, t2, t1, _covar[c]);
      
      //set covariance to the mean covariance within patch for recording
      _covar[c] = my_mean_no_nan (_pcovar[c], nb_patch);

      c++;
    }
  }
  
  if(_nb_trait > 1) { 
    gsl_eigen_symmv (_G, _eval, _evec, _ws);
    gsl_eigen_symmv_sort (_eval, _evec, GSL_EIGEN_SORT_VAL_DESC);
    
    for(unsigned int t1 = 0; t1 < _nb_trait; t1++) {
      _eigval[t1] = gsl_vector_get (_eval, t1);
      for(unsigned int t2 = 0; t2 < _nb_trait; t2++) {
        _eigvect[t2][t1] = gsl_matrix_get (_evec, t2, t1);      
      }
    }
  }
  
#else
  fatal("install the GSL library to get the quanti stats!\n");
#endif
  
  _table_set_age = AGE;
  _table_set_gen = _pop->getCurrentGeneration();  
  _table_set_repl = _pop->getCurrentReplicate();
  
}
// ----------------------------------------------------------------------------------------
// FHwrite
// ----------------------------------------------------------------------------------------
void TTQuantiFH::FHwrite()
{

  if (!_pop->isAlive()) return;
  
  if(!get_service()) fatal("link to file services not set!!\n");

  // sets the pop ptr to a sub sampled pop, if sub sampling happened
  _pop = get_service()->getSampledPop();
  
  if(_output_option == "PLINK" || _output_option == "plink")
    write_PLINK();
  else
    write_TABLE();


  // reset to pop ptr to main pop:
  _pop = get_service()->get_pop_ptr();

}
// ----------------------------------------------------------------------------------------
// write_TABLE
// ----------------------------------------------------------------------------------------
void TTQuantiFH::write_TABLE()
{
  std::string filename = get_filename();
  
  std::ofstream FILE (filename.c_str(), ios::out);
  
  if(!FILE) fatal("could not open \"%s\" output file!!\n",filename.c_str());
  
  //print the genotype at each locus
  bool print_gene = (_output_option == "genotypes" || _output_option == "genotype");

  //print the genotypic value of the individual in sus of the phenotype
  bool print_genotype = (!_FHLinkedTrait->get_env_var().empty());
  
  // First column is the patch ID:
  FILE<<"pop ";

  // print the allelic values?
  if(print_gene) {
    // we print allelic values ordered trait:locus, alleles at loci affecting one trait are grouped
    for(unsigned int k = 0; k < _FHLinkedTrait->get_nb_traits(); k++)
      for(unsigned int l = 0; l < _FHLinkedTrait->get_nb_locus(); l++)
        // "x" maternally inherited, "y" paternally inherited
        FILE<<"t"<<k+1<<"l"<<l+1<<"x "<<"t"<<k+1<<"l"<<l+1<<"y ";
  }
  
  for(unsigned int k = 0; k < _FHLinkedTrait->get_nb_traits(); k++) {
    FILE<<"P"<<k+1<< " "; 
    if(print_genotype) FILE<<"G"<<k+1<< " ";
  }
  
  FILE<<"stage age sex home ped isMigrant father mother ID\n";
  
  age_t pop_age = _pop->getCurrentAge(); //flag telling which age class should contain individuals
  
  //we print anything that is present in the pop:
  if( (pop_age & OFFSPRG) != 0) print(FILE, OFFSx, print_gene, print_genotype);
  
  unsigned int nb_class = _pop->getNumAgeClasses();

  if( (pop_age & ADULTS) != 0) {
  
    for(unsigned int i = 1; i < nb_class; i++ ){

      print(FILE, static_cast<age_idx> (i), print_gene, print_genotype);
    }
  
  }

  FILE.close();  

  // reset to pop ptr to main pop:
  _pop = get_service()->get_pop_ptr();

}
// ----------------------------------------------------------------------------------------
// FHwrite
// ----------------------------------------------------------------------------------------
void TTQuantiFH::print(ofstream& FH, age_idx Ax, bool print_gene, bool print_genotype)
{
  int patchNbr = _pop->getPatchNbr();
  Patch* current_patch;
  Individual* ind;
  TTQuanti* trait;
  double* Tval;
  double **genes;
  unsigned int loc, nb_trait=_FHLinkedTrait->get_nb_traits(), nb_locus = _FHLinkedTrait->get_nb_locus();
  
  for(int i = 0; i < patchNbr; i++) {
    
    current_patch = _pop->getPatch(i);
    
    for(unsigned int j = 0, size = current_patch->size(FEM, Ax); j < size; j++) {
      
      ind = current_patch->get(FEM, Ax, j);
      trait = dynamic_cast<TTQuanti*> (ind->getTrait(_FHLinkedTraitIndex));
      
      FH<<current_patch->getID() +1<<" ";
      Tval = (double*)trait->getValue();
      
      if(print_gene){
        genes = (double**)trait->get_sequence();
        
        FH.precision(6);
        
        for(unsigned int k = 0; k < nb_trait; k++) {
          for(unsigned int l = 0; l < nb_locus; l++) {
            loc = l * nb_trait + k;
            
            FH<<genes[FEM][loc]<<" "<<genes[MAL][loc]<<" ";
          }
        }
      }
      
      FH.precision(4);
      for(unsigned int k = 0; k < nb_trait; k++) {
        FH<<Tval[k]<<" ";
        if(print_genotype) FH << trait->get_genotype(k) << " ";
      }
      
      FH<<Ax<<" "<<ind->getAge()<<" "<<ind->getSex()<<" "<<ind->getHome()+1<<" "<<ind->getPedigreeClass()<<" "
      << (ind->getFather() && ind->getMother() ?
          (ind->getFather()->getHome()!= current_patch->getID() ) + (ind->getMother()->getHome()!= current_patch->getID() ) : 0)
      <<" "<<ind->getFatherID()<<" "<<ind->getMotherID()<<" "<<ind->getID()<<std::endl;
    }
    
    for(unsigned int j = 0, size = current_patch->size(MAL, Ax); j < size; j++) {
      
      ind = current_patch->get(MAL, Ax, j);
      trait = dynamic_cast<TTQuanti*> (ind->getTrait(_FHLinkedTraitIndex));
      
      FH<<current_patch->getID() +1<<" ";
      
      Tval = (double*)trait->getValue();
      
      if(print_gene){
        genes = (double**)trait->get_sequence();
        
        FH.precision(6);
        
        for(unsigned int k = 0; k < nb_trait; k++) {
          for(unsigned int l = 0; l < nb_locus; l++) {
            loc = l * nb_trait + k;
            
            FH<<genes[FEM][loc]<<" "<<genes[MAL][loc]<<" ";
          }
        }
      }
      
      FH.precision(4);
      for(unsigned int k = 0; k < nb_trait; k++) {
        FH<<Tval[k]<<" ";
        if(print_genotype) FH << trait->get_genotype(k) << " ";
      }
      
      FH<<Ax<<" "<<ind->getAge()<<" "<<ind->getSex()<<" "<<ind->getHome()+1<<" "<<ind->getPedigreeClass()<<" "
      << (ind->getFather() && ind->getMother() ?
              (ind->getFather()->getHome()!= current_patch->getID() ) + (ind->getMother()->getHome()!= current_patch->getID() ) : 0)
      <<" "<<ind->getFatherID()<<" "<<ind->getMotherID()<<" "<<ind->getID()<<std::endl;
    }
  }
}
// ----------------------------------------------------------------------------------------
// write_PLINK
// ----------------------------------------------------------------------------------------
void TTQuantiFH::write_PLINK ()
{
  if(_FHLinkedTrait->get_allele_model() > 2)
    fatal("PLINK file output for the quanti trait is only possible for di-allelic loci (for now)\n");

  unsigned int patchNbr = _pop->getPatchNbr();
  int nb_class = _pop->getNumAgeClasses();
  age_t pop_age = _pop->getCurrentAge(); //flag telling which age class should contain individuals
  Patch* current_patch;

  std::string filename = get_path() + this->get_service()->getGenerationReplicateFileName() + ".ped";

  // the PED file -------------------------------------------------------------------------
  ofstream PED (filename.c_str(), ios::out);

  if(!PED) fatal("could not open plink .ped output file!!\n");


#ifdef _DEBUG_
  message("TTQuantiFH::write_PLINK (%s)\n",filename.c_str());
#endif

  for (unsigned int i = 0; i < patchNbr; ++i) {

    current_patch = _pop->getPatch(i);

    if( pop_age & ADULTS ) {

      for(auto a = nb_class -1; a > 0; --a)
        print_PLINK_PED(PED, age_idx(a), current_patch);

    }

    if( pop_age & OFFSPRG )
      print_PLINK_PED(PED, OFFSx, current_patch);

  }


  PED.close();

  // the FAM file -------------------------------------------------------------------------
  // we write the .fam file only when more than one trait; phenotype columns are added here

  if(_FHLinkedTrait->get_nb_traits() > 1) {

    filename = get_path() + this->get_service()->getGenerationReplicateFileName() + ".fam";

    ofstream FAM (filename.c_str(), ios::out);

    if(!FAM) fatal("could not open plink .fam output file!!\n");

#ifdef _DEBUG_
  message("TTQuantiFH::write_PLINK (%s)\n",filename.c_str());
#endif

    for (unsigned int i = 0; i < patchNbr; ++i) {

       current_patch = _pop->getPatch(i);

       if( pop_age & ADULTS ) {

         for(auto a = nb_class -1; a > 0; --a)
           print_PLINK_FAM(FAM, age_idx(a), current_patch);

       }

       if( pop_age & OFFSPRG )
         print_PLINK_FAM(FAM, OFFSx, current_patch);
     }

    FAM.close();
  }



  // the MAP file -------------------------------------------------------------------------
  filename = get_path() + this->get_service()->getGenerationReplicateFileName() + ".map";

  ofstream MAP (filename.c_str(), ios::out);

  if(!MAP) fatal("could not open plink .map output file!!\n");

#ifdef _DEBUG_
  message("TTQuantiFH::write_PLINK (%s)\n",filename.c_str());
#endif

  unsigned int nb_locus = _FHLinkedTrait->get_nb_locus();

  double **map;

  map = new double* [nb_locus];

  for(unsigned int k = 0; k < nb_locus; ++k)
    map[k] = new double[2]; // [0]: chr; [1]: map pos in cM

  bool found = _FHLinkedTrait->_map.getGeneticMap(_FHLinkedTrait->get_type(), map, nb_locus);

  if( found ) {

    for(unsigned int k = 0; k < _FHLinkedTrait->get_nb_traits() ; ++k) {  //_FHLinkedTrait->get_nb_traits()
      for (unsigned int l = 0; l < nb_locus; ++l) { // this will give the locus ID

        // MAP FORMAT (PLINK1.9): chrmsm ID; Loc ID; position (cM); bp ID
        MAP<<map[l][0]+1<<" T"<<k+1<<"loc"<<l+1<<" "<<map[l][1]<<" "<<l+1<<endl;

      }
    }
  } else { // trait didn't register a genetic map, loci are unlinked (free recombination)

    warning("PLINK .map file: we assume QTL are unlinked, separated by 50M and on a single chromosome\n");

    // we're gonna set all loci on a single chrmsme, but 50M apart
    for(unsigned int k = 0; k < _FHLinkedTrait->get_nb_traits() ; ++k) {  //_FHLinkedTrait->get_nb_traits()
      for (unsigned int l = 0; l < nb_locus; ++l) { // this will give the locus ID

        // write: chromosome, locus name, map position, locus number
        MAP<< 1 <<" T"<<k+1<<"loc"<<l+1<<" "<< l*50000000.0 + 1.0 <<" "<<l+1<<endl;

      }
    }
//    error("an error occurred when trying to write the .map PLINK file, skipping.\n");
  }
  MAP.close();

  for(unsigned int k = 0; k < nb_locus; ++k) delete [] map[k];
  delete [] map;
}
// ----------------------------------------------------------------------------------------
// print PED
// ----------------------------------------------------------------------------------------
void TTQuantiFH::print_PLINK_PED(ofstream& FH, age_idx Ax, Patch *patch)
{
  Individual *ind;
  double** seq;
  char BASE[2] = {'A','G'};
  double** ref_all = _FHLinkedTrait->get_allele_values();
  unsigned int loc;
  unsigned int nb_trait = _FHLinkedTrait->get_nb_traits();
  unsigned int nb_locus = _FHLinkedTrait->get_nb_locus();

  // SPECIFICATION FOR THE .fam FILE = 6 first values in .ped files:
//  .fam (PLINK sample information file)
//
//  Sample information file accompanying a .bed binary genotype table.
//  Also generated by "--recode lgen" and "--recode rlist".
//
//  A text file with no header line, and one line per sample with the following six fields:
//
//      1. Family ID ('FID')
//      2. Within-family ID ('IID'; cannot be '0')
//      3. Within-family ID of father ('0' if father isn't in dataset)
//      4. Within-family ID of mother ('0' if mother isn't in dataset)
//      5. Sex code ('1' = male, '2' = female, '0' = unknown)
//      6. Phenotype value ('1' = control, '2' = case, '-9'/'0'/non-numeric = missing data if case/control)
//
//  If there are any numeric phenotype values other than {-9, 0, 1, 2}, the phenotype is interpreted as a quantitative trait instead of case/control status. In this case, -9 normally still designates a missing phenotype; use --missing-phenotype if this is problematic.


  // here, we save the phenotypic value of the first trait by default
  // the complete set of phenotypic values for all traits is saved in the .fam file

  for (unsigned int j = 0; j < patch->size(FEM, Ax); ++j) {

    ind = patch->get(FEM, Ax, j);

    FH<<"fam"<<ind->getHome()+1<<" "<<ind->getID()+1<<" 0 0" //<<ind->getFatherID()<<" "<<ind->getMotherID() //not in file for adults
        <<" 2 "<<((double*)ind->getTraitValue(_FHLinkedTraitIndex))[0]<<" ";

    seq = (double**)ind->getTrait(_FHLinkedTraitIndex)->get_sequence();

    for(unsigned int k = 0; k < nb_trait; ++k) {
      for (unsigned int l = 0; l < nb_locus; ++l) {

        loc = l * nb_trait + k;

        FH<< BASE[ (seq[FEM][loc] == ref_all[l][1]) ]<<" "<< BASE[ (seq[MAL][loc] == ref_all[l][1]) ]<<" ";
      }
    }

    FH <<std::endl;

  }

  for (unsigned int j = 0; j < patch->size(MAL, Ax); ++j) {

    ind = patch->get(MAL, Ax, j);

    FH<<"fam"<<ind->getHome()+1<<" "<<ind->getID()+1<<" 0 0"  //<<ind->getFatherID()<<" "<<ind->getMotherID()
        <<" 1 "<<((double*)ind->getTraitValue(_FHLinkedTraitIndex))[0]<<" ";

    seq = (double**)ind->getTrait(_FHLinkedTraitIndex)->get_sequence();

    for(unsigned int k = 0; k < nb_trait; ++k) {
      for (unsigned int l = 0; l < nb_locus; ++l) {

        loc = l * nb_trait + k;

        FH<< BASE[ (seq[FEM][loc] == ref_all[l][1]) ]<<" "<< BASE[ (seq[MAL][loc] == ref_all[l][1]) ]<<" ";
      }
    }
    FH<<std::endl;

  }
}

// ----------------------------------------------------------------------------------------
// print FAM
// ----------------------------------------------------------------------------------------
void TTQuantiFH::print_PLINK_FAM(ofstream& FH, age_idx Ax, Patch *patch)
{
  Individual *ind;
  unsigned int nb_trait = _FHLinkedTrait->get_nb_traits();

  // SPECIFICATION FOR THE .fam FILE = 6 first values in .ped files:
//  .fam (PLINK sample information file)
//
//  Sample information file accompanying a .bed binary genotype table.
//  Also generated by "--recode lgen" and "--recode rlist".
//
//  A text file with no header line, and one line per sample with the following six fields:
//
//      1. Family ID ('FID')
//      2. Within-family ID ('IID'; cannot be '0')
//      3. Within-family ID of father ('0' if father isn't in dataset)
//      4. Within-family ID of mother ('0' if mother isn't in dataset)
//      5. Sex code ('1' = male, '2' = female, '0' = unknown)
//      6. Phenotype value ('1' = control, '2' = case, '-9'/'0'/non-numeric = missing data if case/control)
//
//  If there are any numeric phenotype values other than {-9, 0, 1, 2}, the phenotype is interpreted as a
//  quantitative trait instead of case/control status. In this case, -9 normally still designates a missing
//  phenotype; use --missing-phenotype if this is problematic.


  // here, we save the phenotypic value of the first trait by default
  // the complete set of phenotypic values for all traits is saved in the .fam file

  for (unsigned int j = 0; j < patch->size(FEM, Ax); ++j) {

    ind = patch->get(FEM, Ax, j);

    FH<<"fam"<< ind->getHome()+1
        <<" "<< ind->getID()+1
        <<" 0 0" //<<ind->getFatherID()<<" "<<ind->getMotherID() //not in file for adults
        <<" 2";

    for(unsigned int k = 0; k < nb_trait; ++k) {
      FH << " " << ((double*)ind->getTraitValue(_FHLinkedTraitIndex))[k];
    }

    FH <<std::endl;

  }

  for (unsigned int j = 0; j < patch->size(MAL, Ax); ++j) {

    ind = patch->get(MAL, Ax, j);

    FH<<"fam"<< ind->getHome()+1
        <<" "<< ind->getID()+1
        <<" 0 0"  //<<ind->getFatherID()<<" "<<ind->getMotherID()
        <<" 1";

    for(unsigned int k = 0; k < nb_trait; ++k) {
      FH << " " << ((double*)ind->getTraitValue(_FHLinkedTraitIndex))[k];
    }

    FH<<std::endl;

  }
}
// ----------------------------------------------------------------------------------------
// FHread
// ----------------------------------------------------------------------------------------
void TTQuantiFH::FHread (string& filename)
{
  unsigned int nb_locus = _FHLinkedTrait->get_nb_locus();
  unsigned int nb_trait = _FHLinkedTrait->get_nb_traits();
  bool has_genotype = (!_FHLinkedTrait->get_env_var().empty());

  unsigned int patchNbr = _pop->getPatchNbr();

  TTQuanti* trait;

  ifstream FILE(filename.c_str(),ios::in);

  if(!FILE) fatal("could not open QUANTI input file \"%s\"\n",filename.c_str());

  unsigned int stage, age, sex, ped, origin, dummy;
  unsigned int loc;
  age_idx stagex;
  unsigned int pop;
  age_idx agex;
  Individual *ind;
  double all0, all1, pheno, geno;
  double *seq[2];
  seq[0] = new double [nb_locus*nb_trait];
  seq[1] = new double [nb_locus*nb_trait];

  int lnbr = 1;

  // swallow the header
  FILE.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

  string str;

  while(FILE>>pop) {

    if(FILE.bad()) {
      error("Reading input genotypes from \"%s\" failed at line %i.\n",filename.c_str(), lnbr);
      FILE.clear();
      FILE >> str;
      fatal("Expecting population number as first element on the line, but received this: %s \n", str.c_str());
    }

    if(pop > patchNbr)
      fatal("Patch number found in file exceeds number of patches in the population.\n");

    for(unsigned int k = 0; k < nb_trait; ++k) {
      for(unsigned int i = 0; i < nb_locus; ++i) {

        loc = i * nb_trait + k;

        FILE>>all0>>all1;

        seq[0][loc] = all0;
        seq[1][loc] = all1;
      }
    }


    for(unsigned int k = 0; k < nb_trait; k++) {
      FILE >> pheno;                   //read phenotypic value
      if(has_genotype) FILE >> geno;   // genotypic values, only present if trait is influenced by env. var.
    }

    // read extra fields
    FILE >> stage >> age >> sex >> origin >> ped >> dummy >> dummy >> dummy >> dummy;

    stagex = age_idx(stage);

    ind = _pop->makeNewIndividual(0, 0, sex_t(sex), origin - 1);
    ind->setPedigreeClass((unsigned char)ped);
    ind->setAge(age);
    trait = dynamic_cast<TTQuanti*> (ind->getTrait(_FHLinkedTraitIndex));
    trait->set_sequence((void**)seq);
    trait->set_value();

    //      ind->show_up();

    _pop->getPatch(pop-1)->add(sex_t(sex), stagex, ind);

    lnbr++;
  }


  FILE.close();

  delete seq[0];
  delete seq[1];
}

// ----------------------------------------------------------------------------------------
// TTQFreqExtractor::resetTable
// ----------------------------------------------------------------------------------------
void TTQFreqExtractor::resetTable()
{
  if(_records.size() != 0) _records.clear();
}
// ----------------------------------------------------------------------------------------
// TTQFreqExtractor::FHwrite
// ----------------------------------------------------------------------------------------
void TTQFreqExtractor::FHwrite()
{
  
  // make sure we don't use a sub-sampled pop
  Metapop* pop = get_service()->get_pop_ptr();
  int patchNbr = pop->getPatchNbr();
  Patch* current_patch;
  Individual* ind;
//  TTQuanti* trait;
//  double* Tval;
  double **genes;
  int total_size = 0;
  
  double *snp_tab;
//  double *snp_tab2;
  
  double **snp_allele = _FHLinkedTrait->get_allele_values();
  
  
  unsigned int loc, rec;
  unsigned int nb_trait = _FHLinkedTrait->get_nb_traits();
  unsigned int nb_locus = _FHLinkedTrait->get_nb_locus();
  
  
  snp_tab = new double [ (nb_trait * nb_locus) ];
  
//  snp_allele = new double [ (nb_trait * nb_locus)];
  
  
  ostringstream REC;
  string current;
  
  //first generation to record, build the record table;
  
  if(pop->getCurrentGeneration() == this->get_GenerationOccurrence() ||
      pop->getCurrentGeneration() == 1) {
    
    resetTable();
    
    //create header
    _records.push_back("pop trait locus allele g" + tstring::int2str(pop->getCurrentGeneration()));
    
    rec = 1;
    for(int i = 0; i < patchNbr; i++)
      for(unsigned int k = 0; k < nb_trait; k++) {
        for(unsigned int l = 0; l < nb_locus; l++) {
                    
          REC<<i+1<<" "<<k+1<<" "<<l+1<<" "<<snp_allele[l][0];  //only '+' or 'A' allele
          
          _records.push_back(REC.str());
          
          REC.str("");
          
        }
      }
    
  } else { //add a 'column' for each generation recorded
    
    current =  _records[0];
    
    _records[0] = current + " g" + tstring::int2str(pop->getCurrentGeneration());
  }
  
  
  //ONLY IMPLEMENTED FOR DIALLELIC LOCI!
  
  rec = 1;
  for(int i = 0; i < patchNbr; i++) {
    

    current_patch = pop->getPatch(i);
    
    // fill the SNP_alleles with the first individual's haplotype, for comparison purposes:
//    ind = current_patch->get(FEM, ADLTx, 0);
//    genes = (double**)ind->getTrait(_FHLinkedTraitIndex)->get_sequence();
    
    for(unsigned int k = 0; k < nb_trait; k++) {
      for(unsigned int l = 0; l < nb_locus; l++) {
        loc = l * nb_trait + k;
        snp_tab [loc] = 0;
      }
    }
    
    total_size = 0;
    total_size += current_patch->size(FEM, ADLTx);
    total_size += current_patch->size(MAL, ADLTx);
    
    
    for(unsigned int j = 0, size = current_patch->size(FEM, ADLTx); j < size; j++) {
      
      ind = current_patch->get(FEM, ADLTx, j);
      
      genes = (double**)ind->getTrait(_FHLinkedTraitIndex)->get_sequence();
      
      
      for(unsigned int k = 0; k < nb_trait; k++) {
        for(unsigned int l = 0; l < nb_locus; l++) {
          loc = l * nb_trait + k;
          
          if (genes[0][loc] == snp_allele[l][0]){
            snp_tab [loc] += 1;
          }
          
          if (genes[1][loc] == snp_allele[l][0]){
            snp_tab [loc] += 1;
          }
          
        }
      }
      
      
    }


    for(unsigned int j = 0, size = current_patch->size(MAL, ADLTx); j < size; j++) {
      
      ind = current_patch->get(MAL, ADLTx, j);
      
      genes = (double**)ind->getTrait(_FHLinkedTraitIndex)->get_sequence();
      
      for(unsigned int k = 0; k < nb_trait; k++) {
        for(unsigned int l = 0; l < nb_locus; l++) {
          loc = l * nb_trait + k;
          
          if (genes[0][loc] == snp_allele[l][0]){
            snp_tab [loc] += 1;
          }
          if (genes[1][loc] == snp_allele[l][0]){
            snp_tab [loc] += 1;
          }
        }
      }
      
      
    }

    //calculate and record frequencies
    for(unsigned int k = 0; k < nb_trait; k++) {
      for(unsigned int l = 0; l < nb_locus; l++) {
        
        loc = l * nb_trait + k;
        //rec = i*nb_trait*nb_locus + k*nb_locus + l;
        
        assert(rec < _records.size());
        
        current = _records[ rec ];
        
        _records[ rec ] = _records[ rec ] + " " + tstring::dble2str( (snp_tab[loc] / (2 * total_size)) );
        
        rec++;
      }
    }    
  }
  
  
  delete [] snp_tab;
  
  if(pop->getCurrentGeneration() == pop->getGenerations() ) { //have we reached the last generation?
    
    std::string filename = get_path() + get_service()->getReplicateFileName() + get_extension();
    std::ofstream FILE ;
    
    //open for writing
    FILE.open(filename.c_str(), ios::out);
    
    if(!FILE) fatal("Trait quanti could not open output file: \"%s\"\n",filename.c_str());
    
    for(unsigned int i = 0; i < _records.size(); ++i)
      FILE<<_records[i]<<endl;
    
    FILE.close();
  }
}


