/**  $Id: LCEbreed.cc,v 1.9.2.9 2017-06-09 09:03:39 fred Exp $
 *
 *  @file LCEbreed.cc
 *  Nemo2
 *
 *   Copyright (C) 2006-2011 Frederic Guillaume
 *   frederic.guillaume@env.ethz.ch
 *
 *   This file is part of Nemo
 *
 *   Nemo is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Nemo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  created on @date 07.07.2004
 *
 *  @author fred
 */
#include <deque>
#include <algorithm>
#include "output.h"
#include "LCEbreed.h"
#include "individual.h"
#include "metapop.h"
#include "Uniform.h"
#include "simenv.h"

// ----------------------------------------------------------------------------------

//                                   LCE_Breed_base

// ----------------------------------------------------------------------------------------
// LCE_Breed_base
// ----------------------------------------------------------------------------------------
LCE_Breed_base::LCE_Breed_base ()
: LifeCycleEvent("", ""),
_mating_system(0), 
_mating_males(1),
_mating_proportion(1), 
_alpha_male(0),
_sd_fecundity(1),
_do_inherit(1),
MatingFuncPtr(0),
DoBreedFuncPtr(0), 
FecundityFuncPtr(0), 
CheckMatingConditionFuncPtr(0), 
GetOffsprgSex(0),
_LeslieMatrix(0),
_nb_class(1),
_reproductive_adults(0)


//,GetPatchFecundityFuncPtr(0)
{
  ParamUpdater<LCE_Breed_base> * updater = new ParamUpdater<LCE_Breed_base> (&LCE_Breed_base::setMatingSystem);
  
  add_parameter("mating_system",INT,true,true,1,6, updater);
  add_parameter("mating_proportion",DBL,false,true,0,1,updater);
  add_parameter("mating_males",INT,false,false,0,0, updater);
  
  updater = new ParamUpdater< LCE_Breed_base > (&LCE_Breed_base::setFecundity);
  add_parameter("mean_fecundity",DBL,false,false,0,0, updater);
  add_parameter("fecundity_dist_stdev",DBL,false,false,0,0, updater);
  add_parameter("fecundity_distribution",STR,false,false,0,0, updater);
  
  updater = new ParamUpdater< LCE_Breed_base > (&LCE_Breed_base::setSexRatio);
  add_parameter("sex_ratio_mode",STR,false,false,0,0, updater);

  add_parameter("patch_capacity",INT,false,false,0,0, updater);

}
// ----------------------------------------------------------------------------------------
// LCE_Breed_base::setParameters
// ----------------------------------------------------------------------------------------
bool LCE_Breed_base::setParameters ()
{
  _nb_class = _popPtr->getNumAgeClasses();

  _LeslieMatrix = _popPtr->getLeslieMatrix();

  return ( setMatingSystem() && setFecundity() && setSexRatio() && setMatingPool());
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_base::set_mating_pool
// ----------------------------------------------------------------------------------------
bool LCE_Breed_base::setMatingPool ()
{
  //set the reproductive class flags and counters, start with stage 1
  unsigned int mask = 2;

  _reproductive_adults = 0;
  _repro_class.clear();

  for(int j = 1; j < (int)_nb_class; j++){ 
      //age class 0 is always offspring and does not reproduce

      if(_LeslieMatrix->get(0, j) != 0){

        _reproductive_adults |= mask; //set the corresponding bit

        _repro_class.push_back(j);    //record age class

      }
      mask <<= 1;
  }

  if(_reproductive_adults == 0) {
    error("Fecundity is null in all age classes.\n");
    error("Fecundity is specified with \"mean_fecundity\" (no stage structure)\n");
    return error(" or \"pop_transition_matrix\" (with stage structure).\n");
  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_base::setMatingSystem
// ----------------------------------------------------------------------------------------
bool LCE_Breed_base::setMatingSystem ()
{
  _mating_system = (int)this->get_parameter_value("mating_system");
  
  if(get_parameter("mating_proportion")->isSet())
    _mating_proportion = this->get_parameter_value("mating_proportion");
  else
    _mating_proportion = 1;
  
  if(_paramSet->isSet("mating_males"))
    _mating_males = (int)_paramSet->getValue("mating_males");
  else
    _mating_males = 1;
  
  //set the mating functions ptr:
  CheckMatingConditionFuncPtr = &LCE_Breed_base::checkNoSelfing;
  DoBreedFuncPtr = &LCE_Breed_base::breed;

  // set the inherit flag, used to cancel recombination in cloning individuals
  //is true unless the mating system is cloning
  _do_inherit = true;

  // or false if no traits are simulated:
  if(_popPtr->getIndividualProtoype()->getTraitNumber() == 0)
    _do_inherit = false;

  // set pointers to the chosen mating functions, it will be used in execute()
  switch(_mating_system) {
      //random mating:
    case 1:
    {
      MatingFuncPtr = &LCE_Breed_base::RandomMating;
      break;
    }
      //polygyny:
    case 2:
    {
      if(_mating_proportion == 1)
        if(_mating_males == 1)
          MatingFuncPtr = &LCE_Breed_base::fullPolyginy;
        else
          MatingFuncPtr = &LCE_Breed_base::fullPolyginy_manyMales;
        else
          if(_mating_males == 1)
            MatingFuncPtr = &LCE_Breed_base::partialPolyginy;
          else
            MatingFuncPtr = &LCE_Breed_base::partialPolyginy_manyMales;
      
      CheckMatingConditionFuncPtr = &LCE_Breed_base::checkPolygyny;

      break;
    }
      //monogamy:
    case 3:
    {
      if(_mating_proportion == 1)
        MatingFuncPtr = &LCE_Breed_base::fullMonoginy;
      else 
        MatingFuncPtr = &LCE_Breed_base::partialMonoginy;
      
      break;
    }
      //selfing:
    case 4:
    {
      if(_mating_proportion == 1)
        MatingFuncPtr = &LCE_Breed_base::fullSelfing;
      else
        MatingFuncPtr = &LCE_Breed_base::partialSelfing;
      
      CheckMatingConditionFuncPtr = &LCE_Breed_base::checkSelfing;

      if(get_parameter("patch_capacity")->isSet()){

        fatal("\"mating_system 4\" should be better initialized together with \"patch_nbfem ...\" and \"patch_nbmal 0\". \n");

      }

      break;
    }
      //cloning
    case 5:
    {
      if(_mating_proportion == 1)
        MatingFuncPtr = &LCE_Breed_base::fullSelfing;
      else
        MatingFuncPtr = &LCE_Breed_base::partialSelfing;
      
      CheckMatingConditionFuncPtr = &LCE_Breed_base::checkCloning;
      DoBreedFuncPtr = &LCE_Breed_base::breed_cloning;

      if(get_parameter("patch_capacity")->isSet()){

        fatal("\"mating_system 5\" should be better initialized together with \"patch_nbfem ...\" and \"patch_nbmal 0\". \n");

      }

      break;
    }
      //random mating with hermaphrodites:
    case 6:
    {
      MatingFuncPtr = &LCE_Breed_base::random_hermaphrodite;
      CheckMatingConditionFuncPtr = &LCE_Breed_base::checkSelfing;

      if(get_parameter("patch_capacity")->isSet()){

        fatal("\"mating_system 6\" should be better initialized together with \"patch_nbfem ...\" and \"patch_nbmal 0\". \n");

      }

      break;
    }
      
  }
  
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_base::setSexRatio
// ----------------------------------------------------------------------------------------
bool LCE_Breed_base::setSexRatio ()
{
  // SEX-RATIO
  
  // by default, the gender is set randomly (Bernoulli trial)

  GetOffsprgSex = &LCE_Breed_base::getOffsprgSexRandom;

  // this can be changed to a fixed 1:1 male:female ratio
  if(get_parameter("sex_ratio_mode")->isSet()) {
    
    if(get_parameter("sex_ratio_mode")->getArg() == "fixed") {
      
      GetOffsprgSex = &LCE_Breed_base::getOffsprgSexFixed;
    
    } else if(get_parameter("sex_ratio_mode")->getArg() != "random") {

      return error("\"sex_ratio_mode\" parameter argument must be either \"fixed\" or \"random\".");
      
    }
    
  }

  // gender-setting function differs for selfing and cloning species
  // in those two cases, only one sex is modeled; females

  switch(_mating_system) {
    case 4: GetOffsprgSex = &LCE_Breed_base::getOffsprgSexSelfing;
      break;
    case 5: GetOffsprgSex = &LCE_Breed_base::getOffsprgSexCloning;
      break;
    case 6: GetOffsprgSex = &LCE_Breed_base::getOffsprgSexSelfing;
      break;
  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_base::setFecundity
// ----------------------------------------------------------------------------------------
bool LCE_Breed_base::setFecundity ()
{
  // FECUNDITY
  _mean_fecundity.reset();

  if (get_parameter("mean_fecundity")->isMatrix()) {

    get_parameter("mean_fecundity")->getMatrix(&_mean_fecundity);

    if(_mean_fecundity.nrows() != 1) {
      return error("\"mean_fecundity\" accepts a single number or a single array with patch-specific values.\n");
    }

    if(_mean_fecundity.ncols() > _popPtr->getPatchNbr()) {
      return error("\"mean_fecundity\" accepts an array of max num patches in length.\n");
    }
    else if (_mean_fecundity.ncols() < _popPtr->getPatchNbr())
    {
      // recycle values provided in input until PtachNbr values are reached
      unsigned int npat = _mean_fecundity.ncols();

      TMatrix tmp(_mean_fecundity);

      _mean_fecundity.reset(1, _popPtr->getPatchNbr());

      for (unsigned int i = 0; i < _popPtr->getPatchNbr(); i++) {
        _mean_fecundity.set(0, i, tmp.get(0 , i % npat));
      }
    }
  } else {
    //input parameter is not a matrix, copy value into the patch array:
    //mean fecundity will be set to -1 if parameter not set in input
    _mean_fecundity.reset(1, _popPtr->getPatchNbr(), get_parameter_value("mean_fecundity"));
  }
  
  // FECUNDITY DISTRIBUTION
  if(get_parameter("fecundity_distribution")->isSet()) {
    
    string dist = get_parameter("fecundity_distribution")->getArg();
    
    if( dist == "fixed" || dist == "fix" )
      
      FecundityFuncPtr = &LCE_Breed_base::getFixedFecundity;
    
    else if( dist == "poisson" )
      
      FecundityFuncPtr = &LCE_Breed_base::getPoissonFecundity;
    
    else if( dist == "normal" ) {
      
      FecundityFuncPtr = &LCE_Breed_base::getGaussianFecundity;
      
      if(get_parameter("fecundity_dist_stdev")->isSet())
      {
    	  _sd_fecundity = get_parameter_value("fecundity_dist_stdev");

      } else {

    	  return error("parameter \"fecundity_dist_stdev\" is missing for \"normal\" fecundity distribution\n");
      }
      
    } else {

    	return error("unknown fecundity distribution parameter's argument!\n");
    }
    
  } else { //default distribution is Poisson:
    FecundityFuncPtr = &LCE_Breed_base::getPoissonFecundity;
  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_base::getOffsprgSexFixed
// ----------------------------------------------------------------------------------------
sex_t LCE_Breed_base::getOffsprgSexFixed  ()
{
  static bool sex = RAND::RandBool();
  sex ^= 1;
  return (sex_t)sex;
}

// ----------------------------------------------------------------------------------------
// LCE_Breed_base::makeOffspring
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_base::makeOffspring(Individual* ind)
{
  unsigned int cat = ind->getPedigreeClass();

  ind->getMother()->DidHaveABaby(cat);

  if(cat!=4) ind->getFather()->DidHaveABaby(cat);
  
  //the call to create will perform recombination and mutation
  return ind->create(doInheritance(), true);
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_base::breed
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_base::breed(Individual* mother, Individual* father, unsigned int LocalPatch)
{
  return _popPtr->makeNewIndividual(mother, father, getOffsprgSex(), LocalPatch);
}
// ----------------------------------------------------------------------------------------
// LCE_Breed_base::breed_cloning
// ----------------------------------------------------------------------------------------
Individual* LCE_Breed_base::breed_cloning(Individual* mother, Individual* father, unsigned int LocalPatch)
{
  Individual *newind;
  
  if(mother == father || father == NULL) {

    newind = _popPtr->getNewIndividual();

    //cloning:
    (*newind) = (*mother); 
    
    newind->reset_counters();
    newind->setFather(NULL);
    newind->setFatherID(0);
    newind->setMother(mother);
    newind->setMotherID(mother->getID());
    newind->setIsSelfed(true);
    newind->setHome(LocalPatch);
    newind->setAge(0);
    _do_inherit = false;  

  } else {

    newind =  _popPtr->makeNewIndividual(mother, father, getOffsprgSex(), LocalPatch);
    _do_inherit = true;

  }
  
  return newind;
}
// ----------------------------------------------------------------------------------------

//                                     LCE_Breed

// ----------------------------------------------------------------------------------------
// LCE_Breed::setParameters
// ----------------------------------------------------------------------------------------
bool LCE_Breed::setParameters ( )
{
//  cout<<"breed set parameters"<<endl;
  return LCE_Breed_base::setParameters();
}
// ----------------------------------------------------------------------------------------
// LCE_Breed::execute
// ----------------------------------------------------------------------------------------
void LCE_Breed::execute()
{
  Patch* patch;
  Individual* mother;
  Individual* father;
  Individual* NewOffsprg;
  unsigned int nbBaby, a;
  age_idx current_age;
  
  //the values within the Leslie matrix may change during a simulation

  _LeslieMatrix = _popPtr->getLeslieMatrix();

  setMatingPool(); //has to be rechecked each time the leslie matrix is modified
  //@TODO optimize setMatingPool to avoid resetting at every generation
  
#ifdef _DEBUG_
  message("LCE_Breed::execute (Patch nb: %i offsprg nb: %i adlt nb: %i adlt mal: %i adlt fem: %i)\n"
          ,_popPtr->getPatchNbr(),_popPtr->size( OFFSPRG ),_popPtr->size( ADULTS ), _popPtr->size(MAL, ADULTS),
	  _popPtr->size(FEM, ADULTS));
#endif
  

  if(_popPtr->size(OFFSPRG) != 0 and _LeslieMatrix->get(0, 0) == 0) {
    
    warning("offspring containers not empty at time of breeding, flushing.\n");

    _popPtr->flush(OFFSx);

  }
  
  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); i++) {

      patch = _popPtr->getPatch(i);

//      cout<<"check breed execute in patch "<<i<<endl;

      if( !checkMatingCondition(patch) ) continue;

      //cycle only through reproductive classes
      for(unsigned int j = 0; j < _repro_class.size(); ++j){

        current_age = static_cast<age_idx> ( _repro_class[j] );

//        cout<<"current age "<<current_age<<"; size = "
//            <<patch->size(FEM, current_age)<<"(F) "<<patch->size(MAL, current_age)<<"(M)"<<endl;

        for(unsigned int size = patch->size(FEM, current_age), indexOfMother = 0;
            indexOfMother < size;
            indexOfMother++)
          {

            mother = patch->get(FEM, current_age, indexOfMother);

            nbBaby = (unsigned int)mother->setFecundity( getFecundity(_LeslieMatrix->get(0, current_age)) );


            //-----------------------------------------------------------------------
            while(nbBaby != 0) {

              father = this->getFatherPtr(patch, mother, indexOfMother);

              NewOffsprg = makeOffspring( do_breed(mother, father, i) );

              patch->add(NewOffsprg->getSex(), OFFSx, NewOffsprg);

              nbBaby--;

            }//END__WHILE


          }//END__for females


      }//END__for age class

    
  }//END__for patch

#ifdef _DEBUG_
message("LCE_Breed::execute (Patch nb: %i offsprg nb: %i adlt nb: %i fem off: %i)\n"
        ,_popPtr->getPatchNbr(),_popPtr->size( OFFSPRG ),_popPtr->size( ADULTS), _popPtr->size(FEM, OFFSx) );
#endif

}

// ----------------------------------------------------------------------------------

//                                      CLONING

// ----------------------------------------------------------------------------------
// LCE_Cloning
// ----------------------------------------------------------------------------------
LCE_Cloning::LCE_Cloning():LifeCycleEvent ("cloning",""), _clonal_rate(0),
    _coeff_competition_cloning(0), _age_target(ADLTx)
{
  ParamUpdater<LCE_Cloning> * updater = new ParamUpdater<LCE_Cloning> (&LCE_Cloning::setParameters);

  add_parameter("cloning_rate", DBL, true, false, 0, 0, updater);

  add_parameter("cloning_competition_coefficient",DBL,false,false,0,0, updater);

  add_parameter("cloning_add_to_stage",INT,false,false,0,0, updater);
}
// ----------------------------------------------------------------------------------
// setParameters
// ----------------------------------------------------------------------------------
bool LCE_Cloning::setParameters ()
{

  if(get_parameter("cloning_rate")->isSet())
    _clonal_rate = get_parameter_value("cloning_rate");

  if(get_parameter("cloning_competition_coefficient")->isSet())
    _coeff_competition_cloning = get_parameter_value("cloning_competition_coefficient");//clonage specifique
  else
    _coeff_competition_cloning = 0;

  if(get_parameter("cloning_add_to_stage")->isSet())
    _age_target = static_cast<age_idx>((unsigned int)get_parameter_value("cloning_add_to_stage"));
  else
    _age_target = ADLTx; //first adult age class (1)

  return true;
}
// ----------------------------------------------------------------------------------
// execute
// ----------------------------------------------------------------------------------
void LCE_Cloning::execute ()
{
  Individual* newind;
  Patch* patch;
  Individual* mother;
  unsigned int pop_size;
  double number_clones;
  double compet_clone;
  
  
  //loop for each patch, for each adult female
  
  for(unsigned int i = 0; i < _popPtr->getPatchNbr(); i++) {
    
    patch = _popPtr->getPatch(i);
    
    pop_size = patch->size(ADULTS);

    //cloning only applies to female individuals (may be hermaphrodites)
    
    for(unsigned int size = patch->size(FEM, ADULTS),
        indexOfMother = 0;indexOfMother < size; indexOfMother++)
    {
      
      mother = patch->get(FEM, ADULTS, indexOfMother);
      
      //competition is based on a beverton-holt function
      //where new clones compete with ALL adult (age tag > 0) individuals

      compet_clone = 1/(1 + pop_size * _coeff_competition_cloning);
      
      //number of individuals to be generated from a poisson distribution
      number_clones = compet_clone * _clonal_rate;
      
      number_clones = round(RAND::Poisson(number_clones));

      for (int k = 0 ; k < number_clones; k++){
        
        newind = _popPtr->getNewIndividual();
        
        //cloning:
        
        (*newind) = (*mother);
        newind->reset_counters();
        newind->setFather(NULL);
        newind->setFatherID(0);
        newind->setMother(mother);
        newind->setMotherID(mother->getID());
        newind->setIsSelfed(true);
        newind->setHome(i);
        newind->setAge(_age_target);
        
        patch->add(FEM, _age_target, newind->create(false,true));
        
      }
    }
  }
}

