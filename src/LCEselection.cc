/**  $Id: LCEselection.cc,v 1.13.2.12 2017-03-31 09:24:42 fred Exp $

*  @file LCEselection.cc
*  Nemo2
*
*   Copyright (C) 2006-2019 Frederic Guillaume
*   frederic.guillaume@ieu.uzh.ch
*
*   This file is part of Nemo
*
*   Nemo is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   Nemo is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*  created on @date 09.10.2007
*
*  @author fred
*/

#include <sstream>
#include <iostream>
#include "LCEselection.h"
#include "Uniform.h"
#include "utils.h"
#include "tstring.h"
#include "simenv.h"
#include "filehandler.h"

// ----------------------------------------------------------------------------------------

//                             LCE_Selection_base

// ----------------------------------------------------------------------------------------
// LCE_Selection_base::LCE_Selection_base
// ----------------------------------------------------------------------------------------
LCE_Selection_base::LCE_Selection_base ( ) : LifeCycleEvent("", ""),
_selection_matrix(0), _gsl_selection_matrix(0), _diffs(0), _res1(0), _local_optima(0), _phe(0),
_selectTraitDimension(1), _letheq(0), _base_fitness(1), _mean_fitness(0), _max_fitness(0), _scaling_factor(1),
_is_local(0), _is_absolute(1), _eVariance(0), _getRawFitness(0), _getFitness(0), _stater(0),_stage_selected(0),
_selection_stage_variance(0), _rate_of_change_is_std(0), _do_change_local_opt(0), _setNewLocalOptima(0),
_set_std_rate_at_generation(0), _std_rate_reference_patch(0), _setScalingFactor(0), _nb_trait(1), _writer(0)

{ }
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::~LCE_Selection_base
// ----------------------------------------------------------------------------------------
LCE_Selection_base::~LCE_Selection_base ( )
{
  reset_selection_matrix_containers();

  if(_local_optima) delete _local_optima;
  if(_selection_stage_variance) delete _selection_stage_variance;
  if(_stage_selected) delete _stage_selected;
  if(_diffs) gsl_vector_free(_diffs);
  if(_res1) gsl_vector_free(_res1);
  if(_stater) delete _stater;
  if(_writer) delete _writer;

}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::reset_selection_matrix_containers
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::reset_selection_matrix_containers()
{
  vector< TMatrix* >::iterator selIT; //= _selection_matrix.begin();
  for(unsigned int i=0; i < _selection_matrix.size(); ++i){
    for(selIT = _selection_matrix[i].begin();
        selIT != _selection_matrix[i].end(); ++selIT)
      if((*selIT)) delete (*selIT);
    _selection_matrix[i].clear();
  }
  _selection_matrix.clear();

  vector< gsl_matrix* >::iterator gslIT;
  for(unsigned int i  = 0; i < _gsl_selection_matrix.size(); ++i){
    for(gslIT = _gsl_selection_matrix[i].begin();
        gslIT != _gsl_selection_matrix[i].end(); ++gslIT)
      if((*gslIT)) gsl_matrix_free( (*gslIT) );
    _gsl_selection_matrix[i].clear();
  }
  _gsl_selection_matrix.clear();

}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::loadStatServices
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::loadStatServices ( StatServices* loader )
{
  if(_stater == NULL) _stater = new LCE_SelectionSH(this);
  loader->attach(_stater);
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::loadFileServices
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::loadFileServices ( FileServices* loader )
{
  if(_paramSet->isSet(_prefix + "_output")) {
  
    if(_writer == NULL) _writer = new LCE_SelectionFH(this);
    
    Param* param = get_parameter(_prefix + "_output_logtime");
    
    if(!param->isSet()) fatal("parameter \"%s_output_logtime\" is missing\n");

    if(param->isMatrix()) {
      
      TMatrix temp;
      
      param->getMatrix(&temp);
      
      _writer->set_multi(true, true, 1, &temp, get_parameter(_prefix + "_output_dir")->getArg());
    //           rpl_per, gen_per, rpl_occ, gen_occ, rank, path, self-ref
    } else
      _writer->set(true, param->isSet(), 1, (param->isSet() ? (int)param->getValue() : 0),
          0, get_parameter(_prefix + "_output_dir")->getArg(), this);
    
  
    loader->attach(_writer);
  
  } else {
    
    if(_writer) delete _writer;
    
    return;
  }
    
  
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::addParameters
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::addParameters (string prefix,
    ParamUpdaterBase* fitMod_updtr,
    ParamUpdaterBase* selMod_updtr,
    ParamUpdaterBase* opt_updtr,
    ParamUpdaterBase* rchge_updtr)
{
  _prefix = prefix;

  // mandatory parameter (no default):
  add_parameter(_prefix + "_trait", STR, true, false, 0, 0); //no updaters here, to keep it safe...

//  ParamUpdater< LCE_Selection_base > * updater =
//  new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_fit_model);

  add_parameter(_prefix + "_fitness_model",STR,false,false,0,0, fitMod_updtr);

//  updater = new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_sel_model);

  add_parameter(_prefix + "_model",STR,true,false,0,0, selMod_updtr); //mandatory, depends on trait
  add_parameter(_prefix + "_base_fitness",DBL,false,true,0,1, selMod_updtr);

  add_parameter(_prefix + "_lethal_equivalents",DBL,false,false,0,0, selMod_updtr);
  add_parameter(_prefix + "_pedigree_F",MAT,false,false,0,0, selMod_updtr);
  //Gaussian & Quadratic models:
  add_parameter(_prefix + "_matrix",MAT,false,false,0,0, selMod_updtr);
  add_parameter(_prefix + "_variance",DBL,false,false,0,0, selMod_updtr);
  add_parameter(_prefix + "_correlation",DBL,false,false,0,0, selMod_updtr);
  add_parameter(_prefix + "_trait_dimension",INT,false,false,0,0, selMod_updtr);
  add_parameter(_prefix + "_randomize",BOOL,false,false,0,0, selMod_updtr);
  add_parameter(_prefix + "_environmental_variance", DBL, false, false, 0, 0, selMod_updtr);
  //stage-specific selection
  add_parameter(_prefix + "_at_stage",MAT,false,false,0,0,selMod_updtr);

//  updater = new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_local_optima);
  add_parameter(_prefix + "_local_optima",DBL,false,false,0,0, opt_updtr);

//  updater = new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_param_rate_of_change);
  add_parameter(_prefix + "_rate_environmental_change", DBL, false, false, 0, 0, rchge_updtr);
  add_parameter(_prefix + "_std_rate_environmental_change", DBL, false, false, 0, 0, rchge_updtr);
  add_parameter(_prefix + "_std_rate_set_at_generation", INT, false, false, 0, 0, rchge_updtr);
  add_parameter(_prefix + "_std_rate_reference_patch", INT, false, false, 0, 0, rchge_updtr);

  //LCE selection output files
  add_parameter(_prefix + "_output", BOOL, false, false, 0, 0, 0);
  add_parameter(_prefix + "_output_logtime", INT, false, false, 0, 0, 0);
  add_parameter(_prefix + "_output_dir", STR, false, false, 0, 0, 0);

}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::setParameters
// ----------------------------------------------------------------------------------------
bool LCE_Selection_base::setBaseParameters ()
{
  assert(_prefix.size() != 0); //check the prefix has been set (with addParameters)

  //selection may act on more than one trait at a time
  _Traits.clear();
  _Traits = get_parameter(_prefix + "_trait")->getMultiArgs();

  _TraitIndices.clear();

  for(unsigned int i = 0; i < _Traits.size(); i++)  {

    if(_popPtr->getTraitIndex(_Traits[i]) == -1) {

      return error("cannot attach trait \"%s\" to life cycle event \"%s\", trait has not been initiated.\n",
            _Traits[i].c_str(), _event_name.c_str());

    } else {
      _TraitIndices.push_back(_popPtr->getTraitIndex(_Traits[i]));


#ifdef _DEBUG_
      cout<<"#### attaching trait \""<<_Traits[i].c_str()<<"\" to selection LCE, index = "<<_TraitIndices[i]<<endl;
#endif

    }
    
  }


  if(!set_fit_model()) return false;

  if(!set_sel_model()) return false;

  if(!set_local_optima()) return false;

  if(!set_param_rate_of_change()) return false;

  _mean_fitness = _max_fitness = 0;

  _nb_trait = _popPtr->getIndividualProtoype()->getTraitNumber();

  _scaling_factor = 1;

  resetCounters();

  assert(_TraitIndices.size() == _getRawFitness.size());

#ifdef _DEBUG_
  cout<<"#### selection is acting on "<<_TraitIndices.size()<<" traits\n";
  cout<<"#### selection is using "<< _getRawFitness.size() <<" fitness functions\n";
#endif
  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::set_fit_model
// ----------------------------------------------------------------------------------------
bool LCE_Selection_base::set_fit_model()
{

  if(_paramSet->isSet(_prefix + "_fitness_model")) {

    string fit_model = _paramSet->getArg(_prefix + "_fitness_model");

    if( fit_model == "absolute") {

      _is_local = true;
      _is_absolute = true;

      _getFitness = &LCE_Selection_base::getFitnessAbsolute;
      _setScalingFactor = &LCE_Selection_base::setScalingFactorAbsolute;

    } else if( fit_model == "relative_local" ) {

      _is_local = true;
      _is_absolute = false;
      _getFitness = &LCE_Selection_base::getFitnessRelative;
      _setScalingFactor = &LCE_Selection_base::setScalingFactorLocal;

    } else if(fit_model == "relative_global") {

      _is_local = false;
      _is_absolute = false;
      _getFitness = &LCE_Selection_base::getFitnessRelative;
      _setScalingFactor = &LCE_Selection_base::setScalingFactorGlobal;

    } else if(fit_model == "relative_max_local") {
      
      _is_local = true;      
      _is_absolute = false;
      _getFitness = &LCE_Selection_base::getFitnessRelative;
      _setScalingFactor = &LCE_Selection_base::setScalingFactorMaxLocal;
      
    } else if(fit_model == "relative_max_global") {
      
      _is_local = false;
      _is_absolute = false;
      _getFitness = &LCE_Selection_base::getFitnessRelative;
      _setScalingFactor = &LCE_Selection_base::setScalingFactorMaxGlobal;

    } else {

      return error("Unknown fitness model \"%s\"", fit_model.c_str());

    }
  } //default case:
  else {

    _is_local = true;
    _is_absolute = true;

    _getFitness = &LCE_Selection_base::getFitnessAbsolute;
    _setScalingFactor = &LCE_Selection_base::setScalingFactorAbsolute;
  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::set_sel_model
// ----------------------------------------------------------------------------------------
bool LCE_Selection_base::set_sel_model()
{
  // ENVIRONMENTAL VARIANCE (only applies to quantitative traits, clarify why it is here relative to tt_quanti)

  if(get_parameter(_prefix + "_environmental_variance")->isSet())
    //the variable actually holds the standard dev...
    _eVariance =  sqrt(get_parameter_value(_prefix + "_environmental_variance"));
  else
    _eVariance = 0;

  // ----------------------------------------------------------------------------------------
  // SELECTION MODELS

  // containers for multi-trait selection
  _SelectionModels.clear();
  _getRawFitness.clear();

  //it is here assumed that the selection models are "aligned" with the traits
  //model 1 is applied to trait 1, model 2 to trait 2 etc. no checks are performed here
  if(!_paramSet->isSet(_prefix + "_model")) {

    _getRawFitness.push_back(&LCE_Selection_base::getFitnessDirect);
    
    _SelectionModels.push_back("direct");

  } else {

    _SelectionModels = get_parameter(_prefix + "_model")->getMultiArgs();

  } //end_if isSet()

  if (_SelectionModels.size() != _TraitIndices.size()) {

    return error("\"%s_trait\" and \"%s_model\" must have the same number of arguments.", _prefix.c_str(), _prefix.c_str());

  }

  // ----------------------------------------------------------------------------------------
  // STAGE UNDER SELECTION
  // reset TMatrix
  if( !_stage_selected ) _stage_selected = new TMatrix;

  // check options from input file:
  if(get_parameter(_prefix + "_at_stage")->isSet()){

    if(get_parameter(_prefix + "_at_stage")->isMatrix()) {

      get_parameter(_prefix + "_at_stage")->getMatrix(_stage_selected);

    } else {

      _stage_selected->reset(1, 1);
      _stage_selected->set(0, 0, get_parameter(_prefix + "_at_stage")->getValue());
    }

  } else { // DEFAULT (selection_at_stage is not set)

    _stage_selected->reset(1, 1);
    _stage_selected->set(0, 0, 0);  //this is the offspring stage, by default

  }
  
  // ----------------------------------------------------------------------------------------
  // SET FUNCTION POINTERS TO FITNESS FUNCTIONS, for multi-trait selection

  string sel_model;
  
  for(unsigned int t = 0; t < _SelectionModels.size(); t++) {

    sel_model = _SelectionModels[t];

    // FIX -----------------------------------------------------------------------
    if(sel_model == "fix") {

      //check if the trait type in the trait table is correct
      if(_Traits[t] != "fix")

        return error("the selection model \"fix\" does not match with the trait type \"%s\"\n", _Traits[t].c_str());

      if(!get_parameter(_prefix + "_lethal_equivalents")->isSet()) {

        return error("\"%s_lethal_equivalents\" parameter is missing with \"fix\" selection!\n", _prefix.c_str());

      } else
        _letheq = get_parameter_value(_prefix + "_lethal_equivalents");

      if(!get_parameter(_prefix + "_base_fitness")->isSet()) {

        warning("\"%s_base_fitness\" parameter is missing under fix selection model, setting it to 1.\n", _prefix.c_str());

        _base_fitness = 1.0;

      } else
        _base_fitness = get_parameter_value(_prefix + "_base_fitness");


      if(!get_parameter(_prefix + "_pedigree_F")->isSet()) {

        return error("\"%s_pedigree_F\" parameter is missing with \"fix\" selection!\n", _prefix.c_str());

      } else {

        TMatrix tmp_mat;
        get_parameter(_prefix + "_pedigree_F")->getMatrix(&tmp_mat);

        if(tmp_mat.getNbCols() != 5) {
          return error("\"%s_pedigree_F\" must be an array of size 5.\n", _prefix.c_str());
        }

        for(unsigned int i = 0; i < 5; i++)
          _Fpedigree[i] = tmp_mat.get(0,i);
      }

      for(unsigned int i = 0; i < 5; i++)
        _FitnessFixModel[i] = _base_fitness * exp( -_letheq * _Fpedigree[i] );

      _getRawFitness.push_back(&LCE_Selection_base::getFitnessFixedEffect);


    // DIRECT -----------------------------------------------------------------------

    } else if(sel_model == "direct") {

      //check if the trait type in the trait table is correct
      if(_Traits[t] != "delet" && _Traits[t] != "dmi")
        return error("the selection model \"direct\" does not match with the trait type \"%s\"\n", _Traits[t].c_str());

       _getRawFitness.push_back(&LCE_Selection_base::getFitnessDirect);


    // QUADRATIC --------------------------------------------------------------------

    } else if(sel_model == "quadratic") {

      //check if the trait type in the trait table is correct
      if(_Traits[t] != "quant")
        return error("the selection model \"quadratic\" does not match with the trait type \"%s\"\n", _Traits[t].c_str());

      if( get_parameter(_prefix + "_trait_dimension")->isSet())
        _selectTraitDimension = get_parameter_value(_prefix + "_trait_dimension");
      else
        _selectTraitDimension = 1;

      if(_selectTraitDimension == 1){

        //check if the number of quanti traits modeled match with the dimensionality:
        if((int)_popPtr->getTraitPrototype("quant")->get_parameter_value("quanti_traits") != _selectTraitDimension)

          return error("The number of quantitative traits in \"quanti_traits\" does not match with\
 the selection trait dimensionality in \"%s_trait_dimension\"\n",
              _prefix.c_str());

        if(!setSelectionMatrix()) return false; //this to set the selection variance params
       
        //now add the fitness function:
         _getRawFitness.push_back(&LCE_Selection_base::getFitnessUnivariateQuadratic);
      
      } else
        return error("the \"quadratic\" fitness model is implemented for a single trait only.\n");


    // GAUSSIAN --------------------------------------------------------------------

    } else if(sel_model == "gaussian") {

      //check if the trait type in the trait table is correct
      if(_Traits[t] != "quant")
        return error("the selection model \"gaussian\" does not match with the trait type \"%s\"\n", _Traits[t].c_str());

      if( get_parameter(_prefix + "_trait_dimension")->isSet() )
        _selectTraitDimension = (unsigned int)get_parameter_value(_prefix + "_trait_dimension");
      else
        _selectTraitDimension = 1;

      //check if the number of quanti traits modeled match with the dimensionality:
      if(_popPtr->getTraitPrototype("quant")->get_parameter_value("quanti_traits") != _selectTraitDimension)
        return error("The number of quantitative traits in \"quanti_traits\" does not match with\
 the selection trait dimensionality in \"%s_trait_dimension\"\n",
            _prefix.c_str());

     // set the selection matrices for multi-stage or spatially variable cases
     if( !setSelectionMatrix() ) return false;

      if(_selectTraitDimension > 1) {

        if(_eVariance > 0)
          _getRawFitness.push_back(&LCE_Selection_base::getFitnessMultivariateGaussian_VE);
        else
          _getRawFitness.push_back(&LCE_Selection_base::getFitnessMultivariateGaussian);

      } else {

        if(_eVariance > 0)
          _getRawFitness.push_back(&LCE_Selection_base::getFitnessUnivariateGaussian_VE);
        else
          _getRawFitness.push_back(&LCE_Selection_base::getFitnessUnivariateGaussian);

      }


    } else {

      return error("wrong selection model, must be either \"fix\", \"direct\", \"quadratic\", or \"gaussian\".\n");

    }
  } //end_if isSet()

  return true;
}
//----------------------------------------------------------------------------------------
//setSelectionMatrixAge
//----------------------------------------------------------------------------------------
bool LCE_Selection_base::setSelectionMatrixMultiStage(){

//this is called only if selection_at_stage has more than one value

#ifdef _DEBUG_
    message("---LCE_Selection_base::setSelectionMatrixMultiStage---\n");
#endif

  TMatrix tmp_mat;
  unsigned int patchNbr = _popPtr->getPatchNbr();

  tmp_mat.reset(_selectTraitDimension, _selectTraitDimension);

  int nb_classes_selected = _stage_selected->getNbCols();
  int numVarInRow = _selection_stage_variance->getNbCols();
  int numRows = _selection_stage_variance->getNbRows();
  int numCovariance = (_selectTraitDimension-1)*_selectTraitDimension/2;
  int age_pos, cov_pos;
  
  if(numVarInRow > _selectTraitDimension) {
    if( (numVarInRow - _selectTraitDimension) != numCovariance) {
      return error("number of covariance terms in \"%s_selection_stage_variance\" does not match with the number of traits\n", _prefix.c_str());
    }
  }
  
  // the selection matrix container is set to receive min of one matrix per patch
  // the matrix will be ordered: [patch][stage]
  // check:
  assert(_selection_matrix.size() == patchNbr);
  
  for(int a = 0; a < nb_classes_selected; a++){

    age_pos = a % numRows; //user may pass only one row that will be repeated for all stages under selection

    //set the selection matrix:
    
    cov_pos = _selectTraitDimension;
    
    for( int i = 0; i < _selectTraitDimension; i++) {
      
      if(numVarInRow < _selectTraitDimension) //repetition of a few values
        tmp_mat.set(i, i, _selection_stage_variance->get(age_pos, i % numVarInRow));
      else
        tmp_mat.set(i, i, _selection_stage_variance->get(age_pos, i));
      
      
      //check if we have covariance in input, i.e. more than _selectTraitDimension elements in a row
      //we already checked that the number of elements corresponds to the number of covariance terms
      
      if(numVarInRow > _selectTraitDimension) {
        
        //{v1,v2,v3,cov12,cov13,cov23}
        //{v1,v2,v3,v4,cov12,cov13,cov14,cov23,cov24,cov34}
        
        for( int j = i+1; j < _selectTraitDimension; j++) {
          tmp_mat.set(i, j, _selection_stage_variance->get(age_pos, cov_pos) );
          tmp_mat.set(j, i, _selection_stage_variance->get(age_pos, cov_pos++) );
        }

      } else {
        
        for( int j = i+1; j < _selectTraitDimension; j++) {
          tmp_mat.set(i, j, 0);
          tmp_mat.set(j, i, 0);
        }
        
      }
    }

    // copy the same matrix to all patches, no spatial variation in selection !!
    // the matrix is ordered: [patch][stage]

    for( unsigned int p = 0; p < patchNbr; p++) {
      _selection_matrix[p].push_back(new TMatrix(tmp_mat));
    }
  }

  //Create gsl matrices//

  //selection on more than one trait
  if(_selectTraitDimension > 1) {

    assert(_gsl_selection_matrix.empty());

    for(unsigned int i = 0; i < patchNbr; ++i)
      _gsl_selection_matrix.push_back(vector<gsl_matrix*>());

    // here we set the stage-specific selection matrices
    // they do not vary spatially

    for(int i = 0; i < nb_classes_selected; i++){

      //Inverting the selection matrices:
      _selection_matrix[0][i]->inverse();

      for( unsigned int p = 0; p < patchNbr; p++) {

        _gsl_selection_matrix[p].push_back( gsl_matrix_alloc(_selectTraitDimension,
                                                             _selectTraitDimension) );

        _selection_matrix[0][i]->get_gsl_matrix(_gsl_selection_matrix[p][i]);
      }
    }

    //allocate the vectors used by the fitness function:
    if(_diffs != NULL) gsl_vector_free(_diffs);
    _diffs = gsl_vector_alloc( _selectTraitDimension );

    if(_res1 != NULL) gsl_vector_free(_res1);
    _res1 = gsl_vector_alloc( _selectTraitDimension );

  }

  return true;

}

// ----------------------------------------------------------------------------------------
// setSelectionMatrix
// ----------------------------------------------------------------------------------------
bool LCE_Selection_base::setSelectionMatrix()
{
  TMatrix tmp_mat;
  unsigned int patchNbr = _popPtr->getPatchNbr();

  if(!get_parameter(_prefix + "_matrix")->isSet() && !get_parameter(_prefix + "_variance")->isSet())
    return error("\"%s_matrix\" or \"%s_variance\" must be set with selection model = \"gaussian\".\n",
        _prefix.c_str(), _prefix.c_str());

  if(get_parameter(_prefix + "_variance")->isSet() && !get_parameter(_prefix + "_trait_dimension")->isSet())
    return error("parameter \"%s_trait_dimension\" is missing!\n", _prefix.c_str());
   

  //clear the selection matrix containers (_selection_matrix and _gsl_selection_matrix)
  reset_selection_matrix_containers();

  // add a matrix container for each patch, will hold patch-specific matrices
  for(unsigned int i = 0; i < patchNbr; ++i)
    _selection_matrix.push_back(vector<TMatrix*>());

  // the same is done for gsl matrices later when checking for trait dimensions


  // MULTI-STAGE selection pressures
  
  if(get_parameter(_prefix + "_at_stage")->isSet()) {
//  if( _stage_selected->length() > 1) {
    //note: at this point the stages under selection have been recorded in _stage_selection
    // see above in set_sel_model()

    //we store the stage-specific selection matrices in one TMatrix,
    //the structure is different from the regular matrix
    //the stage selection matrix has one matrix per row, one row per stage under selection
    if( !_selection_stage_variance )  _selection_stage_variance = new TMatrix;

    if( !get_parameter(_prefix + "_matrix")->isSet() )
      return error("the selection variance-covariance matrices for the stages under selection\
 must be specified with parameter \"%s_matrix\" (see Manual).\n", _prefix.c_str());

    //store the stage-spec matrices:
    get_parameter(_prefix + "_matrix")->getMatrix(_selection_stage_variance);

    return setSelectionMatrixMultiStage();

  }

  // SINGLE STAGE under selection
  // here, selection parameters can vary in space if selection_variance is used

  // check if selection matrix provided in input, will not vary in space
  if(get_parameter(_prefix + "_matrix")->isSet()) {

    //selection matrix provided, same selection surface in each patch
    //this one will only apply to the offspring stage under selection (default)
    _paramSet->getMatrix(_prefix + "_matrix", &tmp_mat);

    if( _selectTraitDimension != tmp_mat.getNbCols() || _selectTraitDimension != tmp_mat.getNbRows() ) {
      error("\"%s_matrix\" must have the same number of rows and columns\n", _prefix.c_str());
      return error("as the number of quanti traits, when \"%s_at_stage\" is not set!\n", _prefix.c_str());
    }

    if(tmp_mat.getNbCols() != tmp_mat.getNbRows()){
      error("\"%s_matrix\" must be a square matrix when \"%s_at_stage\" is not set!\n", _prefix.c_str(), _prefix.c_str());
      return error("use \"%s_variance\" to only specify per-trait omega^2 values.\n", _prefix.c_str());
    }
    //we have one selection matrix per patch and one stage under selection
    //copy the selection matrix in the container for each patch
    for(unsigned int i = 0; i < patchNbr; ++i) {
      _selection_matrix[i].push_back( new TMatrix(tmp_mat) );
    }

  } else
    // no selection matrix provided in input, selection can vary in space for the single stage under selection
  {

    //we have to check for spatial variation in variance and covariances provided with
    //selection_variance and selection_covariance matrices

    TMatrix var_spatmat, corr_spatmat;

    //setting variance spatial matrix:
    var_spatmat.reset(patchNbr, (unsigned)_selectTraitDimension);

    if(get_parameter(_prefix + "_variance")->isMatrix()) {

      _paramSet->getMatrix(_prefix + "_variance", &tmp_mat);

      if( !setSpatialMatrix(_prefix + "_variance", _prefix + "\"_trait_dimension\"",
                            &tmp_mat, &var_spatmat,
                            (unsigned)_selectTraitDimension, patchNbr,
                            _paramSet->isSet(_prefix + "_randomize") )
        ) return false;

    } else {

      var_spatmat.assign(get_parameter_value(_prefix + "_variance"));
    }

    //setting correlation spatial matrix:
    corr_spatmat.reset(patchNbr, (unsigned)_selectTraitDimension*(_selectTraitDimension-1)/2);

    if(get_parameter(_prefix + "_correlation")->isMatrix()) {

      _paramSet->getMatrix(_prefix + "_correlation", &tmp_mat);

      if( !setSpatialMatrix(_prefix + "_correlation","the num of correlation coefficients",
                            &tmp_mat, &corr_spatmat,
                            (unsigned)_selectTraitDimension*(_selectTraitDimension-1)/2,
                            patchNbr, _paramSet->isSet(_prefix + "_randomize") )
        ) return false;

    } else {

      corr_spatmat.assign((get_parameter(_prefix + "_correlation")->isSet() ?
                           get_parameter_value(_prefix + "_correlation") : 0.0 ));
    }
    //set the selection matrix:
    tmp_mat.reset(_selectTraitDimension, _selectTraitDimension);
    double covar;
    unsigned int col;
    for( unsigned int p = 0; p < patchNbr; p++) {
      col = 0;
      for( int i = 0; i < _selectTraitDimension; i++) {
        tmp_mat.set(i, i, var_spatmat.get(p, i));
        for( int j = i+1; j < _selectTraitDimension; j++) {
          covar = corr_spatmat.get(p, col) * sqrt( var_spatmat.get(p, i) * var_spatmat.get(p, j) );
          tmp_mat.set(i, j, covar);
          tmp_mat.set(j, i, covar);
          col++;
        }

        // selection matrix for patch p, stage 0
        _selection_matrix[p].push_back(new TMatrix(tmp_mat));
      }
    }
  } // end if selection_matrix not provided

  // now, build the GSL matrices for multivariate selection cases,
  // AND a single stage under selection,
  // the multi-stage case has called 'return' above
  if(_selectTraitDimension > 1) {

    //selection on more than one trait

    assert(_gsl_selection_matrix.empty());

    //fill in with one matrix container per patch
    for(unsigned int i = 0; i < patchNbr; ++i)
      _gsl_selection_matrix.push_back(vector<gsl_matrix*>());

    // we have only on selection matrix per patch, only one stage under selection here
    for( unsigned int p = 0; p < patchNbr; p++) {
      _selection_matrix[p][0]->show_up();

      _selection_matrix[p][0]->inverse();

      _gsl_selection_matrix[p].push_back( gsl_matrix_alloc(_selectTraitDimension, _selectTraitDimension) );

      _selection_matrix[p][0]->get_gsl_matrix(_gsl_selection_matrix[p][0]);
    }
    //allocate the vectors used by the fitness function:
    if(_diffs != NULL) gsl_vector_free(_diffs);
    _diffs = gsl_vector_alloc( _selectTraitDimension );

    if(_res1 != NULL) gsl_vector_free(_res1);
    _res1 = gsl_vector_alloc( _selectTraitDimension );

  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::set_local_optima
// ----------------------------------------------------------------------------------------
bool LCE_Selection_base::set_local_optima ()
{
  //set the traits' local optima, _selectTraitDimension must be set before that (= nbr of traits to select on)
  string model = _paramSet->getArg(_prefix + "_model");

  if( model == "fix"  || model == "direct") return true;

  if( (model == "gaussian" || model == "quadratic")
     && !get_parameter(_prefix + "_local_optima")->isSet()) {

    return error("parameter \"%s_local_optima\" must be set to have Gaussian or quadratic selection.\n", _prefix.c_str());

  } else {

    TMatrix tmp_mat;

    if(_local_optima == 0) _local_optima = new TMatrix();

    _local_optima->reset(_popPtr->getPatchNbr(), _selectTraitDimension);

    _paramSet->getMatrix(_prefix + "_local_optima", &tmp_mat);

    return setSpatialMatrix(_prefix + "_local_optima", _prefix + "\"_trait_dimension\"", &tmp_mat, _local_optima,
                            _selectTraitDimension, _popPtr->getPatchNbr(), _paramSet->isSet(_prefix + "_randomize"));
  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::set_param_rate_of_change
// ----------------------------------------------------------------------------------------
bool LCE_Selection_base::set_param_rate_of_change ()
{
  _rate_of_change_is_std = false;
  _do_change_local_opt = false;
  _setNewLocalOptima = 0;
  _rate_of_change_local_optima.reset();


  if (!get_parameter(_prefix + "_rate_environmental_change")->isSet()
      && !get_parameter(_prefix + "_std_rate_environmental_change")->isSet() )
    return true;

  if (get_parameter(_prefix + "_rate_environmental_change")->isSet()
      && get_parameter(_prefix + "_std_rate_environmental_change")->isSet() ) {
    return error("both \"%s_rate_environmental_change\" and \"%s_std_rate_environmental_change\" are set, need only one.\n",
        _prefix.c_str(), _prefix.c_str());
  }

  TMatrix tmpMat;

  if (get_parameter(_prefix + "_rate_environmental_change")->isSet()) {

    if(!get_parameter(_prefix + "_rate_environmental_change")->isMatrix()) {

      double val = get_parameter_value(_prefix + "_rate_environmental_change");

      tmpMat.reset(1, _selectTraitDimension);
      tmpMat.assign(val);

    } else {
      get_parameter(_prefix + "_rate_environmental_change")->getMatrix(&tmpMat);
    }

    _setNewLocalOptima = &LCE_Selection_base::changeLocalOptima;

  } else if (get_parameter(_prefix + "_std_rate_environmental_change")->isSet()){

    if(!get_parameter(_prefix + "_std_rate_environmental_change")->isMatrix()) {

      double val = get_parameter_value(_prefix + "_std_rate_environmental_change");

      tmpMat.reset(1, _selectTraitDimension);
      tmpMat.assign(val);

    } else {
      get_parameter(_prefix + "_std_rate_environmental_change")->getMatrix(&tmpMat);
    }

    _rate_of_change_is_std = true;

    if(get_parameter(_prefix + "_std_rate_set_at_generation")->isSet())
      _set_std_rate_at_generation = (unsigned int)get_parameter_value(_prefix + "_std_rate_set_at_generation");
    else
      _set_std_rate_at_generation = 1;

    //check if phenotypic SD is to be computed in a single reference patch
    //is -1 if parameter not set, which corresponds to the whole population then
    _std_rate_reference_patch = get_parameter_value(_prefix + "_std_rate_reference_patch");

    _setNewLocalOptima = &LCE_Selection_base::set_std_rate_of_change;
  }


  if(tmpMat.nrows() > _popPtr->getPatchNbr())
    return error("The matrix of rate of change in local optima has more rows than the number of patches\n");

  if((int)tmpMat.ncols() > _selectTraitDimension)
    return error("The matrix of rate of change in local optima has more columns than the number of quantitative traits\n");

  _do_change_local_opt = true;

  _rate_of_change_local_optima.reset(_popPtr->getPatchNbr(), _selectTraitDimension);

  unsigned int nCol = tmpMat.ncols(), nRow = tmpMat.nrows();

  //copy values, with pattern propagation
  for (int p = 0; p < _popPtr->getPatchNbr(); ++p) {
    for (int i = 0; i < _selectTraitDimension; ++i) {
      _rate_of_change_local_optima.set(p, i, tmpMat.get(p % nRow, i % nCol));
    }
  }

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection::set_std_rate_of_change
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::set_std_rate_of_change()
{
  if(_popPtr->getCurrentGeneration() == _set_std_rate_at_generation) {

    //reset rate_of_change matrix in each new replicate:
    TMatrix tmpMat;

    if(!get_parameter(_prefix + "_std_rate_environmental_change")->isMatrix()) {

      double val = get_parameter_value(_prefix + "_std_rate_environmental_change");

      tmpMat.reset(_popPtr->getPatchNbr(), _selectTraitDimension);
      tmpMat.assign(val);

    } else {
      get_parameter(_prefix + "_std_rate_environmental_change")->getMatrix(&tmpMat);
    }

    unsigned int nCol = tmpMat.ncols(), nRow = tmpMat.nrows();

    _rate_of_change_local_optima.reset(_popPtr->getPatchNbr(), _selectTraitDimension);

    //copy values, with pattern propagation
    for (int p = 0; p < _popPtr->getPatchNbr(); ++p) {
      for (int i = 0; i < _selectTraitDimension; ++i) {
        _rate_of_change_local_optima.set(p, i, tmpMat.get(p % nRow, i % nCol));
      }
    }

    double* SD = new double [_selectTraitDimension];

    for (int i = 0; i < _selectTraitDimension; ++i) {
      SD[i] = 0;
    }

    // check if SD is taken in a reference patch instead of the whole pop
    if (_std_rate_reference_patch > -1) {

      if( _popPtr->getPatch(_std_rate_reference_patch)->size(OFFSx) != 0 )
        addPhenotypicSD(_std_rate_reference_patch, SD);

    } else {
    // compute SD as the mean within-patch SD

      unsigned int cnt = 0;

      //get SD only in extant demes, to avoid nans
      for(unsigned int i = 0; i < _popPtr->getPatchNbr(); ++i) {
        if (_popPtr->getPatch(i)->size(OFFSx) != 0 ) {
          cnt++;
          addPhenotypicSD(i, SD);
        }
      }

      //compute mean within-patch phenotypic standard deviation:
      for (int i = 0; i < _selectTraitDimension; ++i) {
        SD[i]/= cnt;

      }
    } //end if

    //multiply the per-trait rates of change by SD:
    for (int p = 0; p < _popPtr->getPatchNbr(); ++p) {
      for (int i = 0; i < _selectTraitDimension; ++i){
        _rate_of_change_local_optima.multi(p, i, SD[i]);
      }
    }
    //log the rates in the simulation log file:
    SIMenv::MainSim->_FileServices.log("#selection_rate_environmental_change " +
                                       _rate_of_change_local_optima.to_string());

    //compute the change of local optima for current generation:
    changeLocalOptima();

    //now reset the function pointer to changeLocalOptima() for next generation:
    _setNewLocalOptima = &LCE_Selection_base::changeLocalOptima;

    delete [] SD;
  }
}
// ----------------------------------------------------------------------------------------
// LCE_Selection::set_std_rate_of_change
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::addPhenotypicSD (unsigned int deme, double *stDev)
{
  Patch *patch = _popPtr->getPatch(deme);
  Individual* ind;
  unsigned int table_size = patch->size(OFFSx);

  double **phenot = new double* [_selectTraitDimension];

  for (int i = 0; i < _selectTraitDimension; ++i) {

    phenot[i] = new double [table_size];

  }

  unsigned int pos = 0;

  for (unsigned int j = 0; j < patch->size(FEM, OFFSx) && pos < table_size; ++j) {

    ind = patch->get(FEM, OFFSx, j);
    _phe = (double*)ind->getTraitValue(_LCELinkedTraitIndex);

    for (int i = 0; i < _selectTraitDimension; ++i)
      phenot[i][pos] = _phe[i];

    pos++;

  }

  for (unsigned int j = 0; j < patch->size(MAL, OFFSx) && pos < table_size; ++j) {

    ind = patch->get(MAL, OFFSx, j);
    _phe = (double*)ind->getTraitValue(_LCELinkedTraitIndex);

    for (int i = 0; i < _selectTraitDimension; ++i)
      phenot[i][pos] = _phe[i];

    pos++;

  }

  assert(pos == table_size);

  for (int i = 0; i < _selectTraitDimension; ++i) {
    stDev[i] += sqrt( my_variance_with_fixed_mean( phenot[i], table_size, my_mean(phenot[i], table_size) ) );
    delete [] phenot[i];
  }

  delete [] phenot;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection::changeLocalOptima
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::changeLocalOptima ()
{
  for (int i = 0; i < _selectTraitDimension; ++i)
    for (unsigned int j = 0; j < _local_optima->nrows(); ++j) {
      _local_optima->plus(j, i, _rate_of_change_local_optima.get(j, i));
    }
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::checkChangeLocalOptima
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::checkChangeLocalOptima()
{
  // check if change in local optima for quantitative traits
  if(_do_change_local_opt) {

    if(_popPtr->getCurrentGeneration() == 1) {

      set_local_optima(); //reset local optima to initial values

      if(_rate_of_change_is_std) //reset rate of change relative to SD of that replicate
        _setNewLocalOptima = &LCE_Selection_base::set_std_rate_of_change;
    }

    (this->*_setNewLocalOptima)();
  }
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getFitnessUnivariateQuadratic
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getFitnessUnivariateQuadratic ( Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx)
{
  double res2, diff;

   _phe = (double*)ind->getTraitValue(trait);

  diff = _phe[0] - _local_optima->get(patch, 0);

  res2 = diff*diff / _selection_matrix[patch][selected_stage_idx]->get(0,0);

  return 1 - res2;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getFitnessUnivariateGaussian
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getFitnessUnivariateGaussian ( Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx )
{
  double res2, diff;

  _phe = (double*)ind->getTraitValue(trait);

  diff = _phe[0] - _local_optima->get(patch, 0);

  res2 = diff*diff / _selection_matrix[patch][selected_stage_idx]->get(0,0);

  return exp( -0.5 * res2 );
}

// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getFitnessMultivariateGaussian
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getFitnessMultivariateGaussian ( Individual* ind, unsigned int patch, unsigned int trait, unsigned selected_stage_idx )
{
  double res2;

//  if(!ind) fatal("passing NULL ind ptr to LCE_Selection_base::getFitnessMultivariateGaussian!!!\n");

  _phe = (double*)ind->getTraitValue(trait);

  for( int i = 0; i < _selectTraitDimension; i++)
    gsl_vector_set(_diffs, i, _phe[i] - _local_optima->get(patch, i));

  //(diff)T * W * diff:
  //right partial product:
  gsl_blas_dsymv(CblasUpper, 1.0, _gsl_selection_matrix[patch][selected_stage_idx], _diffs, 0.0, _res1);
  //left product:
  gsl_blas_ddot(_diffs, _res1, &res2);

  return exp( -0.5 * res2 );
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getFitnessUnivariateGaussian_VE
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getFitnessUnivariateGaussian_VE ( Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx )
{
  double res2, diff;

  _phe = (double*)ind->getTraitValue(trait);

  //add the environmental variance here:
  diff = _phe[0] + RAND::Gaussian(_eVariance) - _local_optima->get(patch, 0);

  res2 = diff*diff / _selection_matrix[patch][selected_stage_idx]->get(0,0);

  return exp( -0.5 * res2 );
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getFitnessMultivariateGaussian_VE
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getFitnessMultivariateGaussian_VE ( Individual* ind, unsigned int patch, unsigned int trait, unsigned int selected_stage_idx )
{
  double res2;

  _phe = (double*)ind->getTraitValue(trait);

  //add the environmental variance here:
  for( int i = 0; i < _selectTraitDimension; i++)
    gsl_vector_set(_diffs, i, _phe[i]  + RAND::Gaussian(_eVariance) - _local_optima->get(patch, i));

  //(diff)T * W * diff:
  //right partial product:
  gsl_blas_dsymv(CblasUpper, 1.0, _gsl_selection_matrix[patch][selected_stage_idx], _diffs, 0.0, _res1);
  //left product:
  gsl_blas_ddot(_diffs, _res1, &res2);

  return exp( -0.5 * res2 );
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getFitnessAbsolute
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getFitnessAbsolute (Individual* ind, unsigned int patch, unsigned int selected_stage_idx)
{
  double fitness = 1;
  //at this point, _getRawFitness.size() == _TraitIndices.size()
  //and we assume that the functions are "aligned" with the traits
  for(unsigned int i = 0; i < _getRawFitness.size(); i++)
    fitness *= (this->*_getRawFitness[i])(ind, patch, _TraitIndices[i], selected_stage_idx);

  return fitness;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getMeanFitness
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getMeanFitness (unsigned int selected_stage_idx)
{
  double mean = 0;
  Patch *patch;
  age_idx age = age_idx(_stage_selected->get(0, selected_stage_idx));

  for(unsigned int i = 0, npatch = _popPtr->getPatchNbr(); i < npatch; i++) {

    patch = _popPtr->getPatch(i);

    for(unsigned int j = 0, size = patch->size(FEM, age); j < size; j++)
      mean += getFitness( patch->get(FEM, age, j), i, selected_stage_idx);

    for(unsigned int j = 0, size = patch->size(MAL, age); j < size; j++)
      mean += getFitness( patch->get(MAL, age, j), i, selected_stage_idx);
  }
  return mean/_popPtr->size(age);
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getMeanPatchFitness
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getMeanPatchFitness (unsigned int selected_stage_idx, unsigned int p)
{
  double mean = 0;
  Patch *patch = _popPtr->getPatch(p);
  age_idx age = age_idx(_stage_selected->get(0, selected_stage_idx));

  for(unsigned int j = 0, size = patch->size(FEM, age); j < size; j++)
    mean += getFitness( patch->get(FEM, age, j), p, selected_stage_idx);

  for(unsigned int j = 0, size = patch->size(MAL, age); j < size; j++)
    mean += getFitness( patch->get(MAL, age, j), p, selected_stage_idx);

  return mean/patch->size(age);
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getMeanPatchFitness
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getMeanPatchFitnessMultiStage (age_t AGE, unsigned int selected_stage_idx, unsigned int p)
{
  double mean = 0;
  Patch *patch = _popPtr->getPatch(p);
  unsigned int num_stage = _popPtr->getNumAgeClasses();
  unsigned int mask = 1, size, tot_size = 0;
  age_idx age;

  for(unsigned int i = 0; i < num_stage; i++) {

       if( (mask & AGE) )
       {
         age = age_idx(i);

         size = patch->size(FEM, age);
         tot_size += size;

         for(unsigned int j = 0; j < size; j++)
           mean += getFitness( patch->get(FEM, age, j), p, selected_stage_idx);

         size = patch->size(MAL, age);
         tot_size += size;

         for(unsigned int j = 0; j < size; j++)
           mean += getFitness( patch->get(MAL, age, j), p, selected_stage_idx);
       }

       mask <<= 1;
     }

  return mean/tot_size;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getMaxFitness
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getMaxFitness (unsigned int selected_stage_idx)
{
  double max = 0, local_max;
  
  for(unsigned int i = 0, npatch = _popPtr->getPatchNbr(); i < npatch; i++) {
    
    local_max = getMaxPatchFitness(selected_stage_idx, i);
    
    max = (local_max > max ? local_max : max);
  }
  
  return max;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getMaxPatchFitness
// ----------------------------------------------------------------------------------------
double LCE_Selection_base::getMaxPatchFitness (unsigned int selected_stage_idx, unsigned int p)
{
  double max = 0, fit;
  Patch *patch = _popPtr->getPatch(p);
  age_idx age = age_idx(_stage_selected->get(0, selected_stage_idx));
  
  for(unsigned int j = 0, size = patch->size(FEM, age); j < size; j++) {
    fit = getFitness( patch->get(FEM, age, j), p, selected_stage_idx);
    max = (fit > max ? fit : max);
  }
  for(unsigned int j = 0, size = patch->size(MAL, age); j < size; j++){
    fit = getFitness( patch->get(MAL, age, j), p, selected_stage_idx);
    max = (fit > max ? fit : max);
  }
  
  return max;
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::getScaledPatchFitness
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::getScaledPatchFitness (sex_t sex, unsigned int selected_stage_idx,
    Patch* patch, unsigned int N, double scaled_fitness[])
{
  double sum = 0;
  _scaling_factor = 1; //to get the raw fitness when calling getFitness.
  age_idx age = age_idx(_stage_selected->get(0, selected_stage_idx));

  for(unsigned int i = 0; i < N; ++i) {
    scaled_fitness[i] = getFitness( patch->get(sex, age, i), patch->getID(), selected_stage_idx);
    sum += scaled_fitness[i];
  }

  for(unsigned int i = 0; i < N; ++i)
    scaled_fitness[i] /= sum;

}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::setScalingFactorLocal
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::setScalingFactorLocal (unsigned int selected_stage_idx, unsigned int p)
{
  _scaling_factor = 1; //this to have the raw mean fitness below
  _scaling_factor = 1.0/getMeanPatchFitness(selected_stage_idx, p);
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::setScalingFactorGlobal
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::setScalingFactorGlobal (unsigned int selected_stage_idx, unsigned int p)
{
  if(p != 0) return; //we compute the mean fitness only once
  _scaling_factor = 1;
  _scaling_factor = 1.0/getMeanFitness(selected_stage_idx);
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::setScalingFactorMaxLocal
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::setScalingFactorMaxLocal (unsigned int selected_stage_idx, unsigned int p)
{
  _scaling_factor = 1; //this to have the raw fitness when computing fitnesses below
  _scaling_factor = 1.0/getMaxPatchFitness(selected_stage_idx, p);
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::setScalingFactorGlobal
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::setScalingFactorMaxGlobal (unsigned int selected_stage_idx, unsigned int p)
{
  if(p != 0) return; //we compute the max fitness only once
  _scaling_factor = 1;
  _scaling_factor = 1.0/getMaxFitness(selected_stage_idx);
}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::execute
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::execute ()
{
#ifdef _DEBUG_
  message("\nLCE_Selection_base::execute:: \n");
#endif

  Patch * patch;
  age_idx age;

  resetCounters();

  // check if change in local optima for quantitative traits
  checkChangeLocalOptima();

  for(unsigned int p = 0; p < _popPtr->getPatchNbr(); p++) {

    patch = _popPtr->getPatch(p);

      for(int i = 0 ; i < _stage_selected->getNbCols();i++) {

        age = age_idx(_stage_selected->get(0,i));

        (this->*_setScalingFactor)(i, p); //@TODO options for non stage-specific mean/max fitness?

        doViabilitySelection(FEM, age, patch, p, i);

        doViabilitySelection(MAL, age, patch, p, i);

      }
  }
  setMeans();

}
// ----------------------------------------------------------------------------------------
// LCE_Selection_base::doViabilitySelection
// ----------------------------------------------------------------------------------------
void LCE_Selection_base::doViabilitySelection (sex_t SEX, age_idx AGE, Patch* patch,
                                               unsigned int p, unsigned int selected_stage_idx)
{
  Individual* ind;
  double fitness;
  unsigned int cat;

  for(unsigned int i = 0; i < patch->size(SEX, AGE); i++) {

    ind = patch->get(SEX, AGE, i);

    cat = ind->getPedigreeClass();

    fitness = getFitness( ind, p, selected_stage_idx);

    _fitness[cat] += fitness;
    _ind_cntr[cat]++;

    if(RAND::Uniform() > fitness ) {
      //this individual dies

      patch->remove(SEX, AGE, i);

      _popPtr->recycle(ind);

      i--;

    } //else; this individual stays in the patch
    else {
      _survival[cat]++;
    }

  }
}
// ----------------------------------------------------------------------------------------

//                             LCE_Viability_Selection

// ----------------------------------------------------------------------------------------
// LCE_Viability_Selection::LCE_Viability_Selection
// ----------------------------------------------------------------------------------------
LCE_Viability_Selection::LCE_Viability_Selection() : LifeCycleEvent("viability_selection", "")
{
  this->addParameters("selection",  //the parameter name prefix, different from the LCE name
      new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_fit_model),
      new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_sel_model),
      new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_local_optima),
      new ParamUpdater<LCE_Selection_base>(&LCE_Selection_base::set_param_rate_of_change));
}
// ----------------------------------------------------------------------------------------

//                                 LCE_SelectionSH

// ----------------------------------------------------------------------------------------
// LCE_SelectionSH::setStatRecorders
// ----------------------------------------------------------------------------------------
bool LCE_SelectionSH::setStatRecorders (string& token)
{
  string age_tag = token.substr(0,token.find_first_of("."));
  string sub_token;
  age_t AGE = ALL;

  //add stats for all stages under selection
  _stages = _SHLinkedEvent->getAgeClassesSelected();

  if (age_tag.size() != 0 && age_tag.size() != string::npos) {

    if (age_tag == "adlt") AGE = ADULTS;

    else if (age_tag == "off") AGE = OFFSPRG;

    else age_tag = "";

  } else {
    age_tag = "";
  }

  if (age_tag.size() != 0)
    sub_token = token.substr(token.find_first_of(".") + 1, string::npos);
  else
    sub_token = token;

  if(sub_token == "fitness") {
    add("Mean population fitness","fitness.mean",ALL,0,0,&LCE_SelectionSH::getMeanFitness,0,0,0);
    add("Mean population fitness","fitness.outb",ALL,0,0,0,&LCE_SelectionSH::getFitness,0,0);
    add("Mean population fitness","fitness.outw",ALL,1,0,0,&LCE_SelectionSH::getFitness,0,0);
    add("Mean population fitness","fitness.hsib",ALL,2,0,0,&LCE_SelectionSH::getFitness,0,0);
    add("Mean population fitness","fitness.fsib",ALL,3,0,0,&LCE_SelectionSH::getFitness,0,0);
    add("Mean population fitness","fitness.self",ALL,4,0,0,&LCE_SelectionSH::getFitness,0,0);
  } else if(sub_token == "survival") {
    add("Mean offspring survival","survival.outb",ALL,0,0,0,&LCE_SelectionSH::getSurvival,0,0);
    add("Mean offspring survival","survival.outw",ALL,1,0,0,&LCE_SelectionSH::getSurvival,0,0);
    add("Mean offspring survival","survival.hsib",ALL,2,0,0,&LCE_SelectionSH::getSurvival,0,0);
    add("Mean offspring survival","survival.fsib",ALL,3,0,0,&LCE_SelectionSH::getSurvival,0,0);
    add("Mean offspring survival","survival.self",ALL,4,0,0,&LCE_SelectionSH::getSurvival,0,0);
  } else if(sub_token == "fitness.prop") {
    add("Proportion of b/n demes outbreds","prop.outb",ALL,0,0,0,&LCE_SelectionSH::getPedProp,0,0);
    add("Proportion of w/n demes outbreds","prop.outw",ALL,1,0,0,&LCE_SelectionSH::getPedProp,0,0);
    add("Proportion of half-sib crossings","prop.hsib",ALL,2,0,0,&LCE_SelectionSH::getPedProp,0,0);
    add("Proportion of full-sib crossings","prop.fsib",ALL,3,0,0,&LCE_SelectionSH::getPedProp,0,0);
    add("Proportion of selfed progeny","prop.self",ALL,4,0,0,&LCE_SelectionSH::getPedProp,0,0);

  } else if(sub_token == "fitness.patch") {

    addMeanPerPatch(AGE);

  } else if(sub_token == "fitness.var.patch") {

    addVarPerPatch(AGE);

  } else return false;

  return true;
}
// ----------------------------------------------------------------------------------------
// LCE_SelectionSH::addMeanPerPatch
// ----------------------------------------------------------------------------------------
void LCE_SelectionSH::addMeanPerPatch (age_t AGE)
{
  unsigned int patchNbr = _pop->getPatchNbr();

//  //add stats for all stages under selection
//  TMatrix* stages = _SHLinkedEvent->getAgeClassesSelected();

  for(unsigned int i = 0; i < _stages->ncols(); i++) {

    string suffix = "a" + tstring::int2str(_stages->get(0,i));
    string name = suffix + ".W.avg.p";
    string long_name = "Mean fitness in patch ";
    string patch;
    age_idx age = age_idx(_stages->get(0,i));

    void (LCE_SelectionSH::* setter) (void) = (AGE == ADULTS ?
        &LCE_SelectionSH::setAdultTable :
        &LCE_SelectionSH::setOffsprgTable);

    //first patch, gets the data table setter:
    add(long_name + "1", name + "1", AGE, 0, i,
        0,0,&LCE_SelectionSH::getMeanPatchFitness, setter);

    for(unsigned int p = 1; p < patchNbr; p++) {
      patch = tstring::int2str(p+1);
      add(long_name + patch, name + patch, AGE, p, i,
          0,0,&LCE_SelectionSH::getMeanPatchFitness,0);
    }
  }
}
// ----------------------------------------------------------------------------------------
// LCE_SelectionSH::addVarPerPatch
// ----------------------------------------------------------------------------------------
void LCE_SelectionSH::addVarPerPatch (age_t AGE)
{
  unsigned int patchNbr = _pop->getPatchNbr();

//  TMatrix* stages = _SHLinkedEvent->getAgeClassesSelected();

  for(unsigned int i = 0; i < _stages->ncols(); i++) {

    string suffix = "a" + tstring::int2str(_stages->get(0,i));
    string name = suffix + ".W.var.p";
    string long_name = "Mean fitness in patch ";
    string patch;
    age_idx age = age_idx(_stages->get(0,i));

    void (LCE_SelectionSH::* setter) (void) = (AGE == ADULTS ?
        &LCE_SelectionSH::setAdultTable :
        &LCE_SelectionSH::setOffsprgTable);

    //first patch, gets the data table setter:
    add(long_name + "1", name + "1", AGE, 0, i,
        0,0,&LCE_SelectionSH::getVarPatchFitness, setter);

    for(unsigned int p = 1; p < patchNbr; p++) {
      patch = tstring::int2str(p+1);
      add(long_name + patch, name + patch, AGE, p, i,
          0,0,&LCE_SelectionSH::getVarPatchFitness,0);
    }
  }
}
// ----------------------------------------------------------------------------------------
// setDataTable
// ----------------------------------------------------------------------------------------
void LCE_SelectionSH::setDataTable(age_t AGE)
{
//  cout<<" \n--- LCE_SelectionSH::setDataTable ---\n";

  if(_table_set_gen == _pop->getCurrentGeneration()
     && _table_set_repl == _pop->getCurrentReplicate()
     ) return;

//  TMatrix* stages = _SHLinkedEvent->getAgeClassesSelected();
  unsigned int patchNbr = _pop->getPatchNbr();
  unsigned int numClass = _stages->ncols();
  unsigned int** sizes;

  sizes = new unsigned int * [patchNbr];

  for (unsigned int i = 0; i < patchNbr; ++i) {
    sizes[i] = new unsigned int [numClass];
    for(unsigned int j = 0; j < numClass; ++j) {
      sizes[i][j] = _pop->size(age_idx(_stages->get(0,j)), i);
    }
  }

  _phenoTable.allocate(patchNbr, numClass, sizes);

//  cout<<" \n--- LCE_SelectionSH::setDataTable - phenotable allocated\n";

  Patch* patch;
  age_idx age;


  for(unsigned int j = 0; j < numClass; ++j) {

    age = age_idx(_stages->get(0, j));

    for(unsigned int i = 0, n; i < patchNbr; i++) {

      patch = _pop->getPatch(i);

      if(patch->size(age) == 0) continue; //skip this patch if empty

      if( !_SHLinkedEvent->_is_absolute ) {
        if(_SHLinkedEvent->_is_local)
          _SHLinkedEvent->setScalingFactorLocal(j, i);
        else
          _SHLinkedEvent->setScalingFactorGlobal(j, i);
      }

      assert( (patch->size(MAL, age)+patch->size(FEM, age)) == _phenoTable.size(i,j));

      n = 0;

//      cout<<" --- LCE_SelectionSH::setDataTable - computing fitness - stage "<<age<<" size "<<_phenoTable.size(i,j)<<"\n";

      for(unsigned int k = 0, size = patch->size(FEM, age);
          k < size && n < _phenoTable.size(i, j);
          k++)
      {

        _phenoTable.set(i, j, n++, _SHLinkedEvent->getFitness( patch->get(FEM, age, k), i, j));

      }
//      cout<<" --- LCE_SelectionSH::setDataTable - computing fitness - females done (n = "<<n<<")\n";

      for(unsigned int k = 0, size = patch->size(MAL, age);
          k < size && n < _phenoTable.size(i, j);
          k++)
      {

        _phenoTable.set(i, j, n++,  _SHLinkedEvent->getFitness( patch->get(MAL, age, k), i, j));

      }
//      cout<<" --- LCE_SelectionSH::setDataTable - computing fitness - males done (n = "<<n<<")\n";

      if (n != _phenoTable.size(i,j)) {
        fatal("problem while recording fitness trait values; size counter doesn't match table size.\n");
      }
    }
  }

  _table_set_gen  = _pop->getCurrentGeneration();
  _table_set_repl = _pop->getCurrentReplicate();


  for(unsigned int i = 0; i < patchNbr; ++i)
    delete [] sizes[i];
  delete [] sizes;
//  cout<<" \n--- LCE_SelectionSH::setDataTable - exiting\n";
}
// ----------------------------------------------------------------------------------------
// LCE_SelectionSH::getMeanPatchFitness
// ----------------------------------------------------------------------------------------
double LCE_SelectionSH::getMeanPatchFitness (unsigned int i, unsigned int age_pos)
{
  unsigned int patch_size = _pop->size( age_idx(_stages->get(0, age_pos)), i);

  assert(patch_size == _phenoTable.size(i, age_pos));

  if(patch_size == 0) return (nanf("NULL"));

  double mean = 0;

  for(unsigned int j = 0; j < patch_size; j++)

    mean += _phenoTable.get(i, age_pos, j);

  return mean/patch_size;

//  return getMeanPatchFitness(i);
}
// ----------------------------------------------------------------------------------------
// LCE_SelectionSH::getMeanPatchFitness
// ----------------------------------------------------------------------------------------
double LCE_SelectionSH::getMeanPatchFitness (unsigned int i)
{
  double mean = 0;

//  unsigned int size = _phenoTable.size();
//
//  for(unsigned int j = 0; j < size; j++)
//
//    mean += _phenoTable[i][j];

  return mean;
}
// ----------------------------------------------------------------------------------------
// LCE_SelectionSH::getVarPatchFitness
// ----------------------------------------------------------------------------------------
double LCE_SelectionSH::getVarPatchFitness (unsigned int i, unsigned int age_pos)
{
  unsigned int patch_size = _pop->size(age_idx(_stages->get(0, age_pos)), i);

  assert(patch_size == _phenoTable.size(i, age_pos));

  if(patch_size == 0) return (nanf("NULL"));

  double mean = 0;

  for(unsigned int j = 0; j < patch_size; j++)

    mean += _phenoTable.get(i, age_pos, j);

  mean /= patch_size;

  double var = 0;

  for(unsigned int j = 0; j < patch_size; j++) {

    var += pow(_phenoTable.get(i, age_pos, j) - mean, 2.0);
  }

  return var/patch_size;
}
// ----------------------------------------------------------------------------------------
// LCE_SelectionFH::FHwrite
// ----------------------------------------------------------------------------------------
void LCE_SelectionFH::FHwrite()
{
  if (!_pop->isAlive()) return;

  // sets the pop ptr to a sub sampled pop, if sub sampling happened
  _pop = get_service()->getSampledPop();

  TMatrix* stages = _FHLinkedEvent->getAgeClassesSelected();

  unsigned int num_traits = _FHLinkedEvent->_TraitIndices.size();
  
  std::string filename = get_filename();
  
  std::ofstream FILE (filename.c_str(), ios::out);
  
  if(!FILE) fatal("could not open \"%s\" output file!!\n",filename.c_str());

  FILE<<"pop";
  
  for (unsigned int i = 0; i < num_traits; ++i) {
    FILE<<" trait"<< tstring::int2str(i+1);
  }
  
  FILE<< "stage age isMigrant"<<endl;
  
  
  Patch* patch;
  age_idx age;

  for(unsigned int i = 0; i < _pop->getPatchNbr(); ++i ) {

    patch = _pop->getPatch(i);

    for(unsigned int j = 0; j <  stages->ncols(); ++j) {

      age = age_idx(stages->get(0,j));

      if (patch->size(FEM, age) != 0) print(FILE, FEM, age, j, patch, num_traits);
      if (patch->size(MAL, age) != 0) print(FILE, MAL, age, j, patch, num_traits);
    }
  }
  
  FILE.close();
}
// ----------------------------------------------------------------------------------------
// LCE_SelectionFH::print
// ----------------------------------------------------------------------------------------
void LCE_SelectionFH::print(ofstream& FH, sex_t SEX, age_idx AGE, unsigned int stage_selected, Patch* patch, unsigned int ntraits)
{
  Individual* ind = 0;
  
  for (unsigned int i = 0; i < patch->size(SEX, AGE); ++i) {
    
    ind = patch->get(SEX, AGE, i);
    
    FH<<patch->getID()+1;
    
    for(unsigned int j = 0; j < ntraits; ++j) 
      FH<<" "<<(_FHLinkedEvent->*_FHLinkedEvent->_getRawFitness[j])(ind, patch->getID(), _FHLinkedEvent->_TraitIndices[j], stage_selected);
  
    FH<<" "<<AGE<<" "<<ind->getAge()<< " "<< (patch->getID() == ind->getHome() ? 0 : 1) <<endl;
  }
}

