[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3961683.svg)](https://doi.org/10.5281/zenodo.3961683) [![install with bioconda](https://img.shields.io/badge/install%20with-bioconda-brightgreen.svg?style=flat)](http://bioconda.github.io/recipes/nemo-age/README.html)

Latest version: v0.32.0 [10 Mar 2023]
<br><br>


# What's in Nemo-age?

With **Nemo-age**, it is possible to model genetic and phenotypic evolution in 
populations with overlapping generations, a seed bank, and 
multiple age classes with stage-specific transition rates, fecundities, 
selection pressures, and dispersal rates, among other things.

**Nemo-age** is a feature-rich and flexible program with many components that can be 
freely assembled to set up complex demo-genetic simulations in a spatially 
explicit context, efficiently. Eco-evolutionary dynamics of populations set in 
large geographical areas can be modeled using input parameter values from extant 
species. Such data encompass species' life history parameters (fecundities, 
survival and transition rates), dispersal kernels, geographical distribution and 
environmental data describing the ecological niche across space. It is then 
possible to simulate local adaptation of a species to spatially and temporally 
varying environments via selection on multiple polygenic traits.

**Nemo-age** is also an advanced forward-in-time population genetic simulator able 
to simulate genetic diversity data at multiple loci placed on a genetic map. Loci 
may carry continuous values contributing to polygenic trait variation, 
deleterious mutations contributing directly to fitness, or no phenotypic value 
and be neutral. All types of loci can be linked on the same genetic map.

**Nemo-age** is the age/stage-structured version of [Nemo](https://nemo2.sourceforge.io).

## What can I find here?

You will find here instructions on how to get Nemo-age's code and how to install it on your computer.

Information on how to setup a simulation and run it on your computer, or your cluster, is available in the [user's manual](https://bitbucket.org/ecoevo/nemo-age-release/downloads/NEMOAGE-usermanual.pdf).

-----------
# Resources

## Support
Join the [nemo-age mailing list](https://groups.google.com/g/nemo-age) to post your questions. Join the [nemo-announce mailing list](https://groups.google.com/g/nemo-announce) to receive announcements about new releases of all Nemo products, including Nemo-age, Nemo, and nemosub.

## Documentation
You can download the manual from the [Downloads section](https://bitbucket.org/ecoevo/nemo-age-release/downloads)

## Reference
Please cite Nemo-age as: Cotto Olivier, Schmid Max, and Guillaume Frederic. 2020. Nemo-age: spatially explicit simulations of eco-evolutionary dynamics in stage-structured populations under changing environments. *Methods in Ecology and Evolution* 11(10):1227-1236; [DOI:10.1111/2041-210X.13460](https://doi.org/10.1111/2041-210X.13460).

## Tools
**nemosub**:A utility to launch multiple simulations on a cluster from a single parameter file. `nemosub` is available [here](https://bitbucket.org/ecoevo/nemosub). It works for all main types of cluster scheduler (Slurm, OAR, Moab/Torq etc.)

# How to use Nemo-age?
----------------

Nemo-age is a command-line tool with a simple parameter-file interface. It can be run like this:

    $ nemoage path/to/myparameterfile.ini

The parameter file collects the parameters necessary to run the desired simulation. A very basic simulation has parameters for the simulation and population structure and the events executed during the life cycle.

    # example of a parameter file
    
    ## SIMULATION ##
    run_mode overwrite
    random_seed 2
    root_dir examples
    
    filename example1
    
    replicates      10
    generations     10000
    
    ## POPULATION ##
    patch_number    5
    patch_capacity  1000
    
    ### AGE STRUCTURE and TRANSITION MATRIX ###
    pop_age_structure     {{0, 1, 2, 3}}
    pop_transition_matrix {{0, 0, 12, 40}
                           {0.83, 0, 0, 0}
                           {0, 0.92, 0, 0}
                           {0, 0, 0.99, 0}}
    
    ## LIFE CYCLE EVENTS ##
    breed       1
    disperse    2
    save_stats  3
    regulation  4
    aging_multi 5
    save_files  6
    
    ## BREED parameters
    mating_system 6
    
    ## DISPERSE parameters 
    dispersal_rate   0.01
    dispersal_model  1
    
    ## REGULATION parameters
    regulation_carrying_capacity
    
    ## OUTPUT stats
    stat          demography migrants fstat
    stat_log_time 10
    stat_dir      data
    
    ## GENETICS:
    ## NEUTRAL markers (SNP)
    ntrl_loci                   1000
    ntrl_all                    2
    ntrl_mutation_rate          1e-5
    ntrl_mutation_model         2
    ntrl_random_genetic_map     {{rep(10,10)}}
    ntrl_genetic_map_resolution 0.01
    ntrl_save_freq
    ntrl_output_dir             ntrl
    ntrl_output_logtime         1000
    
Please refer to the user's manual for info about the parameters. Only neutral markers not under selection are simulated in the above example, with 100 di-allelic loci per chromosome randomly spread on 10 chromosomes of 10cM each (specified with the macro `rep()`, which inserts the the string "10" ten times, separated by commas).


## Install binaries
---

Executable files compiled for MacOS and Windows (CygWin) are available in the respective release packages. Click on `Downloads` on the left to access them. Refer to the INSTALL file for installation instructions.

## Build from source
---

To compile **Nemo-age**, follow the instructions in the INSTALL file.

The following should work on a *nix system (Mac OSX, Linux): 

Download and uncompress-expend the release package `NemoAge-x.y.z.tgz`:

    $ tar xzf NemoAge-x.y.z.tgz

Change directory to the package directory just created:

    $ cd NemoAge-x.y.z

Compile and install the program with `make` (hint: the `-j8` flag is used to compile in parallel on 8 CPUs. Adapt the number to your CPU specs):

    $ make -j8
    $ sudo make install

The last command will attempt to copy the program to the default `/usr/local/bin` system folder, hence the use of `sudo` to gain administrator access to that directory. If this does not fit your system, you'll have to edit the `Makefile` file and specify the installation folder that fits your settings. Look for the line defining the `BIN_INSTALL` parameter and enter the path to your specific installation folder (e.g., could be something like `BIN_INSTALL=~/bin/` to copy to folder `bin` in your home directory).

**On MacOS**, use the `Makefile-mac` makefile instead of the default `Makefile`. The mac specific Makefile has parameters to indicate the type of processor of your machine to produce the corresponding binary code. On the command line, set `MAC_ARM=1` if you have an M1 processors, or `MAC_INTEL=1` for older Intel processors:

    $ make -f Makefile-mac -j8 MAC_ARM=1
    or
    $ make -f Makefile-mac -j8 MAC_INTEL=1
    
*Note: if compiling fails with an error message regarding the binary architecture of the GSL libraries (libgsl, libgslcblas), have a look at the comments in the Makefile-mac file about how to build the GSL from source for you architecture.*

Try Nemo-age:

    $ nemoage

\FG

