#!/bin/bash
awk 'BEGIN{}/MAIN_VERSION/{a=$3}/MINOR_VERSION/{b=$3}/REVISION/{c=$3}END{print a"."b"."c}' src/version.h
